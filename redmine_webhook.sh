#!/bin/bash

source scripts/redmine-webhook/env_vars

if [ ! -f hooks/post-commit ]; then
  cp scripts/redmine-webhook/post-commit-hook.template hooks/post-commit
  chmod +x hooks/post-commit
fi

function setup_git(){
  git config --local core.hooksPath hooks
  git config --local redmine.host "${REDMINE_HOST}"
  git config --local redmine.apikey "${REDMINE_API_KEY}"

}

setup_git
