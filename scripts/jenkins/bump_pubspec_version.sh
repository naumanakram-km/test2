#!/bin/bash

PASSWORD="${1}"

# Dismantle RELEASE_VERSION into pieces
RELEASE_VERSION=$(yq -r .version pubspec.yaml)
SEMANTIC_VERSION=( ${RELEASE_VERSION//./ } )
major=${SEMANTIC_VERSION[0]}
minor=${SEMANTIC_VERSION[1]}
patch=${SEMANTIC_VERSION[2]}

# Setup proper numbering and iterations
patch=$(( patch + 1 ))
if [[ ${patch} -eq 1000 ]]; then
    minor=$(( minor + 1 ))
    patch=0
fi

if [[ ${minor} -eq 1000 ]]; then
    major=$(( major + 1 ))
    minor=0
fi

# Setup GIT code
git config user.name "jenkins"
git config user.email "jenkins@conqorde.com"

git https://${PASSWORD}@git.conqorde.com/sofiqe/sofiqeflutter.git

git clean -f .
git checkout -- .

# Bump version
DEVELOP_VERSION="${major}.${minor}.${patch}"
echo "Apply version: ${DEVELOP_VERSION} to pubspec.yaml"
sed -i 's/^version:.*/version: '${DEVELOP_VERSION}'/g' pubspec.yaml

git add pubspec.yaml
git commit --no-verify -m "[jenkins-auto-release] Development version updated to ${DEVELOP_VERSION}"
git push -f https://${PASSWORD}@git.conqorde.com/sofiqe/sofiqeflutter.git HEAD:develop