#!/bin/bash

commit_message=$(git log -1 HEAD --pretty=%s)
build_number="${1}"
USER='jenkins'
PASSWORD="${2}"
REGEX="^(\[jenkins\-auto\-release\]).*"

if [[ ${commit_message} =~ ${REGEX} ]]; then
    echo "Skipping build..."
    curl -X POST --user "${USER}:${PASSWORD}" https://j.conqorde.com/job/SofiqueFlutter/job/develop/${build_number}/kill
fi