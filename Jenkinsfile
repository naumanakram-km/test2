pipeline {
    options { disableConcurrentBuilds() }
    //    agent any
    agent {
        docker { 
            image 'mobiledevops/flutter-sdk-image' 
            args '-v /var/lib/gitea/custom/public/sofiqeflutter:/sofiqeflutter'
        }
    }

    stages {
        // stage('Check commit and PASS or NOT') {
        //     when {
        //         branch 'main'
        //     }
        //     steps {
        //         withCredentials([usernamePassword(credentialsId: 'jenkins-ci-build-token', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD' )]) {
        //             sh '''
        //                 ./scripts/jenkins/skip_build.sh $BUILD_NUMBER $PASSWORD
        //             '''
        //         }
        //     }
        // }
        stage('Setup') {
            when {
                branch 'main'
            }
            // environment {
            //     SECRET_FILE = credentials('sofiqueFlutterAnalysisOptions')
            // }
            steps {
                sh('ls')
                sh('sudo apt-get update')
                sh('sudo apt-get install -y python3 python3-pip jq')
                sh('sudo pip install yq')
                // sh('sudo cp \$SECRET_FILE ./analysis_options.yaml')
            }
        }
        // stage('Abort if meets condition') {
        //     when {
        //         branch 'main'
        //     }
        //     steps {
        //         withCredentials([usernamePassword(credentialsId: 'jenkins-ci-build-token', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD' )]) {
        //             sh '''
        //                 sudo ./scripts/jenkins/skip_build.sh $BUILD_NUMBER $PASSWORD
        //             '''
        //         }
        //     }
        // }
        stage('Install Flutter dependencies') {
            when {
                branch 'main'
            }
            steps {
                sh '''
                    sudo flutter packages get
                    sudo flutter pub get
                '''
            }
        }
        stage('Run Flutter Linter') {
            when {
                branch 'main'
            }
            steps {
                catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                    sh('sudo flutter analyze --no-fatal-infos --no-fatal-warnings')
                }
            }
        }
        stage('Build APK') {
            when {
                branch 'main'
            }
            steps {
                catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                    sh '''
                        sudo flutter config --android-sdk /opt/sdk
                        sudo flutter build apk
                    '''
                }
            }
        }
        // stage('Bump pubspec.yaml version') {
        //     when {
        //         branch 'main'
        //     }
        //     steps {
        //         withCredentials([usernamePassword(credentialsId: 'jenkins-gitea-token', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD' )]) {
        //             sh '''
        //                 sudo ./scripts/jenkins/bump_pubspec_version.sh $PASSWORD
        //             '''
        //         }
        //     }
        // }
        stage('Copy APK file to assets') {
            when {
                branch 'main'
            }
            environment {
                BUILD_DATE = """${sh(
                    returnStdout: true,
                    script: 'date +%Y-%m-%d_%H_%m'
                ).trim()}"""
                LAST_COMITTER = """${sh(
                    returnStdout: true,
                    script: "git log -1 --pretty=format:'%an' | awk -F' ' '{print \$1}'"
                ).trim()}"""
            }
            steps {
                catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                    sh """
                        sudo cp build/app/outputs/flutter-apk/app-release.apk /sofiqeflutter/${BUILD_DATE}_sofiqe-flutter-RC-${LAST_COMITTER}.apk
                    """
                }
            }
        }
        stage('Write log file') {
            when {
                branch 'main'
            }
            environment {
                BUILD_DATE = """${sh(
                    returnStdout: true,
                    script: 'date +%Y-%m-%d_%H_%m'
                ).trim()}"""
                LAST_COMITTER = """${sh(
                    returnStdout: true,
                    script: "git log -1 --pretty=format:'%an' | awk -F' ' '{print \$1}'"
                ).trim()}"""
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'jenkins-ci-build-token', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                    script {
                        def consoleUrl = env.BUILD_URL + 'consoleText'
                        def consoleLog = sh(returnStdout: true, script: "curl -s -u ${USER}:${PASS} '${consoleUrl}'")
                        writeFile(file: "jenkinsLog.txt", text: consoleLog)

                        sh """
                            sudo cp ./jenkinsLog.txt /sofiqeflutter/${BUILD_DATE}_jenkins-log-RC-${LAST_COMITTER}.txt
                        """
                    }
                }
            }
        }
        // stage('Create Gitea Wiki page') {
        //     when {
        //         branch 'main'
        //     }
        //     environment {
        //         USER_CREDS = credentials('jenkins-ci-build-token')
        //         BUILD_DATE = """${sh(
        //             returnStdout: true,
        //             script: 'date +%Y-%m-%d_%H_%m'
        //         ).trim()}"""
        //         LAST_COMITTER = """${sh(
        //             returnStdout: true,
        //             script: "git log -1 --pretty=format:'%an' | awk -F' ' '{print \$1}'"
        //         ).trim()}"""
        //         LAST_COMMIT = """${sh(
        //             returnStdout: true,
        //             script: "git log -n1 --pretty=format:'%H'"
        //         ).trim()}"""
        //     }
        //     steps {
        //         script {
        //             withCredentials([usernamePassword(credentialsId: 'jenkins-gitea-token', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD' )]) {
        //                 def changeHistory = sh(returnStdout: true, script: "git log -5 --name-only --oneline --pretty=%an")
        //                 def finalMessage = 'APK File:\n\nhttps://git.conqorde.com/assets/sofiqeflutter/' + BUILD_DATE + '_sofiqe-flutter-RC-' + LAST_COMITTER + '.apk\n\nBuild Log:\n\nhttps://git.conqorde.com/assets/sofiqeflutter/' + BUILD_DATE + '_jenkins-log-RC-' + LAST_COMITTER + '.txt\n\nLast 5 Change History:\n\n' + changeHistory             
        //                 def encodedMessage = finalMessage.bytes.encodeBase64().toString()
        //                 sh """
        //                     curl -X POST https://git.conqorde.com/api/v1/repos/sofiqe/sofiqeflutter/wiki/new?token=$PASSWORD -H "accept: application/json" -H "Content-Type: application/json" -d '{ "content_base64": \"$encodedMessage\", "message": "empty", "title": \"${BUILD_DATE}_sofiqe-flutter-RC-${LAST_COMITTER}\" }'
        //                 """
        //             }
        //         }
        //     }
        // }
        stage('Cleanup') {
            steps {
                sh """
                sudo rm -Rf *
                sudo rm -Rf .* || true
            """
            }
        }
    }
}