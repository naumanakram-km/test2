import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:get/get.dart';

import '../controller/controllers.dart';
import '../controller/nav_controller.dart';

class PageProvider extends GetxController {
  late RxInt page;
  late RxInt lastPage;
  late PageController controller;
  late void Function(int) onTapCallback;

  PageProvider() {
    setDefaults();
  }

  Future<void> share(prodUrl, title) async {
    await FlutterShare.share(
        title: title, text: title, linkUrl: prodUrl, chooserTitle: 'Share');
  }

  void goToPage(Pages p) async {
    switch (p) {
      case Pages.HOME:
        page.value = 0;
        onTapCallback(0);
        break;
      case Pages.TRYITON:
        page.value = 1;
        makeOverProvider.isText.value = true;
        onTapCallback(1);
        break;
      case Pages.SHOP:
        page.value = 1;
        makeOverProvider.isText.value = true;
        onTapCallback(1);
        Get.find<NavController>().setnavbar(false);
        break;

      case Pages.MAKEOVER:
        page.value = 2;
        makeOverProvider.isText.value = true;
        onTapCallback(2);
        break;

      case Pages.MYSOFIQE:
        page.value = 3;
        makeOverProvider.isText.value = true;
        onTapCallback(3);
        break;

    }
  }

  void setDefaults() {
    page = 0.obs;
  }
}

enum Pages { HOME, TRYITON ,SHOP, MAKEOVER, MYSOFIQE }