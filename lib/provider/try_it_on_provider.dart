// ignore_for_file: deprecated_member_use

import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:sofiqe/controller/ms8Controller.dart';
import 'package:sofiqe/model/CentralColorLeftmostModel.dart';
import 'package:sofiqe/model/UserDetailModel.dart';
import 'package:sofiqe/model/lookms3model.dart';
import 'package:sofiqe/model/product_model.dart';
import 'package:sofiqe/model/searchAlternativecolorModel.dart';
import 'package:sofiqe/utils/api/product_details_api.dart';
import 'package:sofiqe/utils/api/try_it_on_product_api.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/utils/states/local_storage.dart';

import '../controller/selectedProductController.dart';
import '../utils/states/user_account_data.dart';
import 'account_provider.dart';

enum FaceArea {
  LIPS,
  CHEEKS,
  EYES,
}

class SelectedProducts {
  String customerId;
  String faceSubarea;
  String hex;
  String sku;

  SelectedProducts(this.customerId, this.faceSubarea, this.hex, this.sku);
}

class TryItOnProvider extends GetxController {
  static TryItOnProvider instance = Get.find();
  RxBool isDefaultShadsColor = false.obs;
  RxString selectedColor = "".obs;
  RxInt isSelected = 0.obs;
  RxInt isMatch = 0.obs;

  var ismy16 = false.obs;
  var homeT1T2 = false;
  var isplaying = true.obs;
  var isFirstCalling = true.obs;
  RxBool showSelected = true.obs;
  var sku = "".obs;
  var isChangeButtonColor = false.obs;
  Color ontapColor = Colors.grey;
  RxInt isSelectColorIndex = 1000.obs;
  late RxInt page;
  late RxInt scan;
  RxInt timer = 0.obs;
  RxInt visTime = 5.obs;
  // RxInt outerIndex = 10000.obs;
  RxInt outerIndex = 100.obs;
  RxInt matchColorIndex = 200.obs;
  RxBool vis = true.obs;

  Rx<PanelController> pc = PanelController().obs;
  late RxBool bottomSheetVisible;
  RxBool selectedProduct = false.obs;
  RxBool selectedShades = false.obs;
  Future<void> toggleShadesIconSelection(bool newVal) async {
    selectedShades.value = newVal;
  }

  RxInt colorMenuVisible = 0.obs;
  Rx<FaceArea> faceArea = FaceArea.CHEEKS.obs;
  RxInt currentSelectedArea = 0.obs;
  RxInt count = 0.obs;
  RxInt indexIs = 0.obs;
  bool isLoadedAfterAddtoCart = false;
  RxString currentSelectedColor = "".obs;
  RxString currentSelectedColorTemp = "".obs;
  RxString currentSelectedProducturl = "".obs;
  RxString currentSelectedProductname = "".obs;
  RxString currentSelectedProductimage = "".obs;
  RxString currentSelectedProductsku = "".obs;
  RxString currentSelectedfacesubarea = "".obs;
  RxMap currentSelectedCentralColor = {"name": "", "code": ""}.obs;

  RxList alternateColors = [].obs;
  RxList alternateColors1 = RxList();
  RxList alternateColors2 = RxList();
  RxBool alternateColorsloading = false.obs;
  RxList alternateColorsProducts = [].obs;

  RxList looklist = RxList();
  RxBool centralColorsloading = true.obs;
  RxBool centralLeftmostloading = true.obs;
  RxList<Colorss> centralcolor = RxList<Colorss>();
  RxList selectedProducts = [].obs;
  RxList<CentralColorLeftmost> centralcolorleftmost =
      RxList<CentralColorLeftmost>();
  RxList<FaceSubareaLeftmostListOfProducts> centralcolorleftmostselected =
      RxList<FaceSubareaLeftmostListOfProducts>();
  RxList<FaceSubareaLeftmostListOfProducts> temp1 =
      RxList<FaceSubareaLeftmostListOfProducts>();
  RxList<GlobalKey> items = RxList<GlobalKey>();

  /* eye color of make over flow */
  String _eye_color = "";

  /* doctor care of make over flow */
  String _doctor_care = "";

  /* lip color of make over flow */
  String _lip_color = "";

  /* hair color of make over flow */
  String _hair_color = "";

  /* skin color of make over flow */
  String _skin_tone = "";

  /* color depth of make over flow */
  String _color_depth = "";

  String get eye => _eye_color;

  String get doctorCare => _doctor_care;

  String get lip => _lip_color;

  String get hair => _hair_color;

  String get skin => _skin_tone;

  String get colorD => _color_depth;

  // All Function is for store choice of color in make over selection for send this color to post api and get relevant data
  seye(String val) {
    this._eye_color = val;
  }

  sDoctorCare(String val) {
    this._doctor_care = val;
  }

  shair(String val) {
    this._hair_color = val;
  }

  sskin(String val) {
    this._skin_tone = val;
  }

  slip(String val) {
    this._lip_color = val;
  }

  scolorD(String val) => _color_depth = val;

  /* eye color of make over flow */
  String _eye_face_area = "";

  /* doctor care of make over flow */
  String _lip_face_area = "";

  /* lip color of make over flow */
  String _cheek_face_area = "";

  String get eyeFaceArea => _eye_face_area;

  String get lipFaceArea => _lip_face_area;

  String get cheekFaceArea => _cheek_face_area;

  // All Function is for store choice of color in make over selection for send this color to post api and get relevant data
  sEyeFaceArea(String val) {
    this._eye_face_area = val;
  }

  sLipFaceArea(String val) {
    this._lip_face_area = val;
  }

  sCheekFaceArea(String val) {
    this._cheek_face_area = val;
  }

  late RxBool faceSubAreaOptionsVisible;
  late RxBool notFound;
  late RxString selectedFaceArea;
  late RxString selectedFaceSubArea;
  late List<String> capturedFileName = [
    'product_label',
    'product_color',
  ];
  late Rx<ScannedResult> scannedResult;
  late Rx<Product> received; // Sent by the API
  late RxList<Product> lookreceived; // Sent by the API
  late Rx<LooksMs3Model> filterlook; // Sent by the API

  late Product? retrieved; // Requested by the APP for full details
  late RxBool exact = true.obs;
  late AnimationController? faceAreaErrorController;

  TextEditingController brandNameController = TextEditingController();
  TextEditingController productLabelController = TextEditingController();
  Ms8Controller controller = Get.put(Ms8Controller());

  var brandName = "".obs;
  var produceLabel = "".obs;
  var dialogShowing = false.obs;
  RxBool directProduct = false.obs;
  RxBool lookProduct = false.obs;
  RxBool lookloading = true.obs;
  RxString lookname = "".obs;
  RxInt lookindex = 0.obs;

  /// CONSTRUCTOR

  TryItOnProvider() {
    // getalternateColors();
    // getCentralColors();
    // getuserfacecolor();
    setDefault();
    // getuserfacecolor();
  }

  /// METHODS
  void playSound() {
    if (isplaying.isTrue) {
      AudioPlayer().play(AssetSource('audio/onclick_sound.mp3'));
    }
  }

  void toggleColorMenu(int code) {
    if (colorMenuVisible.value == code) {
      colorMenuVisible.value = 0;
    } else {
      colorMenuVisible.value = code;
    }
  }

  resetTimeVisibleValues() {
    timer = 0.obs;
    visTime = 5.obs;
    vis = false.obs;
  }

  late Timer timerp;

  startTimer() {
    timerp = Timer.periodic(const Duration(seconds: 1), (_) {
      timer.value += 1;

      if (timer.value >= visTime.value) {
        // setState(() {
        vis.value = !vis.value;
        visTime = vis.value ? 5.obs : 10.obs;
        timer = 0.obs;
        // cPrint('recommendations time :: ${DateTime.now()}');
        // });
      }
    });
  }

  cancelTimer() {
    timerp.cancel();
    vis.value = true;
    visTime = 5.obs;
    timer = 0.obs;
  }

  timerForVisualizingRecommendations() {
    try {
      cancelTimer();
    } catch (e) {}

    startTimer();
  }

  /****************************************************************
      FUNCTION: Loader dialog
      ARGUMENTS: set loader with color and barrier
      RETURNS: Alert dialog with loader
      NOTES: show loader dialog
   ****************************************************************/
  showLoaderDialog(BuildContext context, {Color loaderColor = Colors.white}) {
    final alert = SpinKitDoubleBounce(
      color: loaderColor,
      // color: Color(0xffF2CA8A),
      size: 50.0,
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void toggleloading(bool load) {
    lookloading.value = load;
  }

  void setPage(int pageNo) {
    page.value = pageNo;
  }

  void changearea(String area) {
    if (selectedFaceArea.value != area) {
      selectedFaceArea.value = area;
      selectedProduct.value = false;
//     var temp;
// if(area=="Eyes"){
// temp = "Eyelid";
// }else {
//   temp=area;
// }
      currentSelectedArea.value = 0;
      // getalternateColors(currentSelectedCentralColor['code'], temp, "5");
    }
  }

  void nextScan() {
    if (scan.value == 0) {
      if (selectedFaceSubArea.isEmpty) {
        // Get.showSnackbar(GetSnackBar(
        //   message: 'Select a area where the makeup is applied on before proceeding',
        //   duration: Duration(seconds: 2),
        // ));
        if (faceAreaErrorController != null) {
          faceAreaErrorController!.forward();
        }
        return;
      }
    }
    scan.value += 1;
    // if (scan.value >= capturedFileName.length) {
    //   page.value += 1;
    //   if (page.value == 2) {
    //     this.getScanResults();
    //   }
    // }
  }

  void toggleBottomSheetVisibility() {
    bottomSheetVisible.value = !bottomSheetVisible.value;
  }

  void nextlook() {
    cPrint(looklist);
    if (lookindex < looklist.length - 1) {
      lookindex++;
      lookname.value = looklist[lookindex.value];
      controller.getLookList(looklist[lookindex.value]);
    }
    cPrint(lookindex);
  }

  void previouslook() {
    cPrint(looklist);
    if (lookindex.value != 0) {
      lookindex--;
      lookname.value = looklist[lookindex.value];
      controller.getLookList(looklist[lookindex.value]);
    }
    cPrint(lookindex);
  }

  void showFaceSubAreaOptions() {
    selectedFaceSubArea.value = '';
    faceSubAreaOptionsVisible.value = true;
  }

  void hideFaceSubAreaOptions() {
    faceSubAreaOptionsVisible.value = false;
  }

  Future<bool> getScanResults(BuildContext context) async {
    // try {
    String token = await getUserToken();
    log('======== user token is this  ::  $token =======');
    log('======== brand name is ::  ${brandName.value} =======');
    log('======== product label is  ::  ${produceLabel.value} =======');

    scannedResult.value.label = brandName.value;
    scannedResult.value.ingredients = produceLabel.value;
    // scannedResult.value.color = "#ffe7d5";
    dynamic responseList = await sfAPIScanProduct(
      // List responseList = await sfAPIScanProduct(
      token,
      // scannedResult.value.mapped(
      Provider.of<AccountProvider>(context, listen: false).customerId,
      brandName.value,
      produceLabel.value,
      produceLabel.value,
      // )
    );

    print("RLDSP==> ${responseList}");

    if (responseList is String) {
      Get.showSnackbar(
        GetSnackBar(
          message: 'No product found related to entered details',
          duration: Duration(seconds: 2),
        ),
      );

      throw 'No response received from server';
    }

    if (responseList['items'].isEmpty) {
      throw 'No response received from server';
    }
    // Map responseMap = responseList[0];
    //
    // exact.value = responseMap['exact'];
    Map tempProduct = {};
    if (responseList['items'].isEmpty) {
      throw 'Product list empty';
    } else {
      cPrint(
          '===== 1. receviced the items length is  ${responseList['items'].length}  ======== ');
      responseList['items'].forEach((p) {
        tempProduct = p;
      });
    }

    ///-----Old Function :: Will be removed after APK testing
    // received.value = Product.fromMap(tempProduct['product']);
    cPrint(
        '===== 2. receviced the items length is  ${responseList['items'].length}  ======== ');
    return await getFullProductDetails(tempProduct);
    // } catch (err) {
    //   notFound.value = true;
    //   cPrint('Error finding scanned product: $err');
    //   Get.showSnackbar(
    //     GetSnackBar(
    //       message: 'Looks like we could not find results for your scan. Please try a new scan.',
    //       duration: Duration(seconds: 2),
    //     ),
    //   );
    // }
    return false;
  }

  Future<bool> getFullProductDetails(Map productMap) async {
    try {
      ///-----Old Function :: Will be removed after APK testing
      // http.Response response = await sfAPIGetProductDetailsFromSKU(sku: received.value.sku!);

      String? getDataValue(List customAttributes, String keyToGet) {
        int keyIndex =
            customAttributes.indexWhere((f) => f['attribute_code'] == keyToGet);
        return customAttributes[keyIndex]['value'];
      }

      http.Response response =
          await sfAPIGetProductDetailsFromSKU(sku: productMap['sku']);
      Map responseBody = json.decode(response.body);

      retrieved = Product(
        id: responseBody['id'] ?? 159,
        sku: responseBody['sku'] ?? '202204200001641',
        name: responseBody['name'] ?? '',
        description: '',
        avgRating: responseBody['extension_attributes'] != null &&
                responseBody['extension_attributes']['avgrating'] != null
            ? responseBody['extension_attributes']['avgrating']
            : "0.0",
        price: responseBody['price'] ?? 00,
        // color: productMap['face_color'] ?? '#ffffff',
        color:
            getDataValue(responseBody['custom_attributes'], 'face_color') ?? '',
        faceSubArea: int.parse(productMap['face_sub_area'] ?? "10"),
        // image: productMap['custom_attributes'][0]['value'] ?? '',
        image: getDataValue(productMap['custom_attributes'], 'image') ?? '',
        options: responseBody['options'] ?? [],
        rewardsPoint: responseBody['extension_attributes'] != null &&
                responseBody['extension_attributes']['reward_points'] != null
            ? responseBody['extension_attributes']['reward_points']
            : "0",
      );
      debugPrint(
          '==== retrieved product value is  ${retrieved ?? ' val is null'}  =====');
      received.value = retrieved!;
      return true;
    } catch (err) {
      cPrint('===== Error fetching full product details: $err =====');
    }
    return false;
  }

  void currentSelectedAreaToggle(int code, String url, String name,
      String facesubarea, String sku, String colr, String img) {
    if (currentSelectedArea.value == code && selectedProduct.isTrue) {
      currentSelectedArea.value = code;
    } else {
      currentSelectedArea.value = 0;
      currentSelectedColor.value = '';
      selectedProduct.value = true;
      currentSelectedProductname.value = name;
      currentSelectedProductimage.value = img;
      currentSelectedProductsku.value = sku;
      currentSelectedProducturl.value = url;
      currentSelectedfacesubarea.value = facesubarea;
    }
  }

  void centralColorToogle(String facearea, {bool fromOverlay = false}) {
    var indx = centralcolorleftmost
        .indexWhere((element) => element.faceSubArea == facearea);
    List<Colorss> temp = [];

    if (centralcolorleftmost[indx].centralColours != null) {
      centralcolorleftmost[indx].centralColours!.forEach((element) {
        temp.add(Colorss(
            colourAltHEX: element.colourAltHEX,
            colourAltName: element.colourAltName));
      });
    }
    centralcolor.value = temp;
    currentSelectedcentralColorToggle(
        centralcolor[0].colourAltHEX!, centralcolor[0].colourAltName!,
        fromOverlay: fromOverlay);
  }

  void currentSelectedColorToggle(String colorcode) {
    if (currentSelectedColor.value != colorcode) {
      currentSelectedColor.value = colorcode;
    }
  }

  void currentSelectedcentralColorToggle(String colorcode, String name,
      {bool fromOverlay = false}) {
    cPrint("NAME CODE ${colorcode}  Name ${name}   m16 ${ismy16.value}");
    cPrint(
        "NAME CODE ${currentSelectedCentralColor['code']}  Name ${currentSelectedCentralColor['name']}");
    cPrint("Face Sub Area ${currentSelectedfacesubarea.value}");
    if (currentSelectedCentralColor['name'] != name ||
        currentSelectedCentralColor['code'] != colorcode) {
      cPrint("YES");
      currentSelectedCentralColor.value = {"name": name, "code": colorcode}.obs;
      if (currentSelectedfacesubarea.value != "Concealer" &&
          currentSelectedfacesubarea.value != "Foundation") {
        cPrint("Running Becuase of ${currentSelectedfacesubarea.value}");
        getAlternateColors(colorcode, currentSelectedfacesubarea.value, "5",
            fromOverlay: fromOverlay);
      }
    }
  }

  Future<bool> getAlternateColors(String color, String facearea, String depth,
      {bool fromOverlay = false}) async {
    cPrint("CONTROLL ENTER alternateColors");
    try {
      var data;
      alternateColorsloading.value = true;
      Map<String, dynamic> tokenMap = await sfQueryForSharedPrefData(
          fieldName: 'user-token', type: PreferencesDataType.STRING);
      String token = tokenMap['user-token'] ?? '';
      if (isDefaultShadsColor.value) {
        data =
            await sfAPIGetSearchAlternatecolor(color, facearea, depth, token);
        if (data != null) {
          cPrint("ANC==>");
          cPrint(data);
          alternateColors.value = data["list_colors"] ?? [];
          alternateColorsProducts.value = data["list_colors_products"] ?? [];
          if (!fromOverlay) {
            List<FaceSubareaLeftmostListOfProducts> tempList =
                data["list_of_products"] ?? [];

            var indx = centralcolorleftmost.indexWhere((element) =>
                element.faceSubArea ==
                (SelectedProductController.to.value[0]
                        as FaceSubareaLeftmostListOfProducts)
                    .faceSubAreaName);

            centralcolorleftmost[indx].faceSubareaLeftmostListOfProducts =
                tempList;
            List<CentralColours> centralcolors =
                centralcolorleftmost[indx].centralColours ?? [];
            var indxC = centralcolors.indexWhere(
                (element) => element.colourAltHEX.toString() == color);
            CentralColours centeralcolor = centralcolors.removeAt(indxC);
            centralcolors.insert(0, centeralcolor);
            centralcolorleftmost[indx].centralColours = centralcolors;
            if (tempList.length > 0) {
              cPrint("Alernative Colors Products==>");
              cPrint(tempList);
              TryItOnProvider.instance.currentSelectedColor.value =
                  tempList[0].shadeColor.toString();
              TryItOnProvider.instance.currentSelectedColorTemp.value =
                  tempList[0].shadeColor.toString();
              changeProductatProductsPage(0, tempList[0].shadeColor.toString());
              // currentSelectedColor.value = tempList[0].shadeColor.toString();
              matchColorIndex.value = -1;
            }
          }

          // var indx = alternateColors.length / 2;
// currentSelectedColorToggle(alternateColors[indx.toInt()]);
        } else {
          alternateColors.value = [];
        }
      } else {

        // alternateColors.value = [];
        // alternateColors.value =
      }

      alternateColorsloading.value = false;
      cPrint("alternateColors $alternateColors");
      return true;
    } catch (err) {
      cPrint("error fetching alternate color :-$err");
    }
    return false;
  }

  Future<bool> getCentralColors() async {
    cPrint("CONTROLL ENTER getCentralColors");
    centralColorsloading.value = true;
    try {
      Map<String, dynamic> tokenMap = await sfQueryForSharedPrefData(
          fieldName: 'user-token', type: PreferencesDataType.STRING);
      String token =
          tokenMap['user-token'] == null ? '' : tokenMap['user-token'];
      var facecolor = await sfApiGetUserfaceColor(token);
      var eye = facecolor!.customAttributes!
          .firstWhere(
              (element) => element.attributeCode == "customer_eyecolour")
          .value;
      var lip = facecolor.customAttributes!
          .firstWhere(
              (element) => element.attributeCode == "customer_haircolour")
          .value;
      var hair = facecolor.customAttributes!
          .firstWhere(
              (element) => element.attributeCode == "customer_lipcolour")
          .value;
      var skincolor = facecolor.customAttributes!
          .firstWhere(
              (element) => element.attributeCode == "customer_skincolour")
          .value;

      var result = await sfAPIGetSearchCentralcolor(
          eye!,
          currentSelectedfacesubarea.value,
          lip!,
          hair!,
          skincolor!,
          "dark",
          161,
          token);
      centralcolor.value =
          SearchCentralColorModel.fromJson(result[0]).values!.colors!;
//  currentSelectedCentralColor.value = {
//         "name":centralcolor[0].colourAltName!,
//         "code":centralcolor[0].colourAltHEX!
//       }.obs;
      cPrint("centralcolor.value");
      cPrint(centralcolor);
      if (centralcolor.isNotEmpty) {
        currentSelectedcentralColorToggle(
            centralcolor[0].colourAltHEX!, centralcolor[0].colourAltName!);
      } else {
        alternateColors.value = [];
      }

      centralColorsloading.value = false;
      return true;
    } catch (err) {
      cPrint("error fetching central color :-$err");
    }
    return false;
  }

  Future<bool> getuserfacecolor({bool isfromRecomendation = false}) async {
    centralLeftmostloading.value = true;
    try {
      Map<String, dynamic> tokenMap = await sfQueryForSharedPrefData(
          fieldName: 'user-token', type: PreferencesDataType.STRING);
      String token =
          tokenMap['user-token'] == null ? '' : tokenMap['user-token'];
      Map uidMap = await sfQueryForSharedPrefData(
          fieldName: 'uid', type: PreferencesDataType.INT);
      int uid = uidMap['uid'] as int;
      var result = await sfApiGetUserfaceColor(token);

      // centralcolor.value =
      //     SearchCentralColorModel.fromJson(result as Map<String, dynamic>)
      //         .values!
      //         .colors!;
      // currentSelectedcentralColorToggle(
      //     centralcolor[0].colourAltHEX!, centralcolor[0].colourAltName!);

      cPrint(result);
      var res = await getCentralColorandLeftmost(result!, uid,
          from: isfromRecomendation
              ? "myselection"
              : TryItOnProvider.instance.lookname.value);

      cPrint(instance.lookname.value);
      centralLeftmostloading.value = false;

      cPrint("Data Face Color: $res");
      return res;
    } catch (err) {
      centralLeftmostloading.value = false;
      cPrint("error fetching central color :-$err");
      return false;
    }
  }

  Future<bool> getCentralColorandLeftmost(UserDetailModel data, int id,
      {String from = ""}) async {
    cPrint("inFunction $id");
    try {
      centralcolorleftmostselected =
          RxList<FaceSubareaLeftmostListOfProducts>();

      String eye = "";
      String lip = "";
      String hair = "";
      String skincolor = "";
      if (from == "myselection") {
        for (int i = 0; i < data.customAttributes!.length; i++) {
          CustomAttribute ca = data.customAttributes![i];
          cPrint(ca.attributeCode);
          cPrint(ca.value);
          if (ca.attributeCode.toString() == "customer_eyecolour") {
            eye = ca.value ?? "";
          }
          if (ca.attributeCode.toString() == "customer_lipcolour") {
            lip = ca.value ?? "";
          }
          if (ca.attributeCode.toString() == "customer_haircolour") {
            hair = ca.value ?? "";
          }
          if (ca.attributeCode.toString() == "customer_skincolour") {
            skincolor = ca.value ?? '';
          }
        }
      } else {
        eye = _eye_color;
        lip = _lip_color;
        hair = _hair_color;
        skincolor = _skin_tone;
      }

      // eye, lip, hair and skin color given to post api as per selection
      cPrint("Check color finally::: ${eye}");
      cPrint("Check color finally::: ${lip}");
      cPrint("Check color finally::: ${hair}");
      cPrint("Check color finally::: ${skin}");

      //Values in the providor
      cPrint("Check color finally::: ${_eye_color}");
      cPrint("Check color finally::: ${_lip_color}");
      cPrint("Check color finally::: ${_hair_color}");
      cPrint("Check color finally::: ${_skin_tone}");

      Map<String, dynamic> tokenMap = await sfQueryForSharedPrefData(
          fieldName: 'user-token', type: PreferencesDataType.STRING);
      String token = tokenMap['user-token'] ?? '';
      cPrint("Token: $token");
      var result = await sfApiGetCentralColorAndLeftmost(
          eye, "5", lip, hair, skincolor, id, token);
      centralLeftmostloading.value = false;

      cPrint("Main Data: $result");
      List<CentralColorLeftmost> centraldata = [];
      result!.forEach(
          (element) => centraldata.add(CentralColorLeftmost.fromJson(element)));
      cPrint("CentraData: ${centraldata}");
      centralcolorleftmost.value = centraldata;
      centralcolorleftmost.forEach((element) {
        if (element.faceSubareaLeftmostListOfProducts!.length > 0) {
          selectedProducts.add({
            "customer_id": id,
            "face_subarea":
                element.faceSubareaLeftmostListOfProducts![0].faceSubAreaName,
            "hex": element.faceSubareaLeftmostListOfProducts![0].shadeColor,
            "sku": element.faceSubareaLeftmostListOfProducts![0].sku
          });
        }
        if (element.faceSubareaLeftmostListOfProducts!.length > 0 &&
            !element.faceSubArea!.contains('Undershadow')) {
          List tempColorList = [];
          // centralcolorleftmostselected.addIf(!centralcolorleftmostselected.map((item) => item.entityId).contains(element.faceSubareaLeftmostListOfProducts![0].entityId),element.faceSubareaLeftmostListOfProducts![0]);
          centralcolorleftmostselected
              .add(element.faceSubareaLeftmostListOfProducts![0]);
          element.faceSubareaLeftmostListOfProducts!.forEach((element) {
            tempColorList.add({
              "alternate_color": element.faceColor,
              "sku": element.sku,
              "options": element.hasOptions,
              "type_id": element.typeId,
              "distance_variance": element.distanceVariance,
            });
          });
          alternateColors1.add(tempColorList);
        }
      });
      cPrint("ALTERNATE COLORS ${alternateColors.length}   ${alternateColors}");
      // Add Product if product face area and selected face area same
      centralcolorleftmostselected.forEach((element) {
        cPrint(
            "ENTER DATA TEMP1 ${centralcolorleftmostselected.indexOf(element)}");
        centralcolorleftmostselected[
                    centralcolorleftmostselected.indexOf(element)]
                .alternateColors =
            alternateColors1[centralcolorleftmostselected.indexOf(element)];
        if (element.faceArea == selectedFaceArea.value) {
          temp1.add(element);
        }
      });
      // add Item data
      temp1.forEach((element) {
        cPrint("ENTER LENGTH ${temp1.length}");
        final name = GlobalKey();
        items.add(name);
      });

      // change count as per fetched product data
      count.value = centralcolorleftmostselected.length;

      centralLeftmostloading.value = false;
      cPrint(centraldata[0]);
      cPrint("Nilesh Result: $result");
      return true;
    } catch (err) {
      cPrint("error fetching central color :-$err");
      centralLeftmostloading.value = false;
      return false;
    }
  }

  changeProductatProductsPage(int i, String color) async {
    cPrint("++++++++++++++++++++++++++++++");
    SelectedProductController.to.value.forEach((element) {
      cPrint(element.faceSubAreaName);
    });

    cPrint(
        " ${(SelectedProductController.to.value[0] as FaceSubareaLeftmostListOfProducts).faceSubAreaName}");

    centralcolorleftmost.forEach((element) {
      cPrint(
          "${element.faceSubArea} ==== ${(SelectedProductController.to.value[0] as FaceSubareaLeftmostListOfProducts).faceSubAreaName}");
      // cPrint(
      //     "${element.faceSubArea} ==== ${(SelectedProductController.to.value[3] as FaceSubareaLeftmostListOfProducts).faceSubAreaName}");
    });
    print("Alternative Colors");
    List alternativeColors =
        SelectedProductController.to.value[0].alternateColors;
    print(SelectedProductController.to.value[0].alternateColors);
    var indx = centralcolorleftmost.indexWhere((element) =>
        element.faceSubArea ==
        (SelectedProductController.to.value[0]
                as FaceSubareaLeftmostListOfProducts)
            .faceSubAreaName);
    cPrint(indx);
    int indexforSelected = 0;
    if (indx > 12) {
      indexforSelected = indx - 2;
    } else {
      indexforSelected = indx;
    }
    CentralColorLeftmost element = centralcolorleftmost[indx];
    cPrint(
        "New Product name length==> ${element.faceSubareaLeftmostListOfProducts!.length}");

    List<FaceSubareaLeftmostListOfProducts> listItems =
        element.faceSubareaLeftmostListOfProducts ?? [];
    print(listItems);
    listItems.forEach((element) {
      cPrint(" ${element.shadeColor} === ${color}");
    });
    var indx2 = listItems.indexWhere((element) => element.shadeColor == color);
    if (element.faceSubareaLeftmostListOfProducts!.length >= indx2) {
      listItems[indx2].alternateColors = alternativeColors;
      cPrint(TryItOnProvider
          .instance.centralcolorleftmostselected[indexforSelected].name);
      TryItOnProvider.instance.centralcolorleftmostselected[indexforSelected] =
          listItems[indx2];
      cPrint(TryItOnProvider
          .instance.centralcolorleftmostselected[indexforSelected].name);

      SelectedProductController.to.value[0] = listItems[indx2];
      if (SelectedProductController.to.value.length ==
          SelectedProductController.to.temp.length) {
        SelectedProductController.to.temp[0] = listItems[indx2];
      }
      if (SelectedProductController.to.value.length ==
          SelectedProductController.to.temp1.length) {
        SelectedProductController.to.temp1[0] = listItems[indx2];
      }
      TryItOnProvider.instance.currentSelectedColorTemp.value = color;
      cPrint("New Product name==> ${listItems[indx2].name}");
      cPrint("New Product name==> ${listItems[indx2].alternateColors}");
      cPrint("New Product color==> ${listItems[indx2].shadeColor}");
    }
  }

  changeProductatProductsPageMySelection(int i, String color) async {
    cPrint("++++++++++++++++++++++++++++++");
    // SelectedProductController.to.value.forEach((element) {
    //   cPrint(element.faceSubArea);
    // });
    // cPrint(" ${(SelectedProductController.to.value[0] as Items).faceSubArea}");

    // centralcolorleftmost.forEach((element) {
    //   cPrint("${element.faceSubArea} ==== ${(SelectedProductController.to.value[0] as Items).faceSubArea}");
    //   // cPrint(
    //   //     "${element.faceSubArea} ==== ${(SelectedProductController.to.value[3] as FaceSubareaLeftmostListOfProducts).faceSubAreaName}");
    // });

    // var indx = centralcolorleftmost
    //     .indexWhere((element) => element.faceSubArea == (SelectedProductController.to.value[0] as Items).faceSubArea);
    // cPrint(indx);

    // CentralColorLeftmost element = centralcolorleftmost[indx];
    // cPrint("New Product name length==> ${element.faceSubareaLeftmostListOfProducts!.length}");
    // List<FaceSubareaLeftmostListOfProducts> listItems = element.faceSubareaLeftmostListOfProducts ?? [];
    // var indx2 = listItems.indexWhere((element) => element.shadeColor == color);
    // if (element.faceSubareaLeftmostListOfProducts!.length >= indx2) {
    //   TryItOnProvider.instance.currentSelectedColorTemp.value = color;
    //   // TryItOnProvider.instance.centralcolorleftmostselected.value[indx] = listItems[indx2];
    //   cPrint("New Product name==> ${listItems[indx2].name}");
    //   cPrint("New Product color==> ${listItems[indx2].shadeColor}");
    // }
  }

  Future<bool?> saveSelectedProducts() async {
    try {
      Map<String, dynamic> tokenMap = await sfQueryForSharedPrefData(
          fieldName: 'user-token', type: PreferencesDataType.STRING);
      String token =
          tokenMap['user-token'] ?? '';
      bool? res = await sfAPIStoreMySelectedProducts(token, selectedProducts);
      return res;
    } catch (err) {
      cPrint("SaveselectedProducts error warning :-$err");
    }
    return false;
  }

  Future<bool> getproductwarning(String sku) async {
    try {
      Map<String, dynamic> tokenMap = await sfQueryForSharedPrefData(
          fieldName: 'user-token', type: PreferencesDataType.STRING);
      String token =
          tokenMap['user-token'] ?? '';
      // cPrint("::::::: $sku");
      bool res = await sfAPIGetProductWarning(sku, token);
      return res;
    } catch (err) {
      cPrint("error warning :-$err");
    }
    return false;
  }

  /// DEFAULTS

  setDefault() {
    page = 0.obs;
    scan = 0.obs;
    count = 0.obs;
    bottomSheetVisible = false.obs;
    faceSubAreaOptionsVisible = false.obs;
    notFound = false.obs;
    selectedFaceArea = 'Eyes'.obs;
    selectedFaceSubArea = ''.obs;
    scannedResult = ScannedResult().obs;
    lookname = ''.obs;
    selectedProduct = false.obs;
    alternateColors = RxList();
    centralcolor = RxList<Colorss>();
    currentSelectedArea = 0.obs;
    currentSelectedColor = ''.obs;
    exact = false.obs;
    received = Product(
      name: '',
      description: '',
      faceSubArea: -1,
      price: 0.0,
      image: '',
      sku: '',
      id: -1,
      avgRating: "0.0",
    ).obs;
    retrieved = null;
    lookreceived = [
      Product(
          id: 0,
          name: "",
          sku: "",
          price: 0,
          image: "",
          description: "",
          faceSubArea: 0,
          avgRating: "")
    ].obs;
  }
}

class ScannedResult {
  late String label;
  late String ingredients;
  late String color;

  ScannedResult({
    this.label = '',
    this.ingredients = '',
    this.color = '',
  });

  bool success() {
    if (this.label.isEmpty || this.ingredients.isEmpty || this.color.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  Map<String, String> mapped(dynamic customerID) {
    return {
      "customer_id": "$customerID",
      "name_string": "$label",
      "ingredient_string": "$ingredients",
      "detected_color": "$color",
    };
  }
}
