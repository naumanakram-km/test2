// ignore_for_file: deprecated_member_use

import 'dart:convert';
import 'dart:math';

import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sofiqe/model/data_ready_status_enum.dart';
import 'package:sofiqe/model/product_model.dart';
import 'package:sofiqe/model/product_model_best_seller.dart';
import 'package:sofiqe/utils/api/product_list_api.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/utils/states/local_storage.dart';
import 'package:sofiqe/utils/states/location_status.dart';

/// Shared Preferences field names
String dealOfTheDayListField = 'deal-of-the-day-products';
String dealOfTheDayHourField = 'deal-of-the-day-hour';
String dealOfTheDayLatitudeField = 'deal-of-the-day-latitude';
String dealOfTheDayLongitudeField = 'deal-of-the-day-longitude';

class HomeProvider extends GetxController {
  static HomeProvider to = Get.find();

  RxList<Product> bestSellerList = <Product>[].obs;
  RxList<Product1> bestSellerListt = <Product1>[].obs;

  RxList<Product> dealOfTheDayList = <Product>[].obs;

  Rx<DataReadyStatus> bestSellerListStatus = DataReadyStatus.INACTIVE.obs;
  Rx<DataReadyStatus> dealOfTheDayStatus = DataReadyStatus.INACTIVE.obs;

  callAPis() async {
    Location location = Location();
    // ignore: no_leading_underscores_for_local_identifiers
    PermissionStatus _permissionGranted;
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted == PermissionStatus.granted) {
        await fetchDealOfTheDayList();
      }
    } else if (_permissionGranted == PermissionStatus.granted) {
      await fetchDealOfTheDayList();
    }
    await fetchBestSellersList();
  }

  Future<bool> fetchBestSellersList() async {
    cPrint("Best  --fetchBestSellersList");

    bestSellerListStatus.value = DataReadyStatus.FETCHING;
    try {
      Map<String, dynamic> bestSellerResponse = await sfAPIGetBestSellers();
      cPrint("Best  -->> succ $bestSellerResponse");

      // List<dynamic> data = bestSellerResponse["bestseller_product"];
      List<dynamic> data = bestSellerResponse["product"];
      cPrint("Best Seller Product at 0 index -->> Ress ${data[0]["name"]}");

      cPrint(
          "Best Seller Products List is Not Empty -->> SSs ${data.isNotEmpty}");

      List<Product1> list = [];
      // cPrint("Best  -->> PPP ${list}");
      cPrint("CHECK ERROR");
      data.forEach((p) {
        list.add(Product1(
          id: int.parse(p['product_id']),
          name: p['name'] ?? "",
          sku: p['sku'] ?? "",
          price: double.parse(p['original_price'] ?? 0),
          // price: double.parse(p['price']??0),
          image: p['image'] ?? "",
          color: p['face_color'] ?? "",
          avgrating: p['avgrating'].toString(),
          reviewCount: p['review_count'].toString(),
          rewardPoints: p['reward_points'].toString(),
          description: p['description'] ?? "",
          productUrl: p['product_url'] ?? "",
          discountedPrice: p["discounted_price"] == null
              ? 0.0
              : double.parse(p["discounted_price"]),
        ));
        // cPrint("Best  -->> PPP ${list[0].name}");
      });
      cPrint("NO ERROR");
      bestSellerListt.value = list;

      bestSellerListStatus.value = DataReadyStatus.COMPLETED;
    } catch (e) {
      bestSellerListStatus.value = DataReadyStatus.ERROR;
      cPrint('Error getting best selling products: $e');
      try {
        cPrint("Best selling products could not be fetched ");
      } catch (ee) {}
    }
    return false;
  }

  Future<bool> fetchDealOfTheDayList() async {
    cPrint("Deal_of_the_day  --fetchDealOfTheDayList");

    dealOfTheDayStatus.value = DataReadyStatus.FETCHING;
    try {
      DateTime time = DateTime.now();

      LocationData location = await getCoordinates();
      if (location.latitude == null || location.longitude == null) {
        throw 'Location data not available';
      }
      double latitude = (location.latitude as double).toPrecision(2);

      double longitude = (location.longitude as double).toPrecision(2);
      cPrint("Deal_of_the_day  --firstcondition");

      if (await shouldFetchNewDealOfTheDay(time, latitude, longitude)) {
        /// Get new deal of the day
        cPrint("Deal_of_the_day  --secondcondition");

        List<dynamic> dealOfTheDayResponse =
            await sfAPIGetDealOfTheDay(latitude, longitude);
        cPrint(
            "dealOfTheDayResponse dealOfTheDayResponse $dealOfTheDayResponse");
        if (dealOfTheDayResponse.isNotEmpty) {
          List<Product> list = [];
          dealOfTheDayResponse[0]["product"].forEach((p) {
            list.add(Product(
                color: p["face_color"],
                id: int.parse(p['product_id']),
                name: p['name'].toString(),
                sku: p['sku'].toString(),
                avgRating: p['extension_attributes'] != null &&
                        p['extension_attributes']['avgrating'] != null
                    ? p['extension_attributes']['avgrating'].toString()
                    : "0.0",
                price: double.parse((p['original_price'] ?? "0").toString()),
                discountedPrice: double.parse(p['discounted_price'] != null
                    ? p['discounted_price'].toString()
                    : (p['original_price'] ?? "0").toString()),
                image: p['image'],
                description: p.containsKey('description')
                    ? (p['description'] ??
                        "Best seller of the season and a sofiqers top choice!")
                    : "Best seller of the season and a sofiqers top choice!",
                faceSubArea:
                    p.containsKey('face_sub_area') ? p['face_sub_area'] : -1,
                faceSubAreaName:
                    p['sub_area'].runtimeType == String ? p['sub_area'] : '',
                options: [],
                reviewCount: p['extension_attributes'] != null &&
                        p['extension_attributes']['review_count'] != null
                    ? p['extension_attributes']['review_count'].toString()
                    : "0",
                rewardsPoint: p['extension_attributes'] != null &&
                        p['extension_attributes']['reward_points'] != null
                    ? p['extension_attributes']['reward_points'].toString()
                    : "0"));
          });
          dealOfTheDayList.value = list;
        }
        saveThisDealOfTheDay(
            dealOfTheDayResponse[0]["product"], time, latitude, longitude);
      } else {
        /// Use old Deal of the day
        cPrint("Deal_of_the_day  --failedcondi");

        SharedPreferences pref = await SharedPreferences.getInstance();
        cPrint(
            "SHARED PREFRENCE DATA ${pref.getString('$dealOfTheDayListField')}");
        List<dynamic> dealOfTheDayFromStorage =
            json.decode(pref.getString('$dealOfTheDayListField') as String);
        cPrint("dealOfTheDayFromStorage  ${dealOfTheDayFromStorage}");

        dealOfTheDayList.value = dealOfTheDayFromStorage.map<Product>(
          (p) {
            return Product(
              color: p["face_color"],
              id: int.parse(p['product_id']),
              name: p['name'].toString(),
              sku: p['sku'].toString(),
              avgRating: p['extension_attributes'] != null &&
                      p['extension_attributes']['avgrating'] != null
                  ? p['extension_attributes']['avgrating'].toString()
                  : "0.0",
              price: double.parse((p['original_price'] ?? "0").toString()),
              discountedPrice: double.parse(p['discounted_price'] != null
                  ? p['discounted_price'].toString()
                  : (p['original_price'] ?? "0").toString()),
              image: p['image'],
              description: p.containsKey('description')
                  ? (p['description'] ??
                      "Best seller of the season and a sofiqers top choice!")
                  : "Best seller of the season and a sofiqers top choice!",
              faceSubArea:
                  p.containsKey('face_sub_area') ? p['face_sub_area'] : -1,
              faceSubAreaName:
                  p['sub_area'].runtimeType == String ? p['sub_area'] : '',
              options: [],
              reviewCount: p['extension_attributes'] != null &&
                      p['extension_attributes']['review_count'] != null
                  ? p['extension_attributes']['review_count'].toString()
                  : "0",
              rewardsPoint: p['extension_attributes'] != null &&
                      p['extension_attributes']['reward_points'] != null
                  ? p['extension_attributes']['reward_points'].toString()
                  : "0",
            );
          },
        ).toList();
      }
      dealOfTheDayStatus.value = DataReadyStatus.COMPLETED;
    } catch (e) {
      dealOfTheDayStatus.value = DataReadyStatus.ERROR;
      cPrint('Error fetchDealOfTheDayList: $e');
      try {
        cPrint("Deal of the day could not be fetched");
      } catch (ee) {}
    }
    return false;
  }

  Future<void> saveThisDealOfTheDay(List dealOfTheDayProducts, DateTime now,
      double latitude, double longitude) async {
    cPrint(dealOfTheDayProducts);
    try {
      await sfStoreInSharedPrefData(
          fieldName: '$dealOfTheDayListField',
          value: json.encode(dealOfTheDayProducts),
          type: PreferencesDataType.STRING);
      await sfStoreInSharedPrefData(
          fieldName: '$dealOfTheDayHourField',
          value: now.toString(),
          type: PreferencesDataType.STRING);
      await sfStoreInSharedPrefData(
          fieldName: '$dealOfTheDayLatitudeField',
          value: latitude,
          type: PreferencesDataType.DOUBLE);
      await sfStoreInSharedPrefData(
          fieldName: '$dealOfTheDayLongitudeField',
          value: longitude,
          type: PreferencesDataType.DOUBLE);
    } catch (err) {
      cPrint('Error saveThisDealOfTheDay: $err');
    }
  }

  Future<bool> shouldFetchNewDealOfTheDay(
      DateTime now, double latitude, double longitude) async {
    try {
      SharedPreferences pref = await SharedPreferences.getInstance();

      /// Check for previously stored data
      if (!pref.containsKey('$dealOfTheDayListField')) {
        return true;
      }

      /// Read old time string
      /// convert to DateTime object
      /// compare with the current date time
      String oldTimeString = pref.getString('$dealOfTheDayHourField') as String;
      DateTime oldTime = DateTime.parse(oldTimeString);
      Duration durationSinceLastCall = now.difference(oldTime);
      if (durationSinceLastCall.inHours > 12) {
        return true;
      }

      /// Read old latitude and longitude
      /// compare with the current latitude and longitude
      double oldLatitude =
          pref.getDouble('$dealOfTheDayLatitudeField') as double;
      double oldLongitude =
          pref.getDouble('$dealOfTheDayLongitudeField') as double;

      double distance =
          calculateDistance(latitude, longitude, oldLatitude, oldLongitude);
      cPrint(distance);

      if (distance > 10) {
        return true;
      }

      return false;
    } catch (err) {
      cPrint('Error shouldFetchNewDealOfTheDay: $err');
      return true;
    }
  }

  double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
    double p = 0.017453292519943295;
    double Function(num) c = cos;
    double a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }
}
