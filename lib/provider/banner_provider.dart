import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sofiqe/model/banner_model/banner_model.dart';
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../utils/constants/api_tokens.dart';

class BannerProvider with ChangeNotifier {
  List<BannerModel>? bannerListModel = [];
  dynamic homePageSection1Data;
  int currentIndex = 0;
  bool isLoading = true;

  /// Get Banner List API(ENDPOINTS)
  Future<List> getBannerList(context, {bool isFirst = true}) async {
    // cPrint("URL:${APIEndPoints.getActiveBannerListAPI}");
    SharedPreferences shared_User = await SharedPreferences.getInstance();
    try {
      await http.get(Uri.parse(APIEndPoints.getActiveBannerListAPI),
          headers: {"Authorization": "Bearer ${APITokens.adminBearerId}"}).then((response) async {
        // cPrint("Response Body${response.body}");
        // cPrint("Banner LIST");
        final parsedData = jsonDecode(response.body);
        cPrint("SSS:${parsedData[0]['banners']}");
        if (isFirst) {
          List l = parsedData[0]['banners'];
          for (final element in l) {
            element.forEach((x) {
              bannerListModel!.add(BannerModel.fromJson(x));
            });
          }
        }

        ///--- get only first section
        homePageSection1Data = parsedData[0]['banners'][0][0]['sections'];

        ///------ set the list
        cPrint('====  GOT THE LIST :: length is ${homePageSection1Data.length}  ====');

        if (parsedData != null) {
          String banners = jsonEncode(parsedData);
          var data = shared_User.setString('banners', banners);
          cPrint("successfully added:");
          return data;
        } else {
          return null;
        }
        // cPrint(bannerListModel);
        // bannerListModel = parsedData.map((banner) => new BannerListModel.fromJson(banner)).toList();
        //return parsedData.map((banner) => new BannerListModel.fromJson(banner)).toList();
      });
      isLoading = false;
      notifyListeners();
      return [];
    } on SocketException {
      rethrow;
    } on Exception catch (e) {
      throw Exception(e);
    }
  }

  updateCurrentIndex(int newVal) {
    currentIndex = newVal;
    notifyListeners();
  }

  List listOfImages = [
    'https://images.pexels.com/photos/12528815/pexels-photo-12528815.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    'https://images.pexels.com/photos/4108288/pexels-photo-4108288.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    'https://images.pexels.com/photos/6918619/pexels-photo-6918619.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1'
  ];
}
