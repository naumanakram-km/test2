// ignore_for_file: deprecated_member_use

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
// Model
import 'package:sofiqe/model/product_model.dart';
import 'package:sofiqe/model/product_model_best_seller.dart';
import 'package:sofiqe/utils/api/shopping_cart_api.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/utils/states/local_storage.dart';

import '../controller/cart_loader_controller.dart';
import '../controller/msProfileController.dart';
import '../model/new_product_model.dart';
import '../utils/api/checkout_api.dart';
import '../utils/api/shipping_address_api.dart';
import 'account_provider.dart';

class CartProvider extends ChangeNotifier {
  AccountProvider? userProvider;
  late List<Map<String, dynamic>> chargesList;
  String cartToken = '';
  List<dynamic>? cart;
  Future<List<dynamic>>? futureCart;
  Map<String, dynamic>? cartDetails;
  var itemCount = 0;
  String cartkey = "";
  late Map vipGuestPoint;
  late Map tierListMap;
  late Map spendingRules;
  late int monetaryStep;
  late int spendPoints;
  late Map vipUserPoint;
  double vipRatio = 0;
  late double totalSavingRation = 0.0;
  double get savingRatio => totalSavingRation;

  dynamic userReward = 0;
  late int customerminspendpoints = 0;
  late int customermaxspendpoints = 0;
  late int customerPoints = 0;
  late int tierID = 0;

  late bool isLoggedIn = false;
  int customerID = 0;
  late int freeShipping = 0;

  double lastSpent = 0;
  double totalPayment = 0;
  double vipRatioForCompletedOrder = 0;
  double totalSaving = 0;

  RxBool _isProcessing = false.obs;
  // bool _isProcessing = false;
  bool get isProcessing => _isProcessing.value;

  changeProcessingVal(bool newVal, {bool isRefresh = false}) {
    _isProcessing.value = newVal;
    if (isRefresh) {
      notifyListeners();
    }
  }

  final List listItem = [1, 2, 3, 4, 5];

  void update(login, customerID) async {
    isLoggedIn = login;
    customerID = customerID;
    await fetchCartDetails();
    notifyListeners();
  }

  CartProvider() {
    _initData();
  }

  _initData() async {
    chargesList = [
      {
        'name': 'Subtotal',
        'amount': 0,
        'display': '0',
      },
      {
        'name': 'Delivery',
        'amount': 3.95,
        'display': '3.95',
      },
      {
        'name': 'VAT',
        'amount': 0,
        'display': 'Included',
      },
      {
        'name': 'Total',
        'amount': 0,
        'display': '0',
      },
    ];

    await initializeCart();
  }

  Future<void> initializeCart() async {
    ///bool isreset = false, dynamic context are only to set the address for the customer cart
    String token = await sfAPIInitializeGuestShoppingCart();
    cartToken = token;
    // cPrint("CartToken  -->> succ $cartToken");

    await sfStoreInSharedPrefData(fieldName: 'cart-token', value: '$cartToken', type: PreferencesDataType.STRING);
    await fetchCartDetails();
    notifyListeners();
    await fetchVipCoins(0, '1 initilize cart');
    notifyListeners();
  }

  Future<bool> genrateCart() async {
    Map<String, dynamic> tokenMap =
        await sfQueryForSharedPrefData(fieldName: 'user-token', type: PreferencesDataType.STRING);
    String token = tokenMap['user-token'] ?? '';
    cartkey = await sfAPICreateCustomerCart(token);
    return true;
  }

  // FUNCTION: Loader dialog
  // ARGUMENTS: set loader with color and barrier
  // RETURNS: Alert dialog with loader
  // NOTES: show loader dialog

  showLoaderDialog(BuildContext context) {
    final alert = SpinKitDoubleBounce(
      color: Colors.white,
      // color: Color(0xffF2CA8A),
      size: 50.0,
    );
    // notifyListeners();
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

//   Future<bool> fetchCartDetails() async {

//       itemCount = 0;
//       if (!_isLoggedIn) {
//         cart = await sfAPIGetGuestCartList(cartToken);
//         cartDetails = await sfAPIGetGuestCartDetails(cartToken);
//       } else {
//         cartDetails = await sfAPIGetUserCartDetails();
//         cart = await sfAPIGetUserCartList();
// =======
  Future<List<dynamic>?> fetchCartDetails({bool isreset = false, dynamic context}) async {
    ///bool isreset = false, dynamic context are only to set the address for the customer cart
    // itemCount = 0;
    if (!isLoggedIn) {
      cart = await sfAPIGetGuestCartList(cartToken);
      cartDetails = await sfAPIGetGuestCartDetails(cartToken);
    } else {
      cart = await sfAPIGetUserCartList();
      cartDetails = await sfAPIGetUserCartDetails();
      // cPrint("========CartID USER===============${cartDetails?['id']}");
    }
    if (isreset) {
      if (isLoggedIn) {
        var addressInfo = MsProfileController.to.getAddressToAssignTocart();
        cPrint("New Cart Show Here");
        cPrint(cartDetails?['id']);
        Provider.of<AccountProvider>(context, listen: false).shipAddress =
            await sfAPIAddShippingAddress(addressInfo, isLoggedIn, cartDetails?['id']);
      }
    }
    if (cart!.isEmpty) {
      // cPrint('===== Cart is Empty =======');
    } else {
      // cPrint("=========== Current CART product at 0 = ${cart![0]} ============");
    }
    for (final element in cart!) {
      element['children'] = [];
    }
    for (final element in cart!) {
      if (element['price'] == 0) {
        for (final item in cart!) {
          if (item['name'] == element['extension_attributes']['look_name']) {
            element['extension_attributes']['name'] = element['name'];
            var itemExtension = element['extension_attributes'];
            itemExtension["product_qty"] = element['qty'];
            (item['children'] as List).add(itemExtension);
          }
        }
      }
    }

    // cPrint(cart);
    _setItemCount();
    await calculateCartPrice();
    getTotalQty();
    await fetchVipCoins(Provider.of<AccountProvider>(Get.context!, listen: false).customerId,
        '2 . initstate of cart price distribution file');
    // LoaderController.instance.changeProcessingVal(false);
    LoaderController.instance.isProcessing.value = false;
    // changeProcessingVal(false);
    notifyListeners();
    return cart;
  }

  Future<List<dynamic>?> fetchCartDetailsFuture() async {
    // itemCount = 0;
    if (!isLoggedIn) {
      futureCart = sfAPIGetGuestCartList(cartToken);
      cartDetails = await sfAPIGetGuestCartDetails(cartToken);
      notifyListeners();
      return futureCart;
    } else {
      futureCart = sfAPIGetUserCartList();
      cartDetails = await sfAPIGetUserCartDetails();
      // cPrint("========CartID USER===============${cartDetails?['id']}");
      notifyListeners();
    }
    if (cart!.isEmpty) {
      // cPrint('===== Cart is Empty =======');
    } else {
      // cPrint("=========== Current CART product at 0 = ${cart![0]} ============");
    }
    for (final element in cart!) {
      element['children'] = [];
    }
    for (final element in cart!) {
      if (element['price'] == 0) {
        for (final item in cart!) {
          if (item['name'] == element['extension_attributes']['look_name']) {
            element['extension_attributes']['name'] = element['name'];
            (item['children'] as List).add(element['extension_attributes']);
          }
        }
// >>>>>>> bd28e4e10403ca20a15a8cf1a199a9e704212cb5
      }
    }

    // cPrint(cart);
    _setItemCount();
    calculateCartPrice();
    notifyListeners();
    return futureCart;
  }

  Future<int> fetchVipCoins(customerID, String commingFrom) async {
    // cPrint("fetchVipCoins $customerID");
    // Provider.of<AccountProvider>(context, listen: false).customerId

    cPrint('==== i am coming from method $commingFrom  :: customer id is $customerID   =====');

    try {
      itemCount = 0;

      ///------ Commented For Testing Purposes : will be removed
      // if (!isLoggedIn) {
      //   cPrint('===== 1 is running ====');
      //   vipGuestPoint = await sfAPIGetGuestVipCoins();
      //   if (!vipGuestPoint.containsKey('items')) {
      //     throw 'Key not found: items';
      //   }
      //   List itemsList = vipGuestPoint['items'];
      //   itemsList.forEach((element) {
      //     if (element['id'] == 9) {
      //       if (element.containsKey('tiers')) {
      //         List tiersList = element['tiers'];
      //         tiersList.forEach(
      //           (attribute) {
      //             if (attribute['tier_id'] == 1) {
      //               vipRatio = attribute['earn_points'] / attribute['monetary_step'];
      //               // cPrint("vipRatio" + vipRatio.toString());
      //             }
      //           },
      //         );
      //       }
      //     }
      //   });
      //   cPrint('===== 1.3 is running ====');
      // } else {

      vipGuestPoint = await sfAPIGetGuestVipCoins();
      vipUserPoint = await sfAPIGetUserVipCoins(cartDetails!['id'], customerID);
      if (!vipUserPoint.containsKey('total_segments')) {
        throw 'Key not found: total_segments';
      }

      ///------ Commented For Testing Purposes : will be removed

      // List itemsList1 = vipUserPoint['items'];
      // itemsList1.forEach((element) {
      //   if (element['id'] == 9) {
      //     if (element.containsKey('tiers')) {
      //       List tiersList = element['tiers'];
      //       tiersList.forEach(
      //             (attribute) {
      //           if (attribute['tier_id'] == 1) {
      //             vipRatio =
      //                 attribute['earn_points'] / attribute['monetary_step'];
      //             cPrint("vipRatio" + vipRatio.toString());
      //           }
      //         },
      //       );
      //     }
      //   }
      // });

      List itemsList = vipUserPoint['total_segments'];
      for (final element in itemsList) {
        if (element['code'] == 'rewards-total') {
          userReward = element['value'];
        }
        if (element['code'] == 'grand_total') {
          userReward = element['value'];
        }
        if (element['code'] == 'rewards-spend-min-points') {
          customerminspendpoints = element['value'];
        }
        if (element['code'] == 'rewards-spend-max-points') {
          customermaxspendpoints = element['value'];
        }
      }
      // }
      debugPrint('===== VIP Ratio is $vipRatio  =====');
      debugPrint('===== user reward is $userReward  =====');
      notifyListeners();
      return int.parse(double.parse(userReward.toString()).toStringAsFixed(0));
    } catch (err) {
      cPrint('Error fetchVipCoins: $err');
      return 0;
    }
  }

  Future<bool> fetchTiersList(int customerId) async {
    log("==== in the fetch tier list function =====");
    // cPrint("fetchVipCoins");
    // Provider.of<AccountProvider>(context, listen: false).customerId
    try {
      tierListMap = await sfAPIGetTiersList();
      if (!tierListMap.containsKey('items')) {
        throw 'Key not found: items';
      }
      if (isLoggedIn) {
        // cPrint("VIPPOINT=$customerId");
        customerPoints = await sfAPIGetCustomerPoints(customerId);
      }

      List itemsList = tierListMap['items'];
      log("=== length of items tiers ${itemsList.length} ====");
      // bool forEachDone = false;

      if (customerPoints >= itemsList[itemsList.length - 1]['min_earn_points']) {
        tierID = itemsList[itemsList.length - 1]['tier_id'];
      } else {
        for (final element in itemsList) {
          if (element['min_earn_points'] >= customerPoints && tierID == 0) {
            tierID = element['tier_id'];
            // cPrint("RAAAAA--" + tierID.toString());
          }
        }
      }

      log("=== calculated tier id is  $tierID :: and customer points are $customerPoints ====");
      spendingRules = await sfAPISpendingRules();
      if (!spendingRules.containsKey('items')) {
        throw 'Key not found: items';
      }
      List spendingItemsList = spendingRules['items'];
      log("=== length of spending rules ${spendingItemsList.length} ====");

      for (final element in spendingItemsList) {
        if (element.containsKey('tiers')) {
          List tiersList = element['tiers'];
          for (final attribute in tiersList) {
            if (attribute['tier_id'] == tierID) {
              spendPoints = attribute['spend_points'];
              monetaryStep = attribute['monetary_step'];

              log("=== in calculating ratio :: $spendPoints :: $monetaryStep :: $tierID ===");

              totalSavingRation = monetaryStep / spendPoints;
              log("=== saving ratio is $totalSavingRation ===");

              // cPrint("monetary_step" + monetaryStep.toString());
              // cPrint("spend_points" + spendPoints.toString());
              // cPrint("totalSavingRation" + totalSavingRation.toString());
            }
          }
        }
      }

      return true;
    } catch (err) {
      cPrint('Error fetchVipCoins: $err');
      return false;
    }
  }

  setTheTotalPaymentForCompletedOrder() {
    totalPayment = chargesList[3]['amount'];
    log("===== total payment value is $totalPayment =====");
  }

  calculateVIPPointsNew() {
    try {
      List itemsList = vipGuestPoint['items'];
      for (final element in itemsList) {
        if (element['id'] == 12) {
          if (element.containsKey('tiers')) {
            List tiersList = element['tiers'];
            for (final attribute in tiersList) {
              if (attribute['tier_id'] == 1) {
                vipRatioForCompletedOrder = attribute['earn_points'] / attribute['monetary_step'];
              }
            }
          }
        }
      }
    } catch (e) {
      cPrint(e.toString());
    }
    debugPrint('==== VIP Ratio after completed order is $vipRatioForCompletedOrder =====');
  }

  Future<void> deleteCart() async {
    if (isLoggedIn) {
      await sfRemoveFromSharedPrefData(fieldName: 'cart-token');
    } else {
      // await sfRemoveFromSharedPrefData(fieldName: 'user-token');
    }
    cart = [];
    notifyListeners();
  }

  Future<void> addToCartForAddAll(
      BuildContext context, String sku, List simpleProductOptions, int type, String lookName,
      {bool refresh = true, int quantity = 0}) async {
    cPrint("CONTROLL 22");
    cPrint(cartDetails);
    cPrint(Provider.of<AccountProvider>(context, listen: false).isLoggedIn.toString());

    !Provider.of<AccountProvider>(context, listen: false).isLoggedIn
        ? await sfAPIAddItemToCart(
            cartToken,
            cartDetails!['id'],
            sku,
            simpleProductOptions,
            type,
            'Guest',
            lookName,
            quantity: quantity,
          )
        : await sfAPIAddItemToCart(cartToken, cartDetails!['id'], sku, simpleProductOptions, type, 'LoggedIn', lookName,
            quantity: quantity);
    if (refresh) {
      await fetchCartDetails();
    }
    notifyListeners();
  }

  Future<void> addToCartForListOfSKUs(
      {required BuildContext context, required String listOfSKUs, bool refresh = true}) async {
    cPrint("=== adding list of SKUs ===");
    cPrint(cartDetails.toString());
    cPrint(Provider.of<AccountProvider>(context, listen: false).isLoggedIn.toString());

    await sfAPIAddListOfSKUsToCart(
      cartDetails!['id'],
      listOfAllSKUs: listOfSKUs,
    );

    // !Provider.of<AccountProvider>(context, listen: false).isLoggedIn
    //     ? await sfAPIAddItemToCart(
    //   cartToken,
    //   cartDetails!['id'],
    //   sku,
    //   simpleProductOptions,
    //   type,
    //   'Guest',
    //   lookName,
    //   quantity: quantity,
    // )
    //     : await sfAPIAddItemToCart(cartToken, cartDetails!['id'], sku, simpleProductOptions, type, 'LoggedIn', lookName,
    //     quantity: quantity);
    //
    //

    if (refresh) {
      await fetchCartDetails();
    }
    notifyListeners();
  }

  Future<void> addToCart(BuildContext context, String sku, List simpleProductOptions, int type, String lookName,
      {bool refresh = true, int quantity = 0}) async {
    // try {
    cPrint("CONTROLL 22");
    cPrint(cartDetails);
    cPrint(Provider.of<AccountProvider>(context, listen: false).isLoggedIn.toString());

    !Provider.of<AccountProvider>(context, listen: false).isLoggedIn
        ? await sfAPIAddItemToCart(
            cartToken,
            cartDetails!['id'],
            sku,
            simpleProductOptions,
            type,
            'Guest',
            lookName,
            quantity: quantity,
          )
        : await sfAPIAddItemToCart(cartToken, cartDetails!['id'], sku, simpleProductOptions, type, 'LoggedIn', lookName,
            quantity: quantity);
    if (refresh) {
      await fetchCartDetails();
    }
    notifyListeners();
    // } catch (e) {
    //   if (refresh) {
    //     await fetchCartDetails();
    //   }
    //   notifyListeners();
    //   cPrint('Error adding product to cart: $e');
    //   Map message = e as Map;
    //   if (message['message'].compareTo('The requested qty is not available') == 0) {
    //     Get.showSnackbar(
    //       GetSnackBar(
    //         message: 'The product is out of stock',
    //         duration: Duration(seconds: 2),
    //         isDismissible: true,
    //       ),
    //     );
    //   } else {
    //     Get.showSnackbar(
    //       GetSnackBar(
    //         message: 'Could not add product to cart',
    //         duration: Duration(seconds: 2),
    //       ),
    //     );
    //   }
    // }
  }

  Future<void> addGiftCardToCart(
      BuildContext context, String sku, String email, String message, String optionId1, String optionId2,
      {bool refresh = true, int quantity = 0}) async {
    try {
      if (await sfAPIGiftCardToCart(cartToken, cartDetails!['id'], sku, email, message, optionId1, optionId2)) {
        if (refresh) {
          await fetchCartDetails();
        }

        Get.showSnackbar(
          GetSnackBar(
            message: 'GiftCard Successfully added to cart',
            duration: Duration(
              seconds: 2,
            ),
          ),
        );
        notifyListeners();
      } else {
        Get.showSnackbar(
          GetSnackBar(
            message: 'Sorry, something bad happened, unable to add GiftCard',
            duration: Duration(
              seconds: 2,
            ),
          ),
        );
      }
    } catch (e) {
      if (refresh) {
        await fetchCartDetails();
      }
      notifyListeners();
      Map message = e as Map;
      if (message['message'].compareTo('The requested qty is not available') == 0) {
        Get.showSnackbar(
          GetSnackBar(
            message: 'The product is out of stock',
            duration: Duration(seconds: 2),
            isDismissible: true,
          ),
        );
      } else {
        Get.showSnackbar(
          GetSnackBar(
            message: 'Could not add product to cart',
            duration: Duration(seconds: 2),
          ),
        );
      }
    }
  }

  Future<void> addToCartConfigurableProduct(
      BuildContext context, String sku, List configurableProductOptions, List simpleProductOptions, int type,
      {bool refresh = true, int quantity = 0}) async {
    try {
      !Provider.of<AccountProvider>(context, listen: false).isLoggedIn
          ? await sfAPIAddItemToCartConfigurable(
              cartToken, cartDetails!['id'], sku, simpleProductOptions, configurableProductOptions, type, 'Guest',
              quantity: quantity)
          : await sfAPIAddItemToCartConfigurable(
              cartToken, cartDetails!['id'], sku, simpleProductOptions, configurableProductOptions, type, 'LoggedIn',
              quantity: quantity);
      if (refresh) {
        await fetchCartDetails();
      }
      notifyListeners();
    } catch (e) {
      if (refresh) {
        await fetchCartDetails();
      }
      notifyListeners();
      // cPrint('Error adding product to cart: $e');
      Map message = e as Map;
      if (message['message'].compareTo('The requested qty is not available') == 0) {
        Get.showSnackbar(
          GetSnackBar(
            message: 'The product is out of stock',
            duration: Duration(seconds: 2),
            isDismissible: true,
          ),
        );
      } else {
        Get.showSnackbar(
          GetSnackBar(
            message: 'Could not add product to cart',
            duration: Duration(seconds: 2),
          ),
        );
      }
    }
  }

  Future<void> addHomeProductsToCart(BuildContext context, Product product, {bool refresh = true}) async {
    cPrint("CONTROLL 11 ");
    await addToCart(context, product.sku!, product.options ?? [], 0, "", refresh: refresh);
  }

// add product to cart
  Future<void> addHomeProductsToCartForOverlay(BuildContext context, NewProductModel product,
      {bool refresh = true}) async {
    await addToCart(context, product.sku!, [], 1, "", refresh: refresh);
  }

  // add product to cart with SKU Only (Only for Simple products with Zero Options)
  Future<void> addtoCartSimpleWithSku(BuildContext context, String sku) async {
    await addToCart(context, sku, [], 1, "");
  }

  Future<void> addHomeProductsToCartt(BuildContext context, Product1 product) async {
    addToCart(context, product.sku!, [], 1, "");
  }

  Future<void> removeFromCart(BuildContext context, String itemId,
      {bool refresh = true, bool isUpdateLoader = false}) async {
    // changeProcessingVal(true);
    // showLoaderDialog(context);
    Provider.of<AccountProvider>(context, listen: false).isLoggedIn
        ? await sfAPIRemoveItemFromCart(cartToken, itemId, "LoggedIn")
        : await sfAPIRemoveItemFromCart(cartToken, itemId, "Guest");
    // Navigator.canPop(context) ? Navigator.pop(context) : null;
    // notifyListeners();
    if (refresh) {
      await fetchCartDetails();
      // Navigator.canPop(context) ? Navigator.pop(context) : null;
      // notifyListeners();
    }
    // Navigator.canPop(context) ? Navigator.pop(context) : null;
    if (isUpdateLoader) {
      LoaderController.instance.isProcessing.value = false;
    }

    // LoaderController.instance.changeProcessingVal(false);
    // changeProcessingVal(false);
    notifyListeners();
  }

  Future<void> deleteAllFromCart(BuildContext context) async {
    // try {
    cPrint("Delete All Called");
    cPrint(cartDetails);
    cPrint(Provider.of<AccountProvider>(context, listen: false).isLoggedIn.toString());

    !Provider.of<AccountProvider>(context, listen: false).isLoggedIn
        ? await sfAPIRemoveAllItemFromCart(
            cartToken,
            'Guest',
          )
        : await sfAPIRemoveAllItemFromCart(cartDetails!['id'].toString(), 'LoggedIn');

    await fetchCartDetails();

    notifyListeners();
  }

  void _setItemCount() {
    itemCount = (cart ?? []).length;
    notifyListeners();
  }

  var giftCarApplied = false;
  dynamic appliedGiftCardAmount = 0.0;
  final TextEditingController giftCardCodeTextController = TextEditingController();

  void updateGiftCardApplied(bool newTotal) {
    giftCarApplied = newTotal;
    notifyListeners();
  }

  void updateCartTotal(dynamic newTotal, bool isGiftCard) {
    chargesList[3]['amount'] = newTotal;
    chargesList[3]['display'] = newTotal.toString();
    if (isGiftCard) {
      giftCarApplied = true;
    }
    notifyListeners();
  }

  reduceGiftCardAmount() async {
    if (giftCarApplied == true && appliedGiftCardAmount != 0.0) {
      dynamic cardConsumedResponse =
          await sfAPISubtractGiftCardAmount(appliedGiftCardAmount.toString(), giftCardCodeTextController.text);
      cPrint('========== Gift Card Consumed Response is :: $cardConsumedResponse   ===========');
    }
  }

  updateAppliedGiftCardAmount(double newVal) {
    appliedGiftCardAmount = newVal;
    if (newVal == 0.0) {
      giftCardCodeTextController.clear();
    }
    notifyListeners();
  }

  Future<bool> calculateCartPrice() async {
    if (cart != null) {
      // Calculate subtotal
      chargesList[0]['amount'] = 0;
      for (final item in cart!) {
        chargesList[0]['amount'] += item['price'] * item['qty'];
      }
      chargesList[0]['display'] = '${chargesList[0]['amount']}';

      // Calculate total
      chargesList[3]['amount'] = 0;
      for (final charge in chargesList) {
        if (charge['name'] != 'Total') {
          chargesList[3]['amount'] += charge['amount'];
        }
      }
      chargesList[3]['display'] = chargesList[3]['amount'];
    }

    return true;
  }

  bool isGiftCardApplied = false;

  updateIsGiftCardApplied(bool newVal) {
    isGiftCardApplied = false;
    notifyListeners();
  }

  void setDeliverCharges(double charge) {
    chargesList[1]['amount'] = charge;
    chargesList[1]['display'] = '$charge';
    isGiftCardApplied = true;
    calculateCartPrice();
    cPrint("ODCS==>$chargesList");
    notifyListeners();
  }

  double getSumTotal() {
    return chargesList[3]['amount'];
  }

  int cartQty = 0;
  int getTotalQty() {
    num totalQty = 0;

    for (final element in cart ?? []) {
      cPrint("element ${element}");
      if (element['name'] != element['extension_attributes']['look_name']) {
        totalQty += element!['qty'];
      }
      // log("===== index of item is ${cart?.indexOf(element)} =====");
      // log("===== name of item is ${element!['name']} =====");
      // log("===== qty of item is ${element!['qty']} =====");
    }
    cartQty = totalQty.toInt();
    // notifyListeners();
    // cPrint("total qty ::$totalQty");
    return totalQty.toInt();
  }

  int getCartLength() {
    itemCount = (cart ?? []).length;
    return (cart ?? []).length;
  }
}