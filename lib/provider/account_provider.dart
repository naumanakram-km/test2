// ignore_for_file: deprecated_member_use

import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sofiqe/controller/looksController.dart';
import 'package:sofiqe/controller/msProfileController.dart';
import 'package:sofiqe/model/profile.dart';
import 'package:sofiqe/model/user_model.dart';
import 'package:sofiqe/provider/wishlist_provider.dart';
import 'package:sofiqe/utils/api/user_account_api.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/utils/states/local_storage.dart';

import '../controller/controllers.dart';
import '../controller/natural_me_controller.dart';
import '../controller/reviewController.dart';
import '../utils/api/free_shipping_amount_api.dart';

class AccountProvider extends ChangeNotifier {
  late String userToken;
  late int customerId;
  bool isLoggedIn = false;
  late bool goldPremium;
  User? user;
  String freeShippingAmount = '';
  Profile? profileModle;
  String? shipAddress;
  var userProfileImageUrl = ''.obs;
  var loadingPicture = true.obs;
  String? appVersion;

  AccountProvider() {
    _initData();
  }

  _initData() async {
    // userToken = '';
    customerId = -1;
    isLoggedIn = false;
    goldPremium = false;
    getAppVersionInfo();
    await checkSavedAccount();
    await getFreeShippingInfo();
  }

  getAppVersionInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    appVersion = packageInfo.version;
    debugPrint('===== app version is $appVersion ======');
    notifyListeners();
  }

  Future<bool> getFreeShippingInfo() async {
    try {
      List freeShippingResponse = await sfAPIGetFreeShippingInfo();

      if (freeShippingResponse.isNotEmpty) {
        // String status = freeShippingResponse[0]['status'];
        freeShippingAmount = freeShippingResponse[0]['amount'];
        // String message = freeShippingResponse[0]['message'];
      }
    } catch (e) {
      rethrow;
    }
    return true;
  }

  Future<void> checkSavedAccount() async {
    Map userTokenMap = await sfQueryForSharedPrefData(fieldName: 'user-token', type: PreferencesDataType.STRING);
    if (userTokenMap['found']) {
      getUserDetails(userTokenMap['user-token']);
      userToken = userTokenMap['user-token'];
    }
  }

  //160
  Future<void> getUserDetails(String userToken) async {
    try {
      var result = await sfAPIGetUserDetails(userToken);

      if (result != null) {
        profileModle = Profile.fromJson(result);
        await sfStoreInSharedPrefData(
            fieldName: 'customer-id', value: profileModle?.id ?? -1, type: PreferencesDataType.INT);
        customerId = profileModle?.id ?? -1;
        if (profileModle?.addresses != null && profileModle?.addresses?.length != 0) {
          shipAddress = profileModle?.addresses?[0].street?[0] ?? "";
          if (shipAddress!.isNotEmpty) {
            shipAddress = shipAddress! + ", ";
          }
          try {
            shipAddress = shipAddress! + (profileModle?.addresses?[0].street?[1] ?? "");
            if (shipAddress!.isNotEmpty) {
              shipAddress = shipAddress! + ", ";
            }
          } catch (e) {
            rethrow;
          }
          shipAddress = shipAddress! + (profileModle?.addresses?[0].city ?? "");
          if (shipAddress!.isNotEmpty) {
            shipAddress = shipAddress! + ", ";
          }
          //
          shipAddress = shipAddress! + (profileModle?.addresses?[0].postcode ?? "");
          if (shipAddress!.isNotEmpty) {
            shipAddress = shipAddress! + ", ";
          }
          //
          shipAddress = shipAddress! + (profileModle?.addresses?[0].region!.region ?? "");
          if (shipAddress!.isNotEmpty) {
            shipAddress = shipAddress! + ", ";
          }
          shipAddress = shipAddress! + (profileModle?.addresses?[0].countryId ?? "");
        }
      } else {
        // Get.snackbar('Error', '${result[0]["message"]}', isDismissible: true);
      }
    } catch (e) {
      cPrint(e.toString());
    }

    try {
      Map userMap = await sfAPIGetUserDetails(userToken);
      user = User(
        id: userMap['id'],
        email: userMap['email'] ?? "",
        firstName: userMap['firstname'] ?? "",
        lastName: userMap['lastname'] ?? "",
        addresses: userMap['addresses'] ?? [],
      );
      customerId = user!.id;
      isLoggedIn = true;
      await sfStoreInSharedPrefData(fieldName: 'uid', value: userMap['id'], type: PreferencesDataType.INT);
      await sfStoreInSharedPrefData(fieldName: 'email', value: userMap['email'], type: PreferencesDataType.STRING);
      await sfStoreInSharedPrefData(
          fieldName: 'name',
          value: '${userMap['firstname'] ?? ""} ${userMap['lastname']}',
          type: PreferencesDataType.STRING);

      notifyListeners();
    } catch (e) {
      cPrint("EXCEPTION USER DATA $e");
    }
  }

  Future<void> saveProfilePicture() async {
    var dir = await getApplicationDocumentsDirectory();
    File file = File(join(dir.path, 'front_facing.jpg'));
    log('========  save profile/selfie picture API called   =======');
    sfAPISaveProfilePicture(file, customerId);
  }

  File? pickedImage;
  final picker = ImagePicker();
  Future getGalleryImage(BuildContext context) async {
    final pickedFile =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 60, maxHeight: 1754, maxWidth: 1240);
    if (pickedFile != null) {
      pickedImage = File(pickedFile.path);
      log('====>>>>>> Profile Image is being Uploaded <<<<<======');
      Get.defaultDialog(
          title: "",
          barrierDismissible: false,
          titleStyle: TextStyle(fontSize: .1),
          backgroundColor: Colors.white,
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SpinKitDoubleBounce(
                color: Color(0xffF2CA8A),
                size: 50.0,
              ),
              SizedBox(height: 12),
              Text(
                'Uploading Please wait...',
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.black,
                      fontSize: 10,
                    ),
              )
            ],
          ));
      await sfAPISaveProfilePicture(pickedImage!, customerId);
      await NaturalMeController.to.getNaturalMe(isRefresh: false);
      await getProfilePictureFromServer();
      notifyListeners();
      Get.showSnackbar(
        GetSnackBar(
          message: "New profile image has been uploaded successfully",
          duration: Duration(seconds: 2),
          isDismissible: true,
        ),
      );
      Navigator.pop(context);
      log('====>>>>>> Profile Image has been Uploaded Successfully <<<<<======');
    } else {
      Get.showSnackbar(
        GetSnackBar(
          message: "You haven't picked any image",
          duration: Duration(seconds: 2),
          isDismissible: true,
        ),
      );
      log('==== no image picked =====');
    }
  }

  getProfilePictureFromServer() async {
    try {
      var result = await sfAPIGetUserSelfie(customerId);
      loadingPicture.value = false;
      log('==== selfie result is :: $result ===');
      if (result != null) {
        userProfileImageUrl.value = result == "Selfie has not been uploaded" ? '' : result;
      } else {
        userProfileImageUrl.value = '';
        log('==== get selfie response result is null :: $result');
      }
      notifyListeners();
    } catch (e) {
      userProfileImageUrl.value = '';
      log('==== Error while getting image =====');
      loadingPicture.value = false;
    }
  }

  Future<bool> login(String username, String password, BuildContext c) async {
    cPrint("LOGIN RESULT 1111");
    var result = await sfAPILogin(username, password, c);
    cPrint("LOGIN RESULT $result");
    if (result != 'error') {
      cPrint("LOGIN RESULT 1111 Not error");
      isLoggedIn = true;
      userToken = result;

      await sfStoreInSharedPrefData(fieldName: 'user-token', value: result, type: PreferencesDataType.STRING);
      cPrint("LOGIN RESULT 1111 sfStoreInSharedPrefData set ");
      await getUserDetails(userToken);
      debugPrint('====  customer id is :: $customerId ====');
      getProfilePictureFromServer();
      ReviewController.to.getMyRiviewsData();
      ReviewController.to.getWishListData();
      LooksController.to.getLookList();
      MsProfileController.to.profileName.value;
      cPrint("LOGIN RESULT 1111  getuser detail ");
      notifyListeners();

      /// Fetch wishlist
      WishListProvider wp = Get.find();
      cPrint("LOGIN RESULT 1111  Fetch wishlist ");
      wp.login();
      cPrint("LOGIN RESULT 1111  Fetch wishlist AFTER ");

      ///

      return true;
    } else {
      return false;
    }
  }

  Future<void> logout() async {
    userToken = '';
    customerId = -1;
    isLoggedIn = false;
    goldPremium = false;
    user = null;
    await sfRemoveFromSharedPrefData(fieldName: 'user-token');
    await sfRemoveFromSharedPrefData(fieldName: 'uid');

    /// Remove wishlist
    WishListProvider wp = Get.find();
    makeOverProvider.tryitOn.value = false;
    wp.logout();

    ///

    notifyListeners();
  }

  Future<bool> subscribe(Map<String, String> cardDetails) async {
    Map<String, String> cardMap = {
      "customerId": "$customerId",
      "ccNu": "${cardDetails['card_number']}",
      "cvvNu": "${cardDetails['cvv']}",
      "expDate": "${(cardDetails['expiration_date'] as String).replaceAll('/', '')}",
      "isSubcribe": "1",
    };
    try {
      bool success = await sfAPISubscribeCustomerToGold(cardMap);
      if (success) {
        goldPremium = true;
        notifyListeners();
      } else {
        Get.showSnackbar(
          GetSnackBar(
            message: 'Could not complete request, please try again',
            duration: Duration(seconds: 2),
          ),
        );
      }
      return success;
    } catch (e) {
      Get.showSnackbar(
        GetSnackBar(
          message: 'Could not complete subsciption process: $e',
          duration: Duration(seconds: 2),
        ),
      );
    }
    return false;
  }
}
