// ignore_for_file: unrelated_type_equality_checks

import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:sofiqe/model/data_ready_status_enum.dart';
import 'package:sofiqe/model/product_model.dart';
import 'package:sofiqe/utils/api/catalog_filters.dart';
import 'package:sofiqe/utils/api/product_list_api.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/utils/states/user_account_data.dart';

import '../controller/controllers.dart';
import '../controller/questionController.dart';
import '../model/CategoryResponse.dart';
import '../model/make_over_question_model.dart';
import '../model/question_model.dart';
import '../utils/api/product_details_api.dart';

class CatalogProvider extends GetxController {
  RxBool showSearchBar = false.obs;
  var isplaying = true.obs;
  var sku = "".obs;
  var isChangeButtonColor = false.obs;
  Color ontapColor = Colors.grey;
  RxList<Product> catalogItemsList = <Product>[].obs;
  RxList<Product> catalogSaleItemsList = <Product>[].obs;
  RxInt catalogItemsCurrentPage = 0.obs;

  Rx<FilterType> filterType = FilterType.NONE.obs;
  Rx<FilterType> filterTypePressed = FilterType.NONE.obs;
  Rx<FaceArea> faceArea = FaceArea.ALL.obs;

  Rx<DataReadyStatus> catalogItemsStatus = DataReadyStatus.FETCHING.obs;

  Rx<PriceFilter> priceFilter = PriceFilter().obs;

  Rx<BrandFilter> allBrandFilter = BrandFilter().obs;
  Rx<BrandFilter> eyesBrandFilter = BrandFilter().obs;
  Rx<BrandFilter> lipsBrandFilter = BrandFilter().obs;
  Rx<BrandFilter> cheeksBrandFilter = BrandFilter().obs;

  Rx<ProductFilter> allProductFilter = ProductFilter().obs;
  Rx<ChildrenData> categoryFilter = ChildrenData().obs;
  Rx<ProductFilter> eyesProductFilter = ProductFilter().obs;
  Rx<ProductFilter> lipsProductFilter = ProductFilter().obs;
  Rx<ProductFilter> cheeksProductFilter = ProductFilter().obs;

  Rx<SkinToneFilter> eyesSkinToneFilter = SkinToneFilter().obs;
  Rx<SkinToneFilter> lipsSkinToneFilter = SkinToneFilter().obs;
  Rx<SkinToneFilter> cheeksSkinToneFilter = SkinToneFilter().obs;

  Rx<bool> showSubFilters = false.obs;

  Rx<String> inputText = ''.obs;

  List<Result>? question;
  List<MakeOverQuestion> makeover = [];

  var selectedStar = 10.obs;

  var starValueChanges = false.obs;
  var format = NumberFormat.simpleCurrency(locale: Platform.localeName);
  String currencySymbol = "";

  var availableStar = [
    "5 Star",
    "4 Star",
    "3 Star"
  ]; // update rate list as per mention. now user can filter only 5,4,3 star products
  var availableCheeksColor = [
    "#8d5524",
    "#c68642",
    "#e0ac69",
    "#f1c27d",
    "#ffdbac"
  ];
  var availableLipsColor = [
    "#ae5654",
    "#bd6460",
    "#c2726b",
    "#d2827b",
    "#dc8d88",
    "#e1a6a2"
  ];

  final QuestionsController qc = Get.put(QuestionsController());
  var availableSkinTOnes = [];
  var availableEyeColor = [];
  var availableHairColor = [];
  var newData = [];

  var isProfileSearchEyes = false.obs;
  var isProfileSearchLips = false.obs;
  var isProfileSearchCheek = false.obs;
  var selectedSkinTone = 10.obs;
  var slinTOneValueChanges = false.obs;
  var showOnlySaleItems = false.obs;

  var selectCHeekShadeID = "".obs;
  var selectLipsShadeID = "".obs;

  var selectEyesColor = "".obs;
  var selectHairColor = "".obs;

  Map<FaceArea, int> faceAreaToIdMapping = {
    FaceArea.EYES: 279,
    FaceArea.LIPS: 280,
    FaceArea.CHEEKS: 278,
    FaceArea.ALL: -1,
  };

  // late Map brandFaceArea;

  // Constructor
  CatalogProvider() {
    _initializedData();
    currencySymbol = format.currencySymbol;
  }
  setShowSubFilters(bool value) => showSubFilters.value = value;
  callOtherMethods() async {}

  _initializedData() async {
    defaults();
    var data = await fetchUnfilteredItems(false);
    print(data);
    await fetchCategory();
    await fetchBrandNames(faceAreaToStringMapping[FaceArea.EYES] as String);
    // ignore: cast_nullable_to_non_nullable
    await fetchBrandNames(faceAreaToStringMapping[FaceArea.LIPS] as String);
    await fetchBrandNames(faceAreaToStringMapping[FaceArea.CHEEKS] as String);
    setColors();

    //await this.getAnaliticalQuestions();
  }

  onlySaleItems(bool newVal) {
    showOnlySaleItems = newVal.obs;
    if (newVal == false) {
      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
    }
    update();
  }

  fetchOnSaleSKUsDetails(List skusList) {
    catalogSaleItemsList = <Product>[].obs;

    ///--------- API Calling to get SKU based product details one by one

    catalogItemsStatus.value = DataReadyStatus.FETCHING;

    skusList.forEach((p) async {
      String? getDataValue(List customAttributes, String keyToGet) {
        int keyIndex =
            customAttributes.indexWhere((f) => f['attribute_code'] == keyToGet);
        return customAttributes[keyIndex]['value'];
      }

      double checkDouble(dynamic value) {
        if (value is double) {
          return value;
        } else {
          return value.toDouble();
        }
      }

      //---- call sku detail api
      http.Response response = await sfAPIGetProductDetailsFromSKU(sku: p);
      Map responseBody = json.decode(response.body);
      cPrint(
          "==== API Response of index ${skusList.indexOf(p)} ::   ${response.body} ======");

      //---- add it to list
      catalogSaleItemsList.add(Product(
        id: responseBody['id'] ?? 159,
        sku: responseBody['sku'] ?? '202204200001641',
        name: responseBody['name'] ?? '',
        description: '',
        avgRating: responseBody['extension_attributes'] != null &&
                responseBody['extension_attributes']['avgrating'] != null
            ? responseBody['extension_attributes']['avgrating']
            : "0.0",
        price: checkDouble(responseBody['price']),
        reviewCount: responseBody['extension_attributes'] != null &&
                responseBody['extension_attributes']['review_count'] != null
            ? responseBody['extension_attributes']['review_count']
            : "0.0",
        faceSubArea: int.parse(
            getDataValue(responseBody['custom_attributes'], 'face_sub_area') ??
                '10'),
        image: getDataValue(responseBody['custom_attributes'], 'image') ?? '',
        tryOn: getDataValue(responseBody['custom_attributes'], 'try_on'),
        color:
            getDataValue(responseBody['custom_attributes'], 'face_color') ?? '',
        options: responseBody['options'] ?? [],
        discountedPrice:
            p['extension_attributes'].containsKey('discounted_price')
                ? double.parse(p['extension_attributes']['discounted_price'])
                : 0.0,
        rewardsPoint: responseBody['extension_attributes'] != null &&
                responseBody['extension_attributes']['reward_points'] != null
            ? responseBody['extension_attributes']['reward_points']
            : "0",
      ));
      if (skusList.length == catalogSaleItemsList.length) {
        cPrint(
            '========== data retrieved done :: length is  ${catalogSaleItemsList.length} =========');
        catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      }
    });
  }

  fetchOnSaleCampaignDetails(String campaignId) async {
    catalogSaleItemsList = <Product>[].obs;

    ///--------- API Calling to get SKU based product details one by one

    catalogItemsStatus.value = DataReadyStatus.FETCHING;

    String? getDataValue(List customAttributes, String keyToGet) {
      int keyIndex =
          customAttributes.indexWhere((f) => f['attribute_code'] == keyToGet);
      log("=== value at index 12 in list is ${customAttributes[keyIndex == -1 ? 12 : keyIndex]['value']} ===");
      return customAttributes[keyIndex == -1 ? 12 : keyIndex]['value'];
    }

    double checkDouble(dynamic value) {
      if (value is double) {
        return value;
      } else {
        return value.toDouble();
      }
    }

    ///------------------------
    List<dynamic> dealOfTheDayResponse =
        await getProductsByCampaignIdAPI(campaignId);

    dealOfTheDayResponse.forEach((p) {
      catalogSaleItemsList.add(Product(
        id: p['id'] ?? 159,
        sku: p['sku'] ?? '202204200001641',
        name: p['name'] ?? '',
        description: '',
        avgRating: p['extension_attributes'] != null &&
                p['extension_attributes']['avgrating'] != null
            ? p['extension_attributes']['avgrating']
            : "0.0",
        price: checkDouble(p['price']),
        reviewCount: p['extension_attributes'] != null &&
                p['extension_attributes']['review_count'] != null
            ? p['extension_attributes']['review_count']
            : "0.0",
        faceSubArea: int.parse(
            getDataValue(p['custom_attributes'], 'face_sub_area') ?? '10'),
        tryOn: getDataValue(p['custom_attributes'], 'try_on'),
        image: getDataValue(p['custom_attributes'], 'image') ?? '',
        color: getDataValue(p['custom_attributes'], 'face_color') ?? '',
        options: p['options'] ?? [],
        discountedPrice:
            p['extension_attributes'].containsKey('discounted_price')
                ? double.parse(p['extension_attributes']['discounted_price'])
                : 0.0,
        rewardsPoint: p['extension_attributes'] != null &&
                p['extension_attributes']['reward_points'] != null
            ? p['extension_attributes']['reward_points']
            : "0",
      ));
    });

    catalogItemsStatus.value = DataReadyStatus.COMPLETED;

    ///---------------------
  }

  fetchOnlyGiftCards() async {
    cPrint('===== check 2 print ====');

    catalogSaleItemsList = <Product>[].obs;

    ///--------- API Calling to get SKU based product details one by one

    catalogItemsStatus.value = DataReadyStatus.FETCHING;

    String? getDataValue(List customAttributes, String keyToGet) {
      int keyIndex =
          customAttributes.indexWhere((f) => f['attribute_code'] == keyToGet);
      return customAttributes[keyIndex]['value'];
    }

    double checkDouble(dynamic value) {
      if (value is double) {
        return value;
      } else {
        return value.toDouble();
      }
    }

    Map catalogUnfilteredItemsMap = await sfAPIGetCatalogGiftCardItems();
    if (!catalogUnfilteredItemsMap.containsKey('items')) {
      throw 'Server failed to send catalog list';
    }
    List catalogUnfilteredItemsTempList = catalogUnfilteredItemsMap['items'];

    catalogUnfilteredItemsTempList.forEach((p) {
      catalogSaleItemsList.add(Product(
        id: p['id'] ?? 24802,
        sku: p['sku'] ?? '202204200001641',
        name: p['name'] ?? '',
        description: '',
        avgRating: p['extension_attributes'] != null &&
                p['extension_attributes']['avgrating'] != null
            ? p['extension_attributes']['avgrating']
            : "0.0",
        price: checkDouble(p['price']),
        discountedPrice: p['discounted_price'] != null
            ? double.parse(p['discounted_price'].toString())
            : double.parse(p['price'].toString()),
        reviewCount: p['extension_attributes'] != null &&
                p['extension_attributes']['review_count'] != null
            ? p['extension_attributes']['review_count']
            : "0.0",
        faceSubArea: int.parse('10'),
        image: getDataValue(p['custom_attributes'], 'image') ?? '',
        tryOn: getDataValue(p['custom_attributes'], 'try_on'),
        options: p['options'] ?? [],
        rewardsPoint: p['extension_attributes'] != null &&
                p['extension_attributes']['reward_points'] != null
            ? p['extension_attributes']['reward_points']
            : "0",
      ));
    });

    cPrint(
        '=========== length of this sale products is :: ${catalogSaleItemsList.length} ======');

    catalogItemsStatus.value = DataReadyStatus.COMPLETED;
  }

  fetchAllSaleProducts() async {
    catalogSaleItemsList = <Product>[].obs;

    ///--------- API Calling to get SKU based product details one by one

    catalogItemsStatus.value = DataReadyStatus.FETCHING;

    String? getDataValue(List customAttributes, String keyToGet) {
      int keyIndex =
          customAttributes.indexWhere((f) => f['attribute_code'] == keyToGet);
      return keyIndex == -1 ? null : customAttributes[keyIndex]['value'];
    }

    double checkDouble(dynamic value) {
      if (value is double) {
        return value;
      } else {
        return value.toDouble();
      }
    }

    Map catalogUnfilteredItemsMap = await sfAPIGetCatalogAllSaleItems();
    if (!catalogUnfilteredItemsMap.containsKey('items')) {
      throw 'Server failed to send catalog list';
    }
    List catalogUnfilteredItemsTempList = catalogUnfilteredItemsMap['items'];

    for (final p in catalogUnfilteredItemsTempList) {
      catalogSaleItemsList.add(Product(
        id: p['id'] ?? 24802,
        sku: p['sku'] ?? '202204200001641',
        name: p['name'] ?? '',
        description: '',
        avgRating: p['extension_attributes'] != null &&
                p['extension_attributes']['avgrating'] != null
            ? p['extension_attributes']['avgrating']
            : "0.0",
        price: checkDouble(p['price']),
        reviewCount: p['extension_attributes'] != null &&
                p['extension_attributes']['review_count'] != null
            ? p['extension_attributes']['review_count']
            : "0.0",
        color: getDataValue(p['custom_attributes'], 'face_color') ?? '',
        faceSubArea: int.parse(
            getDataValue(p['custom_attributes'], 'face_sub_area') ?? '10'),
        discountedPrice:
            p['extension_attributes'].containsKey('discounted_price')
                ? double.parse(p['extension_attributes']['discounted_price'])
                : 0.0,
        image: getDataValue(p['custom_attributes'], 'image') ?? '',
        tryOn: getDataValue(p['custom_attributes'], 'try_on'),
        options: p['options'] ?? [],
        rewardsPoint: p['extension_attributes'] != null &&
                p['extension_attributes']['reward_points'] != null
            ? p['extension_attributes']['reward_points']
            : "0",
      ));
    }

    cPrint(
        '=========== length of this sale products is :: ${catalogSaleItemsList.length} ======');

    catalogItemsStatus.value = DataReadyStatus.COMPLETED;
  }

  setColors() {
    availableSkinTOnes = [];
    availableEyeColor = [];
    availableHairColor = [];

    cPrint("Inside Question Cntroller");

    cPrint(questionsController.skinToneQuestion?.questions.toString());
    var naswers = questionsController.skinToneQuestion?.answers;
    (naswers as Map<String, dynamic>).forEach((key2, value2) {
      // cPrint(key2);
      // cPrint(value2);
      availableSkinTOnes.add(value2);
    });

    var eyeColor = questionsController.eyeQuestion?.answers;
    (eyeColor as Map<String, dynamic>).forEach((key2, value2) {
      // cPrint(key2);
      // cPrint(value2);
      availableEyeColor.add(value2);
    });

    var hairColor = questionsController.hairQuestion?.answers;
    (hairColor as Map<String, dynamic>).forEach((key2, value2) {
      // cPrint(key2);
      // cPrint(value2);
      availableHairColor.add(value2);
    });

    cPrint(availableSkinTOnes.length);
  }

  void playSound() {
    if (isplaying.isTrue) {
      AudioPlayer().play(AssetSource('audio/onclick_sound.mp3'));
    }
  }

  Future<void> fetchCategory() async {
    try {
      CategoryResponse responseList = await sfAPIFetchFaceCategory();
      log('=== response from catFilter :: ${responseList.childrenData!.length} ===');
      if (responseList.childrenData != null &&
          responseList.childrenData!.isNotEmpty &&
          responseList.childrenData!.firstWhere(
                  (element) =>
                      element.name != null &&
                      element.name!.toLowerCase() == "shop", orElse: () {
                return ChildrenData();
              }).name ==
              null) {
        throw 'Proper key not found in response';
      }
      List<ChildrenData> faceSubAreaAndParameter = responseList.childrenData!
          .firstWhere(
              (element) =>
                  element.name != null &&
                  element.name!.toLowerCase() == "shop" &&
                  element.childrenData != null,
              orElse: () => ChildrenData())
          .childrenData!;
      categoryFilter.value = responseList.childrenData!.firstWhere(
          (element) =>
              element.name != null &&
              element.name!.toLowerCase() == "shop" &&
              element.childrenData != null,
          orElse: () => ChildrenData());
      faceSubAreaAndParameter.forEach((element) {
        if (element.name != null &&
            element.name!.toLowerCase() == "complexion") {
          allProductFilter.value.subAreas.addAll(element.childrenData!);
          cheeksProductFilter.value.subAreas.addAll(element.childrenData!);
        } else if (element.name != null &&
            element.name!.toLowerCase() == "eyes") {
          allProductFilter.value.subAreas.addAll(element.childrenData!);
          eyesProductFilter.value.subAreas.addAll(element.childrenData!);
        } else if (element.name != null &&
            element.name!.toLowerCase() == "lips") {
          allProductFilter.value.subAreas.addAll(element.childrenData!);
          lipsProductFilter.value.subAreas.addAll(element.childrenData!);
        }
      });
    } catch (err) {
      cPrint('Could not get face areas and parameters: $err');
    }
  }

  // Methods

  void unhideSeachBar() {
    showSearchBar.value = true;
  }

  void hideSearchBar() {
    showSearchBar.value = false;
  }

  void setFaceArea(FaceArea fa) {
    faceArea.value = fa;
    filterTypePressed.value = FilterType.NONE;
    catalogItemsCurrentPage.value = 0;
    selectedStar.value = 10;
    starValueChanges.value = false;
    setFilter(FilterType.NONE, newFilter: true);
  }

  void setFilter(FilterType ft, {bool newFilter = false}) async {
    if (!newFilter) {
      if (filterTypePressed.value != ft) {
        filterTypePressed.value = ft;
        cPrint("filter type is === ${filterTypePressed.value} ====");
        cPrint("object;;;;;;;;======");
      } else {
        filterTypePressed.value = FilterType.NONE;
      }
      // added by kruti to remove unwanted faceAreas statically
      cPrint("filter type pressed ::${filterTypePressed.value}");
      if (filterTypePressed.value == FilterType.SKINTONE) {
        if (faceArea.value == FaceArea.EYES) {
          BrandFilter stf = BrandFilter();
          stf = eyesBrandFilter.value;
          if (stf.subAreas.length > 0) {
            if (!stf.subAreas.contains("Eyelid")) {
              stf.subAreas.add("Eyelid");
            }
            if (!stf.subAreas.contains("Eyesocket")) {
              stf.subAreas.add("Eyesocket");
            }
            if (!stf.subAreas.contains("Orbitalbone")) {
              stf.subAreas.add("Orbitalbone");
            }
            stf.subAreas =
                stf.subAreas.where((item) => item != "Eyeshadow").toList();
            stf.subAreas =
                stf.subAreas.where((item) => item != "Eye Shadow").toList();
            stf.subAreas =
                stf.subAreas.where((item) => item != "Eyeliners").toList();
            stf.subAreas =
                stf.subAreas.where((item) => item != "Brows").toList();
            eyesBrandFilter.value = stf;
          }
        } else if (faceArea.value == FaceArea.LIPS) {
          BrandFilter stf = BrandFilter();
          stf = lipsBrandFilter.value;
          if (stf.subAreas.length > 0) {
            stf.subAreas =
                stf.subAreas.where((item) => item != "Shop All Lips").toList();
          }
          lipsBrandFilter.value = stf;
        }
      }
      if (filterTypePressed.value == FilterType.BRAND) {
        if (faceArea.value == FaceArea.EYES) {
          try {
            BrandFilter stf = BrandFilter();
            stf = eyesBrandFilter.value;
            if (stf.subAreas.length > 0) {
              stf.subAreas =
                  stf.subAreas.where((item) => item != "Eyelid").toList();
              stf.subAreas =
                  stf.subAreas.where((item) => item != "Eyesocket").toList();
              stf.subAreas =
                  stf.subAreas.where((item) => item != "Orbitalbone").toList();
              stf.subAreas =
                  stf.subAreas.where((item) => item != "Orbitalbone").toList();
              stf.subAreas =
                  stf.subAreas.where((item) => item != "Eyeliners").toList();
              if (!stf.subAreas.contains("Eye Shadow")) {
                stf.subAreas.add("Eye Shadow");
              }
              eyesBrandFilter.value = stf;
            }
          } catch (e) {
            cPrint(e.toString());
          }
        }
      }
      if (filterTypePressed.value == FilterType.PRODUCT) {
        if (faceArea.value == FaceArea.EYES) {
          try {
            ProductFilter stf = ProductFilter();
            stf = eyesProductFilter.value;
            if (stf.subAreas.length > 0) {
              stf.subAreas = stf.subAreas
                  .where((item) => item.name != "Eyeshadow")
                  .toList();
            }
            eyesProductFilter.value = stf;
          } catch (e) {
            cPrint(e.toString());
          }
        }
      }
      eyesBrandFilter.refresh();
    }
    if ((ft == filterType.value) && !newFilter) {
      return;
    }
    if (ft != filterType.value) {
      filterType.value = ft;
    }
    // bool isChanged = false;
    switch (ft) {
      /*case FilterType.SKINTONE:
        SkinToneFilter stf = SkinToneFilter();
        if (faceArea.value == FaceArea.EYES) {
          stf = eyesSkinToneFilter.value;
        } else if (faceArea.value == FaceArea.CHEEKS) {
          stf = cheeksSkinToneFilter.value;
        } else if (faceArea.value == FaceArea.LIPS) {
          stf = lipsSkinToneFilter.value;
        }
        if (stf.changed) {
          catalogItemsList.removeWhere((element) => true);
          catalogItemsCurrentPage.value = 0;
          stf.readFilter();
          catalogItemsStatus.value = DataReadyStatus.FETCHING;
          await fetchCentralColorProducts(stf);
        }
        break;*/

      case FilterType.SKINTONE:
        BrandFilter stf = BrandFilter();
        if (faceArea.value == FaceArea.EYES) {
          stf = eyesBrandFilter.value;
          if (stf._subArea != "") {
            if (isProfileSearchEyes.value == true) {
              await fetchSKinToneProfileFilter(
                  stf._subArea, isProfileSearchEyes.value);
            } else if (selectHairColor.value != "" &&
                selectEyesColor.value != "") {
              catalogItemsList.removeWhere((element) => true);
              catalogItemsCurrentPage.value = 0;
              catalogItemsStatus.value = DataReadyStatus.FETCHING;
              String lipColor = "#000000";
              String cheekColor = "#ffccaa";
              await fetchSKinToneFilter(stf._subArea, selectEyesColor.value,
                  selectHairColor.value, lipColor, cheekColor);
            } else {
              if (newFilter) {
                Get.showSnackbar(
                  GetSnackBar(
                    message: 'Please select hair and eye colour',
                    duration: Duration(seconds: 2),
                  ),
                );
              }
            }
          } else {
            if (newFilter) {
              Get.showSnackbar(
                GetSnackBar(
                  message: 'Please select face area',
                  duration: Duration(seconds: 2),
                ),
              );
            }
          }
        } else if (faceArea.value == FaceArea.CHEEKS) {
          stf = cheeksBrandFilter.value;

          if (stf._subArea != "") {
            log('==== this is running ====');
            if (isProfileSearchCheek.value == true) {
              await fetchSKinToneProfileFilter(
                  stf._subArea, isProfileSearchCheek.value);
            } else {
              // else if (selectCHeekShadeID.value != "") {
              catalogItemsList.removeWhere((element) => true);
              catalogItemsCurrentPage.value = 0;
              if (stf._subArea == 'Concealer' ||
                  stf._subArea == "Foundation" ||
                  stf._subArea == "BB and CC Creams" ||
                  stf._subArea == "Setting Sprays & Powders" ||
                  stf._subArea == "Highlighter" ||
                  stf._subArea == "Blusher" ||
                  stf._subArea == "Bronzer" ||
                  stf._subArea == "Contouring" ||
                  stf._subArea == "Primers & Enhancers") {
                catalogItemsStatus.value = DataReadyStatus.FETCHING;
                await fetchCentralColorProductsFunction(
                    selectCHeekShadeID.value, stf._subArea);
              } else {
                catalogItemsList.removeWhere((element) => true);
                catalogItemsCurrentPage.value = 0;
                catalogItemsStatus.value = DataReadyStatus.FETCHING;
                String eyeColor = "";
                String hairColor = "";
                String lipColor = "";
                String cheekColor = selectCHeekShadeID.value;
                String faceSelectedAreaForCheek = stf._subArea;
                await fetchSKinToneFilter(faceSelectedAreaForCheek, eyeColor,
                    hairColor, lipColor, cheekColor);
              }
            }
            // else {
            //   if (newFilter) {
            //     Get.showSnackbar(
            //       GetSnackBar(
            //         message: 'Please select shade colour',
            //         duration: Duration(seconds: 2),
            //       ),
            //     );
            //   }
            // }
          } else {
            if (newFilter) {
              Get.showSnackbar(
                GetSnackBar(
                  message: 'Please select face area',
                  duration: Duration(seconds: 2),
                ),
              );
            }
          }
        } else if (faceArea.value == FaceArea.LIPS) {
          stf = lipsBrandFilter.value;
          if (stf._subArea != "") {
            if (isProfileSearchLips.value == true) {
              await fetchSKinToneProfileFilter(
                  stf._subArea, isProfileSearchLips.value);
            } else if (selectLipsShadeID.value != "") {
              catalogItemsList.removeWhere((element) => true);
              catalogItemsCurrentPage.value = 0;
              catalogItemsStatus.value = DataReadyStatus.FETCHING;
              String eyeColor = "pink";
              String hairColor = "brown";

              String cheekColor = "#ffccaa";
              await fetchSKinToneFilter(stf._subArea, eyeColor, hairColor,
                  selectLipsShadeID.value, cheekColor);
            } else {
              if (newFilter) {
                Get.showSnackbar(
                  GetSnackBar(
                    message: 'Please select shade colour',
                    duration: Duration(seconds: 2),
                  ),
                );
              }
            }
          } else {
            if (newFilter) {
              Get.showSnackbar(
                GetSnackBar(
                  message: 'Please select face area',
                  duration: Duration(seconds: 2),
                ),
              );
            }
          }
        }

        break;

      case FilterType.PRODUCT:
        ProductFilter pf = ProductFilter();
        if (faceArea.value == FaceArea.ALL) {
          pf = allProductFilter.value;
        } else if (faceArea.value == FaceArea.EYES) {
          pf = eyesProductFilter.value;
        } else if (faceArea.value == FaceArea.CHEEKS) {
          pf = cheeksProductFilter.value;
        } else if (faceArea.value == FaceArea.LIPS) {
          pf = lipsProductFilter.value;
        }
        if (pf.changed) {
          catalogItemsList.removeWhere((element) => true);
          catalogItemsCurrentPage.value = 0;
          pf.readFilter();
          catalogItemsStatus.value = DataReadyStatus.FETCHING;
          await fetchProductItems(pf);
        } else {
          if (newFilter) {
            Get.showSnackbar(
              GetSnackBar(
                message: 'Please select face area',
                duration: Duration(seconds: 2),
              ),
            );
          }
        }
        break;
      case FilterType.PRICE:
        if (true) {
          cPrint('==== running with 1 ===');
          // if (priceFilter.value.changed) {
          catalogItemsList.removeWhere((element) => true);
          catalogItemsCurrentPage.value = 0;
          priceFilter.value.readFilter();
          cPrint('==== running with 2 ===');
          catalogItemsStatus.value = DataReadyStatus.FETCHING;
          String selectedFaceArea = "";
          cPrint('==== running with 3 ===');
          if (faceArea.value == FaceArea.EYES) {
            selectedFaceArea = "eyes";
          } else if (faceArea.value == FaceArea.LIPS) {
            selectedFaceArea = "lips";
          } else if (faceArea.value == FaceArea.CHEEKS) {
            selectedFaceArea = "cheeks";
          }
          cPrint(
              '==== running 4 with selected face area :: $selectedFaceArea ===');
          await fetchBetweenPriceItems(selectedFaceArea);
        }
        break;
      case FilterType.BRAND:
        BrandFilter pf = BrandFilter();
        if (faceArea.value == FaceArea.ALL) {
          pf = allBrandFilter.value;
        } else if (faceArea.value == FaceArea.EYES) {
          pf = eyesBrandFilter.value;
        } else if (faceArea.value == FaceArea.CHEEKS) {
          pf = cheeksBrandFilter.value;
        } else if (faceArea.value == FaceArea.LIPS) {
          pf = lipsBrandFilter.value;
        }

        if (pf._subArea != "" && pf._brand != "") {
          if (pf.changed) {
            catalogItemsList.removeWhere((element) => true);
            catalogItemsCurrentPage.value = 0;
            pf.readFilter();
            catalogItemsStatus.value = DataReadyStatus.FETCHING;
            await fetchBrandItems(pf);
          }
        } else {
          if (newFilter) {
            Get.showSnackbar(
              GetSnackBar(
                message: 'Please select face area and brand',
                duration: Duration(seconds: 2),
              ),
            );
          }
        }

        break;
      case FilterType.REVIEW:
        if (starValueChanges.value) {
          cPrint('aaaaaaaaaaa');
          catalogItemsList.removeWhere((element) => true);
          cPrint('bbbbbbb');
          catalogItemsCurrentPage.value = 0;
          cPrint("cccccc");
          catalogItemsStatus.value = DataReadyStatus.FETCHING;
          cPrint("dddddddddd");
          await fetchRatedItems();
          cPrint("fffffffffff");
        } else {
          if (newFilter) {
            Get.showSnackbar(
              GetSnackBar(
                message: 'Please select review',
                duration: Duration(seconds: 2),
              ),
            );
          }
        }
        break;
      case FilterType.POPULAR:
        catalogItemsList.removeWhere((element) => true);
        catalogItemsCurrentPage.value = 0;
        catalogItemsStatus.value = DataReadyStatus.FETCHING;
        await fetchPopularItems();
        break;
      case FilterType.NONE:
        catalogItemsList.removeWhere((element) => true);
        catalogItemsStatus.value = DataReadyStatus.FETCHING;
        if (faceArea.value == FaceArea.ALL) {
          await fetchUnfilteredItems(false);
        } else {
          await fetchUnfilteredFaceArea(
              faceAreaToIdMapping[faceArea.value] as int, false);
        }
        break;
    }
  }

  Future<bool> fetchMoreItems() async {
    bool success = false;

    switch (filterType.value) {
      case FilterType.SKINTONE:
        // SkinToneFilter stf = SkinToneFilter();
        // if (faceArea.value == FaceArea.EYES) {
        //   stf = eyesSkinToneFilter.value;
        // } else if (faceArea.value == FaceArea.CHEEKS) {
        //   stf = cheeksSkinToneFilter.value;
        // } else if (faceArea.value == FaceArea.LIPS) {
        //   stf = lipsSkinToneFilter.value;
        // }
        // success = await fetchCentralColorProducts(stf);
        ///
        ///
        /// All products are fetched in continious succession so no need to call this API again
        ///
        ///
        success = true;
        break;
      case FilterType.PRODUCT:
        ProductFilter pf = ProductFilter();
        if (faceArea.value == FaceArea.ALL) {
          pf = allProductFilter.value;
        } else if (faceArea.value == FaceArea.EYES) {
          pf = eyesProductFilter.value;
        } else if (faceArea.value == FaceArea.CHEEKS) {
          pf = cheeksProductFilter.value;
        } else if (faceArea.value == FaceArea.LIPS) {
          pf = lipsProductFilter.value;
        }
        success = await fetchProductItems(pf);
        break;
      case FilterType.PRICE:
        success = await fetchBetweenPriceItems("");
        break;
      case FilterType.BRAND:
        BrandFilter pf = BrandFilter();
        if (faceArea.value == FaceArea.ALL) {
          pf = allBrandFilter.value;
        } else if (faceArea.value == FaceArea.EYES) {
          pf = eyesBrandFilter.value;
        } else if (faceArea.value == FaceArea.CHEEKS) {
          pf = cheeksBrandFilter.value;
        } else if (faceArea.value == FaceArea.LIPS) {
          pf = lipsBrandFilter.value;
        }
        success = await fetchBrandItems(pf);
        break;
      case FilterType.REVIEW:
        success = true;
        break;
      case FilterType.POPULAR:
        success = await fetchPopularItems();
        break;
      case FilterType.NONE:
        switch (faceArea.value) {
          case FaceArea.ALL:
            success = await fetchUnfilteredItems(false);
            break;
          case FaceArea.EYES:
          case FaceArea.LIPS:
          case FaceArea.CHEEKS:
            success = await fetchUnfilteredFaceArea(
                faceAreaToIdMapping[faceArea.value] as int, false);
            break;
        }
        break;
    }
    return success;
  }

  void applyFilter() {
    filterTypePressed.value = FilterType.NONE;
    setFilter(filterType.value, newFilter: true);
  }

  void search() {
    filterType.value = FilterType.NONE;
    filterTypePressed.value = FilterType.NONE;
    selectedStar.value = 10;
    starValueChanges.value = false;
    hideSearchBar();
    fetchSearchedItems();
  }

  // SkinTone API

  Future<bool> fetchSKinToneFilter(String faceArea, String eyeColor,
      String hairColor, String lipcolor, String cheekColor) async {
    cPrint("CALLED CALLED");
    String token = await getUserToken();
    cPrint(token);
    try {
      catalogItemsCurrentPage.value++;

      Map result = await sfAPIGetSkinToneItems(
          faceArea, eyeColor, hairColor, lipcolor, cheekColor, token);
      cPrint("result retunrnrnrn");
      cPrint(result);
      if (!result.containsKey('items')) {
        throw 'Key not found: items';
      }
      List itemsList = result['items'];
      cPrint("${itemsList.length} djj");

      List<Product> tempProductList = <Product>[];
      itemsList.forEach(
        (item) {
          tempProductList.add(Product.fromDefaultMap(item));
        },
      );

      catalogItemsList.addAll(tempProductList);

      cPrint("Kamlesh");
      cPrint(catalogItemsList.length);
      cPrint(selectedStar.value);
      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      return true;
    } catch (err) {
      cPrint("Kamlesh----");
      cPrint(err);
      catalogItemsCurrentPage.value--;
      catalogItemsStatus.value = DataReadyStatus.ERROR;
      cPrint(err);
      try {
        exit(0);
        Get.showSnackbar(
          GetSnackBar(
            message: 'Could not load more products',
            duration: Duration(seconds: 2),
          ),
        );
      } catch (e) {
        rethrow;
      }
      return false;
    }
  }

  Future<bool> fetchSKinToneProfileFilter(
      String faceArea, bool isProfileSearch) async {
    try {
      catalogItemsStatus.value = DataReadyStatus.FETCHING;
      catalogItemsList.removeWhere((element) => true);

      catalogItemsCurrentPage.value++;

      Map result =
          await sfAPIGetSkinToneProfileItems(faceArea, isProfileSearch);

      if (!result.containsKey('items')) {
        throw 'Key not found: items';
      }
      List itemsList = result['items'];
      cPrint(itemsList.length);

      List<Product> tempProductList = <Product>[];
      itemsList.forEach(
        (item) {
          tempProductList.add(Product.fromDefaultMap(item));
        },
      );

      catalogItemsList.addAll(tempProductList);

      cPrint("Kamlesh");
      cPrint(catalogItemsList.length);
      cPrint(selectedStar.value);
      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      return true;
    } catch (err) {
      cPrint("Kamlesh");
      cPrint(err);
      catalogItemsCurrentPage.value--;
      catalogItemsStatus.value = DataReadyStatus.ERROR;
      cPrint(err);
      try {
        exit(0);
        Get.showSnackbar(
          GetSnackBar(
            message: 'Could not load more products',
            duration: Duration(seconds: 2),
          ),
        );
      } catch (e) {
        rethrow;
      }
      return false;
    }
  }

  // API calls
  Future<bool> fetchUnfilteredFaceArea(int faceArea, bool isForReview) async {
    try {
      catalogItemsCurrentPage.value++;
      Map result = await sfAPIGetUnfilteredFaceAreaItems(
          catalogItemsCurrentPage.value, faceArea);
      if (!result.containsKey('items')) {
        throw 'Key not found: items';
      }
      List itemsList = result['items'];
      print("itemsList:: ${itemsList}");
      List<Product> tempProductList = <Product>[];
      itemsList.forEach(
        (item) {
          tempProductList.add(Product.fromDefaultMap(item));
        },
      );
      cPrint("Resetting price after facearea");
      priceFilter.value.currentMinPrice = 0;
      priceFilter.value.currentMaxPrice = 200;

      catalogItemsList.addAll(tempProductList);

      if (isForReview) {
        if (selectedStar.value == 10) {
          // All
          catalogItemsList.sort((a, b) =>
              double.parse(a.avgRating).compareTo(double.parse(b.avgRating)));
        } else if (selectedStar.value == 0) {
          // 4+ rating
          try {
            catalogItemsList
                .removeWhere((element) => (element.avgRating).startsWith('3'));
            catalogItemsList
                .removeWhere((element) => (element.avgRating).startsWith('2'));
            catalogItemsList
                .removeWhere((element) => (element.avgRating).startsWith('1'));
            catalogItemsList
                .removeWhere((element) => (element.avgRating).startsWith('0'));
          } catch (ex) {
            cPrint(ex);
          }
        } else if (selectedStar.value == 1) {
          // 3+ rating
          catalogItemsList
              .removeWhere((element) => (element.avgRating).startsWith('2'));
          catalogItemsList
              .removeWhere((element) => (element.avgRating).startsWith('1'));
          catalogItemsList
              .removeWhere((element) => (element.avgRating).startsWith('0'));
        } else {
          // 2+ Rating
          catalogItemsList
              .removeWhere((element) => (element.avgRating).startsWith('1'));
          catalogItemsList
              .removeWhere((element) => (element.avgRating).startsWith('0'));
        }
      }

      cPrint("catalogItemsList---" + catalogItemsList.length.toString());

      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      return true;
    } catch (err) {
      cPrint("Kamlesh");
      cPrint(err);
      catalogItemsCurrentPage.value--;
      catalogItemsStatus.value = DataReadyStatus.ERROR;
      cPrint(err);
      try {
        exit(0);
        Get.showSnackbar(
          GetSnackBar(
            message: 'Could not load more products',
            duration: Duration(seconds: 2),
          ),
        );
      } catch (e) {
        rethrow;
      }
      return false;
    }
  }

  Future<bool> fetchUnfilteredItems(bool sortReviewBased) async {
    try {
      catalogItemsCurrentPage.value++;
      Map catalogUnfilteredItemsMap =
          await sfAPIGetCatalogUnfilteredItems(catalogItemsCurrentPage.value);
      if (!catalogUnfilteredItemsMap.containsKey('items')) {
        throw 'Server failed to send catalog list';
      }
      List catalogUnfilteredItemsTempList = catalogUnfilteredItemsMap['items'];
      // Added by kruti for sorting list of catalogItems

      catalogUnfilteredItemsTempList
          .sort((a, b) => a["name"].compareTo(b["name"]));
      List<Product> catalogUnfilteredItemsTempListOfProducts =
          catalogUnfilteredItemsTempList.map<Product>(
        (m) {
          return Product.fromDefaultMap(m);
        },
      ).toList();
      catalogItemsList.addAll(catalogUnfilteredItemsTempListOfProducts);
      if (sortReviewBased) {
        if (selectedStar.value == 10) {
          catalogItemsList.sort((a, b) =>
              double.parse(a.avgRating).compareTo(double.parse(b.avgRating)));
        } else if (selectedStar.value == 0) {
          // 4+ rating
          try {
            var temp = catalogItemsList;
            temp.removeWhere(
                (element) => getReviewList(element.avgRating, '4'));
            /*temp.removeWhere((element) => element.avgRating.startsWith("3"));
            temp.removeWhere((element) => element.avgRating.startsWith("2"));
            temp.removeWhere((element) => element.avgRating.startsWith("1"));
            temp.removeWhere((element) => element.avgRating.startsWith("0"));*/
            catalogItemsList.removeWhere((element) => true);
            catalogItemsList.addAll(temp);
          } catch (ex) {
            cPrint(ex);
          }
        } else if (selectedStar.value == 1) {
          // 3+ rating
          var temp = catalogItemsList;
          temp.removeWhere((element) => getReviewList(element.avgRating, '3'));
          /*temp.removeWhere((element) => element.avgRating.startsWith("2"));
          temp.removeWhere((element) => element.avgRating.startsWith("1"));
          temp.removeWhere((element) => element.avgRating.startsWith("0"));*/
          catalogItemsList.removeWhere((element) => true);
          catalogItemsList.addAll(temp);
        } else {
          // 2+ Rating
          var temp = catalogItemsList;
          temp.removeWhere((element) => getReviewList(element.avgRating, '2'));
          /*temp.removeWhere((element) => element.avgRating.startsWith("1"));
          temp.removeWhere((element) => element.avgRating.startsWith("0"));*/
          catalogItemsList.removeWhere((element) => true);
          catalogItemsList.addAll(temp);
        }
      }

      cPrint("TZS--Raing--" + catalogItemsList.length.toString());
      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      return true;
    } catch (err) {
      catalogItemsCurrentPage.value--;
      catalogItemsStatus.value = DataReadyStatus.ERROR;
      cPrint(err);
      try {
        exit(0);
        Get.showSnackbar(
          GetSnackBar(
            message: 'Could not load more products',
            duration: Duration(seconds: 2),
          ),
        );
      } catch (ee) {
        return false;
      }
      return false;
    }
  }

  /*
  * Function : fetchRatedItems
  * return : product list
  * description : this function will return products as per selected rate(star) filter
  * */

  Future<bool> fetchRatedItems() async {
    try {
      catalogItemsCurrentPage.value++;
      List catalogUnfilteredItemsTempList =
          await fetchItemsByReview(selectedStar.value);
      List<Product> catalogUnfilteredItemsTempListOfProducts =
          catalogUnfilteredItemsTempList.map<Product>(
        (m) {
          return Product(
            id: int.parse(m['product_id']),
            name: m['name'].toString(),
            sku: m['sku'],
            price: double.parse((m['original_price']).toString()),
            image: m['image'],
            description: m['description'],
            faceSubArea: -1,
            avgRating: m['extension_attributes']['avgrating'].toString(),
            reviewCount: m['extension_attributes']['review_count'].toString(),
            rewardsPoint: m['extension_attributes']['reward_points'].toString(),
            discountedPrice: m['discounted_price'] != null
                ? double.parse(m['discounted_price'].toString())
                : double.parse(m['original_price'].toString()),
          );
        },
      ).toList();
      catalogItemsList.addAll(catalogUnfilteredItemsTempListOfProducts);

      cPrint("TZS--Raing--" + catalogItemsList.length.toString());
      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      return true;
    } catch (err) {
      catalogItemsCurrentPage.value--;
      catalogItemsStatus.value = DataReadyStatus.ERROR;
      cPrint(err);
      try {
        exit(0);
        Get.showSnackbar(
          GetSnackBar(
            message: 'Could not load more products',
            duration: Duration(seconds: 2),
          ),
        );
      } catch (ee) {
        return false;
      }
      return false;
    }
  }

  bool getReviewList(String rating, String minRating) {
    cPrint("Average---" + rating);
    double doubleRating = double.parse(rating);
    double doubleMinRating = double.parse(minRating);
    if (doubleRating <= doubleMinRating) {
      return true;
    }
    return false;
  }

  Future<bool> fetchFaceAreasAndParameters(int faceArea) async {
    try {
      List responseList = await sfAPIFetchFaceAreasAndParameters(faceArea);
      Map responseMap = responseList[0];
      if (!responseMap.containsKey('face_sub_area')) {
        throw 'Proper key not found in response';
      }
      List faceSubAreaAndParameter = responseMap['face_sub_area'];

      if (faceArea == faceAreaToIdMapping[FaceArea.CHEEKS]) {
        cheeksSkinToneFilter.value.storeSubAreaOptions(faceSubAreaAndParameter);
      } else if (faceArea == faceAreaToIdMapping[FaceArea.LIPS]) {
        lipsSkinToneFilter.value.storeSubAreaOptions(faceSubAreaAndParameter);
      } else if (faceArea == faceAreaToIdMapping[FaceArea.EYES]) {
        eyesSkinToneFilter.value.storeSubAreaOptions(faceSubAreaAndParameter);
      }
      return true;
    } catch (err) {
      cPrint('Could not get face areas and parameters: $err');
      return false;
    }
  }

  Future<bool> fetchCentralColorProductsFunction(
      String color, String faceSubArea) async {
    try {
      cPrint("Foundation=======");
      catalogItemsCurrentPage.value++;

      /// Find the user token
      String token = await getUserToken();
      // int customer_id = await getCustomerId();

      /// Fetch products for each recommended colors
      /// Add product to catalogListItems List
      List result = await sfAPIFetchCentralColorProductsForFoundation(
        token,
        color,
        faceSubArea,
      );
      dynamic resultMap = result[0];

      if (!resultMap.containsKey('product')) {
        throw 'Key not found: product';
      }

      dynamic tempProductMap = resultMap['product'];
      List<Product> tempProductList = <Product>[];
      tempProductMap.forEach(
        (key, p) {
          tempProductList.add(Product.fromMap(p));
        },
      );
      catalogItemsList.addAll(tempProductList);
      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      // this.catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      return true;
    } catch (err) {
      catalogItemsStatus.value = DataReadyStatus.ERROR;
      catalogItemsCurrentPage.value--;
      cPrint('Error fetching parameterizedFaceSubAreaProducts: $err');
      try {
        exit(0);
        Get.showSnackbar(
          GetSnackBar(
            message: 'Could not load more products',
            duration: Duration(seconds: 2),
          ),
        );
      } catch (ee) {
        rethrow;
      }
      return false;
    }
  }

  Future<bool> fetchBrandNames(String faceArea) async {
    cPrint("Fetch Brand Names  --fetchBrandNames :: $faceArea");
    try {
      List result = await sfAPIGetBrandNames(faceArea);
      Map resultMap = result[0];
      if (!resultMap.containsKey('brand') ||
          !resultMap.containsKey('face_sub_area')) {
        throw 'Key not found: brand OR face_sub_area';
      }
      Map resultFaceSubArea = resultMap['face_sub_area'];
      Map resultBrand = resultMap['brand'];
      // brandFaceArea = resultMap['face_sub_area'];
      //  cPrint("brandFaceArea  fetchBrandNames:: ${brandFaceArea}");

      BrandFilter brandFilter = BrandFilter();

      resultFaceSubArea.forEach(
        (index, b) {
          if (b == "Highligther") {
          } else {
            brandFilter.subAreas.add(b);
            //print("subAreas ::${b}");
          }
        },
      );
      resultBrand.forEach(
        (index, b) {
          brandFilter.brands.add(b);
        },
      );

      if (faceArea == faceAreaToStringMapping[FaceArea.ALL]) {
        allBrandFilter.value = brandFilter;
      } else if (faceArea == faceAreaToStringMapping[FaceArea.EYES]) {
        eyesBrandFilter.value = brandFilter;
      } else if (faceArea == faceAreaToStringMapping[FaceArea.CHEEKS]) {
        cheeksBrandFilter.value = brandFilter;
      } else if (faceArea == faceAreaToStringMapping[FaceArea.LIPS]) {
        lipsBrandFilter.value = brandFilter;
      }
      return true;
    } catch (err) {
      cPrint('Error fetching brand names: $err');
      // This method called again and again...changed on 13-04-2022 by Ashwani
      //fetchBrandNames(faceArea);
      return false;
    }
  }

  Future<bool> fetchFilteredBrandNames(
      String faceArea, String faceSubArea) async {
    cPrint("Fetch Filtered Brand Names  --fetchBrandNames");
    try {
      List result = await sfAPIGetFilteredBrandNames(faceArea, faceSubArea);
      Map resultMap = result[0];
      if (!resultMap.containsKey('brand') ||
          !resultMap.containsKey('face_sub_area')) {
        throw 'Key not found: brand OR face_sub_area';
      }
      //  Map resultFaceSubArea = resultMap['face_sub_area'];
      Map resultBrand = resultMap['brand'];
      //  Map resultFaceSubArea = brandFaceArea;
      //   cPrint("brandFaceArea  fetchFilteredBrandNames:: ${brandFaceArea}");
      BrandFilter brandFilter = BrandFilter();
      if (faceArea == faceAreaToStringMapping[FaceArea.ALL]) {
        List brandFaceArea = allBrandFilter.value.subAreas;
        brandFaceArea.forEach(
          (b) {
            if (b == "Highligther") {
            } else {
              brandFilter.subAreas.add(b);
              if (faceSubArea == b) {
                brandFilter.subArea = b;
              }
            }
          },
        );
      } else if (faceArea == faceAreaToStringMapping[FaceArea.EYES]) {
        List brandFaceArea = eyesBrandFilter.value.subAreas;
        brandFaceArea.forEach(
          (b) {
            if (b == "Highligther") {
            } else {
              brandFilter.subAreas.add(b);
              if (faceSubArea == b) {
                brandFilter.subArea = b;
              }
            }
          },
        );
      } else if (faceArea == faceAreaToStringMapping[FaceArea.CHEEKS]) {
        List brandFaceArea = cheeksBrandFilter.value.subAreas;
        brandFaceArea.forEach(
          (b) {
            if (b == "Highligther") {
            } else {
              brandFilter.subAreas.add(b);
              if (faceSubArea == b) {
                brandFilter.subArea = b;
              }
            }
          },
        );
      } else if (faceArea == faceAreaToStringMapping[FaceArea.LIPS]) {
        List brandFaceArea = lipsBrandFilter.value.subAreas;
        brandFaceArea.forEach(
          (b) {
            if (b == "Highligther") {
            } else {
              brandFilter.subAreas.add(b);
              if (faceSubArea == b) {
                brandFilter.subArea = b;
              }
            }
          },
        );
      }

      resultBrand.forEach(
        (index, b) {
          brandFilter.brands.add(b);
        },
      );

      if (faceArea == faceAreaToStringMapping[FaceArea.ALL]) {
        allBrandFilter.value = brandFilter;
      } else if (faceArea == faceAreaToStringMapping[FaceArea.EYES]) {
        eyesBrandFilter.value = brandFilter;
      } else if (faceArea == faceAreaToStringMapping[FaceArea.CHEEKS]) {
        cheeksBrandFilter.value = brandFilter;
      } else if (faceArea == faceAreaToStringMapping[FaceArea.LIPS]) {
        lipsBrandFilter.value = brandFilter;
      }

      return true;
    } catch (err) {
      cPrint('Error fetching brand names: $err');
      // This method called again and again...changed on 13-04-2022 by Ashwani
      //fetchBrandNames(faceArea);
      return false;
    }
  }

  Future<bool> fetchBrandItems(BrandFilter bf) async {
    try {
      catalogItemsCurrentPage.value++;

      List result = await sfAPIFetchBrandFilteredItems(
          catalogItemsCurrentPage.value, bf.brand, bf.subArea);
      dynamic resultMap = result[0];
      if (!resultMap.containsKey('products')) {
        throw 'Key not found: products';
      }

      if (resultMap['products'] is List) {
        List<dynamic> tempProductsMap = resultMap['products'];
        List<Product> tempProductList = <Product>[];

        tempProductsMap.forEach(
          (p) {
            tempProductList.add(Product.fromMap(p));
          },
        );
        catalogItemsList.addAll(tempProductList);
      } else {
        Map tempProductsMap = resultMap['products'];
        List<Product> tempProductList = <Product>[];

        tempProductsMap.forEach(
          (k, p) {
            tempProductList.add(Product.fromMap(p));
          },
        );
        catalogItemsList.addAll(tempProductList);
      }

      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      return true;
    } catch (err) {
      catalogItemsCurrentPage.value--;
      catalogItemsStatus.value = DataReadyStatus.ERROR;
      cPrint('Error fetching brand filtered items: $err');
      Get.showSnackbar(
        GetSnackBar(
          message: 'Could not fetch products',
          duration: Duration(seconds: 2),
        ),
      );
      return false;
    }
  }

  Future<bool> fetchProductItems(ProductFilter pf) async {
    try {
      catalogItemsCurrentPage.value++;
      Map result = await sfAPIFetchProductItems(
          catalogItemsCurrentPage.value, pf.subArea.id!);
      if (!result.containsKey('items')) {
        throw 'Key not found: items';
      }
      List resultList = result['items'];
      List<Product> tempProductList = <Product>[];

      resultList.forEach(
        (p) {
          tempProductList.add(Product.fromDefaultMap(p));
        },
      );

      catalogItemsList.addAll(tempProductList);
      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      return true;
    } catch (err) {
      catalogItemsCurrentPage.value--;
      catalogItemsStatus.value = DataReadyStatus.ERROR;
      cPrint('Error fetching product filtered items: $err');
      Get.showSnackbar(
        GetSnackBar(
          message: 'Could not fetch products for this filter',
          duration: Duration(seconds: 2),
        ),
      );
      return false;
    }
  }

  Future<bool> fetchPopularItems() async {
    try {
      catalogItemsCurrentPage.value++;

      if (faceArea.value == FaceArea.ALL) {
        Map<String, dynamic> bestSellerResponse = await sfAPIGetBestSellers();
        cPrint("Reture result .........");

        // var responseList = bestSellerResponse["bestseller_product"];
        var responseList = bestSellerResponse["product"];

        List<Product> tempProductList = <Product>[];

        //added by kruti to sort popular items
        // responseList.sort((a, b) => a["name"].compareTo(b["name"]));
        //---facing issue here

        cPrint("Best Seller Items -->> ${responseList[0]["name"]}");
        responseList.forEach((p) {
          // product was not added to the list that's why it was not showing to list of popular products
          tempProductList.add(Product(
            id: int.parse(p['product_id']),
            name: p['name'],
            sku: p['sku'],
            // price: double.parse(p['price']),
            price: double.parse(p['original_price']),
            image: p['image'],
            faceSubArea: -1,
            description: "",
            hasOption: true,

            avgRating: p['extension_attributes'] != null &&
                    p['extension_attributes']['avgrating'] != null
                ? p['extension_attributes']['avgrating']
                : "0.0",
            rewardsPoint: p['reward_points'] != null
                ? double.parse((p['reward_points'] ?? "0").toString())
                    .toStringAsFixed(0)
                : "0.0",
          ));
        });

        catalogItemsList.addAll(tempProductList);
      } else {
        cPrint(' === this one is being run ===.........');
        // API 168
        List responseList =
            await sfAPIGetCatalogPopularItems(catalogItemsCurrentPage.value);
        dynamic responseMap = responseList[0];
        cPrint("response from popular item API is $responseList");
        cPrint("responseMap[0]");
        cPrint(responseMap);
        if (!responseMap.containsKey('products')) {
          cPrint('abcd');
          throw 'Products not found in response';
        }

        if (responseMap['products'] is List) {
          cPrint('abc');
          List<dynamic> tempProductsMap = responseMap['products'];
          List<Product> tempProductList = <Product>[];
          cPrint(' === running 2 ===');
          tempProductsMap.forEach(
            (value) {
              if (value['face_area'] == null ||
                  faceArea.value == FaceArea.ALL) {
                cPrint(' === running 3 ===');
                tempProductList.add(
                  Product(
                      id: int.parse(value['entity_id']),
                      sku: value['sku'],
                      image: value['image'] ??
                          value['small_image'] ??
                          value['thumbnail'] ??
                          "",
                      description:
                          value['short_description'] ?? value['description'],
                      faceSubArea: int.parse(value['face_sub_area']),
                      name: value['name'],
                      price: double.parse(value['price']),
                      options: [],
                      avgRating: value['extension_attributes'] != null &&
                              value['extension_attributes']['avgrating'] != null
                          ? value['extension_attributes']['avgrating']
                          : "0.0",
                      hasOption:
                          value['required_options'] == "1" ? true : false),
                );
              } else {
                cPrint(' === running 4 ===');
                var id = faceAreaToIdMapping[faceArea.value] as int;
                if (value['face_area'].toString() == id.toString()) {
                  tempProductList.add(
                    Product(
                        id: int.parse(value['entity_id']),
                        sku: value['sku'],
                        avgRating: value['extension_attributes'] != null &&
                                value['extension_attributes']['avgrating'] !=
                                    null
                            ? value['extension_attributes']['avgrating']
                            : "0.0",
                        image: value['image'] ??
                            value['small_image'] ??
                            value['thumbnail'] ??
                            "",
                        description:
                            value['short_description'] ?? value['description'],
                        faceSubArea: int.parse(value['face_sub_area']),
                        name: value['name'],
                        price: double.parse(value['price']),
                        options: [],
                        hasOption:
                            value['required_options'] == "1" ? true : false),
                  );
                }
              }
            },
          );
          catalogItemsList.addAll(tempProductList);
          cPrint(' === running 5 ===');
        } else {
          cPrint("humayun");
          Map tempProductsMap = responseMap['products'];
          List<Product> tempProductList = <Product>[];
          tempProductsMap.forEach(
            (key, value) {
              if (value['face_area'] == null ||
                  faceArea.value == FaceArea.ALL) {
                tempProductList.add(
                  Product(
                      id: int.parse(value['entity_id']),
                      sku: value['sku'],
                      image: value['image'] ??
                          value['small_image'] ??
                          value['thumbnail'] ??
                          "",
                      description:
                          value['short_description'] ?? value['description'],
                      faceSubArea: int.parse(value['face_sub_area']),
                      name: value['name'],
                      price: double.parse(value['price']),
                      options: [],
                      avgRating: value['extension_attributes'] != null &&
                              value['extension_attributes']['avgrating'] != null
                          ? value['extension_attributes']['avgrating']
                          : "0.0",
                      hasOption:
                          value['required_options'] == "1" ? true : false),
                );
                cPrint(' === running 8 ===');
              } else {
                // Commented for testing purposes
                // var id = faceAreaToIdMapping[faceArea.value] as int;
                var string =
                    faceAreaToStringMappingforPopularItems[faceArea.value];
                // Commented for testing purposes
                // var faceAreaString = faceArea.value;
                // cPrint('faceAreaString');
                // cPrint(faceAreaString);
                cPrint(' === running 9 ===');
                cPrint(
                    value['face_area'].toString() + "==" + string.toString());
                // cPrint(
                //     '=====  matching >  ${value['face_area']} :: with :: ${id.toString()} :: where actual is ${faceArea.value} =====');
                if (value['face_area'].toString() == string.toString()) {
                  tempProductList.add(
                    Product(
                        id: int.parse(value['entity_id']),
                        sku: value['sku'],
                        image: value['image'] ??
                            value['small_image'] ??
                            value['thumbnail'] ??
                            "",
                        description:
                            value['short_description'] ?? value['description'],
                        // faceSubArea: int.parse(value['face_sub_area']),
                        faceSubArea: 0,
                        name: value['name'],
                        avgRating: value['extension_attributes'] != null &&
                                value['extension_attributes']['avgrating'] !=
                                    null
                            ? value['extension_attributes']['avgrating']
                            : "0.0",
                        price: double.parse(value['price']),
                        options: [],
                        rewardsPoint: value['reward_points'] != null
                            ? double.parse(
                                    (value['reward_points'] ?? "0").toString())
                                .toStringAsFixed(0)
                            : "0.0",
                        hasOption:
                            value['required_options'] == "1" ? true : false),
                  );
                }
              }
            },
          );

          cPrint(tempProductList.length);
          catalogItemsList.addAll(tempProductList);
          cPrint(' === items length is ${catalogItemsList.length} ===');
        }
      }
      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      return true;
    } catch (err) {
      catalogItemsCurrentPage.value--;
      catalogItemsStatus.value = DataReadyStatus.ERROR;
      cPrint(err);
      try {
        exit(0);
        Get.showSnackbar(
          GetSnackBar(
            message: 'Could not load more products',
            duration: Duration(seconds: 2),
          ),
        );
      } catch (ee) {
        return false;
      }
      return false;
    }
  }

  Future<bool> fetchBetweenPriceItems(String selectedFaceArea) async {
    try {
      catalogItemsCurrentPage.value++;
      List response = await sfAPIGetCatalogBetweenPriceItems(
          catalogItemsCurrentPage.value,
          priceFilter.value.currentMinPrice,
          priceFilter.value.currentMaxPrice,
          selectedFaceArea);
      cPrint('==== running with 5 :: $response ===');
      Map responseMap = response[0];
      if (!responseMap.containsKey('products')) {
        throw 'Products not found in response';
      }
      cPrint('==== running with 6 ===');

      Map productMap = responseMap['products'];

      //faceAreaToIdMapping[faceArea.value] as int
      List<Product> tempProductList = <Product>[];

      productMap.forEach(
        (key, value) {
          // if (value['face_area'] == null || faceArea.value == FaceArea.ALL) {
          tempProductList.add(
            Product(
              id: int.parse(value['entity_id']),
              sku: value['sku'] ?? '',
              image: value['image'] ?? '',
              description: value['description'] ?? '',
              faceSubArea: value['face_area'] == 'Cheeks'
                  ? faceAreaToIdMapping[FaceArea.CHEEKS]!
                  : value['face_area'] == 'Lips'
                      ? faceAreaToIdMapping[FaceArea.LIPS]!
                      : faceAreaToIdMapping[FaceArea.EYES]!,
              // faceSubArea: 280,
              // faceSubArea: int.parse(value['face_area']),
              name: value['name'] ?? '',
              avgRating: value['extension_attributes'] != null &&
                      value['extension_attributes']['avgrating'] != null
                  ? value['extension_attributes']['avgrating']
                  : "0.0",
              price: double.parse(value['price']),
              rewardsPoint: value['reward_points'] != null
                  // ? "111"
                  ? (value['reward_points']).toStringAsFixed(0)
                  : "0.0",
              options: [],
              hasOption: value['required_options'] == "1" ? true : false,
            ),
          );

          ///-----old one - already commented
          //     /*} else {
          //         tempProductList.add(
          //           Product(
          //               id: int.parse(value['entity_id']),
          //               sku: value['sku'],
          //               image: value['image'],
          //               avgRating: value['extension_attributes'] != null &&
          //                       value['extension_attributes']['avgrating'] != null
          //                   ? value['extension_attributes']['avgrating']
          //                   : "0.0",
          //               description: value['description'] ?? '',
          //               faceSubArea: int.parse(value['face_sub_area']),
          //               name: value['name'],
          //               price: double.parse(value['price']),
          //               options: [],
          //               hasOption: value['required_options'] == "1" ? true : false),
          //         );
          //
          //     }*/
        },
      );

      cPrint('==== running with 8 ===');

      catalogItemsList.addAll(tempProductList);
      cPrint(
          '==== running with 9 :: length of itemList ${catalogItemsList.length} ===');
      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      return true;
    } catch (err) {
      cPrint('==== running error 1 ===');

      catalogItemsCurrentPage.value--;
      catalogItemsStatus.value = DataReadyStatus.ERROR;
      cPrint('Could not load items between price range $err');
      cPrint('==== running error 2 ===');

      try {
        exit(0);
        Get.showSnackbar(
          GetSnackBar(
            message: 'Could not load more products',
            duration: Duration(seconds: 2),
          ),
        );
      } catch (ee) {
        cPrint('==== running sub error 1 :: $ee ===');
      }
      return false;
    }
  }

  Future<bool> fetchSearchedItems() async {
    try {
      catalogItemsStatus.value = DataReadyStatus.FETCHING;
      catalogItemsList.removeWhere((element) => true);
      Map response = await sfAPIGetSearchedItems(inputText.value);
      cPrint("SEARCH RESPONSE $response");
      List responseList = response['items'] ?? [];
      if (responseList.isEmpty) {
        catalogItemsStatus.value = DataReadyStatus.COMPLETED;
        return false;
      }
      List<Product> tempProductList = <Product>[];
      responseList.forEach(
        (m) {
          tempProductList.add(
            Product.fromDefaultMap(m),
          );
        },
      );

      catalogItemsList.addAll(tempProductList);
      catalogItemsStatus.value = DataReadyStatus.COMPLETED;
      return true;
    } catch (err) {
      catalogItemsStatus.value = DataReadyStatus.ERROR;
      cPrint('Could not find user searched items: $err');
      Get.showSnackbar(
        GetSnackBar(
          message: 'No results found!',
          duration: Duration(seconds: 2),
        ),
      );
      return false;
    }
  }

  // Defaults
  void defaults() {
    showSearchBar = false.obs;
    catalogItemsList = <Product>[].obs;
    catalogItemsCurrentPage = 0.obs;
    catalogItemsStatus = DataReadyStatus.INACTIVE.obs;
    filterType = FilterType.NONE.obs;
    filterTypePressed = FilterType.NONE.obs;
    faceArea = FaceArea.ALL.obs;
  }

  void softDefaults() {
    showSearchBar = false.obs;
    catalogItemsCurrentPage = 0.obs;
    catalogItemsStatus = DataReadyStatus.INACTIVE.obs;
    filterType = FilterType.NONE.obs;
    filterTypePressed = FilterType.NONE.obs;
    faceArea = FaceArea.ALL.obs;
  }

  // FUNCTION: Loader dialog
  // ARGUMENTS: set loader with color and barrier
  // RETURNS: Alert dialog with loader
  // NOTES: show loader dialog

  BuildContext? dcontext;
  showLoaderDialog(BuildContext context) {
    final alert = SpinKitDoubleBounce(
      color: Colors.white,
      // color: Color(0xffF2CA8A),
      size: 50.0,
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        dcontext = context;
        return alert;
      },
    );
  }

  dimissLoaderDialog() {
    Navigator.pop(dcontext!, true);
  }
}

Map<FaceArea, String> faceAreaToStringMappingforPopularItems = {
  FaceArea.EYES: 'Eyes',
  FaceArea.LIPS: 'Lips',
  FaceArea.CHEEKS: 'Cheeks',
  FaceArea.ALL: '',
};

Map<FaceArea, String> faceAreaToStringMapping = {
  FaceArea.EYES: 'Eyes',
  FaceArea.LIPS: 'Lips',
  FaceArea.CHEEKS: 'Complexion',
  FaceArea.ALL: '',
};

Map<String, int> faceAreaToStandardId = {
  'foundation': 1,
  'bronzer': 2,
  'highligther': 3,
  'blusher': 4,
  'consealer': 12,
  'eyelid': 5,
  'eyesocket': 6,
  'orbital-bone': 7,
  'eyeliner': 8,
  'eyebrow': 9,
  'lipliner': 10,
  'lipstick': 11,
};

enum FilterType {
  SKINTONE,
  PRODUCT,
  PRICE,
  BRAND,
  REVIEW,
  POPULAR,
  NONE,
}

enum FaceArea {
  ALL,
  EYES,
  LIPS,
  CHEEKS,
}

class PriceFilter {
  int minPrice;
  int maxPrice;
  int _currentMinPrice = 0;
  int _currentMaxPrice = 200;
  bool changed = false;

  PriceFilter({
    this.minPrice = 0,
    this.maxPrice = 200,
  });

  void readFilter() {
    changed = false;
  }

  int get currentMinPrice {
    return _currentMinPrice;
  }

  int get currentMaxPrice {
    return _currentMaxPrice;
  }

  set currentMinPrice(int cmp) {
    _currentMinPrice = cmp;
    changed = true;
  }

  set currentMaxPrice(int cmp) {
    _currentMaxPrice = cmp;
    changed = true;
  }
}

class BrandFilter {
  List<String> brands = <String>[];
  List<String> subAreas = <String>[];

  bool changed = false;

  late String _brand = '';
  late String _subArea = '';

  BrandFilter();

  set brand(String b) {
    _brand = b;
    changed = true;
  }

  set subArea(String s) {
    _subArea = s;
    changed = true;
  }

  String get brand {
    return _brand;
  }

  String get subArea {
    return _subArea;
  }

  void readFilter() {
    changed = false;
  }
}

class ProductFilter {
  List<ChildrenData> subAreas = <ChildrenData>[];

  bool changed = false;

  ChildrenData _subArea = ChildrenData();

  ProductFilter();

  set subArea(ChildrenData sa) {
    _subArea = sa;
    changed = true;
  }

  ChildrenData get subArea {
    return _subArea;
  }

  void readFilter() {
    changed = false;
  }
}

class SkinToneFilter {
  List<String> faceSubAreas = <String>[];
  Map<String, List<SkinToneFilterParameters>> options =
      <String, List<SkinToneFilterParameters>>{};

  String _selectedFaceSubArea = '';

  bool changed = false;

  SkinToneFilter();

  String get selectedFaceSubArea {
    return _selectedFaceSubArea;
  }

  void toggleFaceSubArea(String fsa) {
    if (selectedFaceSubArea.contains(fsa)) {
      unselectFaceSubArea(fsa);
    } else {
      selectFaceSubArea(fsa);
    }
  }

  void selectFaceSubArea(String fsa) {
    _selectedFaceSubArea = fsa;
    changed = true;
  }

  void unselectFaceSubArea(String fsa) {
    _selectedFaceSubArea = '';
  }

  void readFilter() {
    changed = false;
  }

  void storeSubAreaOptions(List areaOptionsList) {
    areaOptionsList.forEach(
      (subArea) {
        String subAreaName = subArea['sub_area_name'];
        List parameters = subArea['parameters'];
        options[subAreaName] = [];
        List<String> uniqueParameterNames = <String>[];
        for (final p in parameters) {
          if (!uniqueParameterNames.contains(p['parameter_name'])) {
            uniqueParameterNames.add(p['parameter_name']);
          }
        }

        for (final pN in uniqueParameterNames) {
          Map tempOptions = {};
          for (var p in parameters) {
            if (p['parameter_name'] == pN) {
              tempOptions[p['value']] = p['parameter_options'];
            }
          }

          options[subAreaName]!.add(SkinToneFilterParameters(
            parameterName: pN,
            parameterOptions: tempOptions,
          ));
        }

        faceSubAreas.add(subAreaName);
      },
    );
  }
}

class SkinToneFilterParameters {
  String parameterName;
  Map parameterOptions = {};
  String selected = '';
  List selectedColors = [];

  SkinToneFilterParameters({
    required this.parameterName,
    required this.parameterOptions,
  });

  void select(String s) {
    selected = s;
  }
}
