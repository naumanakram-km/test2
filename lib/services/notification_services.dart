import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../controller/controllers.dart';
import '../screens/catalog_screen.dart';
import '../screens/home_screen.dart';
import '../screens/product_detail_1_screen.dart';
import '../widgets/scaffold/scaffold_template.dart';
import 'other_filer_services.dart';

class NotificationService {
  static void _launchScreen(Map<String, dynamic>? data) {
    cPrint("FIRST ON TAP");
    cPrint("Data on Launch Screen is $data");
    if (data != null) {
      // push(context: Get.context!, screen: ProductDetail1Screen(sku: '220725001991234'));
      if (Get.context != null) {
        cPrint("SECOND ON TAP");

        ///---- Splitting the string
        if (data.containsKey('additional_params')) {
          cPrint("THIRD ON TAP");
          var completeString = data['additional_params'];
          List<String> stringParts = completeString.split("?amcounter=");
          // Check if the first index contains "SKU=" or "campaign="
          if (stringParts[0].contains("SKU=")) {
            cPrint("FOURTH 01 ON TAP");

            // Split the first index by "SKU="
            List<String> skuParts = stringParts[0].split("SKU=");

            // Check if the second index of skuParts contains ","
            if (skuParts[1].contains(",")) {
              cPrint("FIFHT ON TAP");

              // Do something if it contains "," - means it contains multiple SKU IDs
              List<String> allSkuIDs = skuParts[1].split(",");

              cPrint(
                  '==== length of total SKUs is ${allSkuIDs.length} =======');

              push(
                  context: Get.context!,
                  screen: ScaffoldTemplate(
                    index: 0,
                    child: HomeScreen(),
                  ));
            } else {
              cPrint("SIXTH ON TAP");

              // when it only has one SKU ID
              cPrint("SINGLE SKU PRODUCT :: ${skuParts[1]}");
              push(
                  context: Get.context!,
                  screen: ProductDetail1Screen(
                    sku: skuParts[1],
                    isOnSale: true,
                  ));
            }
          } else if (stringParts[0].contains("CAMPAIGN=")) {
            cPrint("FOURTH 02 ON TAP");
            //----- when this is campaign
            List<String> campaignId = stringParts[0].split("CAMPAIGN=");
            cPrint("CAMPAIGN ID IS :: ${campaignId[1]}");

            push(
                context: Get.context!,
                screen: CatalogScreen(
                  isFromSale: true,
                  campaignId: campaignId[1],
                  comingFromMainIndex: navController.currentMainIndex.value,
                ));
          } else {
            cPrint("No Campaign or SKU Found");
            cPrint("FOURTH 03 ON TAP");

            push(
                context: Get.context!,
                screen: ScaffoldTemplate(
                  child: HomeScreen(),
                  index: 0,
                ),
                pushUntil: true);
          }
        } else {
          cPrint(" additional parameters not found in notification data");
          push(
              context: Get.context!,
              screen: ScaffoldTemplate(
                child: HomeScreen(),
                index: 0,
              ),
              pushUntil: true);
        }
      }
    }
  }

  static void onInitMessage() async {
    /// This will call when app will open from notification tap
    FirebaseMessaging.instance.getInitialMessage().then((data) {
      if (data != null) {
        cPrint('===== opening app 4 =====');
        _launchScreen(data.data);
      }
    });
  }

  ///
  static final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static AndroidNotificationChannel channel = const AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    description:
        'This channel is used for important notifications.', // description
    importance: Importance.high,
    sound: RawResourceAndroidNotificationSound('@mipmap/ic_launcher'),
  );

  ///
  static void showNotification(RemoteMessage message) async {
    cPrint("Showing Notification");
    cPrint("Showing Notification 001");
    getJson(message.data);
    flutterLocalNotificationsPlugin.show(
        message.hashCode,
        message.notification!.title!,
        message.notification!.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            channel.id,
            channel.name,
            channelDescription: channel.description,
            playSound: true,
            icon: '@mipmap/ic_launcher',
          ),
          iOS: const DarwinNotificationDetails(
            presentAlert: true,
            presentSound: true,
          ),
        ),
        payload: jsonEncode(message.data));
  }

  /// Background Handler Method
  static Future<void> _firebaseBackgroundNotificationHandler(
      RemoteMessage message) async {
    getJson(message.data);
    cPrint('===== opening app 3 =====');
    _launchScreen(message.data);
  }

  static int i = 0;

  static Future<void> init() async {
    log("Notification Service Started", name: "NOTIFICATION SERVICE");
    FirebaseMessaging.onBackgroundMessage(
        _firebaseBackgroundNotificationHandler);

    /// This will show when any firebase push notification is received..
    FirebaseMessaging.onMessageOpenedApp.listen((data) {
      cPrint("Background Notification --> ${data.data.toString()}");
      getJson(data.data);
      cPrint('===== opening app 2 =====');
      _launchScreen(data.data);
    });

    /// This will call when app will open from notification tap

    ///Init android settings
    const initSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');

    /// Init IOS settings
    const DarwinInitializationSettings initSettingsIOS =
        DarwinInitializationSettings();
    const initializationSettings = InitializationSettings(
      android: initSettingsAndroid,
      iOS: initSettingsIOS,
    );

    flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse: (payload) async {
        Map<String, dynamic> _data = json.decode(payload.payload!);
        cPrint('===== opening app 1 =====');
        _launchScreen(_data);
      },
    );

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    /// Notification settings
    await FirebaseMessaging.instance.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    ///
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: Platform.isIOS ? false : true,
      badge: Platform.isIOS ? false : true,
      sound: Platform.isIOS ? false : true,
    );

    await FirebaseMessaging.instance
        .getToken()
        .then((value) => cPrint("FCM TOKEN: $value"));

    /// Foreground Notification Stream..
    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      final status = await FirebaseAuth.instance.authStateChanges().first;
      if (status != null) {
        showNotification(message);
      }
    });
  }
}
