import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:sofiqe/utils/states/function.dart';

import 'package:flutter/material.dart';
import 'package:sofiqe/services/api_base_manager.dart';

class FoundationAndConcelarApiServices {
  

}

String getJson(jsonObject, {name}) {
  var encoder = const JsonEncoder.withIndent("     ");
  log(encoder.convert(jsonObject), name: name ?? "");
  return encoder.convert(jsonObject);
}

Future<T?> push<T>({
  required BuildContext context,
  required Widget screen,
  bool pushUntil = false,
}) {
  if (pushUntil) {
    return Navigator.of(context)
        .pushAndRemoveUntil<T>(MaterialPageRoute(builder: (_) => screen), (Route<dynamic> route) => false);
  }
  return Navigator.of(context).push<T>(MaterialPageRoute(builder: (_) => screen));
}
