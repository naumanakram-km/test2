// ignore_for_file: unused_element

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sofiqe/controller/currencyController.dart';
import 'package:sofiqe/model/data_ready_status_enum.dart';
import 'package:sofiqe/model/product_model.dart';
import 'package:sofiqe/provider/account_provider.dart';
import 'package:sofiqe/provider/cart_provider.dart';
import 'package:sofiqe/provider/home_provider.dart';
import 'package:sofiqe/provider/page_provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/screens/evaluate_screen.dart';
import 'package:sofiqe/screens/product_detail_1_screen.dart';
import 'package:sofiqe/screens/try_it_on_screen.dart';
import 'package:sofiqe/utils/api/product_details_api.dart';
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/widgets/png_icon.dart';
import 'package:sofiqe/widgets/product_detail/order_notification.dart';
import 'package:sofiqe/widgets/product_error_image.dart';
import 'package:sofiqe/widgets/wishlist.dart';

import '../../controller/fabController.dart';
import '../../main.dart';
import '../../utils/constants/api_end_points.dart';

class H1A extends StatelessWidget {
  H1A({Key? key}) : super(key: key);

  final HomeProvider hp = Get.find();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(vertical: size.height * 0.028),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          //_Greetings(),
          SizedBox(height: size.height * 0.02),
          Expanded(
            child: Obx(
              () {
                if (hp.dealOfTheDayStatus.value == DataReadyStatus.COMPLETED) {
                  return (hp.dealOfTheDayList.isEmpty)
                      ? DealOfTheDayError()
                      : DealOfTheDayItems();
                } else if (hp.dealOfTheDayStatus.value ==
                        DataReadyStatus.FETCHING ||
                    hp.dealOfTheDayStatus.value == DataReadyStatus.INACTIVE) {
                  return DealOfTheDayBuffering();
                } else if (hp.dealOfTheDayStatus.value ==
                    DataReadyStatus.ERROR) {
                  return DealOfTheDayError();
                } else {
                  return DealOfTheDayError();
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _Greetings extends StatelessWidget {
  const _Greetings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String dayDate = '';
    String timeOfDayGreetings = '';
    String name = '';
    Size size = MediaQuery.of(context).size;

    DateTime date = DateTime.now();

    dayDate = DateFormat('EEEE d MMMM').format(date);

    int hour = date.hour;
    if (hour > 18) {
      timeOfDayGreetings = 'Good Evening';
    } else if (hour > 12) {
      timeOfDayGreetings = 'Good Afternoon';
    } else if (hour > 6) {
      timeOfDayGreetings = 'Good Morning';
    } else if (hour >= 0) {
      timeOfDayGreetings = 'Good Night';
    }

    if (Provider.of<AccountProvider>(context, listen: false).isLoggedIn) {
      name =
          ', ${Provider.of<AccountProvider>(context, listen: false).user!.firstName}';
    }

    return Container(
      child: Column(
        children: [
          Text(
            '$dayDate',
            style: Theme.of(context).textTheme.headline2!.copyWith(
                  color: Colors.black,
                  fontSize: size.height * 0.016,
                  letterSpacing: 0,
                ),
          ),
          Text(
            '$timeOfDayGreetings$name',
            style: Theme.of(context).textTheme.headline2!.copyWith(
                  color: Colors.black,
                  fontSize: size.height * 0.019,
                  letterSpacing: 1.2,
                ),
          ),
        ],
      ),
    );
  }
}

class DealOfTheDayItems extends StatelessWidget {
  DealOfTheDayItems({Key? key}) : super(key: key);

  final HomeProvider hp = Get.find();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    int index = 0;
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: size.width * 0.05, vertical: size.height * 0.01),
        child: Row(
          children: hp.dealOfTheDayList.map<GestureDetector>(
            (Product p) {
              int i = index++;
              return GestureDetector(
                onTap: () {
                  gtm.push(
                    'press_dealoftheday',
                  );

                  FABController.to.closeOpenedMenu();

                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext c) {
                        return ProductDetail1Screen(sku: p.sku!);
                      },
                    ),
                  );
                },
                child: _OfferOfTheDayCard(
                  index: i,
                  total: hp.bestSellerList.length,
                  product: p,
                ),
              );
            },
          ).toList(),
        ),
      ),
    );
  }
}

class _OfferOfTheDayCard extends StatelessWidget {
  final num index;
  final num total;
  final Product product;
  _OfferOfTheDayCard({
    Key? key,
    required this.index,
    required this.total,
    required this.product,
  }) : super(key: key);

  final HomeProvider hp = Get.find();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    double topLeft = 10;
    double bottomLeft = 0;
    double topRight = 10;
    double bottomRight = 0;
    if (index == 0) {
      topLeft = 10;
      bottomLeft = 10;
    }
    if (index == total - 1) {
      topRight = 10;
      bottomRight = 10;
    }
    return Container(
      width: size.width * 0.85,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.025),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(topLeft),
          bottomLeft: Radius.circular(bottomLeft),
          topRight: Radius.circular(topRight),
          bottomRight: Radius.circular(bottomRight),
        ),
        image: DecorationImage(
          alignment: Alignment.topCenter,
          fit: BoxFit.fitWidth,
          image: AssetImage(
            'assets/images/offer_of_day_background_image.png',
          ),
        ),
        boxShadow: const [
          BoxShadow(
            color: Color(0x15000029),
            // color: Colors.red,
            blurRadius: 4,
            spreadRadius: 1.5,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: Column(
        children: [
          Expanded(
            child: _OfferDetails(
              product: product,
            ),
          )
        ],
      ),
    );
  }
}

class _OfferDetails extends StatefulWidget {
  final Product product;
  _OfferDetails({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  State<_OfferDetails> createState() => _OfferDetailsState();
}

class _OfferDetailsState extends State<_OfferDetails> {
  final CurrencyController currencycntrl = Get.put(CurrencyController());

  Future<void> share(prodUrl, title) async {
    await FlutterShare.share(
        title: title, text: title, linkUrl: prodUrl, chooserTitle: 'Share');
  }

  final TryItOnProvider tiop = Get.find();

  final PageProvider pp = Get.find();
  bool flagAddingtoCart = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: size.width * 0.08),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: size.height * 0.12),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Image.network(
                  '${widget.product.image}',
                  width: 100,
                  height: 100,
                  errorBuilder: (BuildContext c, Object o, StackTrace? st) {
                    return ProductErrorImage(width: 100, height: 100);
                  },
                ),
                Container(
                  padding: EdgeInsets.only(top: 5),
                  child: Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0),
                        child: WishList(
                            sku: widget.product.sku!,
                            itemId: widget.product.id!),
                      ),
                    ],
                  ),
                ),
              ]),
          SizedBox(height: size.height * 0.014),
          Text(
            'DEAL OF THE DAY',
            style: Theme.of(context).textTheme.headline2!.copyWith(
                  color: Colors.black,
                  fontSize: size.height * 0.015,
                  letterSpacing: 0,
                ),
          ),
          SizedBox(height: size.height * 0.01),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                  child: Text(
                '${widget.product.name}',
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.black,
                      fontSize: size.height * 0.03,
                      fontWeight: FontWeight.bold,
                    ),
              )),
              InkWell(
                onTap: () {
                  cPrint(widget.product.productURL);
                  // share(product.productURL, product.name);
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Icon(Icons.share, color: Colors.grey, size: 20.0),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: () {
              FABController.to.closeOpenedMenu();
              Get.to(() => EvaluateScreen(widget.product.image,
                  widget.product.sku, widget.product.name));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                RatingBar.builder(
                  ignoreGestures: true,
                  itemSize: 18,
                  initialRating: double.parse(widget.product.avgRating),
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(
                    horizontal: 0.0,
                  ),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {},
                ),
                SizedBox(width: 10),
                Text(
                  widget.product.avgRating != "null"
                      ? widget.product.avgRating.toString()
                      : '0',
                  style: TextStyle(color: Colors.black, fontSize: 10),
                ),
                SizedBox(width: 10),
                Text(
                  '(${widget.product.reviewCount.toString()})',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 10,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: size.height * 0.01),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.product.discountedPrice != null
                    ? '${widget.product.discountedPrice!.toString().toProperCurrency()}'
                    : '${widget.product.price!.toString().toProperCurrency()}',
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Color.fromARGB(255, 2, 2, 2),
                      fontSize: size.height * 0.025,
                    ),
              ),
              // SizedBox(height: size.height * 0.01),
              Container(
                  margin: EdgeInsets.only(right: 20),
                  child: widget.product.price != widget.product.discountedPrice
                      ? Text(
                          '${widget.product.price!.toString().toProperCurrency()}',
                          style:
                              Theme.of(context).textTheme.headline2!.copyWith(
                                    color: Colors.red[400],
                                    fontSize: size.height * 0.015,
                                    decoration: TextDecoration.lineThrough,
                                  ),
                        )
                      : Container()),
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: size.width * 0.0),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 3.0),
                  child: Text(
                    // '€ ${(responseBody['price'] as num).toStringAsFixed(2)}',
                    'Earn ' + widget.product.rewardsPoint,
                    style: Theme.of(context).textTheme.headline2!.copyWith(
                          color: SplashScreenPageColors.earnColor,
                          fontSize: 10.0,
                        ),
                  ),
                ),
                SizedBox(
                  width: 10.0,
                  child: Image.asset(
                    "assets/images/coin.png",
                  ),
                ),
                Text(
                  // '€ ${(responseBody['price'] as num).toStringAsFixed(2)}',
                  ' VIP points',
                  style: Theme.of(context).textTheme.headline2!.copyWith(
                        color: SplashScreenPageColors.earnColor,
                        fontSize: 10.0,
                      ),
                )
              ],
            ),
          ),
          SizedBox(height: size.height * 0.04),
          SizedBox(
            height: size.height * 0.07,
            child: Text(
              '${widget.product.description}',
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: Colors.black,
                    fontSize: size.height * 0.018,
                    letterSpacing: 0,
                  ),
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          SizedBox(height: size.height * 0.05),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Flexible(
                flex: 1,
                child: SizedBox(
                  height: size.height * 0.07,
                  width: 155,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                      backgroundColor:
                          MaterialStateProperty.all(AppColors.primaryColor),
                      overlayColor: MaterialStateProperty.resolveWith<Color?>(
                        (Set<MaterialState> states) {
                          if (states.contains(MaterialState.pressed))
                            return tiop.ontapColor; //<-- SEE HERE
                          return null; // Defer to the widget's default.
                        },
                      ),
                    ),
                    onPressed: () {
                      tiop.isChangeButtonColor.value = true;
                      tiop.isChangeButtonColor.value = true;
                      tiop.playSound();
                      Future.delayed(Duration(milliseconds: 10)).then((value) {
                        tiop.received.value = widget.product;
                        tiop.page.value = 2;
                        tiop.directProduct.value = true;
                        tiop.lookProduct.value = false;
                        tiop.currentSelectedProducturl.value =
                            widget.product.productURL;
                        tiop.currentSelectedProductname.value =
                            widget.product.name!;
                        Get.to(() => TryItOnScreen());
                      });
                      // // Navigator.pop(context);
                      // tiop.received.value = selectedproduct;
                      // tiop.page.value = 2;
                      // tiop.directProduct.value = true;
                      // tiop.lookProduct.value = false;
                      // Get.to(() => TryItOnScreen(
                      //       isDetail: isDetail,
                      //       selectShadeOption: selectShadeOption,
                      //     ));
                      // tiop.received.value = product;
                      // tiop.page.value = 2;
                      // tiop.directProduct.value = true;
                      // tiop.LookProduct.value=false;
                      // pp.goToPage(Pages.TRYITON);
                    },
                    child: Text(
                      'TRY ON',
                      style: Theme.of(context).textTheme.headline2!.copyWith(
                            color: Colors.black,
                            fontSize: size.height * 0.014,
                          ),
                    ),
                  ),
                ),
                // :: Commented for testing purpsoe. Will be removed after QA apprval
                // CapsuleButton(
                //   backgroundColor: Color(0xFFF2CA8A),
                //   height: size.height * 0.07,
                //   onPress: () {
                //     tiop.received.value = widget.product;
                //     tiop.page.value = 2;
                //     tiop.directProduct.value = true;
                //     tiop.lookProduct.value = false;
                //     tiop.currentSelectedProducturl.value =
                //         widget.product.productURL;
                //     tiop.currentSelectedProductname.value =
                //         widget.product.name!;
                //     Get.to(() => TryItOnScreen());
                //   },
                //   child: Text(
                //     'TRY ON',
                //     style: Theme.of(context).textTheme.headline2!.copyWith(
                //           color: Colors.black,
                //           fontSize: size.height * 0.014,
                //         ),
                //   ),
                // ),
              ),
              SizedBox(width: size.width * 0.03),
              Flexible(
                flex: 1,
                child: flagAddingtoCart
                    ? SpinKitFadingCircle(
                        color: Colors.black,
                        size: 40.0,
                      )
                    : SizedBox(
                        height: size.height * 0.07,
                        // width: 155,
                        child: flagAddingtoCart
                            ? SpinKitFadingCircle(
                                color: Color(0xffF2CA8A),
                                size: 40.0,
                              )
                            : ElevatedButton(
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                  ),
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.black),
                                  overlayColor:
                                      MaterialStateProperty.resolveWith<Color?>(
                                    (Set<MaterialState> states) {
                                      if (states
                                          .contains(MaterialState.pressed))
                                        return tiop.ontapColor; //<-- SEE HERE
                                      return null; // Defer to the widget's default.
                                    },
                                  ),
                                ),
                                // style: ElevatedButton.styleFrom(
                                //     shape: RoundedRectangleBorder(
                                //       borderRadius:
                                //       BorderRadius.circular(100),
                                //     ),
                                //     primary:
                                //     SplashScreenPageColors.textColor),
                                onPressed: () async {
                                  tiop.isChangeButtonColor.value = true;
                                  tiop.playSound();
                                  Future.delayed(Duration(milliseconds: 10))
                                      .then((value) async {
                                    tiop.isChangeButtonColor.value = false;
                                    setState(() {
                                      flagAddingtoCart = true;
                                    });
                                    var res =
                                        await sfAPIGetProductDetailsFromSKU(
                                            sku: widget.product.sku!);
                                    Map<String, dynamic> responseBody =
                                        json.decode(res.body);
                                    cPrint(responseBody['type_id']);
                                    String type = responseBody['type_id'];
                                    if (type == "configurable") {
                                      setState(() {
                                        flagAddingtoCart = false;
                                      });
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (BuildContext c) {
                                            return ProductDetail1Screen(
                                                sku: widget.product.sku!);
                                          },
                                        ),
                                      );
                                    } else {
                                      CartProvider cartP =
                                          Provider.of<CartProvider>(context,
                                              listen: false);
                                      cPrint(
                                          "CartProvider  -->> SSs ${cartP.cartToken}");
                                      // setState(() {
                                      //   flagAddingtoCart = true;
                                      // });
                                      await cartP.addHomeProductsToCart(
                                          context, widget.product);
                                      cPrint(
                                          "Name  -->> EEE ${widget.product.image}");
                                      setState(() {
                                        flagAddingtoCart = false;
                                      });
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                          padding: EdgeInsets.all(0),
                                          backgroundColor: Colors.black,
                                          duration: Duration(seconds: 1),
                                          content: Container(
                                            child: CustomSnackBar(
                                              sku: widget.product.sku!,
                                              image: widget.product.image
                                                  .replaceAll(
                                                      RegExp(APIEndPoints
                                                              .mainBaseUrl +
                                                          '/media/catalog/product'),
                                                      ''),
                                              name: widget.product.name!,
                                            ),
                                          ),
                                        ),
                                      );
                                    }
                                  });
                                  // try {
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    PngIcon(
                                      image:
                                          'assets/icons/add_to_cart_white.png',
                                      height: size.height * 0.015,
                                      width: size.height * 0.015,
                                    ),
                                    SizedBox(
                                      width: size.width * 0.01,
                                    ),
                                    Text(
                                      'ADD TO BAG',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline2!
                                          .copyWith(
                                            color: Colors.white,
                                            fontSize: size.height * 0.011,
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                      ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class DealOfTheDayBuffering extends StatelessWidget {
  const DealOfTheDayBuffering({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    double topLeft = 10;
    double bottomLeft = 10;
    double topRight = 10;
    double bottomRight = 10;

    return Container(
      width: size.width * 0.85,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.025),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(topLeft),
          bottomLeft: Radius.circular(bottomLeft),
          topRight: Radius.circular(topRight),
          bottomRight: Radius.circular(bottomRight),
        ),
        image: DecorationImage(
          alignment: Alignment.topCenter,
          fit: BoxFit.fitWidth,
          image: AssetImage(
            'assets/images/offer_of_day_background_image.png',
          ),
        ),
        boxShadow: [
          BoxShadow(
            color: Color(0x15000029),
            // color: Colors.red,
            blurRadius: 4,
            spreadRadius: 1.5,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: Shimmer.fromColors(
        baseColor: Colors.grey.shade300,
        highlightColor: Colors.grey.shade100,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 150,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 20,
                    width: 180,
                    decoration: const BoxDecoration(color: Colors.grey),
                  ),
                  Container(
                    height: 30,
                    width: 30,
                    decoration: const BoxDecoration(color: Colors.grey),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 20,
                width: 200,
                decoration: const BoxDecoration(color: Colors.grey),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                height: 20,
                width: 150,
                decoration: const BoxDecoration(color: Colors.grey),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: List.generate(
                        5,
                        (index) => Container(
                              margin: EdgeInsets.only(right: 6),
                              height: 20,
                              width: 20,
                              child: Icon(Icons.star),
                              decoration: BoxDecoration(
                                // color: Colors.grey,
                                shape: BoxShape.circle,
                              ),
                            ))),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 20,
                    width: 180,
                    decoration: const BoxDecoration(color: Colors.grey),
                  ),
                  Container(
                    height: 10,
                    width: 30,
                    decoration: const BoxDecoration(color: Colors.grey),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 10,
                width: 80,
                decoration: const BoxDecoration(color: Colors.grey),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 15,
                width: double.infinity,
                decoration: const BoxDecoration(color: Colors.grey),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                height: 15,
                width: double.infinity,
                decoration: const BoxDecoration(color: Colors.grey),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                height: 15,
                width: double.infinity,
                decoration: const BoxDecoration(color: Colors.grey),
              ),
              SizedBox(
                height: 5,
              ),
              Expanded(
                child: Container(),
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.all(10),
                      height: 50,
                      width: 100,
                      decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(30)),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.all(10),
                      height: 50,
                      width: 100,
                      decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(30)),
                    ),
                  ),
                ],
              ),

              // Text(
              //   'Loading deal of the day near you!',
              //   style: Theme.of(context).textTheme.headline2!.copyWith(
              //         color: Colors.black,
              //         fontSize: size.height * 0.025,
              //       ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}

class DealOfTheDayError extends StatelessWidget {
  const DealOfTheDayError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    double topLeft = 10;
    double bottomLeft = 10;
    double topRight = 10;
    double bottomRight = 10;

    return Container(
      width: size.width * 0.85,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.025),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(topLeft),
          bottomLeft: Radius.circular(bottomLeft),
          topRight: Radius.circular(topRight),
          bottomRight: Radius.circular(bottomRight),
        ),
        image: DecorationImage(
          alignment: Alignment.topCenter,
          fit: BoxFit.fitWidth,
          image: AssetImage(
            'assets/images/offer_of_day_background_image.png',
          ),
        ),
        boxShadow: [
          BoxShadow(
            color: Color(0x15000029),
            // color: Colors.red,
            blurRadius: 4,
            spreadRadius: 1.5,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Looks like no deal is available near you...',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline2!.copyWith(
                  color: Colors.black,
                  fontSize: size.height * 0.025,
                ),
          ),
        ),
      ),
    );
  }
}
