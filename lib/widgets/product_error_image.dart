import 'package:flutter/material.dart';

class ProductErrorImage extends StatelessWidget {
  final double? width;
  final double? height;
  const ProductErrorImage({Key? key, this.width, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width ?? 400,
      height: height ?? 400,
      child: Image.asset(
        'assets/images/product_error_image.png',
        width: width ?? 400,
        height: height ?? 400,
      ),
    );
  }
}
