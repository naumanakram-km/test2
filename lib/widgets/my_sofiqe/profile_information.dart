import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/controller/controllers.dart';
import 'package:sofiqe/controller/msProfileController.dart';
import 'package:sofiqe/provider/account_provider.dart';
import 'package:sofiqe/screens/Ms1/sofiqueEditProfile.dart';
import 'package:sofiqe/widgets/png_icon.dart';
import 'package:sofiqe/widgets/profile_picture.dart';
import '../../controller/fabController.dart';
import '../../provider/page_provider.dart';
import '../makeover/make_over_login_custom_widget.dart';

class ProfileInformation extends StatefulWidget {
  const ProfileInformation({Key? key}) : super(key: key);

  @override
  State<ProfileInformation> createState() => _ProfileInformationState();
}

class _ProfileInformationState extends State<ProfileInformation> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final ap = Provider.of<AccountProvider>(context, listen: true);

    String membership = "Not Signed Up Yet";

    if (ap.isLoggedIn && ap.user != null) {
      Future.delayed(Duration(seconds: 1), () {
        MsProfileController.to.profileName.value =
            '${profileController.firstNameController.text} ${profileController.lastNameController.text}';
      });
    } else {
      Future.delayed(Duration(seconds: 1), () {
        MsProfileController.to.profileName.value = 'Guest';
      });
    }

    if (ap.isLoggedIn) {
      membership = 'Premium Member';
    }

    return Container(
      height: size.height * 0.12,
      color: Colors.black,
      child: Padding(
        padding: EdgeInsets.only(left: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 20,
                    bottom: 20,
                  ),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        FABController.to.closeOpenedMenu();
                        if (profileController.screen.value == 3) {
                          profileController.screen.value = 0;
                        } else {
                          pp.goToPage(Pages.HOME);
                        }
                      },
                      splashColor: Colors.transparent,
                      borderRadius: BorderRadius.circular(100),
                      child: Container(
                        // color: Colors.yellow,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100)),
                        padding: EdgeInsets.only(
                          left: 10,
                          right: 10,
                        ),
                        child: SvgPicture.asset(
                          'assets/svg/arrow.svg',
                          width: 12,
                          height: 12,
                        ),
                      ),
                    ),
                  ),
                ),
                // SizedBox(
                //   width: 10,
                // ),

                ///----- if screen value == 3 (Natural Me Screen) then there should be Camera Icon and Gallery Image Picker Function will be there
                ///----- else there would be pen icon for editing the whole user profile information
                GestureDetector(
                  onTap: profileController.screen.value == 3
                      ? () {
                          ap.getGalleryImage(context);
                        }
                      : () async {
                          FABController.to.closeOpenedMenu();
                          if (!ap.isLoggedIn) {
                            profileController.makeOverMessage.value = false;
                            profileController.screen.value = 1;
                          } else {
                            Get.to(() => SofiqueEditProfile());
                          }
                        },
                  child: Stack(
                    alignment: Alignment.bottomRight,
                    children: <Widget>[
                      ProfilePicture(),
                      profileController.screen.value == 3
                          ? Container(
                              height: 24,
                              width: 24,
                              child: Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    // color: Colors.white
                                    color: Color(0xFFAFA0A0)),
                                child: Center(
                                  child: SvgPicture.asset(
                                    'assets/images/add_a_photo_outlined.svg',
                                    width: 15,
                                    height: 15,
                                  ),
                                ),
                              ),
                            )
                          : Container(
                              height: 22,
                              width: 22,
                              child: Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color(0xFFAFA0A0)),
                                child: Center(
                                  child: SvgPicture.asset(
                                    'assets/svg/edit_profile.svg',
                                    width: 12,
                                    height: 12,
                                  ),
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
              ],
            ),
            //todo: This need to fixed.
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 6),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    'sofiqe',
                    style: Theme.of(context).textTheme.headline1!.copyWith(
                          color: Colors.white,
                          fontSize: 23,
                        ),
                  ),
                  Obx(() {
                    return Text(
                      '${MsProfileController.to.profileName.value}',
                      style: Theme.of(context).textTheme.headline2!.copyWith(
                            color: Colors.white,
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                    );
                  }),
                  Text(
                    membership,
                    style: Theme.of(context).textTheme.headline2!.copyWith(
                          color: Color(0xFFAFA0A0),
                          fontSize: 10,
                        ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  InkWell(
                    onTap: () async {
                      FABController.to.closeOpenedMenu();
                      await FlutterShare.share(
                          title: 'a',
                          text: ' ',
                          linkUrl:
                              'https://play.google.com/store/apps/details?id=com.sofiqe.app',
                          chooserTitle: '');
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color(0xFFF2CA8A),
                        shape: BoxShape.circle,
                      ),
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          PngIcon(
                            image: 'assets/icons/share/send@3x.png',
                            padding: EdgeInsets.only(left: size.width * 0.01),
                            height: 22,
                            width: 22,
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                bottom: size.width * 0.015,
                                top: size.width * 0.01,
                                right: size.width * 0.015,
                                left: size.width * 0.015),
                            child: Text(
                              'SHARE\nAPP',
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline2!
                                  .copyWith(
                                    color: Colors.black,
                                    fontSize: size.height * 0.009,
                                  ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  ap.isLoggedIn
                      ? Container()
                      : GestureDetector(
                          onTap: () {
                            FABController.to.closeOpenedMenu();

                            profileController.screen.value = 1;
                          },
                          child: Text(
                            'UPGRADE',
                            style:
                                Theme.of(context).textTheme.headline2!.copyWith(
                                      color: Color(0xFFF2CA8A),
                                      fontSize: size.height * 0.01,
                                    ),
                          ),
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
