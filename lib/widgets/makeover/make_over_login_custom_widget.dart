import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sofiqe/controller/controllers.dart';
import 'package:sofiqe/provider/make_over_provider.dart';
import 'package:sofiqe/provider/page_provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/screens/login_screen.dart';
import 'package:sofiqe/screens/signup_screen.dart';
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/widgets/capsule_button.dart';

import '../../controller/fabController.dart';
import '../png_icon.dart';

final PageProvider pp = Get.find();

///------- Log In or Register Prompt Page

class LogInRegisterPromptPage extends StatelessWidget {
  final bool flowFromMs, makeOverMessage;
  final Function? onBack;

  const LogInRegisterPromptPage({
    Key? key,
    this.flowFromMs = true,
    this.makeOverMessage = false,
    this.onBack,
  }) : super(key: key);
  void initState() {
    //  ControlStatusBar.enableStatusBar();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        debugPrint('==== whole screen tapped =====');
        FABController.to.closeOpenedMenu();
      },
      child: Container(
        // color: Color(0xFF797373),
        decoration: new BoxDecoration(
            image: new DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.8), BlendMode.dstATop),
          image: new AssetImage("assets/images/portrait-of-woman-247206.png"),
          fit: BoxFit.cover,
        )),
        //  color: Colors.black,
        child: Column(
          children: [
            _TopBar(
                flowFromMs: flowFromMs,
                onBackTap: onBack != null ? onBack : null),
            Expanded(
                child: _Body(
              makeOverMessage: makeOverMessage,
            )),
          ],
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  final bool makeOverMessage;

  _Body({Key? key, required this.makeOverMessage}) : super(key: key);

  final MakeOverProvider mop = Get.find();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: size.width * 0.1),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          // ProfilePicture(),
          SizedBox(height: size.height * 0.03),
          RichText(
            text: TextSpan(children: [
              TextSpan(
                text: 'We are ',
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: size.height * 0.025,
                      letterSpacing: 1.2,
                    ),
              ),
              TextSpan(
                  text: 'sofiqe. ',
                  style: Theme.of(context).textTheme.headline2!.copyWith(
                        color: Colors.black,
                        // fontWeight: FontWeight.bold,
                        fontSize: size.height * 0.025,
                        letterSpacing: 1.2,
                      )),
            ]),
          ),

          SizedBox(height: size.height * 0.03),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(children: [
              TextSpan(
                text: makeOverMessage
                    ? ' You must make a makeover analysis to access this area'
                    : ' You must register to access this area',
                // text: ' To see your shopping bag,\nplease login or register',
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: size.height * 0.016,
                      letterSpacing: 0.7,
                    ),
              ),
              // TextSpan(
              //     text: ' Sofiqe ',
              //     style: Theme.of(context).textTheme.headline2!.copyWith(
              //           color: Colors.black,
              //           fontSize: size.height * 0.016,
              //           letterSpacing: 0.7,
              //         )),
              // TextSpan(
              //   text: 'member.',
              //   style: Theme.of(context).textTheme.headline2!.copyWith(
              //         color: Colors.black,
              //         fontWeight: FontWeight.bold,
              //         fontSize: size.height * 0.016,
              //         letterSpacing: 0.7,
              //       ),
              // ),
            ]),
          ),

          SizedBox(height: size.height * 0.04),
          CapsuleButton(
            backgroundColor: Color(0xFFF2CA8A),
            onPress: () async {
              FABController.to.closeOpenedMenu();
              profileController.screen.value = 0;
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext _) {
                    return SignupScreen();
                  },
                ),
              );
              // if (Provider.of<AccountProvider>(context, listen: false)
              //     .isLoggedIn) {
              //   mop.sendResponse(
              //       Provider.of<AccountProvider>(context, listen: false)
              //           .customerId);
              //   Provider.of<AccountProvider>(context, listen: false)
              //       .saveProfilePicture();
              //   mop.screen.value = 4;
              // }
            },
            child: Text(
              'JOIN SOFIQE FOR FREE',
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: Colors.black,
                    fontSize: size.width * 0.036,
                  ),
            ),
          ),
          SizedBox(height: size.height * 0.12),
          CapsuleButton(
            backgroundColor: Colors.black,
            borderColor: Color(0xFF393636),
            onPress: () async {
              FABController.to.closeOpenedMenu();
              profileController.screen.value = 0;
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext _) {
                    return LoginScreen();
                  },
                ),
              );
              // AccountProvider acc =
              //     Provider.of<AccountProvider>(context, listen: false);
              // if (acc.isLoggedIn) {
              //   mop.sendResponse(
              //       Provider.of<AccountProvider>(context, listen: false)
              //           .customerId);
              //   Provider.of<AccountProvider>(context, listen: false)
              //       .saveProfilePicture();
              //   mop.screen.value = 4;
              // }
            },
            child: Text(
              'ALREADY A SOFIQE? SIGN IN',
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: Colors.white,
                    fontSize: size.width * 0.033,
                  ),
            ),
          ),
          SizedBox(height: size.height * 0.05),
        ],
      ),
    );
  }
}

///------- Do MakeOver Analysis First - Prompt Page

class DoMakeOverAnalysisPromptPage extends StatelessWidget {
  final String title;
  final String description;
  final bool flowFromMs;
  final Function? onBack;

  const DoMakeOverAnalysisPromptPage(
      {Key? key,
      required this.title,
      required this.description,
      this.flowFromMs = true,
      this.onBack})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        debugPrint('==== whole screen tapped =====');
        FABController.to.closeOpenedMenu();
      },
      child: Container(
        // color: Color(0xFF797373),
        decoration: new BoxDecoration(
            image: new DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.8), BlendMode.dstATop),
          image: new AssetImage("assets/images/portrait-of-woman-247206.png"),
          fit: BoxFit.cover,
        )),
        //  color: Colors.black,
        child: Column(
          children: [
            _TopBar(
                flowFromMs: flowFromMs,
                onBackTap: onBack != null ? onBack : null),
            Expanded(
                child: Body(
                    title: title,
                    description: description,
                    flowFromMs: flowFromMs,
                    onBackPressed: onBack != null ? onBack : null)),
          ],
        ),
      ),
    );
  }
}

class Body extends StatelessWidget {
  final String title;
  final String description;
  final bool flowFromMs;
  final Function? onBackPressed;
  final PageProvider pp = Get.find();
  Body({
    Key? key,
    required this.title,
    required this.description,
    required this.flowFromMs,
    this.onBackPressed,
  }) : super(key: key);

  final MakeOverProvider mop = Get.find();
  final TryItOnProvider tiop = Get.find();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(
            bottom: size.width * 0.3,
            top: size.width * 0.3,
            left: size.width * 0.1,
            right: size.width * 0.1,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // ProfilePicture(),
              SizedBox(height: size.height * 0.2),
              Text(
                title,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: size.height * 0.025,
                      letterSpacing: 1.2,
                    ),
              ),
              SizedBox(height: size.height * 0.03),
              Container(
                width: size.width * 0.66,
                child: Text(
                  'You must make a makeover analysis to access this area',
                  // description,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.black,
                      fontSize: size.height * 0.016,
                      letterSpacing: 0.7,
                      fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(height: size.height * 0.03),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                    elevation: MaterialStateProperty.all(0),
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black, width: 1.5),
                          borderRadius: BorderRadius.circular(30.0)),
                    ),
                    foregroundColor: MaterialStateProperty.all(
                      AppColors.navigationBarSelectedColor,
                    ),
                    backgroundColor: MaterialStateProperty.all(
                        // AppColors.buttonBackgroundShopping,
                        Colors.transparent),
                    overlayColor: MaterialStateProperty.resolveWith<Color?>(
                      (Set<MaterialState> states) {
                        if (states.contains(MaterialState.pressed))
                          return tiop.ontapColor; //<-- SEE HERE
                        return null; // Defer to the widget's default.
                      },
                    ),
                  ),
                  onPressed: onBackPressed != null
                      ? () {
                          onBackPressed!();
                        }
                      : () {
                          flowFromMs
                              ? profileController.screen.value = 0
                              : Get.back();
                        },
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                    child: Text(
                      'CANCEL'.toUpperCase(),
                      style: Theme.of(context).textTheme.headline2!.copyWith(
                            color: Colors.black,
                            fontSize: size.height * 0.016,
                            letterSpacing: 0.7,
                            fontWeight: FontWeight.w600,
                          ),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 10), // Space between the buttons
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                    ),
                    foregroundColor: MaterialStateProperty.all(
                      AppColors.navigationBarSelectedColor,
                    ),
                    backgroundColor: MaterialStateProperty.all(
                        // AppColors.buttonBackgroundShopping,
                        Color(0xffF2CA8A)),
                    overlayColor: MaterialStateProperty.resolveWith<Color?>(
                      (Set<MaterialState> states) {
                        if (states.contains(MaterialState.pressed))
                          return tiop.ontapColor; //<-- SEE HERE
                        return null; // Defer to the widget's default.
                      },
                    ),
                  ),
                  onPressed: () async {
                    tiop.isChangeButtonColor.value = true;
                    tiop.playSound();
                    Future.delayed(Duration(milliseconds: 10)).then((value) {
                      // tiop.isChangeButtonColor.value = false;
                      // SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
                      // Get.back();
                      // makeOverProvider.colorAna.value = true;
                      navController.currentMainIndex.value = 2;
                      navController.innerPageIndex.value = 0;
                      pp.goToPage(Pages.MAKEOVER);

                      // profileController.screen.value = 0;
                    });
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                    child: Text('Go to makeover'.toUpperCase(),
                        style: Theme.of(context).textTheme.headline2!.copyWith(
                              color: Colors.black,
                              fontSize: size.height * 0.017,
                            )),
                  ),
                ),
              ),
            ],
          ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //   children: [
          //     GestureDetector(
          //       onTap: onBackPressed != null
          //           ? () {
          //               onBackPressed!();
          //             }
          //           : () {
          //               flowFromMs
          //                   ? profileController.screen.value = 0
          //                   : Get.back();
          //             },
          //       child: Container(
          //         padding: EdgeInsets.symmetric(
          //           vertical: 15,
          //           horizontal: 30,
          //         ),
          //         decoration: BoxDecoration(
          //             borderRadius: BorderRadius.circular(100),
          //             border: Border.all(color: Colors.black, width: 1.5)),
          //         child: Text(
          //           'CANCEL',
          //           style: Theme.of(context).textTheme.headline3!.copyWith(
          //               color: Colors.black,
          //               fontSize: size.height * 0.016,
          //               letterSpacing: 0.7,
          //               fontWeight: FontWeight.w600),
          //         ),
          //       ),
          //     ),

          //   ],
          // ),
        ),

        // SizedBox(height: size.height * 0.12),
      ],
    );
  }
}

///------- Common Tap Bar For Both Screens
class _TopBar extends StatelessWidget {
  final bool flowFromMs;
  final Function? onBackTap;
  const _TopBar({Key? key, required this.flowFromMs, this.onBackTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
              width: size.width * 0.18,
              height: size.width * 0.18,
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Material(
                type: MaterialType.transparency,
                child: InkWell(
                  borderRadius:
                      BorderRadius.all(Radius.circular(size.width * 0.18)),
                  onTap: onBackTap != null
                      ? () {
                          onBackTap!();
                        }
                      : () {
                          flowFromMs
                              ? profileController.screen.value = 0
                              : Get.back();
                        },
                  child: Transform.rotate(
                    angle: 3.1439,
                    child: PngIcon(
                      color: Colors.black,
                      image: 'assets/icons/arrow-2-white.png',
                    ),
                  ),
                ),
              )

              // BackButtonApp(
              //   flowFromMs: flowFromMs,
              //   // flowFromMs: true,
              //   child: Transform.rotate(
              //     angle: 3.1439,
              //     child: PngIcon(
              //       color: Colors.black,
              //       image: 'assets/icons/arrow-2-white.png',
              //     ),
              //   ),
              // ),
              ),
          Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                    bottom: size.height * 0.01, left: size.width * 0.2),
                child: Text(
                  'sofiqe',
                  style: Theme.of(context).textTheme.headline1!.copyWith(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: size.height * 0.04,
                      ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
