import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/widgets/horizontal_bar.dart';
import '../../utils/constants/api_end_points.dart';
import '../product_error_image.dart';

class CustomSnackBarForAddAll extends StatelessWidget {
  final String? message;

  const CustomSnackBarForAddAll({Key? key, this.message}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    cPrint("ENTER 123");
    var size = MediaQuery.of(context).size;
    // var cart = Provider.of<CartProvider>(context).cart;

    return Container(
      margin: EdgeInsets.symmetric(vertical: size.width * 0.03, horizontal: size.height * 0.03),
      height: size.height * 0.25,
      color: Colors.black,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CachedNetworkImage(
                  imageUrl:
                      APIEndPoints.mainBaseUrl + "/media/catalog/product/cache/983707a945d60f44d700277fbf98de57\$image",
                  // fit: BoxFit.cover,
                  placeholder: (context, _) => Image.asset(
                    'assets/images/sofiqe-font-logo-2.png',
                  ),
                  height: size.height * 0.1, width: size.height * 0.1,
                  errorWidget: (context, url, error) {
                    return ProductErrorImage(
                      width: 400,
                      height: 400,
                    );
                  },
                ),
                if (message != null)
                  Text(
                    message.toString(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline2!.copyWith(
                          color: Colors.white,
                          fontSize: size.height * 0.015,
                          letterSpacing: 1.13,
                        ),
                  ),
                HorizontalBar(
                  color: Colors.black,
                  height: 2,
                  width: size.width * 0.14,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}