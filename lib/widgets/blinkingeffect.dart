import 'package:flutter/material.dart';

class BlinkingAnimation extends StatefulWidget {
  final Widget? widget;
  const BlinkingAnimation({Key? key, this.widget}) : super(key: key);
  @override
  _BlinkingAnimationState createState() => _BlinkingAnimationState();
}

class _BlinkingAnimationState extends State<BlinkingAnimation>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 4))
          ..repeat();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FadeTransition(
        opacity: _controller,
          child: widget.widget,
      ),
    );
  }
}

class BlinkingAnimationShort extends StatefulWidget {
  final Widget? widget;
  const BlinkingAnimationShort({Key? key, this.widget}) : super(key: key);
  @override
  _BlinkingAnimationShortState createState() => _BlinkingAnimationShortState();
}

class _BlinkingAnimationShortState extends State<BlinkingAnimationShort>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  int times = 0;
  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 1))
          ..repeat()
          ..addListener(() {
            if (_controller.value > 0.99) {
              if (times >= 1) {
                _controller.stop();
              }
              times++;
              print(_controller.value);
              // _controller.stop();
            }

            // if (_controller.value > (4 / levelClock)) {    //4 = to stop after 5 seconds
            //   _controller.stop();
            // }
          });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: _controller,
      child: widget.widget,
    );
  }
}
