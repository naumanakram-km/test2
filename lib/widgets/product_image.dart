import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:sofiqe/widgets/product_error_image.dart';

import '../utils/constants/api_end_points.dart';

class ProductImage extends StatelessWidget {
  final String imageShortPath;
  final double width;
  final double height;

  const ProductImage({
    Key? key,
    this.imageShortPath = '',
    this.width = 400,
    this.height = 400,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String image = imageShortPath;
    bool isLooksImagePath = false;
    if (imageShortPath.startsWith('http')) {
      image = image.replaceAll(
          RegExp(APIEndPoints.mainBaseUrl + '/media/catalog/product'), '');
    }
    print(image);
    if (imageShortPath.startsWith(APIEndPoints.mainBaseUrl + '/media/looks/')) {
      isLooksImagePath = true;
    }

    return SizedBox(
       width: width,
      height: height,
       child: CachedNetworkImage(
        imageUrl: isLooksImagePath
            ? imageShortPath
            : imageShortPath == ""
                ? APIEndPoints.myBaseUri +
                    "/media/catalog/product/cache/983707a945d60f44d700277fbf98de57/$image"
                : image != "/I/m/Image_-1_2_8.png"
                    ? APIEndPoints.mainBaseUrl +
                        '/media/catalog/product/cache/983707a945d60f44d700277fbf98de57$image'
                    : APIEndPoints.mainBaseUrl +
                        '/media/catalog/product/$image',
        placeholder: (context, _) => Image.asset(
          'assets/images/sofiqe-font-logo-2.png',
        ),
        errorWidget: (context, url, error) {
          return ProductErrorImage(
             width: width,
            height: height,
          );
        },
      ),
    );
  }
}
