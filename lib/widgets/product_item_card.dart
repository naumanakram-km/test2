import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:sofiqe/controller/looksController.dart';
import 'package:sofiqe/model/product_model.dart';
import 'package:sofiqe/provider/cart_provider.dart';
import 'package:sofiqe/provider/page_provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/screens/product_detail_1_screen.dart';
import 'package:sofiqe/screens/try_it_on_screen.dart';
import 'package:sofiqe/utils/api/product_details_api.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/widgets/png_icon.dart';
import 'package:sofiqe/widgets/product_detail/order_notification.dart';
import 'package:sofiqe/widgets/product_image.dart';
import 'package:sofiqe/widgets/round_button.dart';
import 'package:sofiqe/widgets/wishlist.dart';

import '../controller/controllers.dart';
import '../controller/fabController.dart';
import '../provider/account_provider.dart';
import '../screens/MS8/looks_package_details.dart';
import '../screens/evaluate_screen.dart';
import '../utils/constants/api_end_points.dart';
import '../utils/constants/app_colors.dart';
import 'package:http/http.dart' as http;

class ProductItemCard extends StatelessWidget {
  final Product product;

  ProductItemCard({
    Key? key,
    required this.product,
  }) : super(key: key);

  final PageProvider pp = Get.find();
  final TryItOnProvider tiop = Get.find();

  String firstFewWords(String m, int brk) {
    int startIndex = 0, indexOfSpace = m.length;
    cPrint("test");
    for (int i = 0; i < brk; i++) {
      indexOfSpace = m.indexOf(' ', startIndex);
      if (indexOfSpace == -1) {
        //-1 is when character is not found
        return m;
      }
      startIndex = indexOfSpace + 1;
    }

    return m.substring(0, indexOfSpace) + '...';
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        FABController.to.closeOpenedMenu();
        if (product.name?.toUpperCase() == "VAMP" ||
            product.name?.toUpperCase() == "NATURAL" ||
            product.name?.toUpperCase() == "PROFESSIONAL" ||
            product.name?.toUpperCase() == "GLAMOUR") {
          if (!Provider.of<AccountProvider>(context, listen: false)
                  .isLoggedIn ||
              !makeOverProvider.tryitOn.value) {
            Get.snackbar(
                'Do Makeover First', "Sorry you haven't done your makeover yet",
                margin: EdgeInsets.only(bottom: 20, left: 10, right: 10),
                snackPosition: SnackPosition.BOTTOM,
                snackStyle: SnackStyle.FLOATING,
                borderRadius: 8,
                backgroundColor: Colors.black,
                colorText: Colors.white,
                icon: Icon(
                  Icons.error,
                  color: Colors.white,
                ));
          } else {
            List temp = [];
            Get.find<LooksController>().lookModel!.items!.forEach((element) {
              temp.add(element.name);
            });
            cPrint(
                "ITEM IS == ${Get.find<LooksController>().lookModel!.items![Get.find<LooksController>().lookModel!.items!.indexWhere((f) => f.name.toString() == product.name)].name}");
            Get.to(() => LookPackageMS8(
                  item: Get.find<LooksController>().lookModel!.items![
                      Get.find<LooksController>().lookModel!.items!.indexWhere(
                          (f) => f.name.toString() == product.name)],
                  looklist: temp,
                  id: Get.find<LooksController>()
                      .lookModel!
                      .items!
                      .indexWhere((f) => f.name.toString() == product.name),
                ));
          }
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext c) {
                return ProductDetail1Screen(sku: product.sku!);
              },
            ),
          );
        }
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: WishList(
                    sku: product.sku!,
                    itemId: product.id!,
                  ),
                ),
                Spacer(),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: InkWell(
                    onTap: () {
                      pp.share(product.productURL, product.name);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Icon(Icons.share, color: Colors.black, size: 20.0),
                    ),
                  ),
                )
              ],
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: size.width * 0.035),
              child: ProductImage(
                imageShortPath: product.image,
                width: double.infinity,
                height: size.height * 0.10,
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: size.width * 0.035),
              child: Text(
                firstFewWords(product.name.toString(), 26),
                textAlign: TextAlign.start,
                maxLines: 2,
                overflow: TextOverflow.fade,
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.black,
                      fontSize: size.height * 0.018,
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Get.to(() =>
                    EvaluateScreen(product.image, product.sku, product.name));
              },
              child: Padding(
                padding: EdgeInsets.only(left: size.width * .035),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    RatingBar.builder(
                      ignoreGestures: true,
                      itemSize: 14,
                      initialRating: double.parse(product.avgRating),
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {},
                    ),
                    SizedBox(width: 5),
                    Text(
                      double.parse(product.avgRating).toStringAsFixed(1),
                      style: TextStyle(color: Colors.black, fontSize: 10),
                    ),
                    SizedBox(width: 5),
                    Text(
                      '(${product.reviewCount})',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 10,
                      ),
                    ),
                    // SizedBox(width: 5),
                    // Text(
                    //   '(${product.reviewCount.toString()})',
                    //   style: TextStyle(
                    //     color: Colors.black,
                    //     fontSize: 10,
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: size.width * 0.035),
              child: (product.name == 'Professional' ||
                      product.name == 'Glamour' ||
                      product.name == 'Natural' ||
                      product.name == 'Vamp')
                  ? Text('Individual Price')
                  : ((product.discountedPrice ?? 0) < (product.price ?? 0) &&
                          (product.discountedPrice ?? 0) != 0
                      // product.discountedPrice != null
                      )
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '${product.discountedPrice.toString().toProperCurrency()}',
                              // '${product.discountedPrice != null ? product.discountedPrice!.toString().toProperCurrency() : product.price!.toString().toProperCurrency()}',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline2!
                                  .copyWith(
                                    color: Colors.red,
                                    fontSize: size.height * 0.015,
                                  ),
                            ),
                            Text(
                              '${product.price.toString().toProperCurrency()}',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline2!
                                  .copyWith(
                                    color: Colors.black,
                                    fontSize: size.height * 0.011,
                                    decoration: TextDecoration.lineThrough,
                                  ),
                            ),
                          ],
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              product.price.toString().toProperCurrency(),
                              // '${product.discountedPrice != null ? product.discountedPrice!.toString().toProperCurrency() : product.price!.toString().toProperCurrency()}',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline2!
                                  .copyWith(
                                    color: Colors.black,
                                    fontSize: size.height * 0.015,
                                  ),
                            ),
                            // Text(
                            //   '${product.discountedPrice != null ? '${product.price.toString().toProperCurrency()}' : ''}',
                            //   style: Theme.of(context).textTheme.headline2!.copyWith(
                            //         color: Colors.red,
                            //         fontSize: size.height * 0.011,
                            //         decoration: TextDecoration.lineThrough,
                            //       ),
                            // ),
                          ],
                        ),
            ),
            Padding(
              padding: EdgeInsets.only(left: size.width * .035, top: 3),
              child: Row(
                children: [
                  Text(
                    // '€ ${(responseBody['price'] as num).toStringAsFixed(2)}',
                    (product.name == 'Professional' ||
                            product.name == 'Glamour' ||
                            product.name == 'Natural' ||
                            product.name == 'Vamp')
                        ? 'Earn '
                        : "Earn " + product.rewardsPoint,
                    // 'Earn ' + product.price!.round().toString(),
                    //product.rewardsPoint,
                    style: Theme.of(context).textTheme.headline2!.copyWith(
                          color: SplashScreenPageColors.earnColor,
                          fontSize: 11.0,
                        ),
                  ),
                  Image.asset(
                    "assets/images/coin.png",
                    width: 10.0,
                  ),
                  Text(
                    // '€ ${(responseBody['price'] as num).toStringAsFixed(2)}',
                    ' VIP points',
                    style: Theme.of(context).textTheme.headline2!.copyWith(
                          color: SplashScreenPageColors.earnColor,
                          fontSize: 10.0,
                        ),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.height * 0.01),
            Container(
              padding: EdgeInsets.symmetric(horizontal: size.width * 0.035),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  if (product.name
                      .toString()
                      .toUpperCase()
                      .contains('GIFTCARD'))
                    Expanded(child: Container())
                  else
                    product.tryOn == "1"
                        ? Obx(() => RoundButton(
                              sku: product.name.toString(),
                              backgroundColor: tiop
                                          .isChangeButtonColor.isTrue &&
                                      tiop.sku.value == product.name.toString()
                                  ? tiop.ontapColor
                                  : const Color(0xFFF2CA8A),
                              size: size.height * 0.068,
                              child: Text(
                                'TRY ON',
                                textAlign: TextAlign.center,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline2!
                                    .copyWith(
                                      color: Colors.black,
                                      fontSize: size.height * 0.012,
                                    ),
                              ),
                              onPress: () async {
                                if (product.name?.toUpperCase() == "VAMP" ||
                                    product.name?.toUpperCase() == "NATURAL" ||
                                    product.name?.toUpperCase() ==
                                        "PROFESSIONAL" ||
                                    product.name?.toUpperCase() == "GLAMOUR") {
                                  if (!Provider.of<AccountProvider>(context,
                                              listen: false)
                                          .isLoggedIn ||
                                      !makeOverProvider.tryitOn.value) {
                                    Get.snackbar('Do Makeover First',
                                        "Sorry you haven't done your makeover yet",
                                        margin: EdgeInsets.only(
                                            bottom: 20, left: 10, right: 10),
                                        snackPosition: SnackPosition.BOTTOM,
                                        snackStyle: SnackStyle.FLOATING,
                                        borderRadius: 8,
                                        backgroundColor: Colors.black,
                                        colorText: Colors.white,
                                        icon: Icon(
                                          Icons.error,
                                          color: Colors.white,
                                        ));
                                  } else {
                                    List temp = [];
                                    Get.find<LooksController>()
                                        .lookModel!
                                        .items!
                                        .forEach((element) {
                                      temp.add(element.name);
                                    });
                                    cPrint(
                                        "ITEM IS == ${Get.find<LooksController>().lookModel!.items![Get.find<LooksController>().lookModel!.items!.indexWhere((f) => f.name.toString() == product.name)].name}");
                                    Get.to(() => LookPackageMS8(
                                          item: Get.find<LooksController>()
                                                  .lookModel!
                                                  .items![
                                              Get.find<LooksController>()
                                                  .lookModel!
                                                  .items!
                                                  .indexWhere((f) =>
                                                      f.name.toString() ==
                                                      product.name)],
                                          looklist: temp,
                                          id: Get.find<LooksController>()
                                              .lookModel!
                                              .items!
                                              .indexWhere((f) =>
                                                  f.name.toString() ==
                                                  product.name),
                                        ));
                                  }
                                } else {
                                  context.loaderOverlay.show();
                                  cPrint("Hello1234");
                                  cPrint(product.color);
                                  tiop.received.value = product;
                                  http.Response response =
                                      await sfAPIGetProductDetailsFromSKU(
                                          sku: product.sku as String);
                                  Map responseBody = json.decode(response.body);
                                  tiop.received.value.color = responseBody[
                                                      'extension_attributes']?[
                                                  'configurable_product_options']
                                              ?.first?['values']
                                              ?.first?['extension_attributes']
                                          ?['value_label'] ??
                                      '#ffffff';
                                  tiop.received.value.values = responseBody[
                                                  'extension_attributes']
                                              ?['configurable_product_options']
                                          ?.first?['values'] ??
                                      [];
                                  tiop.page.value = 2;
                                  tiop.directProduct.value = true;
                                  tiop.lookProduct.value = false;
                                  tiop.currentSelectedProducturl.value =
                                      product.productURL;
                                  tiop.currentSelectedProductname.value =
                                      product.name!;

                                  context.loaderOverlay.hide();
                                  // pp.goToPage(Pages.TRYITON);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (builder) =>
                                              TryItOnScreen()));
                                }
                              },
                            ))
                        : SizedBox(
                            width: size.height * 0.068,
                          ),
                  Obx(
                    () => RoundButton(
                        sku: product.sku.toString(),
                        backgroundColor: tiop.isChangeButtonColor.isTrue &&
                                tiop.sku.value == product.sku.toString()
                            ? tiop.ontapColor
                            : Colors.black,
                        size: size.height * 0.068,
                        child: PngIcon(
                            image: 'assets/icons/add_to_cart_white.png'),
                        onPress: () async {
                          if (product.name?.toUpperCase() == "VAMP" ||
                              product.name?.toUpperCase() == "NATURAL" ||
                              product.name?.toUpperCase() == "PROFESSIONAL" ||
                              product.name?.toUpperCase() == "GLAMOUR") {
                            if (!Provider.of<AccountProvider>(context,
                                        listen: false)
                                    .isLoggedIn ||
                                !makeOverProvider.tryitOn.value) {
                              Get.snackbar('Do Makeover First',
                                  "Sorry you haven't done your makeover yet",
                                  margin: EdgeInsets.only(
                                      bottom: 20, left: 10, right: 10),
                                  snackPosition: SnackPosition.BOTTOM,
                                  snackStyle: SnackStyle.FLOATING,
                                  borderRadius: 8,
                                  backgroundColor: Colors.black,
                                  colorText: Colors.white,
                                  icon: Icon(
                                    Icons.error,
                                    color: Colors.white,
                                  ));
                            } else {
                              List temp = [];
                              Get.find<LooksController>()
                                  .lookModel!
                                  .items!
                                  .forEach((element) {
                                temp.add(element.name);
                              });
                              cPrint(
                                  "ITEM IS == ${Get.find<LooksController>().lookModel!.items![Get.find<LooksController>().lookModel!.items!.indexWhere((f) => f.name.toString() == product.name)].name}");
                              Get.to(() => LookPackageMS8(
                                    item: Get.find<LooksController>()
                                            .lookModel!
                                            .items![
                                        Get.find<LooksController>()
                                            .lookModel!
                                            .items!
                                            .indexWhere((f) =>
                                                f.name.toString() ==
                                                product.name)],
                                    looklist: temp,
                                    id: Get.find<LooksController>()
                                        .lookModel!
                                        .items!
                                        .indexWhere((f) =>
                                            f.name.toString() == product.name),
                                  ));
                            }
                          } else {
                            cPrint(product.typeid);
                            cPrint("Product Options");
                            cPrint(product.options);
                            cPrint(product.hasOption);
                            try {
                              cPrint("CLICK${product}");
                              if (product.typeid != null &&
                                  product.typeid == "configurable") {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (BuildContext c) {
                                      return ProductDetail1Screen(
                                          sku: product.sku!);
                                    },
                                  ),
                                );
                              } else if (product.typeid != null &&
                                  product.typeid == "simple") {
                                context.loaderOverlay.show();

                                CartProvider cartP = Provider.of<CartProvider>(
                                    context,
                                    listen: false);
                                cPrint(
                                    "CartProvider  -->> SSs ${cartP.cartToken}");
                                await cartP.addHomeProductsToCart(
                                    context, product);
                                cPrint("Name  -->> EEE ${product.image}");
                                context.loaderOverlay.hide();
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    padding: EdgeInsets.all(0),
                                    backgroundColor: Colors.black,
                                    duration: Duration(seconds: 1),
                                    content: Container(
                                      child: CustomSnackBar(
                                        sku: product.sku!,
                                        image: product.image.replaceAll(
                                            RegExp(APIEndPoints.mainBaseUrl +
                                                '/media/catalog/product'),
                                            ''),
                                        name: product.name!,
                                      ),
                                    ),
                                  ),
                                );
                              } else {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (BuildContext c) {
                                      return ProductDetail1Screen(
                                          sku: product.sku!);
                                    },
                                  ),
                                );
                              }
                            } catch (e) {
                              context.loaderOverlay.hide();
                              cPrint("This is error");
                              String error = "";
                              error = e.toString().length > 150
                                  ? e.toString().substring(0, 149)
                                  : e.toString();

                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  padding: EdgeInsets.all(0),
                                  backgroundColor: Colors.black,
                                  duration: Duration(seconds: 2),
                                  content: Container(
                                    padding: EdgeInsets.all(10),
                                    child: Text(
                                        "Error in adding Product to cart \n" +
                                            error.toString()),
                                  ),
                                ),
                              );
                            }
                          }
                        }),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.height * 0.01),
          ],
        ),
      ),
    );
  }
}
