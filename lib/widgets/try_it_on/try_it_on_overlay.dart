// ignore_for_file: deprecated_member_use

import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:sofiqe/controller/controllers.dart';
import 'package:sofiqe/controller/looksController.dart';
import 'package:sofiqe/controller/ms8Controller.dart';
import 'package:sofiqe/controller/nav_controller.dart';
import 'package:sofiqe/controller/selectedProductController.dart';
import 'package:sofiqe/model/ms8Model.dart';
import 'package:sofiqe/model/searchAlternativecolorModel.dart';
import 'package:sofiqe/model/selectedProductModel.dart';
import 'package:sofiqe/provider/account_provider.dart';
import 'package:sofiqe/provider/make_over_provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/utils/states/function.dart';

// import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/widgets/capsule_button.dart';
import 'package:sofiqe/widgets/png_icon.dart';
import 'package:sofiqe/widgets/product_detail/custom_notification_for_add_all.dart';
import 'package:sofiqe/widgets/round_button.dart';
import 'package:sofiqe/widgets/try_it_on/try_it_on_buttons.dart';
import 'package:sofiqe/widgets/try_it_on/try_it_on_color_selector.dart';
import 'package:sofiqe/widgets/try_on_Lookproduct.dart';
import 'package:sofiqe/widgets/try_on_product.dart' as tryOn;
import '../../controller/fabController.dart';
import '../../model/CentralColorLeftmostModel.dart';
import '../../model/model_for_color_order.dart';
import '../../model/product_model.dart';
import '../../provider/cart_provider.dart';

// import '../../provider/page_provider.dart';
import '../../screens/shopping_bag_screen.dart';
import '../../utils/constants/api_end_points.dart';
import '../../utils/constants/route_names.dart';
import '../catalog/checkout/widget_loader_with_text.dart';
import '../product_error_image.dart';
// import 'blink_text_widget.dart';

class TryItOnOverlay extends StatefulWidget {
  final bool? isDetail;
  final dynamic selectShadeOption;
  final CameraController camera;
  final List<dynamic>? values;

  const TryItOnOverlay(
      {Key? key,
      required this.camera,
      this.isDetail,
      this.selectShadeOption,
      this.values})
      : super(key: key);

  @override
  _TryItOnOverlayState createState() => _TryItOnOverlayState();
}

class _TryItOnOverlayState extends State<TryItOnOverlay>
    with TickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: const Duration(milliseconds: 200),
    vsync: this,
  );

  // bool vis = false;

  final TryItOnProvider tiop = Get.find<TryItOnProvider>();
  final MakeOverProvider mokeOver = Get.find();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  /* timer for blinking text */
  // int timer = 0;

  /* Visible and invisible time */
  // int vistime = 5;

  /* bool for blink text visible or not */

  @override
  void initState() {
    // hide Android default navigation bar
    // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);

    // blinking text visible for 5 sec and invisible for 10 sec
    // Timer.periodic(
    //   Duration(seconds: 1),
    //   (_) {
    //     timer++;
    //     if (timer == vistime) {
    //       setState(() {
    //         vis = !vis;
    //         vistime = vis ? 5 : 10;
    //         timer = 0;
    //         cPrint('${DateTime.now()}');
    //       });
    //     }
    //   },
    // );

    tiop.resetTimeVisibleValues();
    tiop.timerForVisualizingRecommendations();

    selectedcontroller.getSelectedProduct();
    super.initState();
  }

  Ms8Controller controller = Get.find();
  SelectedProductController selectedcontroller =
      Get.find<SelectedProductController>();

  bool adding = false;
  bool open = false;

  Future<bool> captureImageAndShare() async {
    try {
      XFile image = await widget.camera.takePicture();
      var dir = await getExternalStorageDirectory();
      File file = File(join(dir!.path, 'scanned_product_try_on.jpg'));
      if (await file.exists()) {
        await file.delete();
      }
      await image.saveTo(file.path);
      await Share.shareFiles([file.path]);
      return true;
    } catch (e) {
      Get.showSnackbar(
        GetSnackBar(
          message: 'Error occured: $e',
          duration: Duration(seconds: 2),
        ),
      );
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);

    // SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive, overlays: [SystemUiOverlay.top]);

    // bool cameraShouldWait = false;
// <<<<<<< HEAD
//     var cartItems = Provider.of<CartProvider>(context).cart != null
//         ? Provider.of<CartProvider>(context).cart!.length
//         : 0;
// =======
    var cartItems = Provider.of<CartProvider>(context).getCartLength();
    var cartTotalQty = Provider.of<CartProvider>(context).getTotalQty();

    // var totalCartQty = Provider.of<CartProvider>(context).getTotalQty();
    String name = '';

    if (Provider.of<AccountProvider>(context, listen: false).isLoggedIn) {
      name =
          Provider.of<AccountProvider>(context, listen: false).user!.firstName;
    }
//asd

    return WillPopScope(
      onWillPop: () async {
        if (tiop.lookname.value == "m16") {
          if (tiop.selectedProducts.isNotEmpty) {
            await tiop.saveSelectedProducts();
          }
          final MakeOverProvider mop = Get.find();
          mop.colorAna.value = false;
          mop.screen.value = 1;
          mop.questions.value = questionsController.makeover;
          Get.find<NavController>().setnavbar(false);
          mop.update();
        } else {
          cPrint('Back pressed');
          if (tiop.homeT1T2) {
            Get.toNamed(RouteNames.homeScreen);
            Get.find<NavController>().setnavbar(false);
            tiop.homeT1T2 = false;
          } else {
            Navigator.pop(context);
            Get.find<NavController>().setnavbar(false);
          }
        }
        final FABController fabController = Get.find();
        fabController.showFab.value = true;
        return false;
      },
      child: Container(
        width: size.width,
        child: Column(
          children: [
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10, top: 20),
                    child: Material(
                      color: Colors.transparent,
                      child: IconButton(
                        icon: const Icon(Icons.arrow_back, color: Colors.black),
                        onPressed: () async {
                          makeOverProvider.isText.value = true;
                          isselected = true;
                          setState(() {});
                          if (tiop.lookname.value == "m16") {
                            if (tiop.selectedProducts.isNotEmpty) {
                              await tiop.saveSelectedProducts();
                            }
                            final MakeOverProvider mop = Get.find();
                            mop.colorAna.value = false;
                            mop.screen.value = 1;
                            mop.questions.value = questionsController.makeover;
                            Get.find<NavController>().setnavbar(false);
                            mop.update();
                          } else {
                            cPrint('Back pressed');
                            if (tiop.homeT1T2) {
                              Get.toNamed(RouteNames.homeScreen);
                              Get.find<NavController>().setnavbar(false);
                              tiop.homeT1T2 = false;
                            } else {
                              Navigator.pop(context);
                              Get.find<NavController>().setnavbar(false);
                            }
                          }
                          final FABController fabController = Get.find();
                          fabController.showFab.value = true;
                        },
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      Text(
                        'sofiqe',
                        style: Theme.of(context).textTheme.headline1!.copyWith(
                              fontSize: 30,
                              color: Colors.black,
                            ),
                      ),
                      Text(
                        tiop.directProduct.value
                            ? "TRY ON"
                            : tiop.lookname.value == "myselection"
                                ? "MY SELECTION"
                                : tiop.lookname.value == "m16"
                                    ? 'COLOUR MATCHING'
                                    : "LOOKS",
                        style: Theme.of(context).textTheme.headline2!.copyWith(
                            fontSize: size.height * 0.018,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                      Obx(() {
                        return Text(
                          '${name.isNotEmpty ? '$name,' : ''} you look ${tiop.lookProduct.value == true && tiop.lookname.value != "myselection" && tiop.lookname.value != "m16" ? tiop.lookname : 'sofiqe'} today',
                          style:
                              Theme.of(context).textTheme.headline2!.copyWith(
                                    fontSize: size.height * 0.018,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                        );
                      })
                    ],
                  ),

                  SizedBox(
                    height: AppBar().preferredSize.height,
                    width: AppBar().preferredSize.height * 1,
                    child: Center(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, RouteNames.cartScreen);
                        },
                        child: Obx(
                          () => Container(
                            height: AppBar().preferredSize.height * 0.7,
                            width: AppBar().preferredSize.height * 0.7,
                            decoration: BoxDecoration(
                              color: tiop.isChangeButtonColor.isTrue
                                  ? tiop.ontapColor
                                  : Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(
                                  AppBar().preferredSize.height * 0.7)),
                            ),
                            child: Stack(
                              alignment: Alignment.topRight,
                              children: [
                                PngIcon(
                                  image: 'assets/images/Path_6.png',
                                ),
                                if (cartItems == 0)
                                  SizedBox()
                                else
                                  Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.red),
                                      padding: EdgeInsets.all(4),
                                      child: Text(
                                        cartTotalQty.toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline2!
                                            .copyWith(
                                              fontSize: 13,
                                              color: Colors.white,
                                            ),
                                      ))
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),

                  ///----- OLD SHOPPING BAG
                  // Container(
                  //   margin: EdgeInsets.only(right: 10, top: 20),
                  //   child: RoundButton(
                  //     borderColor: Color(0xfff4f2f0).withOpacity(0.7),
                  //     backgroundColor: Color(0xfff4f2f0).withOpacity(0.7),
                  //     size: size.height * 0.05,
                  //     onPress: () {
                  //       Navigator.push(
                  //         context,
                  //         MaterialPageRoute(
                  //           builder: (BuildContext c) {
                  //             return ShoppingBagScreen();
                  //           },
                  //         ),
                  //       );
                  //     },
                  //     child: Obx(() {
                  //       return Stack(
                  //         alignment: Alignment.topRight,
                  //         children: [
                  //           PngIcon(
                  //             image: 'assets/images/Path_6.png',
                  //           ),
                  //           if (cartItems == 0) SizedBox() else
                  //             Container(
                  //                 decoration: BoxDecoration(
                  //                     shape: BoxShape.circle,
                  //                     color: Colors.red),
                  //                 padding: EdgeInsets.all(5),
                  //                 child: Text(cartTotalQty.toString()))
                  //         ],
                  //       );
                  //     }),
                  //   ),
                  // )
                ],
              ),
            ),
            //Side icons
            GestureDetector(
                onTap: () {
                  // SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,overlays:[SystemUiOverlay.top,SystemUiOverlay.bottom]);
                },
                child: TryItOnButtons(controller: _controller)),

            ///------Glassy Container with Color Pallet
            Obx(() {
              return tiop.directProduct.value
                  // &&
                  // tiop.bottomSheetVisible.value
                  // !tiop.bottomSheetVisible.value
                  ? Container()
                  : (tiop.lookname.value == "m16" ||
                          tiop.lookname.value == "myselection")
                      ? !tiop.bottomSheetVisible.value
                          ? tiop.selectedProduct.isTrue
                              ? tiop.selectedShades.isTrue
                                  ? Container(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      // height: 100,
                                      color: Color(0xFFF4F2F0).withOpacity(.8),
                                      child: Column(
                                        children: [
                                          ///-----Tabs
                                          Row(
                                            children: [
                                              ///------Text
                                              SizedBox(
                                                width: size.width * 0.22,
                                                child: Text(
                                                  'Recommended Colours',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 2,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline2!
                                                      .copyWith(
                                                        color: Colors.black,
                                                        fontSize: size.height *
                                                            0.0128,
                                                        letterSpacing: 0,
                                                      ),
                                                ),
                                              ),

                                              ///-----Recommended Colors Tabs List
                                              Expanded(
                                                child: Obx(() {
                                                  return (tiop.lookname.value ==
                                                                  "myselection" ||
                                                              tiop.lookname
                                                                      .value ==
                                                                  "m16") &&
                                                          tiop.selectedProduct
                                                              .isTrue &&
                                                          tiop.centralcolor
                                                                  .length >
                                                              0
                                                      ? BottomSheetTabs(
                                                          tmo: tiop,
                                                        )
                                                      : Container();
                                                }),
                                              ),
                                            ],
                                          ),

                                          ///----Alternative Colors
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              ///------Text
                                              SizedBox(
                                                width: size.width * 0.25,
                                                child: Text(
                                                  'Alternatives in the same range',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 2,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline2!
                                                      .copyWith(
                                                        color: Colors.black,
                                                        fontSize:
                                                            size.height * 0.013,
                                                        letterSpacing: 0,
                                                      ),
                                                ),
                                              ),

                                              ///------List
                                              Center(
                                                child: Container(
                                                  child: TryItOnColorSelector(),
                                                ),
                                              ),

                                              ///-----Add to Bag Button
                                              Container(
                                                height: AppBar()
                                                        .preferredSize
                                                        .height *
                                                    0.7,
                                                width: AppBar()
                                                        .preferredSize
                                                        .height *
                                                    0.7,
                                                decoration: BoxDecoration(
                                                  color: Colors.black,
                                                  borderRadius: BorderRadius
                                                      .all(Radius.circular(
                                                          AppBar()
                                                                  .preferredSize
                                                                  .height *
                                                              0.7)),
                                                ),
                                                child: GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () async {
                                                    cPrint("TIOO==>");
                                                    cPrint(TryItOnProvider
                                                        .instance
                                                        .outerIndex
                                                        .value);
                                                    ModelForColorOrder? product;
                                                    try {
                                                      product = TryItOnProvider
                                                                  .instance
                                                                  .alternateColorsProducts[
                                                              TryItOnProvider
                                                                  .instance
                                                                  .outerIndex
                                                                  .value]
                                                          as ModelForColorOrder;

                                                      CartProvider cartP =
                                                          Provider.of<
                                                                  CartProvider>(
                                                              context,
                                                              listen: false);
                                                      context.loaderOverlay
                                                          .show();
                                                      await cartP
                                                          .addtoCartSimpleWithSku(
                                                              context,
                                                              product.sku);

                                                      context.loaderOverlay
                                                          .hide();
                                                      Get.showSnackbar(
                                                        GetSnackBar(
                                                          message:
                                                              "Product has been successfully added to cart",
                                                          duration: Duration(
                                                              seconds: 2),
                                                          isDismissible: true,
                                                        ),
                                                      );
                                                      TryItOnProvider.instance
                                                              .isLoadedAfterAddtoCart =
                                                          false;
                                                    } catch (e) {
                                                      cPrint(e);
                                                    }
                                                    if (product != null) {
                                                      cPrint(product.sku);
                                                      cPrint(
                                                          product.face_color);
                                                    }
                                                    cPrint(TryItOnProvider
                                                            .instance
                                                            .alternateColorsProducts[
                                                        TryItOnProvider.instance
                                                            .outerIndex.value]);
                                                  },
                                                  child: PngIcon(
                                                    height: AppBar()
                                                            .preferredSize
                                                            .height *
                                                        0.3,
                                                    width: AppBar()
                                                            .preferredSize
                                                            .height *
                                                        0.3,
                                                    image:
                                                        'assets/icons/add_to_cart_white.png',
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: AppBar()
                                                        .preferredSize
                                                        .height *
                                                    0.05,
                                              ),
                                              // Container(
                                              //   // height: 30,
                                              //   // width: 30,
                                              //   decoration: BoxDecoration(
                                              //     color: Colors.black,
                                              //     shape: BoxShape.circle
                                              //   ),
                                              //   child: Icon(
                                              //
                                              //   ),
                                              // ),
                                            ],
                                          )
                                        ],
                                      ),
                                      //               child: Obx(
                                      //                     () {
                                      //                   return TryOnProduct(
                                      //                     product: tiop.received.value,
                                      //                   );
                                      //                 },
                                      // // <<<<<<< HEAD
                                      //               ),
                                    )
                                  : Container()
                              : Container()
                          : Container()
                      : Container();
            }),

            ///----- Bottom Sheet
            if (tiop.directProduct.value)
              Obx(() {
                return Container(
                  height: 97,
                  color: Color(0xFFF4F2F0),
                  child: tryOn.TryOnProduct(
                      selectShadeOption: widget.selectShadeOption,
                      isDetail: widget.isDetail,
                      product: tiop.received.value,
                      values: widget.values),
                );
              })
            else
              Obx(() {
                int count = tiop.count.value;
                List<ItemData>? temp = [];
                temp = controller.ms8model?.itemData!
                    .where((element) =>
                        element.faceArea == tiop.selectedFaceArea.value)
                    .toList();

                return SlidingUpPanel(
                  controller: tiop.pc.value,
                  maxHeight: temp?.length == 0
                      ? size.height * .1
                      : tiop.selectedFaceArea.value == "Lips"
                          ? size.height - size.height * .7
                          : size.height - size.height * .35,
                  // M16 adjusting height.
                  minHeight: size.height * .07 ,
                  isDraggable: true,
                  onPanelOpened: () {
                    tiop.toggleShadesIconSelection(false);
                    tiop.toggleBottomSheetVisibility();
                  },
                  onPanelClosed: () {
                    tiop.toggleShadesIconSelection(true);
                    tiop.toggleBottomSheetVisibility();
                  },
                  // panel: Container(),
                  header: Material(
                    child: Container(
                      alignment: Alignment.center,
                      width: size.width,
                      height: size.height * 0.07,
                      padding:
                          EdgeInsets.symmetric(horizontal: size.width * 0.02),
                      decoration: BoxDecoration(
                        color: Color(0XFFFFFFFF),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                            child: count > 1
                                ? tiop.vis.value
                                    ? Text(
                                        '${count > 13 ? 13 : count} RECOMMENDATIONS',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline2!
                                            .copyWith(
                                              color: Colors.black,
                                              fontSize: 9,
                                              letterSpacing: 0,
                                            ),
                                      )
                                    : Container(
                                        width: 100,
                                      )
                                : Container(
                                    width: 100,
                                  ),
                          ),
                          // Center ADD all and blink text
                          Expanded(
                              child: Column(
                            children: [
                              SizedBox(
                                height: size.height * .005,
                              ),
                              Container(
                                width: 45,
                                height: 6,
                                decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(.6),
                                    borderRadius: BorderRadius.circular(100)),
                              ),
                              SizedBox(
                                height: size.height * .005,
                              ),
                              Obx(
                                () => CapsuleButton(
                                  onPress: count <= 1
                                      ? () {
                                          Get.snackbar('Products are loading',
                                              'Please wait',
                                              margin: EdgeInsets.only(
                                                  bottom: 20,
                                                  left: 10,
                                                  right: 10),
                                              snackPosition:
                                                  SnackPosition.BOTTOM,
                                              snackStyle: SnackStyle.FLOATING,
                                              borderRadius: 8,
                                              backgroundColor: Colors.black,
                                              colorText: Colors.white,
                                              icon: Icon(
                                                Icons.warning,
                                                color: Colors.white,
                                              ));
                                        }
                                      : () async {
                                          tiop.sku.value = "1234";
                                          tiop.isChangeButtonColor.value = true;
                                          tiop.playSound();
                                          Future.delayed(
                                                  Duration(milliseconds: 200))
                                              .then((value) async {
                                            tiop.isChangeButtonColor.value =
                                                false;
                                            tiop.sku.value = "";

                                            cPrint(
                                                "ENTER 00  ${tiop.lookname.value}");
                                            // show loader
                                            tiop.showLoaderDialog(context);
                                            setState(() {
                                              adding = true;
                                            });
                                            try {
                                              (tiop.lookname.value ==
                                                          "myselection" ||
                                                      tiop.lookname.value ==
                                                          "m16")
                                                  ? AddToCardm16(context)
                                                  : addlooks(context);
                                              // AddToCarLooks(context);
                                            } catch (err) {
                                              cPrint(err);
                                              cPrint("cart errorr");
                                            } finally {
                                              setState(() {
                                                adding = false;
                                              });
                                            }
                                          });
                                        },
                                  backgroundColor:
                                      tiop.isChangeButtonColor.isTrue &&
                                              tiop.sku.value == "1234"
                                          ? tiop.ontapColor
                                          : Colors.black,
                                  height: size.height * 0.04,
                                  child: adding == true
                                      ? Center(
                                          child: SpinKitDoubleBounce(
                                            color: Color(0xffF2CA8A),
                                            size: 30.0,
                                          ),
                                          // CircularProgressIndicator(),
                                        )
                                      : Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            PngIcon(
                                              image:
                                                  'assets/icons/add_to_cart_yellow.png',
                                            ),
                                            SizedBox(width: size.width * 0.012),
                                            Text(
                                              'ADD ALL',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline2!
                                                  .copyWith(
                                                    color: Colors.white,
                                                    fontSize:
                                                        size.height * 0.012,
                                                    letterSpacing: 0,
                                                  ),
                                            ),
                                          ],
                                        ),
                                ),
                              ),
                            ],
                          )),
                          Expanded(
                              child: Container(
                                  alignment: Alignment.centerRight,
                                  child: IconButton(
                                    onPressed: () async {
                                      cPrint(
                                          "print(tiop.bottomSheetVisible.value); false ${tiop.bottomSheetVisible.value}");
                                      //print("print(tiop.bottomSheetVisible.value); ${tiop.bottomSheetVisible.value}");
                                      if (tiop.bottomSheetVisible.value) {
                                        await _controller.reverse();
                                        await tiop.pc.value.close();
                                        // tiop.toggleBottomSheetVisibility();
                                        // tiop.toggleShadesIconSelection(true);
                                      } else {
                                        // tiop.toggleBottomSheetVisibility();
                                        // tiop.toggleShadesIconSelection(false);
                                        await tiop.pc.value.open();
                                        await _controller.forward();
                                        cPrint(
                                            "print(tiop.bottomSheetVisible.value); false ${tiop.bottomSheetVisible.value}");
                                      }
                                    },
                                    icon: PngIcon(
                                      image: tiop.bottomSheetVisible.value
                                          ? 'assets/icons/push_down_black.png'
                                          : 'assets/icons/push_up_black.png',
                                    ),
                                  )))
                        ],
                      ),
                    ),
                  ),
                  panel: Material(child:
                      // Container(
                      //   width: size.width,
                      //   height: size.height * 0.06,
                      //   padding: EdgeInsets.symmetric(horizontal: size.width * 0.02),
                      //   decoration: BoxDecoration(
                      //     color: Colors.red
                      //   ),
                      // ),
                      Obx(() {
                    if (tiop.directProduct.value) {
                      return Container(
                        height: 97,
                        color: Color(0xFFF4F2F0),
                        child: Obx(
                          () {
                            ///--->> sliding panel
                            return tryOn.TryOnProduct(
                              product: tiop.received.value,
                            );
                          },
                        ),
                      );
                    } else {
                      return Container(
                          // 1.5 row in bottom menu sheet
                          // height: tiop.selectedProduct.value == true ? 190 : 150,
                          color: Color(0xFFF4F2F8),
                          child: BottomSheetBody(
                            tiop: tiop,
                            controller: controller,
                            selectefcontroller: selectedcontroller,
                          ));
                    }
                  })),
                );
              }),
          ],
        ),
      ),
    );
  }

  AddToCardMySelection(BuildContext context) async {
    Navigator.pop(context);
    int length = 0;
    List errorProducts = [];
    // selectedcontroller
    //     .selectedProduct!.items?.forEach((element) async{
    // selectedcontroller.tryMySelectionList.forEach((element) async {

    for (int i = 0; i < selectedcontroller.tryMySelectionList.length; i++) {
      cPrint("ENTER 11 $length");
      dynamic element = selectedcontroller.tryMySelectionList[i];
      CartProvider cartP = Provider.of<CartProvider>(context, listen: false);
      cPrint("ENTER 33 $length");
      cPrint("CartProvider  -->> SSs ${cartP.cartToken}");
      try {
        context.loaderOverlay.show(
            widget: LoaderWithText(
          msg: "Adding " +
              element.product!.name +
              "\n\n" +
              (selectedcontroller.tryMySelectionList.length - length)
                  .toString() +
              " Remaining",
        ));
        await cartP.addToCartForAddAll(context, element.product!.sku, [], 0, "",
            refresh: true);
        length++;
      } catch (e) {
        errorProducts.add(element.product!.name);
      }

      cPrint(
          "ENTER 22 $length  total ${selectedcontroller.selectedProduct!.items!.length}");
    }
    if (length == selectedcontroller.tryMySelectionList.length) {
      context.loaderOverlay.hide();
      // product snack bar
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        padding: EdgeInsets.all(0),
        backgroundColor: Colors.black,
        duration: Duration(seconds: 2),
        content: Container(
          child: CustomSnackBarForAddAll(
            message: 'Added all products $length',
          ),
        ),
      ));
    } else {
      context.loaderOverlay.hide();

      String productfailed = "\nFailed Products";
      for (int i = 0; i < errorProducts.length; i++) {
        productfailed += "\n" + errorProducts[i];
      }
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        padding: EdgeInsets.all(0),
        backgroundColor: Colors.black,
        duration: Duration(seconds: 10),
        content: Container(
          child: CustomSnackBarForAddAll(
            message:
                "$length Products Added and ${tiop.centralcolorleftmostselected.length - length} Failed to Added due to some error." +
                    productfailed,
          ),
        ),
      ));
    }
  }

  AddToCardm16(BuildContext context) async {
    String allSKUs = '';
    for (int i = 0; i < tiop.centralcolorleftmostselected.length; i++) {
      dynamic element = tiop.centralcolorleftmostselected[i];
      allSKUs += element.sku;
      if (i < tiop.centralcolorleftmostselected.length - 1) {
        allSKUs += ',';
      }
    }

    Future.delayed(Duration(seconds: 2), () async {
      log('==== list of get SKUs is $allSKUs ====');
      CartProvider cartP = Provider.of<CartProvider>(context, listen: false);

      try {
        await cartP.addToCartForListOfSKUs(
            context: context, listOfSKUs: allSKUs, refresh: true);
        Navigator.pop(context);

        Get.snackbar(
            'Successfully Added', 'All products successfully added to cart',
            margin: EdgeInsets.only(bottom: 20, left: 10, right: 10),
            snackPosition: SnackPosition.BOTTOM,
            snackStyle: SnackStyle.FLOATING,
            borderRadius: 8,
            backgroundColor: Colors.black,
            colorText: Colors.white,
            icon: Icon(
              Icons.check_circle,
              color: Colors.white,
            ));

        // Get.showSnackbar(
        //   GetSnackBar(
        //     message: 'User profile updated Successfully',
        //     duration: Duration(seconds: 2),
        //     backgroundColor: Colors.white,
        //   ),
        // );
        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        //   padding: EdgeInsets.all(0),
        //   backgroundColor: Colors.black,
        //   duration: Duration(seconds: 2),
        //   content: CustomSnackBarForAddAll(
        //     message: 'Added all products to cart',
        //   ),
        // ));
      } catch (e) {
        Navigator.pop(context);
        Get.snackbar(
            'Error in Adding Products', 'Sorry, but something went wrong',
            margin: EdgeInsets.only(bottom: 20, left: 10, right: 10),
            snackPosition: SnackPosition.BOTTOM,
            snackStyle: SnackStyle.FLOATING,
            borderRadius: 8,
            backgroundColor: Colors.black,
            colorText: Colors.white,
            icon: Icon(
              Icons.error,
              color: Colors.white,
            ));
      }
    });

    ///------ OLD
    // //tiop.showLoaderDialog(context);
    // Navigator.pop(context);
    // int length = 0;
    //
    // List errorProducts = [];
    // // selectedcontroller
    // //     .selectedProduct!.items?.forEach((element) async{
    // // selectedcontroller.tryMySelectionList.forEach((element) async {
    //
    // for (int i = 0; i < tiop.centralcolorleftmostselected.length; i++) {
    //   cPrint("ENTER 11 $length");
    //   dynamic element = tiop.centralcolorleftmostselected[i];
    //   CartProvider cartP = Provider.of<CartProvider>(context, listen: false);
    //   cPrint("ENTER 33 $length");
    //   cPrint("CartProvider  -->> SSs ${cartP.cartToken}");
    //   cPrint(tiop.centralcolorleftmostselected[i].name);
    //   try {
    //     context.loaderOverlay.show(
    //         widget: LoaderWithText(
    //           msg: "Adding " +
    //               element.name +
    //               "\n\n" +
    //               (tiop.centralcolorleftmostselected.length - length)
    //                   .toString() +
    //               " Remaining",
    //         ));
    //     await cartP.addToCartForAddAll(
    //         context, element.sku, [], 1, "", refresh: true);
    //     length++;
    //   } catch (e) {
    //     errorProducts.add(element.name);
    //     cPrint("TIOOL==>");
    //     cPrint(e);
    //   }
    //
    //   cPrint("ENTER 22 $length  total ${tiop.centralcolorleftmostselected
    //       .length}");
    // }
    //
    // if (length == tiop.centralcolorleftmostselected.length) {
    //   context.loaderOverlay.hide();
    //   // product snack bar
    //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //     padding: EdgeInsets.all(0),
    //     backgroundColor: Colors.black,
    //     duration: Duration(seconds: 2),
    //     content: Container(
    //       child: CustomSnackBarForAddAll(
    //         message: 'Added all products $length',
    //       ),
    //     ),
    //   ));
    // } else {
    //   context.loaderOverlay.hide();
    //   String productfailed = "\nFailed Products";
    //   for (int i = 0; i < errorProducts.length; i++) {
    //     productfailed += "\n" + errorProducts[i];
    //   }
    //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //     padding: EdgeInsets.all(0),
    //     backgroundColor: Colors.black,
    //     duration: Duration(seconds: 10),
    //     content: Container(
    //       child: CustomSnackBarForAddAll(
    //         message:
    //         "$length Products Added and ${tiop.centralcolorleftmostselected
    //             .length - length} Failed to Added due to some error." +
    //             productfailed,
    //       ),
    //     ),
    //   ));
    // }
  }

  addlooks(BuildContext context) async {
    Navigator.pop(context);
    context.loaderOverlay.show(
        widget: LoaderWithText(
      msg: "Adding Looks Products",
    ));
    try {
      if (tiop.lookname.value != "myselection" &&
          tiop.lookname.value != "m16") {
        LooksController looksController = Get.find();
        int skuindex = looksController.lookModel!.items!
            .indexWhere((element) => element.name == tiop.lookname.value);
        await Provider.of<CartProvider>(context, listen: false).addToCart(
            context,
            looksController.lookModel!.items![skuindex].sku!,
            [],
            1,
            tiop.lookname.value);
        context.loaderOverlay.hide();
      }
    } catch (e) {
      context.loaderOverlay.hide();

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        padding: EdgeInsets.all(0),
        backgroundColor: Colors.black,
        duration: Duration(seconds: 2),
        content: Container(
          child: CustomSnackBarForAddAll(
            message: 'Error in Adding Looks \n $e',
          ),
        ),
      ));
    }
  }

  AddToCarLooks(BuildContext context) async {
    //tiop.showLoaderDialog(context);
    Navigator.pop(context);
    int length = 0;

    List errorProducts = [];
    // selectedcontroller
    //     .selectedProduct!.items?.forEach((element) async{
    // selectedcontroller.tryMySelectionList.forEach((element) async {
    cPrint("DIAAML==>");
    cPrint(controller.ms8model!.itemData!);
    for (int i = 0; i < controller.ms8model!.itemData!.length; i++) {
      cPrint("ENTER 11 $length");
      ItemData element = controller.ms8model!.itemData![i];
      CartProvider cartP = Provider.of<CartProvider>(context, listen: false);
      cPrint("ENTER 33 $length");
      cPrint("CartProvider  -->> SSs ${cartP.cartToken}");
      try {
        context.loaderOverlay.show(
            widget: LoaderWithText(
          msg: "Adding " +
              element.name.toString() +
              "\n\n" +
              (controller.ms8model!.itemData!.length - length).toString() +
              " Remaining",
        ));
        await cartP.addToCartForAddAll(
            context, element.sku.toString(), [], 1, "",
            refresh: true);
        length++;
      } catch (e) {
        errorProducts.add(element.name);
        cPrint("TIOOL==>");
        cPrint(e);
      }

      cPrint(
          "ENTER 22 $length  total ${controller.ms8model!.itemData!.length}");
    }

    if (length == controller.ms8model!.itemData!.length) {
      context.loaderOverlay.hide();
      // product snack bar
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        padding: EdgeInsets.all(0),
        backgroundColor: Colors.black,
        duration: Duration(seconds: 2),
        content: Container(
          child: CustomSnackBarForAddAll(
            message: 'Added all products $length',
          ),
        ),
      ));
    } else {
      context.loaderOverlay.hide();
      String productfailed = "\nFailed Products";
      for (int i = 0; i < errorProducts.length; i++) {
        productfailed += "\n" + errorProducts[i];
      }
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        padding: EdgeInsets.all(0),
        backgroundColor: Colors.black,
        duration: Duration(seconds: 10),
        content: Container(
          child: CustomSnackBarForAddAll(
            message:
                "$length Products Added and ${controller.ms8model!.itemData!.length - length} Failed to Added due to some error." +
                    productfailed,
          ),
        ),
      ));
    }
  }

  Tryitonscan(Ms8Controller controller, BuildContext context) {
    cPrint("Tryitonscan 11");
    cPrint(
        "controller.ms8model!.itemData ${controller.ms8model!.itemData!.length}");
    int length = 0;
    controller.ms8model!.itemData!.forEach((element) async {
      CartProvider cartP = Provider.of<CartProvider>(context, listen: false);
      cPrint("CartProvider  -->> SSs ${cartP.cartToken}");
      await cartP.addHomeProductsToCart(
          context,
          Product(
              id: int.parse(element.entityId!),
              name: element.name,
              sku: element.sku,
              price: double.parse(element.price!),
              image: element.image!,
              description: element.description!,
              faceSubArea: int.parse(element.faceSubArea!),
              avgRating: "0"));
      length++;
      if (length == controller.ms8model?.itemData!.length) {
        Navigator.pop(context);
        Get.showSnackbar(
          GetSnackBar(
            message: 'Added all products $length',
            duration: Duration(seconds: 2),
            icon: Padding(
              padding: const EdgeInsets.all(8.0),
              child: CachedNetworkImage(
                imageUrl: APIEndPoints.mainBaseUrl +
                    "/media/catalog/product/cache/983707a945d60f44d700277fbf98de57\$image",
                // fit: BoxFit.cover,
                placeholder: (context, _) => Image.asset(
                  'assets/images/sofiqe-font-logo-2.png',
                ),
                errorWidget: (context, url, error) {
                  return ProductErrorImage(
                    width: 400,
                    height: 400,
                  );
                },
              ),
            ),
          ),
        );
      }
    });
  }
}

///---------Bottom Sheet Body
class BottomSheetBody extends StatefulWidget {
  final TryItOnProvider tiop;
  final Ms8Controller controller;
  final SelectedProductController selectefcontroller;

  BottomSheetBody(
      {Key? key,
      required this.tiop,
      required this.controller,
      required this.selectefcontroller})
      : super(key: key);

  @override
  State<BottomSheetBody> createState() => _BottomSheetBodyState();
}

class _BottomSheetBodyState extends State<BottomSheetBody> {
  @override
  void initState() {
    widget.selectefcontroller.isreplace.value = false;
    widget.selectefcontroller.isreplace_1.value = false;
    widget.selectefcontroller.temp1 = [];
    widget.selectefcontroller.value = [];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // cPrint("widget.tiop.lookname.value ${widget.tiop.lookname.value}");
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: Get.height * .07,
          ),
          Expanded(
              child: widget.tiop.lookname.value == "myselection"
                  ? LeftMostList(
                      tmo: widget.tiop,
                    )

                  //  BottomSheetItemList(
                  //     selectedcontroller: widget.selectefcontroller,
                  //     tmo: widget.tiop,
                  //   )
                  : widget.tiop.lookname.value == "m16"
                      ? LeftMostList(
                          tmo: widget.tiop,
                        )
                      : LookList(
                          tmo: widget.tiop,
                          controller: widget.controller,
                        )),
        ],
      ),
    );
  }
}

///-------------------------------------

///------Recommend Color Tabs
class BottomSheetTabs extends StatelessWidget {
  final TryItOnProvider tmo;

  const BottomSheetTabs({Key? key, required this.tmo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.05,
      width: size.width,
      decoration: BoxDecoration(),
      child: CentralColorSelector(tmo: tmo),
    );
  }
}

class CentralColorSelector extends StatelessWidget {
  final TryItOnProvider tmo;

  CentralColorSelector({Key? key, required this.tmo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // tmo.Lookname.value == "myselection" ? tmo.getCentralColors() : null;
    return Container(
      child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Obx(
            () {
              tmo.isSelectColorIndex.value = 1000;
              int code = tmo.currentSelectedArea.value;
              List<Colorss> recom = [];
              tmo.centralcolor.forEach((Colorss a) {
                recom.add(a);
              });
              return recom.isNotEmpty
                  ? Row(
                      children: [
                        ...recom.map(
                          (color) {
                            return TryOnColorChoice(
                              hex: color.colourAltHEX!,
                              name: color.colourAltName!,
                              code: code,
                            );
                          },
                        ).toList(),
                      ],
                    )
                  : Container();
            }
            //  }
            ,
          )),
    );
  }
}

class TryOnColorChoice extends StatelessWidget {
  final String hex;
  final String name;
  final int code;

  TryOnColorChoice(
      {Key? key, required this.hex, required this.name, required this.code})
      : super(key: key);

  final TryItOnProvider tmo = Get.find();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(onTap: () {
      cPrint("pressss  ${name}");
      cPrint(hex);
      cPrint(tmo.currentSelectedCentralColor);
      tmo.showSelected.value = true;
      tmo.isFirstCalling.value = true;
      tmo.currentSelectedcentralColorToggle(hex, name);
      tmo.isDefaultShadsColor.value = true;
    }, child: Obx(
      () {
        return Container(
          padding: EdgeInsets.symmetric(
              vertical: size.height * 0.01, horizontal: size.width * 0.01),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: tmo.currentSelectedCentralColor['name'] == name
                    ? AppColors.primaryColor
                    // ? Color.fromRGBO(242, 202, 138, 1)
                    : Colors.transparent,
                width: 1.5,
              ),
            ),
          ),
          child: Text(
            '$name'.toUpperCase(),
            style: Theme.of(context).textTheme.headline2!.copyWith(
                  color: Colors.black,
                  fontSize: size.height * 0.014,
                  letterSpacing: 0,
                ),
          ),
        );
      },
    ));
  }
}

///------------------------

///-----Bottom Sheet Items
class BottomSheetItemList extends StatefulWidget {
  final TryItOnProvider tmo;
  final SelectedProductController selectedcontroller;

  BottomSheetItemList(
      {Key? key, required this.selectedcontroller, required this.tmo})
      : super(key: key);

  @override
  State<BottomSheetItemList> createState() => _BottomSheetItemListState();
}

class _BottomSheetItemListState extends State<BottomSheetItemList> {
  bool isSelectedProductLoading = true;
  SelectedProduct? selectedProduct;
  List<GlobalKey> items = [];

  @override
  void initState() {
    super.initState();
    widget.selectedcontroller.IstryMySelection.value = true;
    widget.selectedcontroller.isreplace.value = false;
    widget.selectedcontroller.isreplace_1.value = false;
    widget.selectedcontroller.temp1 = [];
    widget.selectedcontroller.value = [];
    WidgetsBinding.instance.addPostFrameCallback((_) {
      widget.tmo.getuserfacecolor();
    });
  }

  Future scroll(int num) async {
    cPrint("EOS==> ${num}");
    final context = items[num].currentContext!;
    Scrollable.ensureVisible(context);
    scrollController.animateTo(
        //go to top of scroll
        0, //scroll offset to go
        duration: Duration(milliseconds: 500), //duration of scroll
        curve: Curves.fastOutSlowIn //scroll type
        );
  }

  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    cPrint("DTD==> ");
    return Obx(() {
      return widget.selectedcontroller.loadin.isTrue ||
              widget.tmo.centralLeftmostloading.isTrue
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // CircularProgressIndicator(),
                SpinKitDoubleBounce(
                  color: Color(0xffF2CA8A),
                  size: 30.0,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Please wait,\nwe are loading your previous selections",
                  style: Theme.of(context).textTheme.headline2!.copyWith(
                        color: Colors.black,
                        fontSize: 13,
                        letterSpacing: 0,
                      ),
                  textAlign: TextAlign.center,
                )
              ],
            ))
          : SingleChildScrollView(
              controller: scrollController,
              child: widget.selectedcontroller.selectedProduct != null
                  ? Obx(() {
                      List<Items> temp3 = [];
                      List<Items> temp1 = [];
                      List<Items> temp2 = [];
                      List<String> subeye = [];
                      List<String> subcheeks = [];
                      List<String> sublips = [];
                      widget.tmo.centralcolorleftmostselected
                          .forEach((element) {
                        for (int i = 0;
                            i <
                                widget.selectedcontroller.selectedProduct!
                                    .items!.length;
                            i++) {
                          Items? ele = widget
                              .selectedcontroller.selectedProduct!.items![i];
                          if (element.sku == ele.product!.sku) {
                            if (element.faceArea == "Eyes") {
                              int index =
                                  subeye.indexOf(ele.faceSubArea.toString());
                              if (index == -1) {
                                temp1.add(ele);
                                subeye.add(ele.faceSubArea.toString());
                                break;
                              }
                            } else if (element.faceArea == "Lips") {
                              int index =
                                  sublips.indexOf(ele.faceSubArea.toString());
                              if (index == -1) {
                                temp2.add(ele);
                                sublips.add(ele.faceSubArea.toString());
                                break;
                              }
                            } else if (element.faceArea == "Cheeks") {
                              int index =
                                  subcheeks.indexOf(ele.faceSubArea.toString());
                              if (index == -1) {
                                temp3.add(ele);
                                subcheeks.add(ele.faceSubArea.toString());
                                break;
                              }
                            }
                          }
                        }
                      });
                      if (widget.selectedcontroller.IstryMySelection.isTrue) {
                        widget.selectedcontroller.tryMySelectionList.value = [
                          ...temp1,
                          ...temp2,
                          ...temp3
                        ];
                        widget.selectedcontroller.IstryMySelection.value =
                            false;
                      }
                      if (widget.tmo.selectedFaceArea.value == "Eyes") {
                        widget.selectedcontroller.temp = temp1;
                      } else if (widget.tmo.selectedFaceArea.value == "Lips") {
                        widget.selectedcontroller.temp = temp2;
                      } else if (widget.tmo.selectedFaceArea.value ==
                          "Cheeks") {
                        widget.selectedcontroller.temp = temp3;
                      }
                      widget.selectedcontroller.temp.forEach((element) {
                        final name = new GlobalKey();
                        items.add(name);
                      });
                      cPrint((temp1.length + temp2.length + temp3.length)
                          .toString());
                      widget.tmo.count.value =
                          temp1.length + temp2.length + temp3.length;
                      return ObserveableProducts(
                        items: items,
                        scroll: scroll,
                        selectedcontroller: widget.selectedcontroller,
                        tmo: widget.tmo,
                        scrollController: scrollController,
                      );
                    })
                  : Center(
                      child: Text(
                        "No Products",
                        style: TextStyle(
                            fontSize: 18,
                            decoration: TextDecoration.none,
                            color: Colors.black),
                      ),
                    ),
            );
    });
  }
}

class ObserveableProducts extends StatelessWidget {
  final List<GlobalKey> items;
  final Function scroll;
  final TryItOnProvider tmo;
  final SelectedProductController selectedcontroller;
  final ScrollController scrollController;

  ObserveableProducts(
      {required this.items,
      required this.scroll,
      required this.selectedcontroller,
      required this.tmo,
      required this.scrollController});

  @override
  Widget build(BuildContext context) {
    return selectedcontroller.temp.length > 0 &&
            selectedcontroller.selectedProduct != null
        ? Obx(() {
            // cPrint("${selectedcontroller.temp.length}  ${tmo.count.value}");
            return Column(
                children: selectedcontroller.isreplace.value
                    ? selectedcontroller.value.map((item) {
                        // debugPrint(
                        //     '=== image of index ${selectedcontroller.temp.indexOf(item)}  :: ${item.product!.name}  ::  ${item.product!.image}   =====');
                        return Container(
                          child: TryOnLookProduct(
                            scrll: scroll,
                            index: selectedcontroller.selectedProduct!.items!
                                .indexOf(item),
                            index1: selectedcontroller.value.indexOf(item),
                            product: ItemData(
                              attributeSetId: item.product!.attributeSetId,
                              brand: item.product!.brand,
                              createdAt: item.product!.createdAt,
                              dealFromDate: item.product!.dealFromDate,
                              dealToDate: item.product!.dealToDate,
                              description: item.product!.description,
                              entityId: item.product!.entityId,
                              shadeColor: item.product!.shadeColor,
                              extensionAttributes: ExtensionAttributes(
                                  avgratings: item.product!.avgrating,
                                  rewardPoints:
                                      item.product!.rewardPoints.toString(),
                                  reviewCount: item.product!.reviewCount),
                              faceArea: item.faceSubArea,
                              faceColor: item.product!.faceColor,
                              faceSubArea: item.product!.faceSubArea,
                              price: item.product!.price,
                              hasOptions: item.product!.hasOptions,
                              image: item.product!.image,
                              ingredients: item.product!.ingredients,
                              sku: item.product!.sku,
                              name: item.product!.name,
                              recommendedColor: item.product!.shadeColor,
//item.product!.faceColor,
// recommendedColor: "#e0119d",
                              urlPath: item.product!.urlPath ?? "",
                            ),
                          ),
                        );
                      }).toList()
                    : selectedcontroller.isreplace_1.value
                        ? selectedcontroller.temp1.map((item) {
                            cPrint("Called on My Selection 1");

                            // debugPrint(
                            //     '+=== image of index ${selectedcontroller.temp.indexOf(item)}  :: ${item.product!.name}  ::  ${item.product!.image}   =====');
                            return Container(
                              // key: items[selectedcontroller.temp.indexOf(item)],
                              child: TryOnLookProduct(
                                scrll: scroll,
                                index: selectedcontroller
                                    .selectedProduct!.items!
                                    .indexOf(item),
                                index1: selectedcontroller.temp1.indexOf(item),
                                product: ItemData(
                                  attributeSetId: item.product!.attributeSetId,
                                  brand: item.product!.brand,
                                  createdAt: item.product!.createdAt,
                                  dealFromDate: item.product!.dealFromDate,
                                  dealToDate: item.product!.dealToDate,
                                  description: item.product!.description,
                                  entityId: item.product!.entityId,
                                  shadeColor: item.product!.shadeColor,
                                  extensionAttributes: ExtensionAttributes(
                                      avgratings: item.product!.avgrating,
                                      rewardPoints:
                                          item.product!.rewardPoints.toString(),
                                      reviewCount: item.product!.reviewCount),
                                  faceArea: item.faceSubArea,
                                  faceColor: item.product!.faceColor,
                                  faceSubArea: item.product!.faceSubArea,
                                  price: item.product!.price,
                                  hasOptions: item.product!.hasOptions,
                                  image: item.product!.image,
                                  ingredients: item.product!.ingredients,
                                  sku: item.product!.sku,
                                  name: item.product!.name,
                                  recommendedColor: item.product!.shadeColor,
//item.product!.faceColor,
// recommendedColor: "#e0119d",
                                  urlPath: item.product!.urlPath,
                                ),
                              ),
                            );
                          }).toList()
                        : selectedcontroller.temp.map((item) {
                            cPrint("Called on My Selection 1");

                            return Container(
                              // key: items[selectedcontroller.temp.indexOf(item)],
                              child: TryOnLookProduct(
                                scrll: scroll,
                                index: selectedcontroller
                                    .selectedProduct!.items!
                                    .indexOf(item),
                                index1: selectedcontroller.temp.indexOf(item),
                                product: ItemData(
                                  attributeSetId: item.product!.attributeSetId,
                                  brand: item.product!.brand,
                                  createdAt: item.product!.createdAt,
                                  dealFromDate: item.product!.dealFromDate,
                                  dealToDate: item.product!.dealToDate,
                                  description: item.product!.description,
                                  entityId: item.product!.entityId,
                                  shadeColor: item.product!.shadeColor,
                                  extensionAttributes: ExtensionAttributes(
                                      avgratings: item.product!.avgrating,
                                      rewardPoints:
                                          (item.product!.rewardPoints ?? 0.0)
                                              .toString(),
                                      reviewCount: item.product!.reviewCount),
                                  faceArea: item.faceSubArea,
                                  faceColor: item.product!.faceColor,
                                  faceSubArea: item.product!.faceSubArea,
                                  price: item.product!.price,
                                  hasOptions: item.product!.hasOptions,
                                  image: item.product!.image,
                                  ingredients: item.product!.ingredients,
                                  sku: item.product!.sku,
                                  name: item.product!.name,
                                  recommendedColor: item.product!.shadeColor,
                                  urlPath: item.product!.urlPath,
                                ),
                              ),
                            );
                          }).toList());
          })
        : Center(
            child: Text(
              "No Products",
              style: TextStyle(
                  fontSize: 18,
                  decoration: TextDecoration.none,
                  color: Colors.black),
            ),
          );
  }
}

///-------------------------

///---------Look List
class LookList extends StatefulWidget {
  final TryItOnProvider tmo;

  final Ms8Controller controller;

  LookList({Key? key, required this.controller, required this.tmo})
      : super(key: key);

  @override
  State<LookList> createState() => _LookListState();
}

class _LookListState extends State<LookList> {
  getdata() async {
    await widget.controller.getLookList(widget.tmo.lookname.value);
  }

  List<GlobalKey> items = [];

  @override
  void initState() {
    selectedProductController.isreplace.value = false;
    getdata();
    super.initState();
  }

  Future scroll(int num) async {
    final context = items[num].currentContext!;
    Scrollable.ensureVisible(context);
  }

  final SelectedProductController selectedProductController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return widget.tmo.lookloading.isTrue
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Swirl Text
                // CircularProgressIndicator(),
                SpinKitDoubleBounce(
                  color: Color(0xffF2CA8A),
                  size: 30.0,
                ),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Text(
                    "Please wait,\nwe are collecting your\npersonal colours",
                    style: Theme.of(context).textTheme.headline2!.copyWith(
                          color: Colors.black,
                          fontSize: 13,
                          letterSpacing: 0,
                        ),
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            ))
          : Padding(
              padding: EdgeInsets.only(right: 5.0),
              child: Scrollbar(
                  isAlwaysShown: true,
                  thickness: 5.0,
                  child: SingleChildScrollView(
                    child: Container(child: Obx(() {
                      widget.controller.ms8model?.itemData!.forEach((element) {
                        // cPrint("${element.faceArea} == ${widget.tmo.selectedFaceArea.value}");
                      });
                      List<ItemData>? temp = [];
                      temp = widget.controller.ms8model?.itemData!
                          .where((element) =>
                              element.faceArea ==
                              widget.tmo.selectedFaceArea.value)
                          .toList();
                      selectedProductController.temp = temp ?? [];
                      items = [];
                      (temp ?? []).forEach((element) {
                        final name = GlobalKey();
                        items.add(name);
                      });
                      widget.tmo.count.value =
                          widget.controller.ms8model?.itemData!.length ?? 0;

                      // cPrint('===== length of temp is ${items.length} ======');

                      return (temp ?? []).isNotEmpty
                          ? Column(
                              children: (temp ?? []).length < 1
                                  ? [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      SpinKitDoubleBounce(
                                        color: Color(0xffF2CA8A),
                                        size: 30.0,
                                      ),
                                      // CircularProgressIndicator(),
                                    ]
                                  : selectedProductController.isreplace.isFalse
                                      ? (temp ?? [])
                                          .map((item) => Container(
                                              key: items[temp!.indexOf(item)],
                                              child: TryOnLookProduct(
                                                index1: temp.indexOf(item),
                                                scrll: scroll,
                                                index: temp.indexOf(item),
                                                product: item,
                                              )))
                                          .toList()
                                      : selectedProductController.value
                                          .map((item) => Container(
                                              key: items[temp!.indexOf(item)],
                                              child: TryOnLookProduct(
                                                index1: temp.indexOf(item),
                                                scrll: scroll,
                                                index: temp.indexOf(item),
                                                product: item,
                                              )))
                                          .toList())
                          : Center(
                              child: Text(
                                "No Products",
                                style: TextStyle(
                                    fontSize: 20,
                                    decoration: TextDecoration.none,
                                    color: Colors.black),
                              ),
                            );
                    })),
                  )));
    });
  }
}

///-------------------------

class LeftMostList extends StatefulWidget {
  final TryItOnProvider tmo;

  LeftMostList({Key? key, required this.tmo}) : super(key: key);

  @override
  State<LeftMostList> createState() => _LeftMostListState();
}

class _LeftMostListState extends State<LeftMostList> {
  List<GlobalKey> items = [];
  List itemList = [];

// fetching Api Data
  getData() async {
    await widget.tmo.getuserfacecolor();
  }

  @override
  void initState() {
    super.initState();
    selectedProductController.isreplace.value = false;
    getData();
    getItem();
  }

  Future scroll(int num) async {
    scrollController.animateTo(
        //go to top of scroll
        0, //scroll offset to go
        duration: Duration(milliseconds: 500), //duration of scroll
        curve: Curves.fastOutSlowIn //scroll type
        );
    try {
      final context = itemList[num].currentContext!;
      Scrollable.ensureVisible(context);
    } catch (e) {}
  }

// fetching global key list
  getItem() async {
    itemList = await widget.tmo.items;
    if (widget.tmo.temp1.length > itemList.length) {
      widget.tmo.temp1.forEach((element) {
        final name = GlobalKey();
        itemList.add(name);
      });
    }
  }

  final SelectedProductController selectedProductController = Get.find();
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    Future.delayed(
        Duration(seconds: 2),
        () => Center(
              child: SpinKitDoubleBounce(
                color: Color(0xffF2CA8A),
                size: 30.0,
              ),
              // CircularProgressIndicator(),
            ));
    return Obx(() {
      List<FaceSubareaLeftmostListOfProducts> temp1 = [];
      // cPrint("LEST LEST ${widget.tmo.centralcolorleftmostselected.length}");
      cPrint("SUBAREA +++++++++++++");
      widget.tmo.centralcolorleftmostselected.forEach((element) {
        // cPrint(" widget. ${element.entityId}    element.faceArea${widget.tmo.selectedFaceArea.value} ${element.name} ");

        if (element.faceArea == widget.tmo.selectedFaceArea.value) {
          // cPrint("SUBAREA ${element.faceSubAreaName}");
          temp1.add(element);
        }
        // cPrint(temp1.length);
      });
      selectedProductController.temp = temp1;

      return widget.tmo.centralLeftmostloading.isTrue && temp1.isEmpty
          ? Center(
              child: SpinKitDoubleBounce(
                color: Color(0xffF2CA8A),
                size: 30.0,
              ),
              // CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.only(right: 5.0),
              child: Scrollbar(
                  isAlwaysShown: true,
                  thickness: 10.0,
                  child: SingleChildScrollView(
                    controller: scrollController,
                    child: Container(
                      child: temp1.isNotEmpty
                          ? Column(
                              children: selectedProductController
                                      .isreplace.value
                                  ? selectedProductController.value
                                      .map((item) => showProducts(
                                          item, selectedProductController.value.indexOf(item),
                                          valuecheck: "1"))
                                      .toList()
                                  : selectedProductController.isreplace_1.value
                                      ? selectedProductController.temp1
                                          .map((item) => showProducts(
                                              item,
                                              selectedProductController.temp1
                                                  .indexOf(item),
                                              valuecheck: "2"))
                                          .toList()
                                      : selectedProductController.temp
                                          .map((item) => showProducts(
                                              item, selectedProductController.temp.indexOf(item),
                                              valuecheck: "3"))
                                          .toList())
                          : Center(
                              child: Text(
                                "No Products",
                                style: TextStyle(
                                    fontSize: 20,
                                    decoration: TextDecoration.none,
                                    color: Colors.black),
                              ),
                            ),
                    ),
                  )));
    });
  }

  Widget showProducts(item, index, {String valuecheck = ""}) {
    cPrint("Values Check" + valuecheck);
    // cPrint("${(item as FaceSubareaLeftmostListOfProducts).name}");
    return GestureDetector(
      onTap: () {},
      child: Container(
          //key: itemList[temp1.indexOf(item)],
          child: TryOnLookProduct(
        index1: index,
        scrll: scroll,
        index: index,
        product: ItemData(
            attributeSetId: item.attributeSetId,
            sku: item.sku,
            urlPath: item.urlPath,
            name: item.name,
            faceArea: item.faceSubAreaName,
            keyName: item.faceSubAreaName,
            image: item.image,
            recommendedColor: item.faceColor,
            extensionAttributes: ExtensionAttributes(
                rewardPoints: "0", productUrl: "", avgratings: ""),
            description: item.description,
            price: item.price,
            entityId: item.entityId,
            faceColor: item.faceColor,
            shadeColor: item.shadeColor,
            faceSubArea: item.faceSubArea,
            AlternateColors: item.alternateColors),
      )),
    );
  }
}
