import 'dart:convert';
import 'dart:developer';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:sofiqe/controller/ms8Controller.dart';
import 'package:sofiqe/controller/selectedProductController.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/screens/my_sofiqe.dart';
import '../../model/model_for_color_order.dart';

class TryItOnColorSelector extends StatefulWidget {
  TryItOnColorSelector({Key? key}) : super(key: key);

  @override
  State<TryItOnColorSelector> createState() => _TryItOnColorSelectorState();
}

class _TryItOnColorSelectorState extends State<TryItOnColorSelector> {
  ScrollController controler = ScrollController();
  Ms8Controller lookcontroller = Get.find();
  SelectedProductController selectedcontroller = Get.find();
  final TryItOnProvider tmo = Get.find();
  CarouselController _carouselController = CarouselController();
  int isSelected = 0;
  // RxBool isMatch = false.obs;

  getdata() async {
    if (tmo.lookname.value == "myselection") {
      tmo.alternateColorsloading.value = false;
      // await tmo.getCentralColors();
    }

    if (tmo.lookname.value == "m16")
      await tmo.getAlternateColors(tmo.currentSelectedCentralColor['code'],
          tmo.selectedFaceArea.toString(), "5");
    print(tmo.alternateColors);
    print("object");
  }

  @override
  void initState() {
    super.initState();
    // getdata();
    // tmo.setSelectedToDefault();
    setValues();
    print("PCASHADES==>");
    print(tmo.alternateColors);
    controler = ScrollController();
  }

  setValues() {
    print("tmo.AlternateColors.first");
    print(TryItOnProvider.instance.currentSelectedColorTemp.value);
    print(TryItOnProvider.instance.currentSelectedColor.value);
    print(TryItOnProvider.instance.isLoadedAfterAddtoCart);
    tmo.isFirstCalling.value = true;
    if (TryItOnProvider.instance.currentSelectedColorTemp.value !=
        "") if (TryItOnProvider.instance.isLoadedAfterAddtoCart) {
      // TryItOnProvider.instance.isLoadedAfterAddtoCart.value = false;
      print("NLS==>");
    } else {
      print("NLS==> 2");

      TryItOnProvider.instance.currentSelectedColor.value =
          TryItOnProvider.instance.currentSelectedColorTemp.value;
    }
  }

  indexChecker(int index) {
    log('=======  second one is calling  =======');

    List colorList = tmo.alternateColors;

    int check = colorList
        .indexWhere((element) => element == tmo.currentSelectedColor.value);

    log('=======  second one is calling  =======' + check.toString());
    tmo.matchColorIndex.value = check;
    if (check == -1) {
      tmo.showSelected.value = true;
      // return -1;
    } else {
      if (index == check) {
        log('======= yes now both are same =====');
        tmo.showSelected.value = false;
        log('======= yes now both are same value is ${tmo.showSelected.value} =====');
      } else {
        log('======= no same :: value is ${tmo.showSelected.value}  =====');
        tmo.showSelected.value = true;
      }
      log('=====  color index is $check   ======');
      // return check;
    }
  }

  int intialIndex = 0;
  int returnIndexIfAvailable() {
    if (tmo.isFirstCalling.isTrue) {
      tmo.isFirstCalling.value = false;
      log('=======  first one is calling  =======');
      List colorList = tmo.alternateColors;
      int check = colorList.indexWhere(
          (element) => element == tmo.currentSelectedColorTemp.value);
      tmo.matchColorIndex.value = check;
      if (check == -1) {
        tmo.outerIndex.value = 0;
        indexChecker(0);
        intialIndex = 0;
        return check;
      } else {
        tmo.outerIndex.value = check;
        indexChecker(check);
        intialIndex = check;
        return check;
      }
    } else {
      return intialIndex;
    }
  }

  @override
  Widget build(BuildContext context) {
    // print("tmo.AlternateColors.first");
    // print(TryItOnProvider.instance.currentSelectedColorTemp.value);
    // print(TryItOnProvider.instance.currentSelectedColor.value);
    // print(TryItOnProvider.instance.isLoadedAfterAddtoCart);
    // tmo.isFirstCalling.value = true;
    // if (TryItOnProvider.instance.currentSelectedColorTemp.value != "") if (TryItOnProvider
    //     .instance.isLoadedAfterAddtoCart) {
    //   // TryItOnProvider.instance.isLoadedAfterAddtoCart.value = false;
    //   print("NLS==>");
    // } else {
    //   print("NLS==> 2");

    //   TryItOnProvider.instance.currentSelectedColor.value = TryItOnProvider.instance.currentSelectedColorTemp.value;
    // }

    // print(tmo.alternateColors);
    Size size = MediaQuery.of(context).size;
    return Obx(() {
      List colorList = tmo.alternateColors;

      // List colorList2 = tmo.alternateColors2;
      // int outerIndex = -2;
      return SizedBox(
        child: Center(
          child: Row(
            children: [
              GestureDetector(
                onTap: () {
                  // isMatch.value = true;
                  _carouselController.previousPage();
                },
                child: Icon(Icons.arrow_back_ios_new_rounded,
                    color: Colors.black, size: 16),
              ),
              Container(
                constraints: BoxConstraints(
                  maxHeight: size.height * 0.08 +
                      size.height * 0.006 +
                      size.width * 0.01,
                  maxWidth: size.width / 2.3,
                ),
                width: size.width * 0.6 - 42,
                margin: EdgeInsets.symmetric(
                  vertical: size.height * 0.01,
                ),
                padding: EdgeInsets.symmetric(
                    horizontal: 4, vertical: size.width * 0.01),
                // color: Color.fromARGB(255, 255, 255, 255),
                child: tmo.alternateColorsloading.value == true
                    ? Center(
                        child: SpinKitDoubleBounce(
                          color: Color(0xffF2CA8A),
                          size: 35.0,
                        ),
                      )
                    : colorList.isNotEmpty
                        ? Stack(
                            alignment: Alignment.center,
                            children: [
                              CarouselSlider(
                                  carouselController: _carouselController,
                                  options: CarouselOptions(
                                    height: 400,
                                    viewportFraction: 0.33,
                                    initialPage: returnIndexIfAvailable() == -1
                                        ? 0
                                        : returnIndexIfAvailable(),
                                    enableInfiniteScroll: false,
                                    reverse: false,
                                    autoPlay: false,
                                    autoPlayInterval: Duration(seconds: 3),
                                    autoPlayAnimationDuration:
                                        Duration(milliseconds: 800),
                                    autoPlayCurve: Curves.fastOutSlowIn,
                                    enlargeCenterPage: false,
                                    enlargeFactor: 0.3,
                                    scrollDirection: Axis.horizontal,
                                    onPageChanged: (index, reason) {
                                      print("Change from Carasol");
                                      print(colorList.length);
                                      print(tmo.currentSelectedArea);

                                      // // setState(() {
                                      tmo.outerIndex.value = index;
                                      indexChecker(index);
                                      print(TryItOnProvider
                                          .instance.lookname.value);

                                      TryItOnProvider
                                              .instance.alternateColorsProducts[
                                          TryItOnProvider
                                              .instance.outerIndex.value];
                                      TryItOnProvider.instance
                                          .changeProductatProductsPage(
                                              index, colorList[index]);
                                    },
                                  ),
                                  items:
                                      List.generate(colorList.length, (index) {
                                    return GestureDetector(
                                      onTap: () {
                                        // isSelected = index;
                                        print("Change from Carasol 33");
                                        print(colorList.length);
                                        print(tmo.currentSelectedArea);

                                        // // setState(() {
                                        tmo.outerIndex.value = index;
                                        indexChecker(index);

                                        TryItOnProvider.instance
                                                .alternateColorsProducts[
                                            TryItOnProvider
                                                .instance.outerIndex.value];
                                        TryItOnProvider.instance
                                            .changeProductatProductsPage(
                                                index, colorList[index]);

                                        _carouselController
                                            .animateToPage(index);
                                      },
                                      child: Column(
                                        children: [
                                          index == 0
                                              ? Text(
                                                  // (tmo.matchColorIndex.value ==
                                                  //             tmo.outerIndex
                                                  //                 .value &&
                                                  //         tmo.currentSelectedColor
                                                  //                 .value ==
                                                  //             colorList[tmo
                                                  //                 .outerIndex
                                                  //                 .value])
                                                  tmo.outerIndex.value == 0
                                                      ? 'MATCH'
                                                      : "",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline2!
                                                      .copyWith(
                                                        color: Colors.black,
                                                        fontSize:
                                                            size.height * 0.011,
                                                        letterSpacing: 0,
                                                      ),
                                                )
                                              // tmo.currentSelectedColor.value ==
                                              //         colorList[index]
                                              //     ? Text(
                                              // (tmo.matchColorIndex.value ==
                                              //             tmo.outerIndex
                                              //                 .value &&
                                              //         tmo.currentSelectedColor
                                              //                 .value ==
                                              //             colorList[tmo
                                              //                 .outerIndex
                                              //                 .value])
                                              //             ? 'MATCH'
                                              //             : "",
                                              //         style: Theme.of(context)
                                              //             .textTheme
                                              //             .headline2!
                                              //             .copyWith(
                                              //               color: Colors.black,
                                              //               fontSize:
                                              //                   size.height * 0.011,
                                              //               letterSpacing: 0,
                                              //             ),
                                              //       )
                                              // )
                                              : SizedBox(
                                                  height: size.height * 0.013,
                                                  width: size.width / 7.05,
                                                ),
                                          // Container(
                                          //   height: size.height * 0.06,
                                          //   width: size.height * 0.06,
                                          //   decoration: BoxDecoration(
                                          //     color: HexColor(colorList[index]),
                                          //     border: tmo.currentSelectedColor
                                          //                 .value ==
                                          //             colorList[index]
                                          //         ? Border.all(
                                          //             width: 2,
                                          //             color: Colors.pink)
                                          //         : Border.all(
                                          //             // width: tmo.isSelectColorIndex.value == index ? 2 : 0,
                                          //             //      color: tmo.isSelectColorIndex.value == index ? Colors.black : Colors.white
                                          //             width: .1,
                                          //             color: Colors.white),
                                          //   ),
                                          // ),
                                          Container(
                                            height: size.height * 0.06,
                                            width: size.height * 0.06,
                                            decoration: BoxDecoration(
                                              color: HexColor(colorList[index]),
                                              border: index == 0
                                                  ? Border.all(
                                                      width: 2,
                                                      color: Colors.pink)
                                                  : Border.all(
                                                      // width: tmo.isSelectColorIndex.value == index ? 2 : 0,
                                                      //      color: tmo.isSelectColorIndex.value == index ? Colors.black : Colors.white
                                                      width: .1,
                                                      color: Colors.white),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  })),
                              // (tmo.matchColorIndex.value !=
                              //             tmo.outerIndex.value &&
                              //         tmo.currentSelectedColor.value !=
                              //             colorList[tmo.outerIndex.value])
                              //     ? Container(
                              //         child: Column(
                              //           children: [
                              //             Text(
                              //               'SELECTED',
                              //               style: Theme.of(context)
                              //                   .textTheme
                              //                   .headline2!
                              //                   .copyWith(
                              //                     color: Colors.black,
                              //                     fontSize: size.height * 0.011,
                              //                     letterSpacing: 0,
                              //                   ),
                              //             ),
                              //             Container(
                              //               height: size.height * 0.06,
                              //               width: size.height * 0.06,
                              //               decoration: BoxDecoration(
                              //                 color: Colors.transparent,
                              //                 border: Border.all(
                              //                   width: 2,
                              //                   color: Colors.black,
                              //                 ),
                              //               ),
                              //             ),
                              //           ],
                              //         ),
                              //       )
                              //     : const SizedBox(),
                              // tmo.matchColorIndex.value !=
                              //             tmo.outerIndex.value &&
                              //         tmo.currentSelectedColor.value !=
                              //             colorList[tmo.outerIndex.value])
                              //     ?

                              // ?

                              tmo.outerIndex.value != 0
                                  ? Container(
                                      child: Column(
                                        children: [
                                          Text(
                                            'SELECTED',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline2!
                                                .copyWith(
                                                  color: Colors.black,
                                                  fontSize: size.height * 0.011,
                                                  letterSpacing: 0,
                                                ),
                                          ),
                                          Container(
                                            height: size.height * 0.06,
                                            width: size.height * 0.06,
                                            decoration: BoxDecoration(
                                              color: Colors.transparent,
                                              border: Border.all(
                                                width: 2,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  : const SizedBox(),
                            ],
                          )
                        : Container(
                            height: size.height * 0.05 - 8,
                            alignment: Alignment.center,
                            child: Text(
                              "No Colors",
                              style: TextStyle(
                                  fontSize: 10,
                                  decoration: TextDecoration.none,
                                  color: Colors.black),
                              textAlign: TextAlign.center,
                            )),
              ),
              GestureDetector(
                onTap: () {
                  _carouselController.nextPage();
                  // isMatch.value = true;
                },
                child: Icon(Icons.arrow_forward_ios_rounded,
                    color: Colors.black, size: 16),
              ),
            ],
          ),
        ),
      );

      // : Container();
    });
  }
}
