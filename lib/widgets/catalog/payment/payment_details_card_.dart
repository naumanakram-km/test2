// ignore_for_file: avoid_renaming_method_parameters

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:loader_overlay/loader_overlay.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/controller/msProfileController.dart';
import 'package:sofiqe/provider/cart_provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/widgets/custom_white_cards.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../config.dart';
import '../../../screens/Ms1/privacy_policy.dart';
import '../../../utils/constants/api_end_points.dart';
import '../../../utils/constants/app_colors.dart';
import '../checkout/checkout_page.dart';

class PaymentDetailsPageCard extends StatefulWidget {
  final void Function(Map<String, dynamic>) callback;
  final String changeAddress;

  PaymentDetailsPageCard(this.changeAddress, {Key? key, required this.callback}) : super(key: key);

  @override
  State<PaymentDetailsPageCard> createState() => _PaymentDetailsPageState();
}

class _PaymentDetailsPageState extends State<PaymentDetailsPageCard> {
  MsProfileController _ = Get.find<MsProfileController>();
  List cardList = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (Provider.of<CartProvider>(context, listen: false).isLoggedIn) {
        getData();
      } else {
        setState(() {
          loading_data = false;
        });
      }
    });
  }

  @override
  void dispose() {
    // controller.removeListener(update);
    controller.dispose();
    super.dispose();
  }

  dynamic defualtPaymentmethod = {};
  List listCustomerCards = [];
  List listpaymentMethod = [];
  getData() async {
    try {
      setState(() {
        loading_data = true;
      });
      // context.loaderOverlay.show();
      List listCustomerCardsTemp = await _.getCustomersavedcards();
      dynamic obj = getpaymentMethod(listCustomerCardsTemp, selectedIndex);
      // context.loaderOverlay.hide();

      setState(() {
        loading_data = false;
        listCustomerCards = listCustomerCardsTemp;
        selectedPaymentMethod = obj;
        if (listCustomerCardsTemp.length > 0) {
          showScreen = 1;
          listpaymentMethod = listCustomerCardsTemp[0]["payment_method"] ?? [];
        }
      });
      context.loaderOverlay.hide();
    } catch (e) {
      context.loaderOverlay.hide();
    }
  }

  getpaymentMethod(List list, int index) {
    if (list.length > 0) {
      cPrint("getpaymentmethod");
      cPrint(list[index]);
      cPrint(list[index]["payment_method"] ?? []);
      List methods = list[index]["payment_method"] ?? [];
      if (methods.length > 0) {
        return methods[0];
      }
    }
    return {};
  }

  dynamic selectedPaymentMethod = {};

  bool loading_data = false;

  bool showCard = false;
  bool storeCard = true;
  bool setDefault = true;
  String saveCardInformation = "on";
  String saveCreditInformation = "on";
  String billingInformation = "on";
  var height;
  var isLoggedIn;
  String value = "";
  final controller = CardFormEditController();

  int selectedIndex = 0;

  int showScreen = 0;

  @override
  Widget build(BuildContext context) {
    final TryItOnProvider tiop = Get.find();
    Size size = MediaQuery.of(context).size;
    height = size.height;
    isLoggedIn = Provider.of<CartProvider>(context).isLoggedIn;

    return loading_data
        ? Container()
        : SingleChildScrollView(
            child:Container(
                height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
                child: showScreen == 0
                    ? Column(
                        children: [
                          if (listpaymentMethod.length > 0)
                            SizedBox(
                              height: 10,
                            ),
                          if (listpaymentMethod.length > 0)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 20),
                                  child: ElevatedButton(
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                      ),
                                      foregroundColor: MaterialStateProperty.all(AppColors.navigationBarSelectedColor),
                                      backgroundColor: MaterialStateProperty.all(
                                        AppColors.questionCardBackgroundColor,
                                      ),
                                      overlayColor: MaterialStateProperty.resolveWith<Color?>(
                                        (Set<MaterialState> states) {
                                          if (states.contains(MaterialState.pressed))
                                            return tiop.ontapColor; //<-- SEE HERE
                                          return null; // Defer to the widget's default.
                                        },
                                      ),
                                    ),
                                    onPressed: () async {
                                      tiop.isChangeButtonColor.value = true;
                                      tiop.playSound();
                                      Future.delayed(Duration(milliseconds: 10)).then((value) {
                                        tiop.isChangeButtonColor.value = false;
                                        SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
                                        setState(() {
                                          showScreen = 1;
                                        });
                                      });
                                    },
                                    child: Text(
                                      'Use Existing',
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context).textTheme.headline2!.copyWith(
                                            color: AppColors.primaryColor,
                                            fontSize: 14,
                                            letterSpacing: 0.8,
                                          ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          SizedBox(
                            height: 10,
                          ),
                          CardFormField(
                            controller: controller,
                            onCardChanged: (details) {
                              setState(() {});
                            },
                          ),
                          CustomWhiteCards(
                            padding: EdgeInsets.symmetric(vertical: 25, horizontal: 30),
                            child: [
                              Center(
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                    ),
                                    backgroundColor: MaterialStateProperty.all(Colors.black),
                                    overlayColor: MaterialStateProperty.resolveWith<Color?>(
                                      (Set<MaterialState> states) {
                                        if (states.contains(MaterialState.pressed))
                                          return tiop.ontapColor; //<-- SEE HERE
                                        return null; // Defer to the widget's default.
                                      },
                                    ),
                                  ),
                                  onPressed: () async {
                                    tiop.isChangeButtonColor.value = true;
                                    tiop.playSound();
                                    Future.delayed(Duration(milliseconds: 10)).then((value) {
                                      tiop.isChangeButtonColor.value = false;
                                      SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
                                      FocusManager.instance.primaryFocus?.unfocus();
                                      if (controller.details.complete) {
                                        context.loaderOverlay.show();
                                        _handlePayPress().then((value) {
                                          cPrint("Brijesh");
                                        });
                                      } else {
                                        controller.details.printError();
                                        cPrint(controller.details.toJson());
                                        Fluttertoast.showToast(
                                            msg: "Some details in the cart are incorrect/incomplete");
                                      }
                                    });
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 110, vertical: 20),
                                    child: Text(
                                      'PAY',
                                      style: Theme.of(context).textTheme.headline2!.copyWith(
                                            color: Colors.white,
                                            fontSize: height * 0.02,
                                            letterSpacing: 0.7,
                                            fontWeight: FontWeight.bold,
                                          ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 22),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "By continuing you accept Sofiqe's ",
                                    style: Theme.of(context).textTheme.headline2!.copyWith(
                                          color: Colors.black,
                                          fontSize: 10,
                                          letterSpacing: 0.4,
                                        ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Get.to(() => PrivacyPolicyScreen(
                                            isTerm: true,
                                            isReturnPolicy: false,
                                          ));
                                    },
                                    child: Text(
                                      'terms and condition',
                                      style: Theme.of(context).textTheme.headline2!.copyWith(
                                            decoration: TextDecoration.underline,
                                            color: Colors.black,
                                            fontSize: 10,
                                            letterSpacing: 0.4,
                                          ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          // Container(color: Colors.white),
                          // ResponseCard(
                          //   response: controller.details.toJson().toString(),
                          // ),
                        ],
                      )
                    : (showScreen == 1)
                        ? Column(
                            children: [
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 20),
                                child: Align(alignment: Alignment.centerLeft, child: Text("Payment Card")),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'For managing the cards login to your account on the website here: ',
                                    style: Theme.of(context).textTheme.headline2!.copyWith(
                                          color: Colors.black,
                                          fontSize: 10,
                                          letterSpacing: 0.4,
                                        ),
                                  ),
                                  GestureDetector(
                                    onTap: () async {
                                      var url = APIEndPoints.mainBaseUrl;
                                      if (await canLaunchUrl(Uri.parse(url))) {
                                        await launchUrl(Uri.parse(url));
                                      } else {
                                        throw 'Could not launch $url';
                                      }
                                    },
                                    child: Text(
                                      APIEndPoints.mainBaseUrl,
                                      style: Theme.of(context).textTheme.headline2!.copyWith(
                                            decoration: TextDecoration.underline,
                                            color: Colors.black,
                                            fontSize: 10,
                                            letterSpacing: 0.4,
                                          ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                padding: EdgeInsets.all(8),
                                margin: EdgeInsets.symmetric(horizontal: 20),
                                decoration:
                                    BoxDecoration(border: Border.all(), borderRadius: BorderRadius.circular(10)),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              Text(
                                                "Card: ",
                                                style: TextStyle(fontSize: 18),
                                              ),
                                              Text(
                                                ((selectedPaymentMethod["card"] ?? {})["brand"] ?? "")
                                                    .toString()
                                                    .toUpperCase(),
                                                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                                              ),
                                              Text(
                                                "**** **** **** " +
                                                    ((selectedPaymentMethod["card"] ?? {})["last4"] ?? "").toString(),
                                                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                                              )
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                child: Text(
                                                  (((selectedPaymentMethod["billing_address"] ?? {})["address"] ??
                                                                  {})["line1"] ??
                                                              "")
                                                          .toString() +
                                                      " " +
                                                      (((selectedPaymentMethod["billing_address"] ?? {})["address"] ??
                                                                  {})["line2"] ??
                                                              "")
                                                          .toString() +
                                                      " " +
                                                      (((selectedPaymentMethod["billing_address"] ?? {})["address"] ??
                                                                  {})["city"] ??
                                                              "")
                                                          .toString() +
                                                      " " +
                                                      (((selectedPaymentMethod["billing_address"] ?? {})["address"] ??
                                                                  {})["country"] ??
                                                              "")
                                                          .toString(),
                                                  style: TextStyle(fontSize: 14, height: 1.5),
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                    IconButton(
                                        onPressed: () {
                                          setState(() {
                                            showScreen = 2;
                                          });
                                        },
                                        icon: Icon(Icons.arrow_forward_ios))
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              CustomWhiteCards(
                                padding: EdgeInsets.symmetric(vertical: 25, horizontal: 30),
                                child: [
                                  Center(
                                    child: ElevatedButton(
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all(
                                          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                        ),
                                        backgroundColor: MaterialStateProperty.all(Colors.black),
                                        overlayColor: MaterialStateProperty.resolveWith<Color?>(
                                          (Set<MaterialState> states) {
                                            if (states.contains(MaterialState.pressed))
                                              return tiop.ontapColor; //<-- SEE HERE
                                            return null; // Defer to the widget's default.
                                          },
                                        ),
                                      ),
                                      onPressed: () async {
                                        tiop.isChangeButtonColor.value = true;
                                        tiop.playSound();
                                        Future.delayed(Duration(milliseconds: 10)).then((value) {
                                          tiop.isChangeButtonColor.value = false;
                                          SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
                                          FocusManager.instance.primaryFocus?.unfocus();
                                          cPrint("PMID==>");
                                          cPrint(selectedPaymentMethod["id"]);
                                          if (selectedPaymentMethod["id"] != "" &&
                                              selectedPaymentMethod["id"] != null) {
                                            widget.callback({
                                              "card": {"id": selectedPaymentMethod["id"]},
                                              "method": "stripe-card"
                                            });
                                            Navigator.pop(context);
                                          } else {
                                            Fluttertoast.showToast(msg: "Invalid payment method Please select another");
                                          }
                                        });
                                      },
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(horizontal: 110, vertical: 20),
                                        child: Text(
                                          'PAY',
                                          style: Theme.of(context).textTheme.headline2!.copyWith(
                                                color: Colors.white,
                                                fontSize: height * 0.02,
                                                letterSpacing: 0.7,
                                                fontWeight: FontWeight.bold,
                                              ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 22),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "By continuing you accept Sofiqe's ",
                                        style: Theme.of(context).textTheme.headline2!.copyWith(
                                              color: Colors.black,
                                              fontSize: 10,
                                              letterSpacing: 0.4,
                                            ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Get.to(() => PrivacyPolicyScreen(
                                                isTerm: true,
                                                isReturnPolicy: false,
                                              ));
                                        },
                                        child: Text(
                                          'terms and condition',
                                          style: Theme.of(context).textTheme.headline2!.copyWith(
                                                decoration: TextDecoration.underline,
                                                color: Colors.black,
                                                fontSize: 10,
                                                letterSpacing: 0.4,
                                              ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          )
                        : Container(
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.symmetric(horizontal: 20),
                                      child: ElevatedButton(
                                        style: ButtonStyle(
                                          shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                          ),
                                          foregroundColor:
                                              MaterialStateProperty.all(AppColors.navigationBarSelectedColor),
                                          backgroundColor: MaterialStateProperty.all(
                                            AppColors.questionCardBackgroundColor,
                                          ),
                                          overlayColor: MaterialStateProperty.resolveWith<Color?>(
                                            (Set<MaterialState> states) {
                                              if (states.contains(MaterialState.pressed))
                                                return tiop.ontapColor; //<-- SEE HERE
                                              return null; // Defer to the widget's default.
                                            },
                                          ),
                                        ),
                                        onPressed: () async {
                                          tiop.isChangeButtonColor.value = true;
                                          tiop.playSound();
                                          Future.delayed(Duration(milliseconds: 10)).then((value) {
                                            tiop.isChangeButtonColor.value = false;
                                            SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
                                            setState(() {
                                              showScreen = 0;
                                            });
                                          });
                                        },
                                        child: Text(
                                          'Add New Card',
                                          textAlign: TextAlign.center,
                                          style: Theme.of(context).textTheme.headline2!.copyWith(
                                                color: AppColors.primaryColor,
                                                fontSize: 13,
                                                letterSpacing: 0.8,
                                              ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Expanded(
                                  child: ListView.builder(
                                      itemCount: listpaymentMethod.length,
                                      itemBuilder: (context, index) {
                                        dynamic selectedPaymentMethodList = listpaymentMethod[index];
                                        return Container(
                                          padding: EdgeInsets.all(8),
                                          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 2),
                                          decoration: BoxDecoration(
                                              border: Border.all(), borderRadius: BorderRadius.circular(10)),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Text(
                                                          "Card: ",
                                                          style: TextStyle(fontSize: 18),
                                                        ),
                                                        Text(
                                                          ((selectedPaymentMethodList["card"] ?? {})["brand"] ?? "")
                                                              .toString()
                                                              .toUpperCase(),
                                                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                                                        ),
                                                        Text(
                                                          "**** **** **** " +
                                                              ((selectedPaymentMethodList["card"] ?? {})["last4"] ?? "")
                                                                  .toString(),
                                                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                                                        )
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        Expanded(
                                                          child: Text(
                                                            (((selectedPaymentMethodList["billing_address"] ??
                                                                                {})["address"] ??
                                                                            {})["line1"] ??
                                                                        "")
                                                                    .toString() +
                                                                " " +
                                                                (((selectedPaymentMethodList["billing_address"] ??
                                                                                {})["address"] ??
                                                                            {})["line2"] ??
                                                                        "")
                                                                    .toString() +
                                                                " " +
                                                                (((selectedPaymentMethodList["billing_address"] ??
                                                                                {})["address"] ??
                                                                            {})["city"] ??
                                                                        "")
                                                                    .toString() +
                                                                " " +
                                                                (((selectedPaymentMethodList["billing_address"] ??
                                                                                {})["address"] ??
                                                                            {})["country"] ??
                                                                        "")
                                                                    .toString(),
                                                            style: TextStyle(fontSize: 14, height: 1.5),
                                                            maxLines: 1,
                                                            overflow: TextOverflow.ellipsis,
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                              IconButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      selectedPaymentMethod = selectedPaymentMethodList;
                                                      showScreen = 1;
                                                    });
                                                  },
                                                  icon: Icon(Icons.radio_button_off))
                                            ],
                                          ),
                                        );
                                      }),
                                )
                              ],
                            ),
                          )),
          );
  }

Future<void> _handlePayPress() async {
    try {
      cPrint('click 3');

      final billingDetails = BillingDetails(
        address: Address(
          city: null,
          country: null,
          line1: null,
          line2: null,
          state: null,
          postalCode: null,
        ),
      );

      // 2. Create payment method
      final paymentMethod = await Stripe.instance.createPaymentMethod(
        params: PaymentMethodParams.card(
          paymentMethodData: PaymentMethodData(
            billingDetails: billingDetails,
          ),
        ),
      );
      cPrint("payment =====id===${paymentMethod.id}");
      context.loaderOverlay.hide();
      widget.callback({
        "card": {"id": paymentMethod.id},
        "method": "stripe-card"
      });
      Navigator.pop(context);
    } catch (e) {
      context.loaderOverlay.hide();

      Fluttertoast.showToast(msg: "Order Failed $e ");
    }

    // widget.callback.call({"id": paymentMethod.id});
  }

  CardFieldInputDetails? card;

  Future<void> confirmIntent(String paymentIntentId) async {
    final result = await callNoWebhookPayEndpointIntentId(paymentIntentId: paymentIntentId);
    if (result['error'] != null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Error: ${result['error']}')));
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Success!: The payment was confirmed successfully!')));
    }
  }

  Future<Map<String, dynamic>> callNoWebhookPayEndpointIntentId({
    required String paymentIntentId,
  }) async {
    final url = Uri.parse('$kApiUrl');
    final response = await http.post(
      url,
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode({'paymentIntentId': paymentIntentId}),
    );
    return json.decode(response.body);
  }

  Future<Map<String, dynamic>> callNoWebhookPayEndpointMethodId({
    required bool useStripeSdk,
    required String paymentMethodId,
    required String currency,
    List<String>? items,
  }) async {
    final url = Uri.parse('$kApiUrl');
    final response = await http.post(
      url,
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(
          {'useStripeSdk': useStripeSdk, 'paymentMethodId': paymentMethodId, 'currency': currency, 'items': items}),
    );
    return json.decode(response.body);
  }

  bool isUpdate = true;

  final String salesPolicyUrl = APIEndPoints.mainBaseUrl + '/terms-and-conditions';

  // void _launchURL() async {
  //   await launchUrl(Uri.parse(salesPolicyUrl));
  // }

  Widget coverWidgetWithPadding({Widget? child}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: child,
    );
  }

  Widget displayGenderContainer({required String image, required String title}) {
    return InkWell(
      onTap: () {
        _.selectedGender = title;
      },
      child: Container(
        height: 65,
        width: 58,
        decoration: BoxDecoration(
          border: (title == _.selectedGender) ? Border(bottom: BorderSide(color: Color(0xffF2CA8A))) : null,
          color: (title == _.selectedGender) ? Color(0xffF4F2F0) : Colors.white,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              'assets/images/$image',
              height: 28,
              width: 15,
            ),
            Text(
              title,
              style: TextStyle(fontSize: 8, color: Colors.black),
            )
          ],
        ),
      ),
    );
  }

  bool checkEmpty() {
    if (_.isShiping.value) {
      if (_.nameController.value.text.isEmpty ||
          _.countryController.value.text.isEmpty ||
          _.streetController.value.text.isEmpty ||
          _.postCodeController.value.text.isEmpty ||
          _.cityController.value.text.isEmpty ||
          _.phoneController.value.text.isEmpty) {
        return true;
      } else {
        return false;
      }
    } else {
      if (_.nameController.value.text.isEmpty ||
          _.countryController.value.text.isEmpty ||
          _.streetController.value.text.isEmpty ||
          _.postCodeController.value.text.isEmpty ||
          _.cityController.value.text.isEmpty ||
          _.phoneController.value.text.isEmpty ||
          _.billingNameController.value.text.isEmpty ||
          _.billingCountryController.value.text.isEmpty ||
          _.billingStreetController.value.text.isEmpty ||
          _.billingPostZipController.value.text.isEmpty ||
          _.billingCityController.value.text.isEmpty ||
          _.billingPhoneController.value.text.isEmpty) {
        return true;
      } else {
        return false;
      }
    }
  }

  displayTextFieldContainer(
      {String? title,
      TextEditingController? controller,
      Color? backgroundColor,
      String? prefix,
      TextInputType? textInputType,
      String? hint}) {
    return Container(
      height: 66,
      color: backgroundColor,
      padding: EdgeInsets.only(top: 5),
      child: coverWidgetWithPadding(
          child: Column(
        children: [
          Row(
            children: [
              Row(
                children: [
                  Text(
                    title!,
                    style: TextStyle(fontSize: 11, color: Colors.black),
                  ),
                  Text(
                    ' *',
                    style: TextStyle(fontSize: 11, color: Colors.red),
                  ),
                ],
              ),
            ],
          ),
          TextFormField(
            validator: (str) {
              if (str == '' || str == null) {
                isUpdate = false;
              }
              return null;
            },
            keyboardType: textInputType ?? TextInputType.text,
            controller: controller,
            decoration: InputDecoration(hintText: hint, prefixText: prefix, border: InputBorder.none),
          ),
        ],
      )),
    );
  }

  displayTextFieldPhoneContainer(
      {String? title,
      TextEditingController? controller,
      Color? backgroundColor,
      Widget? prefix,
      TextInputType? textInputType,
      String? hint}) {
    return Container(
      height: 66,
      color: backgroundColor,
      padding: EdgeInsets.only(top: 5),
      child: coverWidgetWithPadding(
          child: Column(
        children: [
          Row(
            children: [
              Row(
                children: [
                  Text(
                    title!,
                    style: TextStyle(fontSize: 11, color: Colors.black),
                  ),
                  Text(
                    ' *',
                    style: TextStyle(fontSize: 11, color: Colors.red),
                  ),
                ],
              ),
            ],
          ),
          TextFormField(
            textAlignVertical: TextAlignVertical.center,
            validator: (str) {
              if (str == '' || str == null) {
                isUpdate = false;
              }
              return null;
            },
            keyboardType: textInputType ?? TextInputType.text,
            controller: controller,
            decoration: InputDecoration(hintText: hint, prefixIcon: prefix, border: InputBorder.none),
          ),
        ],
      )),
    );
  }

  displayColorDivider() {
    return Divider(
      color: Color(0xffF4F2F0),
      thickness: 5,
    );
  }
}

class CustomFormFieldPaymentCard extends StatelessWidget {
  final String icon;
  final String label;
  final TextEditingController controller;
  final double height;
  final double width;
  final Color backgroundColor;
  final Widget? prefix;
  final Widget? backgroundWidget;
  final String placeHolder;
  final bool active;
  final Function? onTap;
  final bool obscure;
  final Function? onChange;
  final double? space;

  const CustomFormFieldPaymentCard(
      {Key? key,
      required this.label,
      required this.icon,
      required this.controller,
      this.height = 45,
      this.width = 200,
      this.backgroundColor = Colors.white,
      this.prefix,
      this.backgroundWidget,
      this.placeHolder = '',
      this.active = true,
      this.onTap,
      this.obscure = false,
      this.onChange,
      this.space})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                '$label',
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.black,
                      fontSize: 11,
                      letterSpacing: 0.5,
                    ),
              ),
            ],
          ),
          SizedBox(height: space ?? 10),
          ClipRRect(
            child: Container(
              height: height,
              width: width,
              decoration: BoxDecoration(
                color: backgroundColor,
              ),
              child: GestureDetector(
                child: CupertinoTextField(
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                    CardNumberFormatter(),
                  ],
                  maxLength: 19,
                  onChanged: onChange == null ? (val) {} : onChange as Function(String),
                  obscureText: obscure,
                  enabled: active,
                  placeholder: '$placeHolder',
                  placeholderStyle: Theme.of(context).textTheme.headline2!.copyWith(
                        color: Color(0xFFD0C5C5),
                        fontSize: size.height * 0.017,
                        letterSpacing: 0.6,
                        fontWeight: FontWeight.bold,
                      ),
                  prefix: Container(
                    padding: EdgeInsets.symmetric(horizontal: 0),
                    child: Center(
                      child: prefix,
                    ),
                  ),
                  suffix: (icon == "pay")
                      ? Padding(
                          padding: const EdgeInsets.all(15),
                          child: Image.asset(
                            "assets/images/pay.png",
                            height: 30,
                            width: 30,
                          ),
                        )
                      : Container(),
                  style: Theme.of(context).textTheme.headline2!.copyWith(
                        color: Colors.black,
                        fontSize: size.height * 0.018,
                        letterSpacing: 0.5,
                      ),
                  controller: controller,
                  decoration: BoxDecoration(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomFormFieldPaymentCardExpiry extends StatelessWidget {
  final String icon;
  final String label;
  final TextEditingController controller;
  final double height;
  final double width;
  final Color backgroundColor;
  final Widget? prefix;
  final Widget? backgroundWidget;
  final String placeHolder;
  final bool active;
  final Function? onTap;
  final bool obscure;
  final Function? onChange;
  final double? space;

  const CustomFormFieldPaymentCardExpiry(
      {Key? key,
      required this.label,
      required this.icon,
      required this.controller,
      this.height = 45,
      this.width = 200,
      this.backgroundColor = Colors.white,
      this.prefix,
      this.backgroundWidget,
      this.placeHolder = '',
      this.active = true,
      this.onTap,
      this.obscure = false,
      this.onChange,
      this.space})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                '$label',
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.black,
                      fontSize: 11,
                      letterSpacing: 0.5,
                    ),
              ),
            ],
          ),
          SizedBox(height: space ?? 10),
          ClipRRect(
            child: Container(
              height: height,
              width: width,
              decoration: BoxDecoration(
                color: backgroundColor,
              ),
              child: GestureDetector(
                child: CupertinoTextField(
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                    CardExpiryFormatter(),
                  ],
                  maxLength: 5,
                  onChanged: onChange == null? (val) {}: onChange as Function(String),
                  obscureText: obscure,
                  enabled: active,
                  placeholder: '$placeHolder',
                  placeholderStyle:Theme.of(context).textTheme.headline2!.copyWith(
                            color: Color(0xFFD0C5C5),
                            fontSize: size.height * 0.017,
                            letterSpacing: 0.6,
                            fontWeight: FontWeight.bold,
                          ),
                  prefix: Container(
                    padding: EdgeInsets.symmetric(horizontal: 0),
                    child: Center(
                      child: prefix,
                    ),
                  ),
                  suffix: (icon == "pay")
                      ? Padding(
                          padding: const EdgeInsets.all(15),
                          child: Image.asset(
                            "assets/images/pay.png",
                            height: 30,
                            width: 30,
                          ),
                        )
                      : Container(),
                  style: Theme.of(context).textTheme.headline2!.copyWith(
                        color: Colors.black,
                        fontSize: size.height * 0.018,
                        letterSpacing: 0.5,
                      ),
                  controller: controller,
                  decoration: BoxDecoration(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomFormFieldPaymentCvc extends StatelessWidget {
  final String icon;
  final String label;
  final TextEditingController controller;
  final double height;
  final double width;
  final Color backgroundColor;
  final Widget? prefix;
  final Widget? backgroundWidget;
  final String placeHolder;
  final bool active;
  final Function? onTap;
  final bool obscure;
  final Function? onChange;
  final double? space;

  const CustomFormFieldPaymentCvc(
      {Key? key,
      required this.label,
      required this.icon,
      required this.controller,
      this.height = 45,
      this.width = 200,
      this.backgroundColor = Colors.white,
      this.prefix,
      this.backgroundWidget,
      this.placeHolder = '',
      this.active = true,
      this.onTap,
      this.obscure = false,
      this.onChange,
      this.space})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                '$label',
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.black,
                      fontSize: 11,
                      letterSpacing: 0.5,
                    ),
              ),
            ],
          ),
          SizedBox(height: space ?? 10),
          ClipRRect(
            child: Container(
              height: height,
              width: width,
              decoration: BoxDecoration(
                color: backgroundColor,
              ),
              child: GestureDetector(
                child: CupertinoTextField(
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  maxLength: 3,
                  onChanged: onChange == null ? (val) {} : onChange as Function(String),
                  obscureText: obscure,
                  enabled: active,
                  placeholder: '$placeHolder',
                  placeholderStyle: Theme.of(context).textTheme.headline2!.copyWith(
                        color: Color(0xFFD0C5C5),
                        fontSize: size.height * 0.017,
                        letterSpacing: 0.6,
                        fontWeight: FontWeight.bold,
                      ),
                  prefix: Container(
                    padding: EdgeInsets.symmetric(horizontal: 0),
                    child: Center(
                      child: prefix,
                    ),
                  ),
                  suffix: (icon == "pay")
                      ? Padding(
                          padding: const EdgeInsets.all(15),
                          child: Image.asset(
                            "assets/images/pay.png",
                            height: 30,
                            width: 30,
                          ),
                        )
                      : Container(),
                  style: Theme.of(context).textTheme.headline2!.copyWith(
                        color: Colors.black,
                        fontSize: size.height * 0.018,
                        letterSpacing: 0.5,
                      ),
                  controller: controller,
                  decoration: BoxDecoration(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CardNumberFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue previousValue,
    TextEditingValue nextValue,
  ) {
    var inputText = nextValue.text;

    if (nextValue.selection.baseOffset == 0) {
      return nextValue;
    }

    var bufferString = StringBuffer();
    for (int i = 0; i < inputText.length; i++) {
      bufferString.write(inputText[i]);
      var nonZeroIndexValue = i + 1;
      if (nonZeroIndexValue % 4 == 0 && nonZeroIndexValue != inputText.length) {
        bufferString.write(' ');
      }
    }

    var string = bufferString.toString();
    return nextValue.copyWith(
      text: string,
      selection: TextSelection.collapsed(
        offset: string.length,
      ),
    );
  }
}

class CardExpiryFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue previousValue,
    TextEditingValue nextValue,
  ) {
    var inputText = nextValue.text;

    if (nextValue.selection.baseOffset == 0) {
      return nextValue;
    }

    var bufferString = StringBuffer();
    for (int i = 0; i < inputText.length; i++) {
      bufferString.write(inputText[i]);
      var nonZeroIndexValue = i + 1;
      if (nonZeroIndexValue % 2 == 0 && nonZeroIndexValue != inputText.length) {
        bufferString.write('/');
      }
    }

    var string = bufferString.toString();
    return nextValue.copyWith(
      text: string,
      selection: TextSelection.collapsed(
        offset: string.length,
      ),
    );
  }
}
