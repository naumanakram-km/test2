import 'package:flutter/material.dart';

// Custom packges
import 'package:sofiqe/widgets/catalog/checkout/payment_options_list.dart';

class PaymentOptionsAndCheckout extends StatefulWidget {
  PaymentOptionsAndCheckout({Key? key, required this.isshowloder, required this.onclose}): super(key: key);
  bool isshowloder;
  void Function(bool) onclose;
  @override
  State<PaymentOptionsAndCheckout> createState() => _PaymentOptionsAndCheckoutState();
}

class _PaymentOptionsAndCheckoutState extends State<PaymentOptionsAndCheckout> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(vertical: 11, horizontal: 10),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: PaymentOptionsList(isshowloder: widget.isshowloder,onclose: (value) {
          setState(() {
            widget.isshowloder = value;
            widget.onclose(widget.isshowloder);
          });
        },
      ),
    );
  }
}
