import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../../../controller/controllers.dart';
import '../../../provider/cart_provider.dart';

class GiftCardEarning extends StatefulWidget {
  const GiftCardEarning({Key? key}) : super(key: key);

  @override
  State<GiftCardEarning> createState() => _GiftCardEarningState();
}

class _GiftCardEarningState extends State<GiftCardEarning> {
  final giftAmount = TextEditingController();
  var height, size;
  dynamic giftCardAmount = 0;
  bool fetchedGiftCard = false;
  bool isProcessing = false;

  double _bodyHeight = 0.0;

  @override
  void initState() {
    // todo: implement initState
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<CartProvider>(context, listen: false).updateGiftCardApplied(false);
      Provider.of<CartProvider>(context, listen: false).updateAppliedGiftCardAmount(0.0);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    height = size.height;
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        children: [
          SizedBox(
            height: height * 0.004,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              InkWell(
                  onTap: () {
                    setState(() {
                      if (_bodyHeight == 0) {
                        _bodyHeight = 80.0;
                      } else {
                        _bodyHeight = 0.0;
                      }
                    });
                  },
                  child: (Provider.of<CartProvider>(context, listen: false).giftCardCodeTextController.text.isEmpty ||
                          Provider.of<CartProvider>(context, listen: false).giftCardCodeTextController.text.length <
                              13 ||
                          giftAmount.text.isEmpty)
                      ? Text(
                          'Giftcard or promo code',
                          style: Theme.of(context).textTheme.headline2!.copyWith(
                                color: Color(0xFF000000),
                                fontSize: height * 0.014,
                                fontFamily: "Arial",
                                letterSpacing: 0.4,
                              ),
                        )
                      : Text(
                          'Giftcard or promo code ${Provider.of<CartProvider>(context, listen: false).giftCardCodeTextController.text.replaceRange(0, 9, "*****")}',
                          style: Theme.of(context).textTheme.headline2!.copyWith(
                                color: Color(0xFF000000),
                                fontSize: height * 0.014,
                                fontFamily: "Arial",
                                letterSpacing: 0.4,
                              ),
                        )),
              InkWell(
                onTap: () {
                  setState(() {
                    if (_bodyHeight == 0) {
                      _bodyHeight = 80.0;
                    } else {
                      _bodyHeight = 0.0;
                    }
                  });
                },
                child: _bodyHeight == 0
                    ? Icon(
                        Icons.keyboard_arrow_right,
                        size: height * 0.02,
                      )
                    : Icon(Icons.keyboard_arrow_down, size: height * 0.02),
              ),
              Spacer(),
              Text(
              Provider.of<CartProvider>(context, listen: true).giftCarApplied == true ?     "-${Provider.of<CartProvider>(context, listen: false).appliedGiftCardAmount.toString().toProperCurrency()}" :  "${Provider.of<CartProvider>(context, listen: false).appliedGiftCardAmount.toString().toProperCurrency()}",
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Color(0xFF000000),
                      fontSize: height * 0.014,
                      fontFamily: "Arial",
                      letterSpacing: 0.4,
                    ),
              )
            ],
          ),
          AnimatedContainer(
            padding: EdgeInsets.zero,
            curve: Curves.easeInOut,
            duration: const Duration(milliseconds: 500),
            height: _bodyHeight,
            child: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(color: Color(0xFF88847D)),
                              ),
                              child: TextFormField(
                                controller:
                                    Provider.of<CartProvider>(context, listen: false).giftCardCodeTextController,
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(14),
                                ],
                                onChanged: (val) async {
                                  if (val.length == 14) {
                                    setState(() {});
                                    isProcessing = true;
                                    fetchedGiftCard = false;

                                    //---- call giftcard API
                                    giftCardAmount = await checkoutController.sfAPIApplyGiftCard(
                                        Provider.of<CartProvider>(context, listen: false)
                                            .giftCardCodeTextController
                                            .text);
                                    if (giftCardAmount == "") {
                                      setState(() {});
                                      // giftAmount.clear();

                                      isProcessing = false;
                                      Get.showSnackbar(
                                        GetSnackBar(
                                          message: 'Entered GiftCard is invalid',
                                          duration: Duration(seconds: 2),
                                        ),
                                      );
                                    } else if (0.0 == double.parse(giftCardAmount)) {
                                      setState(() {});
                                      isProcessing = false;
                                      Get.showSnackbar(
                                        GetSnackBar(
                                          message: 'Entered GiftCard has been consumed already',
                                          duration: Duration(seconds: 2),
                                        ),
                                      );
                                    } else {
                                      giftAmount.text = double.parse(giftCardAmount).toStringAsFixed(2);
                                      giftCardAmount = double.parse(giftCardAmount);
                                      fetchedGiftCard = true;
                                      isProcessing = false;
                                      setState(() {});
                                    }
                                  } else {
                                    cPrint('==== length is short =====');
                                  }
                                },
                                style: TextStyle(
                                  fontSize: height * 0.013,
                                ),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide.none,
                                    ),
                                    isDense: true,
                                    contentPadding: EdgeInsets.all(5),
                                    hintText: 'Promo or gift card number'),
                              )),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(color: Color(0xFF88847D)),
                              ),
                              child: TextField(
                                controller: giftAmount,
                                style: TextStyle(
                                  fontSize: height * 0.013,
                                ),
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15),
                                      borderSide: BorderSide.none,
                                    ),
                                    isDense: true,
                                    contentPadding: EdgeInsets.all(5),
                                    hintText: 'Amount'),
                              )),
                        ),
                      ],
                    ),
                  ),
                  if (fetchedGiftCard) SizedBox(
                          width: double.infinity,
                          child: Text(
                            'GiftCard full value is ${giftCardAmount.toStringAsFixed(2)}',
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline2!.copyWith(
                                  color: Color(0xFF000000),
                                  fontSize: height * 0.014,
                                  fontFamily: "Arial",
                                  letterSpacing: 0.4,
                                ),
                          ),
                        ) else SizedBox(),
                  if (isProcessing) SizedBox(
                          width: double.infinity,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Fetching GiftCard Details, Please wait ...',
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.headline2!.copyWith(
                                      color: Color(0xFF000000),
                                      fontSize: height * 0.014,
                                      fontFamily: "Arial",
                                      letterSpacing: 0.4,
                                    ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              SizedBox(
                                height: 16,
                                width: 16,
                                child: CircularProgressIndicator(
                                  color: Colors.black,
                                  strokeWidth: 1.5,
                                ),
                              ),
                            ],
                          ),
                        ) else SizedBox(),
                  SizedBox(
                    height: 6,
                  ),
                  InkWell(
                    onTap: () {
                      if (Provider.of<CartProvider>(context, listen: false).giftCardCodeTextController.text.isEmpty ||
                          Provider.of<CartProvider>(context, listen: false).giftCardCodeTextController.text.length <
                              13 ||
                          giftAmount.text.isEmpty ||
                          (double.parse(giftAmount.text) == 0.0)) {
                        Get.showSnackbar(
                          GetSnackBar(
                            message: 'Value can not empty.',
                            duration: Duration(seconds: 2),
                          ),
                        );
                      } else {
                        ///----- if already applied
                        if (Provider.of<CartProvider>(context, listen: false).appliedGiftCardAmount != 0.0) {
                          Get.showSnackbar(
                            GetSnackBar(
                              message: 'You have already applied GiftCard',
                              duration: Duration(seconds: 2),
                            ),
                          );
                        } else {
                          if (double.parse(giftAmount.text) > giftCardAmount) {
                            Get.showSnackbar(
                              GetSnackBar(
                                message: 'Entered GiftCard Amount is too big, please revise.',
                                duration: Duration(seconds: 2),
                              ),
                            );
                          } else {
                            double cartSum = Provider.of<CartProvider>(context, listen: false).getSumTotal();
                            if (double.parse(giftAmount.text) < cartSum) {
                              Provider.of<CartProvider>(context, listen: false)
                                  .updateAppliedGiftCardAmount(double.parse(giftAmount.text));
                              Provider.of<CartProvider>(context, listen: false)
                                  .updateCartTotal(cartSum - double.parse(giftAmount.text), true);

                              Get.showSnackbar(
                                GetSnackBar(
                                  message:
                                      "Select payment method to pay remaining ${(cartSum - double.parse(giftAmount.text)).toStringAsFixed(2).toProperCurrency()}",
                                  duration: Duration(seconds: 2),
                                ),
                              );

                              cPrint(
                                  '======= 1 - current cart amount is :: ${Provider.of<CartProvider>(context, listen: false).getSumTotal()} =========');

                              setState(() {});
                            } else if (double.parse(giftAmount.text) > cartSum) {
                              Provider.of<CartProvider>(context, listen: false).updateAppliedGiftCardAmount(cartSum);
                              Provider.of<CartProvider>(context, listen: false).updateCartTotal(0.0, true);
                              Get.showSnackbar(
                                GetSnackBar(
                                  message:
                                      "You will have ${(double.parse(giftAmount.text) - cartSum).toStringAsFixed(2).toProperCurrency()} remaining in your GiftCard",
                                  duration: Duration(seconds: 2),
                                ),
                              );
                              cPrint(
                                  '======= 2 - current cart amount is :: ${Provider.of<CartProvider>(context, listen: false).getSumTotal()} =========');
                              setState(() {});
                            } else {
                              //---- when both are equal
                              Provider.of<CartProvider>(context, listen: false).updateAppliedGiftCardAmount(cartSum);
                              Provider.of<CartProvider>(context, listen: false).updateCartTotal(0.0, true);
                              Get.showSnackbar(
                                GetSnackBar(
                                  message: "GiftCard has been applied to the total to pay amount",
                                  duration: Duration(seconds: 2),
                                ),
                              );
                              setState(() {});
                            }
                          }
                        }
                      }
                    },
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 500),
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      height: height * 0.030,
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFFF2CA8A)),
                        borderRadius: BorderRadius.all(Radius.circular(25)),
                        color: Color(0xFFF2CA8A),
                      ),
                      child: Center(
                        child: Text(
                          "ADD/CHANGE",
                          style: Theme.of(context).textTheme.headline2!.copyWith(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: height * 0.013,
                                letterSpacing: 0.5,
                              ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}