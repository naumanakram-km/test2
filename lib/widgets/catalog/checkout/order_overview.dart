import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_instance/get_instance.dart';
import 'package:sofiqe/provider/page_provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/widgets/makeover/make_over_login_custom_widget.dart';

class OrderOverView extends StatelessWidget {
  const OrderOverView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _Header(),
          SizedBox(
            height: 10,
          )
          // CheckoutItems(),
        ],
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TryItOnProvider tiop = Get.find();

    return Row(
      children: [
        Expanded(
          child: Text(
            'ORDER OVERVIEW',
            textAlign: TextAlign.start,
            style: Theme.of(context).textTheme.headline2!.copyWith(
                  color: Colors.black,
                  fontSize: 14,
                  letterSpacing: 0.55,
                  fontWeight: FontWeight.bold,
                ),
          ),
        ),
        Container(
          height: 25,
          width: 120,
          child: ElevatedButton(
            style: ButtonStyle(
              shape: MaterialStateProperty.all(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)),
              ),
              foregroundColor: MaterialStateProperty.all(
                AppColors.navigationBarSelectedColor,
              ),
              backgroundColor:
                  MaterialStateProperty.all(AppColors.buttonBackgroundShopping
                      // AppColors.buttonBackgroundShopping,
                      ),
              overlayColor: MaterialStateProperty.resolveWith<Color?>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.pressed))
                    return tiop.ontapColor; //<-- SEE HERE
                  return null; // Defer to the widget's default.
                },
              ),
            ),
            // style: ElevatedButton.styleFrom(
            //   shape: RoundedRectangleBorder(
            //       borderRadius: BorderRadius.circular(30.0)),
            //   foregroundColor: AppColors.navigationBarSelectedColor,
            //   backgroundColor: AppColors.buttonBackgroundShopping,
            // ),
            onPressed: () async {
              tiop.isChangeButtonColor.value = true;
              Future.delayed(Duration(milliseconds: 10)).then((value) {
                tiop.isChangeButtonColor.value = false;
                SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);

                SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
                Navigator.popUntil(context, (route) => route.isFirst);
                pp.goToPage(Pages.HOME);
              });
            },
            child: Text(
              'Shop More',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    fontSize: 12,
                    letterSpacing: 0.8,
                  ),
            ),
          ),
        ),
      ],
    );
  }
}
