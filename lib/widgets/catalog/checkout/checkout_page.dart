import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/controller/checkoutController.dart';
import 'package:sofiqe/provider/account_provider.dart';
import 'package:sofiqe/provider/cart_provider.dart';
import 'package:sofiqe/utils/constants/api_tokens.dart';
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/widgets/catalog/checkout/bonous_point_alert.dart';
// Custom packages
import 'package:sofiqe/widgets/catalog/checkout/order_overview.dart';
import 'package:sofiqe/widgets/catalog/checkout/payment_options.dart';
import 'package:sofiqe/widgets/catalog/checkout/shipping_options.dart';
import 'package:sofiqe/widgets/catalog/checkout/total_amount.dart';
import 'package:sofiqe/widgets/catalog/checkout/total_saving_amount.dart';
import 'package:sofiqe/widgets/catalog/checkout/vippoint_earning.dart';

import '../../../controller/msProfileController.dart';
import '../../../utils/api/shipping_address_api.dart';
import '../../../utils/api/shopping_cart_api.dart';
import '../../../utils/states/local_storage.dart';
import 'additional_charges.dart';
import 'giftcard_or_promocode.dart';

var isLoggedIn;
var size, height, width;

class CheckOutPage extends StatefulWidget {
  CheckOutPage({Key? key}) : super(key: key);

  @override
  State<CheckOutPage> createState() => _CheckOutPageState();
}

class _CheckOutPageState extends State<CheckOutPage> {
  bool isshowloder = false;
  Future<void> getAddress(BuildContext context) async {
    if (isLoggedIn) {
      await Provider.of<AccountProvider>(context, listen: false).getUserDetails(await APITokens.customerSavedToken);
      Provider.of<CartProvider>(context, listen: false)
          .fetchTiersList(Provider.of<AccountProvider>(context, listen: false).customerId);
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      isLoggedIn = Provider.of<CartProvider>(context, listen: false).isLoggedIn;
      if (isLoggedIn) loadData();
      MsProfileController.instance.loadShippindAdressinProvider();
      setShippingDetailsForCustomer();
      loadEstimateDelivery();
    });
  }

    setShippingDetailsForCustomer() async {
    if (isLoggedIn) {
      try {
        var cartid = Provider.of<CartProvider>(context, listen: false).cartDetails!['id'];
        bool isStored = false;

        var data =
            await sfQueryForSharedPrefData(fieldName: "customer_shipping_check", type: PreferencesDataType.STRING);
        if (data["found"]) {
          var dataNew = jsonDecode(data["customer_shipping_check"]);
          if (dataNew["cartId"] == cartid && dataNew["status"]) {
            isStored = true;
          }
        }
        // if (!isStored) {
        cPrint("COS ==> Data Not Stored");
        var addressInfo = MsProfileController.to.getAddressToAssignTocart();
        cPrint("New Cart Show Here");
        cPrint(addressInfo);

        Provider.of<AccountProvider>(context, listen: false).shipAddress =
            await sfAPIAddShippingAddress(addressInfo, isLoggedIn, cartid);

        String res = await sfAPIcartIDUser();
        var bodyForEstimateCost =
            MsProfileController.to.getEstimateShipingAddressForCheckOut(customerId: int.parse(res));
        cPrint("COS ==> Shipping data");
        cPrint(bodyForEstimateCost);
        await CheckoutController.to.sfAPIEstimateShippingCost(bodyForEstimateCost, isLoggedIn, int.parse(res));
        dynamic dataToStore = {"cartId": cartid, "status": true};
        sfStoreInSharedPrefData(
            fieldName: "customer_shipping_check", value: jsonEncode(dataToStore), type: PreferencesDataType.STRING);
        // } else {
        //   var addressInfo = MsProfileController.to.getAddressToAssignTocart();
        //   cPrint("Address on Found");
        //   cPrint(addressInfo);
        //   cPrint("COS ==> Data Found");
        // }
      } catch (e) {
        cPrint("COS ==> Some Error" + e.toString());
      }
    } else {
      cPrint("COS ==> Not Logged In");
    }
  }

  MsProfileController _ = Get.find<MsProfileController>();

  loadEstimateDelivery() async {
    var bodyForEstimateCost;
    if (isLoggedIn) {
      bodyForEstimateCost = MsProfileController.to.getEstimateShipingAddressForCheckOut();
    } else {
      dynamic shipping_address =
          await sfQueryForSharedPrefData(fieldName: 'shipping_address', type: PreferencesDataType.STRING);

      dynamic phoneNoCode =
          await sfQueryForSharedPrefData(fieldName: 'phone_no_code', type: PreferencesDataType.STRING);
      if (shipping_address["found"]) {
        dynamic shippind_data = jsonDecode(shipping_address["shipping_address"]);
        _.countryController.text = shippind_data["region"] ?? "";
        _.streetController.text = (shippind_data["street"] ?? [""])[0];
        _.nameController.text = (shippind_data["firstname"] ?? "") + " " + (shippind_data["lastname"] ?? "");
        _.firstNameController.text = shippind_data["firstname"] ?? "";
        _.lastNameController.text = shippind_data["lastname"] ?? "";
        _.phoneController.text = shippind_data["telephone"] ?? "";
        _.phoneNumberCodeController.text = phoneNoCode["found"] ? phoneNoCode["phone_no_code"] : "+1";
        _.postCodeController.text = shippind_data["postcode"] ?? "";
        _.cityController.text = shippind_data["city"] ?? "";
        _.countryCodeController.text = shippind_data["country_id"] ?? "";
        _.regionCodeController.text = shippind_data["region_code"] ?? "";
        _.regionIdController.text = (shippind_data["region_id"] ?? "").toString();
        _.emailController.text = shippind_data["email"] ?? "";
      }

      bodyForEstimateCost = MsProfileController.to.getEstimateShipingAddressForCheckOut();
    }
    try {
      if (isLoggedIn) {
        cPrint("ESC == Call for Loggin");
        String res = await sfAPIcartIDUser();
        await CheckoutController.to.sfAPIEstimateShippingCost(bodyForEstimateCost, isLoggedIn, int.parse(res));
      } else {
        cPrint("ESC == Call for Guest $bodyForEstimateCost");
        var cartid = Provider.of<CartProvider>(context, listen: false).cartDetails!['id'];
        await CheckoutController.to.sfAPIEstimateShippingCost(bodyForEstimateCost, isLoggedIn, cartid);
        dynamic selectedmethod =
            CheckoutController.instance.listShippingMethod[CheckoutController.instance.selectedShippingMethod.value];
        Provider.of<CartProvider>(context, listen: false)
            .setDeliverCharges(double.parse((selectedmethod['price'] ?? 0).toString()));
      }
    } catch (e) {
      cPrint("EOCHP==> $e");
    }
  }

  loadData() {
    getAddress(context);
    int id = Provider.of<AccountProvider>(context, listen: false).customerId;

    sfAPIGetBillingAddressFromCustomerID(customerId: id);
    sfAPIGetShippingAddressFromCustomerID(customerId: id);
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    isLoggedIn = Provider.of<CartProvider>(context).isLoggedIn;

    cPrint(' =====  build method of checkout page has been called  ====== ');

    return Container(
      color: Colors.white,
      child: Column(
        children: [
          // Container(alignment: Alignment.topCenter, child: OrderOverView()),
          Expanded(
            flex: 2,
            child: Container(
              alignment: Alignment.topCenter,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    OrderOverView(),
                    AdditionalCharge(),
                    ShippingOptions(),
                    TotalAmount(),
                    if (isLoggedIn) VipPointEarn() else Container(),
                    GiftCardEarning(),
                    TotalSavingAmount(),
                    SizedBox(height: 6),
                    BonousPointAlert(),
                    SizedBox(height: 50),
                    if (isshowloder)
                      SpinKitDoubleBounce(color: AppColors.primaryColor, size: 100.0),
                  ],
                ),
              ),
            ),
          ),
          Container(
            color: Color(0xFFF4F2F0),
            height: 7,
          ),
          Container(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children:  [
                  PaymentOptionsAndCheckout(
                    isshowloder: isshowloder,
                    onclose: (value) {
                      setState(() {
                        isshowloder = value;
                      });
                    },
                  ),
                ],
              ))
        ],
      ),
    );
  }
}