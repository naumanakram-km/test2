import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/model/data_ready_status_enum.dart';
import 'package:sofiqe/provider/account_provider.dart';
import 'package:sofiqe/provider/cart_provider.dart';
import 'package:sofiqe/provider/catalog_provider.dart';
// Remove to test background service
// import 'package:sofiqe/provider/freeshiping_provider.dart';
import 'package:sofiqe/screens/my_sofiqe.dart';
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/widgets/cart/empty_bag.dart';
import 'package:sofiqe/widgets/product_item_card.dart';

import '../../../controller/fabController.dart';
import '../../../provider/page_provider.dart';
import '../../Shimmers/catalog_buffering_shimmer.dart';

class Catalog extends StatefulWidget {
  const Catalog({Key? key}) : super(key: key);

  @override
  State<Catalog> createState() => _CatalogState();
}

class _CatalogState extends State<Catalog> {
  String freeShippingAmount = "Loading..";
  ScrollController gridScrollController = ScrollController();
  bool _showBufferingCatalog = true;

  final CatalogProvider cp = Get.find();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 5), () {
      setState(() {
        _showBufferingCatalog = false;
      });
    });
    gridScrollController.addListener(loadMore);
    String freeshipping =
        Provider.of<AccountProvider>(context, listen: false).freeShippingAmount;
    freeShippingAmount =
        'Free Shipping above ' + freeshipping.toString().toProperCurrency();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> loadMore() async {
    if (gridScrollController.offset ==
        gridScrollController.position.maxScrollExtent) {
      cPrint('Loading More');
      if (await cp.fetchMoreItems()) {
        setState(() {});
        gridScrollController.animateTo(gridScrollController.offset + 20,
            duration: Duration(milliseconds: 200), curve: Curves.linear);
      } else {
        cPrint('Could not fetch more products');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    FABController.to.closeOpenedMenu();

    Size size = MediaQuery.of(context).size;
    double padding = size.height * 0.04;
    return Obx(
      () {
        DataReadyStatus status = cp.catalogItemsStatus.value;
        cPrint("STATUS STATUS ${status}");
        if (status == DataReadyStatus.INACTIVE) {
          return ErrorCatalog();
        } else if (status == DataReadyStatus.FETCHING) {
          return BufferingCatalog();
        } else if (status == DataReadyStatus.COMPLETED) {
          if (_showBufferingCatalog) {
            // If _showBufferingCatalog is true, display the BufferingCatalog widget.
            return BufferingCatalog();
          } else {
            // If _showBufferingCatalog is false, check if the condition for displaying
            // the EmptyCatalog widget is met. The condition depends on the value of
            // cp.showOnlySaleItems and whether the respective catalog list is empty.
            if (cp.showOnlySaleItems.value
                ? cp.catalogSaleItemsList.isEmpty
                : cp.catalogItemsList.isEmpty) {
              // If the condition is met, display the EmptyCatalog widget.
              return ErrorCatalog();
            }
          }
          return Stack(
            children: <Widget>[
              Container(
                width: size.width,
                decoration: BoxDecoration(
                  color: Color(0xFFF4F2F0),
                ),
                child: GridView.builder(
                  physics: const ClampingScrollPhysics(),
                  controller: gridScrollController,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: padding / 2,
                    mainAxisSpacing: padding / 2,
                    childAspectRatio:
                        ((size.width - padding) / 2) / (size.height * 0.4),
                  ),
                  padding: EdgeInsets.symmetric(
                      vertical: size.height * 0.05, horizontal: padding / 2),
                  itemCount: cp.showOnlySaleItems.value
                      ? cp.catalogSaleItemsList.length
                      : cp.catalogItemsList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ProductItemCard(
                      product: cp.showOnlySaleItems.value
                          ? cp.catalogSaleItemsList[index]
                          : cp.catalogItemsList[index],
                    );
                  },
                ),
              ),
              // added by kruti for displaying pink var with updated value if cart is empty it display default else
              // getContainer widget will display  updated amount
              (Provider.of<CartProvider>(context).cart ?? []).length == 0
                  ? Container(
                      width: double.infinity,
                      height: 25.0,
                      color: SplashScreenPageColors.freeShippingColor,
                      child: Center(
                        child: Text(freeShippingAmount,
                            style:
                                TextStyle(color: Colors.black, fontSize: 15.0)),
                      ))
                  : getContainerWidget(context, size),
            ],
          );
        } else if (status == DataReadyStatus.ERROR) {
          return ErrorCatalog();
        } else {
          return ErrorCatalog();
        }
      },
    );
  }
}

// added by kruti to display pink bar with updated values
Container getContainerWidget(BuildContext context, Size size) {
  // CurrencyController currencycntrl = Get.put(CurrencyController());
  String shippingText = "";
  String freeshipping =
      Provider.of<AccountProvider>(context, listen: false).freeShippingAmount;
  if ((Provider.of<CartProvider>(context).cart ?? []).length == 0) {
    shippingText = 'Free shipping above ' +
        // currencycntrl.defaultCurrency! +
        freeshipping.toString().toProperCurrency();
    return Container(
        color: HexColor("#EB7AC1"),
        height: 25,
        width: size.width,
        child: Center(
            child: Text(
          shippingText,
          style: TextStyle(fontSize: 12),
        )));
  } else {
    double minusAmount = (double.parse(
            Provider.of<AccountProvider>(context, listen: false)
                .freeShippingAmount) -
        double.parse(Provider.of<CartProvider>(context)
            .chargesList[0]['amount']
            .toString()));
    if (minusAmount > 0) {
      shippingText = 'Add ' +
          // currencycntrl.defaultCurrency! +
          minusAmount.toString().toProperCurrency() +
          " to your cart to get free shipping";

      return Container(
          color: HexColor("#EB7AC1"),
          height: 25,
          width: size.width,
          child: Center(
              child: Text(
            shippingText,
            style: TextStyle(fontSize: 12),
          )));
    } else {
      return Container();
    }
  }
}

//  Container(
//   width: size.width,
//   decoration: BoxDecoration(
//     color: Color(0xFFF4F2F0),
//   ),
//   child: Center(
//     child: Column(
//       mainAxisSize: MainAxisSize.min,
//       children: [
//         Image.asset(
//           'assets/images/warning.png',
//           height: 60,
//         ),
//         SizedBox(
//           height: 10,
//         ),
//         Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 30),
//           child: Text(
//             "We can't find products matching the selection. Please try a different filter",
//             textAlign: TextAlign.center,
//             style: Theme.of(context).textTheme.headline2!.copyWith(
//                   fontSize: size.height * 0.02,
//                   color: Colors.black,
//                   letterSpacing: 1,
//                   fontWeight: FontWeight.bold,
//                 ),
//           ),
//         ),
//       ],
//     ),
//   ),
// );

// ignore: must_be_immutable
class EmptyCatalog extends StatelessWidget {
  PageProvider pp = Get.find();

  EmptyCatalog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      decoration: BoxDecoration(
        color: Color(0xFFF4F2F0),
      ),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              'assets/images/warning.png',
              height: 60,
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Text(
                "We can't find products matching the selection. Please try a different filter",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      fontSize: size.height * 0.02,
                      color: Colors.black,
                      letterSpacing: 1,
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
          ],
        ),
      ),
    );

    // CustomEmptyBagPage(
    //   title: 'Oops, You must do the makeover \n analysis to see this screen.',
    //   ontap: () async {
    //     //      profileController.screen.value = 0;
    //     // Get.back();
    //     pp.goToPage(Pages.SHOP);
    //   },
    //   emptyBagButtonText: 'GO MAKEOVER',
    // );
  }
}

class BufferingCatalog extends StatelessWidget {
  const BufferingCatalog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CatalogBufferingShimmer();
  }
}

// ignore: must_be_immutable
class ErrorCatalog extends StatelessWidget {
  PageProvider pp = Get.find();
  ErrorCatalog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomEmptyBagPage(
      title: 'Oops, we could not find any results',
      // title: 'Oops You have no reviews yet,\nwant to go to shopping? ',
      ontap: () async {
        //      profileController.screen.value = 0;
        Get.back();
        pp.goToPage(Pages.SHOP);
      },
      emptyBagButtonText: "",
    );
  }
}
