// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:gtm/gtm.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/provider/page_provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/screens/Ms1/ms1_profile.dart';
import 'package:sofiqe/screens/catalog_screen.dart';
import 'package:sofiqe/screens/home_screen.dart';
import 'package:sofiqe/screens/make_over_screen.dart';
import 'package:sofiqe/widgets/scaffold/custom_bottom_navigation_bar.dart';
import '../../controller/controllers.dart';
import '../../controller/fabController.dart';
import '../../main.dart';
import '../../provider/account_provider.dart';
import '../../provider/catalog_provider.dart';
import '../../provider/make_over_provider.dart';
import '../../screens/MS6/reviews.dart';
import '../../screens/Ms3/looks_screen.dart';
import '../../screens/try_it_on_screen.dart';
import '../../utils/AppLayout.dart';
import 'custom_fab.dart';

class ScaffoldTemplate extends StatefulWidget {
  final Widget child;
  final int index;

  const ScaffoldTemplate({Key? key, required this.child, this.index = 0})
      : super(key: key);

  @override
  _ScaffoldTemplateState createState() => _ScaffoldTemplateState();
}

class _ScaffoldTemplateState extends State<ScaffoldTemplate> {
  final PageProvider pp = Get.find();
  final TryItOnProvider tiop = Get.find();
  final CatalogProvider catp = Get.find();
  final MakeOverProvider mop = Get.find();
  final FABController fabController = Get.find();

  late int index = widget.index;
  PageController pageController = PageController();
  List<Widget> body = [
    HomeScreen(),
    CatalogScreen(),
    MakeOverScreen(),
    Ms1Profile(),
  ];

  @override
  void initState() {
    super.initState();
    fabController.showFab.value = true;
    pp.onTapCallback = onTap;
  }

  void onTap(int index) async {
    HapticFeedback.vibrate();
    tiop.playSound();
    setState(() {
      this.index = index;
      makeOverProvider.isText.value = true;
    });
    print("CSI==> $index");
    if (index != 2) {
      FABController.to.setfabvalue(true);
    }
    pageController.animateToPage(index,
        duration: Duration(microseconds: 500), curve: Curves.ease);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Stack(
          clipBehavior: Clip.none,
          // fit: StackFit.loose,
          children: [
            PageView(
              physics: NeverScrollableScrollPhysics(),
              controller: pageController,
              allowImplicitScrolling: false,
              children: body,
            ),
            Obx(() {
              return MediaQuery.of(context).viewInsets.bottom > 0.0 ||
                      fabController.showFab.value == false
                  ? SizedBox()
                  : Container(
                      margin: EdgeInsets.only(
                        bottom: AppLayout.getHeight(0),
                        // top: AppLayout.getHeight(10),
                        right: AppLayout.getHeight(30),
                      ),
                      child: FabCircularMenu(
                          key: fabController.fabKey,
                          onDisplayChange: (bool val) {
                            if (val) {
                              gtm.push(
                                'press_selection',
                              );
                            }
                            // cPrint('==== is open $val  =====');
                          },
                          alignment: Alignment.bottomCenter,
                          fabMargin: EdgeInsets.zero,
                          fabColor: Colors.transparent,
                          fabSize: AppLayout.getHeight(61),
                          ringColor: Color(0xffF6DFED),
                          ringWidth: AppLayout.getWidth(160),
                          ringDiameter: AppLayout.getHeight(380),
                          fabElevation: 0,
                          fabOpenColor: Colors.transparent,
                          fabChild: Image.asset(
                            'assets/images/menu-cirlce 01.png',
                            // height: ,
                          ),
                          children: <Widget>[
                            ///------ Selection Button
                            Container(
                              margin: EdgeInsets.only(
                                  right: AppLayout.getHeight(90),
                                  top: AppLayout.getHeight(50)),
                              child: GestureDetector(
                                onTap: index == 3
                                    ? Provider.of<AccountProvider>(context,
                                                listen: true)
                                            .isLoggedIn
                                        ? !makeOverProvider.tryitOn.value
                                            ? () {
                                                profileController
                                                    .makeOverMessage
                                                    .value = true;
                                                HapticFeedback.vibrate();
                                                SystemSound.play(
                                                    SystemSoundType.click);
                                                fabController
                                                    .fabKey.currentState!
                                                    .close();
                                                profileController.screen.value =
                                                    2;
                                              }
                                            : () {
                                                HapticFeedback.vibrate();
                                                SystemSound.play(
                                                    SystemSoundType.click);
                                                fabController
                                                    .fabKey.currentState!
                                                    .close();
                                                tiop.lookname.value =
                                                    "myselection";
                                                tiop.page.value = 2;
                                                tiop.lookProduct.value = true;
                                                tiop.directProduct.value =
                                                    false;
                                                Get.to(
                                                  () => TryItOnScreen(),
                                                  transition:
                                                      Transition.downToUp,
                                                  duration: const Duration(
                                                      milliseconds: 500),
                                                );
                                              }
                                        : () {
                                            profileController
                                                .makeOverMessage.value = true;

                                            HapticFeedback.vibrate();
                                            SystemSound.play(
                                                SystemSoundType.click);
                                            fabController.fabKey.currentState!
                                                .close();
                                            profileController.screen.value = 1;
                                          }
                                    : Provider.of<AccountProvider>(context,
                                                listen: true)
                                            .isLoggedIn
                                        ? makeOverProvider.tryitOn.value
                                            ? () {
                                                HapticFeedback.vibrate();
                                                SystemSound.play(
                                                    SystemSoundType.click);
                                                fabController
                                                    .fabKey.currentState!
                                                    .close();
                                                tiop.lookname.value =
                                                    "myselection";
                                                tiop.page.value = 2;
                                                tiop.lookProduct.value = true;
                                                tiop.directProduct.value =
                                                    false;
                                                Get.to(
                                                  () => TryItOnScreen(),
                                                  transition:
                                                      Transition.downToUp,
                                                  duration: const Duration(
                                                      milliseconds: 500),
                                                );
                                              }
                                            : () {
                                                HapticFeedback.vibrate();
                                                SystemSound.play(
                                                    SystemSoundType.click);
                                                FABController.to
                                                    .closeOpenedMenu();
                                                navController
                                                    .innerPageIndex.value = 2;
                                              }
                                        : () {
                                            profileController
                                                .makeOverMessage.value = true;
                                            HapticFeedback.vibrate();
                                            SystemSound.play(
                                                SystemSoundType.click);
                                            FABController.to.closeOpenedMenu();
                                            navController.innerPageIndex.value =
                                                1;
                                          },
                                behavior: HitTestBehavior.opaque,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    //----Icon
                                    SvgPicture.asset(
                                      'assets/images/menu-eye.svg',
                                    ),
                                    SizedBox(
                                      height: AppLayout.getHeight(3),
                                    ),
                                    Text(
                                      'Selections',
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline3!
                                          .copyWith(
                                            color: Color(0xffEB7AC1),
                                            fontSize: AppLayout.getHeight(11),
                                            letterSpacing: 0,
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ),

                            ///------ Reviews & W Wishlist Button
                            Container(
                              margin: EdgeInsets.only(
                                right: AppLayout.getHeight(65),
                                bottom: AppLayout.getHeight(15),
                              ),
                              child: GestureDetector(
                                onTap: () {
                                  gtm.push(
                                    'press_reviewswishlist',
                                  );
                                  HapticFeedback.vibrate();
                                  SystemSound.play(SystemSoundType.click);
                                  fabController.fabKey.currentState!.close();
                                  profileController.makeOverMessage.value =
                                      false;
                                  Get.to(
                                    () => ReviewsMS6(),
                                    transition: Transition.upToDown,
                                    duration: const Duration(milliseconds: 500),
                                  );
                                },
                                behavior: HitTestBehavior.opaque,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    //----Icon
                                    Image.asset(
                                      'assets/images/menu-reviews.png',
                                      // height: 40,
                                      width: AppLayout.getWidth(26),
                                    ),
                                    SizedBox(
                                      height: AppLayout.getHeight(3),
                                    ),
                                    Text(
                                      'Reviews\nWishlist',
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline3!
                                          .copyWith(
                                            height: AppLayout.getHeight(1.1),
                                            color: Color(0xffEB7AC1),
                                            fontSize: AppLayout.getHeight(11),
                                            letterSpacing: 0,
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ),

                            ///------ Try On Button
                            Container(
                              margin: EdgeInsets.only(
                                  bottom: AppLayout.getHeight(60)),
                              child: GestureDetector(
                                onTap: () {
                                  gtm.push(
                                    'press_tryon',
                                  );
                                  HapticFeedback.vibrate();
                                  SystemSound.play(SystemSoundType.click);
                                  fabController.fabKey.currentState!.close();
                                  Get.to(
                                    () => TryItOnScreen(),
                                    transition: Transition.fadeIn,
                                    duration: const Duration(milliseconds: 500),
                                  );
                                },
                                behavior: HitTestBehavior.opaque,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    //----Icon
                                    SvgPicture.asset(
                                      'assets/images/menu-mobile.svg',
                                      width: AppLayout.getWidth(20),
                                    ),
                                    SizedBox(
                                      height: AppLayout.getHeight(4),
                                    ),
                                    Text(
                                      'Try On',
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline3!
                                          .copyWith(
                                            color: Color(0xffEB7AC1),
                                            fontSize: AppLayout.getHeight(11),
                                            letterSpacing: 0,
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ),

                            ///------ Looks Button
                            Container(
                              margin: EdgeInsets.only(
                                  left: AppLayout.getHeight(40),
                                  bottom: AppLayout.getHeight(50)),
                              child: GestureDetector(
                                onTap: () {
                                  gtm.push(
                                    'press_tryon',
                                  );
                                  HapticFeedback.vibrate();
                                  SystemSound.play(SystemSoundType.click);
                                  profileController.makeOverMessage.value =
                                      true;

                                  index == 3
                                      ? Provider.of<AccountProvider>(context,
                                                  listen: false)
                                              .isLoggedIn
                                          ? !makeOverProvider.tryitOn.value
                                              ? profileController.screen.value =
                                                  2
                                              : Get.to(
                                                  () => LooksScreen(),
                                                  transition: Transition
                                                      .leftToRightWithFade,
                                                  duration: const Duration(
                                                      milliseconds: 500),
                                                )
                                          : profileController.screen.value = 1
                                      :

                                      ///----- if current page index is not 3

                                      Provider.of<AccountProvider>(context,
                                                  listen: false)
                                              .isLoggedIn
                                          ? !makeOverProvider.tryitOn.value
                                              ? navController
                                                  .innerPageIndex.value = 2
                                              : Get.to(
                                                  () => LooksScreen(),
                                                  transition: Transition
                                                      .leftToRightWithFade,
                                                  duration: const Duration(
                                                      milliseconds: 500),
                                                )
                                          : navController.innerPageIndex.value =
                                              1;

                                  fabController.fabKey.currentState!.close();
                                },
                                behavior: HitTestBehavior.opaque,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Image.asset(
                                      'assets/images/women-menu.png',
                                      width: AppLayout.getWidth(50),
                                      height: AppLayout.getHeight(40),
                                    ),
                                    Text(
                                      'Looks',
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline3!
                                          .copyWith(
                                            color: Color(0xffEB7AC1),
                                            fontSize: AppLayout.getHeight(11),
                                            letterSpacing: 0,
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ),

                            ///------ SALE Button
                            Container(
                              margin: EdgeInsets.only(
                                  left: AppLayout.getHeight(60),
                                  top: AppLayout.getHeight(40)),
                              child: InkWell(
                                onTap: () {
                                  gtm.push(
                                    'press_sale',
                                  );
                                  HapticFeedback.vibrate();
                                  SystemSound.play(SystemSoundType.click);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (BuildContext c) {
                                        return CatalogScreen(
                                          isFromSale: true,
                                          isViewingAllSaleProducts: true,
                                          comingFromMainIndex: navController
                                              .currentMainIndex.value,
                                        );
                                      },
                                    ),
                                  );
                                  fabController.fabKey.currentState!.close();
                                },
                                child: Image.asset(
                                  'assets/images/sale-menu.png',
                                  width: AppLayout.getWidth(50),
                                  height: AppLayout.getHeight(35),
                                ),
                              ),
                            ),
                          ]),
                    );
            }),
          ],
        ),
        bottomNavigationBar: CustomBottomNavigationBar(
          currentIndex: index,
          onTap: onTap,
        ),
      ),
    );
  }
}
