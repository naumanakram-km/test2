import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/controller/controllers.dart';
import 'package:sofiqe/provider/page_provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/widgets/capsule_button.dart';
import '../../controller/looksController.dart';
import '../../controller/nav_controller.dart';
import '../../provider/cart_provider.dart';

class EmptyBagPage extends StatelessWidget {
  final String? emptyBagButtonText;
  EmptyBagPage({Key? key, this.emptyBagButtonText}) : super(key: key);
  final PageProvider pp = Get.find();

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Transform.translate(
            offset: Offset(0, MediaQuery.of(context).size.height * 0.02),
            child: Text(
              'No way, your shopping bag is empty!',
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: Colors.black,
                    fontSize: 14,
                    letterSpacing: 0.6,
                  ),
            ),
          ),
          Image.asset(
            'assets/images/empty_bag_background.png',
            height: MediaQuery.of(context).size.height * 0.55,
          ),
          if (emptyBagButtonText?.isNotEmpty ?? false)
            CapsuleButton(
              backgroundColor: Colors.black,
              height: MediaQuery.of(context).size.height * 0.07,
              width: MediaQuery.of(context).size.width * 0.7,
              onPress: () {
                profileController.screen.value = 0;
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  Get.find<NavController>().setnavbar(false);
                });
                Navigator.popUntil(context, (route) => route.isFirst);

                pp.goToPage(Pages.HOME);

                // Navigator.of(context).pop();
              },
              child: Text(
                emptyBagButtonText?.isNotEmpty ?? false
                    ? emptyBagButtonText as String
                    : 'GO SHOPPING',
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Color(0xFFF2CA8A),
                      fontSize: 16,
                      letterSpacing: 1.4,
                    ),
              ),
            ),
          SizedBox(
            height: 55,
          )
        ],
      ),
    );
  }
}

class CustomEmptyBagPage extends StatelessWidget {
  final String emptyBagButtonText;
  final VoidCallback ontap;
  final String title;
  CustomEmptyBagPage(
      {Key? key,
      required this.emptyBagButtonText,
      required this.ontap,
      required this.title})
      : super(key: key);
  final TryItOnProvider tiop = Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // Transform.translate(
          //   offset: Offset(0, MediaQuery.of(context).size.height * 0.04),
          //   child:
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline2!.copyWith(
                        color: Colors.black,
                        fontSize: 14,
                        letterSpacing: 0.6,
                      ),
                ),
                SizedBox(height: 10),
                if (!Provider.of<CartProvider>(context, listen: false)
                    .isLoggedIn)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "or are you not logged in?",
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headline2!.copyWith(
                              color: Colors.black,
                              fontSize: 14,
                              letterSpacing: 0.6,
                            ),
                      ),
                      SizedBox(width: 5),
                      InkWell(
                        onTap: (() {
                          profileController.makeOverMessage.value = true;
                          navController.innerPageIndex.value = 1;
                        }),
                        child: const Text(
                          'Log In',
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.blue,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      )
                    ],
                  )
              ],
            ),
          ),
          //   ),
          Image.asset(
            'assets/images/empty_bag_background.png',
            height: MediaQuery.of(context).size.height *
                (emptyBagButtonText.isNotEmpty ? 0.45 : 0.55),
          ),
          if (emptyBagButtonText.isNotEmpty)
            Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: Container(
                // height: MediaQuery.of(context).size.height * 0.065,
                width: MediaQuery.of(context).size.width * 0.8,
                // alignment: Alignment.center,
                child: Container(
                  // alignment: Alignment.center,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                      ),
                      foregroundColor: MaterialStateProperty.all(
                        AppColors.navigationBarSelectedColor,
                      ),
                      backgroundColor: MaterialStateProperty.all(
                          // AppColors.buttonBackgroundShopping,
                          Color(0xFFF2CA8A)),
                      overlayColor: MaterialStateProperty.resolveWith<Color?>(
                        (Set<MaterialState> states) {
                          if (states.contains(MaterialState.pressed))
                            return tiop.ontapColor; //<-- SEE HERE
                          return null; // Defer to the widget's default.
                        },
                      ),
                    ),
                    onPressed: () {
                      tiop.isChangeButtonColor.value = true;
                      tiop.playSound();

                      Future.delayed(Duration(milliseconds: 10)).then((value) {
                        tiop.isChangeButtonColor.value = false;
                        ontap();
                      });
                    },
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      child: Text(
                        emptyBagButtonText,
                        style: Theme.of(context).textTheme.headline2!.copyWith(
                              color: Colors.black,
                              fontSize: 15,
                              letterSpacing: 1.4,
                            ),
                      ),
                    ),
                  ),
                ),
              ),
              // CapsuleButton(
              //   height: MediaQuery.of(context).size.height * 0.065,
              //   width: MediaQuery.of(context).size.width * 0.65,
              //   onPress: ontap,
              //   backgroundColor: Color(0xFFF2CA8A),
              //   child: Text(
              //     emptyBagButtonText,
              //     style: Theme.of(context).textTheme.headline2!.copyWith(
              //           color: Colors.black,
              //           fontSize: 15,
              //           letterSpacing: 1.4,
              //         ),
              //   ),
              // ),
            ),
        ],
      ),
    );
  }
}
