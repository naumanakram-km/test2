import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

// 3rd party packages
import 'package:provider/provider.dart';

// Provider
import 'package:sofiqe/provider/cart_provider.dart';

// Custom packages
import 'package:sofiqe/widgets/cart/cart_item.dart';

import '../../controller/cart_loader_controller.dart';
import '../../screens/MS8/looks_package_details.dart';
import '../../screens/product_detail_1_screen.dart';

class CartItemList extends StatefulWidget {
  const CartItemList({Key? key}) : super(key: key);

  @override
  _CartItemListState createState() => _CartItemListState();
}

class _CartItemListState extends State<CartItemList> {
  @override
  Widget build(BuildContext context) {
    List<dynamic> cartItems = Provider
        .of<CartProvider>(context, listen: true)
        .cart ?? [];

    return Obx(() {
      return ModalProgressHUD(
        inAsyncCall: LoaderController.instance
            .isProcessing.value,
        // inAsyncCall: Provider
        //     .of<CartProvider>(context)
        //     .isProcessing,
        progressIndicator: SpinKitDoubleBounce(
          color: Colors.black,
          // color: Color(0xffF2CA8A),
          size: 50.0,
        ),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    children: [


                      ...List.generate(cartItems.length, (index) {
                        if (cartItems[index]['price'] == 0 &&
                            cartItems.any((element) =>
                            cartItems[index]['extension_attributes']['look_name'] ==
                                element['name'])) {
                          return Container();
                        }
                        return GestureDetector(
                            onTap: () {

                              if(cartItems[index]['children'].isNotEmpty){
                                // Get.to(() => LookPackageMS8(
                                //   item: cartItems[index],
                                //   looklist: const [],
                                //   id: cartItems[index]['sku'],
                                // ));
                              } else {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (BuildContext c) {
                                      return ProductDetail1Screen(
                                          sku: cartItems[index]['sku']);
                                    },
                                  ),
                                );
                              }


                            },
                            child: CartItem(item: cartItems[index]));
                      }),


                      // ...cartItems.map(
                      //   (dynamic i) {
                      //     // cPrint("===cartItem====$i");
                      //     // if (i['price'] == 0 &&
                      //     //     cartItems.any((element) => i['extension_attributes']['look_name'] == element['name']))
                      //     //   return Container();
                      //     return GestureDetector(
                      //         onTap: () {
                      //           Navigator.push(
                      //             context,
                      //             MaterialPageRoute(
                      //               builder: (BuildContext c) {
                      //                 return ProductDetail1Screen(sku: i['sku']);
                      //               },
                      //             ),
                      //           );
                      //         },
                      //         child: CartItem(item: i));
                      //   },
                      // ).toList(),
                    ],
                  )),
            )
          ],
        ),
      );
    });
  }
}