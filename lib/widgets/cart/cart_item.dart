import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/controller/cart_loader_controller.dart';
import 'package:sofiqe/provider/cart_provider.dart';
// Utils
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../../provider/catalog_provider.dart';

class CartItem extends StatefulWidget {
  final Map<String, dynamic> item;
  CartItem({Key? key, required this.item}) : super(key: key);

  @override
  _CartItemState createState() => _CartItemState();
}

class _CartItemState extends State<CartItem> {
  // late final List listItem = [1, 2, 3, 4, 5];

  var image = "".obs;
  final CatalogProvider catp = Get.find();
  late int valueChoose;
  // late int valueChoose = widget.item['qty'];

  RxBool showChildren = false.obs;

  @override
  void initState() {
    super.initState();
    setItemQuantity();
    // loadProductImage();
  }

  setItemQuantity() {
    setState(() {
      valueChoose = widget.item['qty'];
    });
  }

  @override
  Widget build(BuildContext context) {
    final shouldShowDropdown = widget.item['children'] != null && widget.item['children'] != [] && (widget.item['children']?.length ?? 0) > 0;
    final shouldShowShadeColor = widget.item['extension_attributes']['shade_color'] != null && (widget.item['extension_attributes']['shade_color']?.toString().trim() ?? '').isNotEmpty;
    final shouldShowSize = widget.item['extension_attributes']['size'] != null && (widget.item['extension_attributes']['size']?.toString().trim() ?? '').isNotEmpty;
    image.value = widget.item['extension_attributes']['image'];

    log("===== children are here :: ${widget.item['children']}  =======");

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2.0),
      child: Card(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 25, top: 29),
                  child: Obx(() => image.isEmpty
                      ? Image.asset(
                          'assets/icons/lip_1.png',
                          width: 50,
                          // width: 9,
                          // height: 5,
                        )
                      : Image.network(
                      widget.item['extension_attributes']['image'],
                      width: 50, fit: BoxFit.fill,
                          errorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
                          return Image.asset(
                            'assets/images/sofiqe_grey_default.png',
                            width: 50,
                            // width: 9,
                            // height: 5,
                          );
                        })),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 28,
                      ),
                      Text(
                        '${widget.item['name']}',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'Arial, Bold',
                            fontWeight: FontWeight.bold,
                            color: SplashScreenPageColors.backgroundColor),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text("sku: ${widget.item['sku']}".toUpperCase(),

                              style: TextStyle(
                                fontSize: 11,
                                 fontFamily: 'Arial, Regular',
                                color: SplashScreenPageColors.backgroundColor,
                              ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                          if(shouldShowShadeColor)
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text('Colour:'.toUpperCase(),
                                  style: TextStyle(
                                    fontSize: 11,
                                    fontFamily: 'Arial, Regular',
                                    color: SplashScreenPageColors.backgroundColor,
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Container(
                                  height: 12,
                                  width: 12,
                                  decoration: BoxDecoration(
                                      color: Color(int.parse(
                                          widget.item['extension_attributes']['shade_color']
                                              .toString()
                                              .substring(1, 7),
                                          radix: 16) +
                                          0xFF000000),
                                  ),
                                ),
                              ],
                            ),
                          if(shouldShowSize)
                            SizedBox(
                              height: 2,
                            ),
                          if(shouldShowSize)
                            Text("size: ${widget.item['extension_attributes']['size'] ?? ''}".toUpperCase(),
                              style: TextStyle(
                                fontSize: 11,
                                fontFamily: 'Arial, Regular',
                                color: SplashScreenPageColors.backgroundColor,
                              ),
                            ),
                          if(shouldShowShadeColor || shouldShowSize)
                            SizedBox(
                              height: 5,
                            ),
                          ],
                        ),
                      if (shouldShowDropdown)
                        InkWell(
                          onTap: () {
                            showChildren.value = !showChildren.value;
                          },
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text('${widget.item['children']?.length} Products'),
                              Obx(
                                () => Icon(showChildren.value ? Icons.arrow_drop_down : Icons.arrow_right),
                              )
                            ],
                          ),
                        )
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 11.5, right: 11.5),
                      child: IconButton(
                        onPressed: () {
                          // Provider.of<CartProvider>(context, listen: false).changeProcessingVal(true, isRefresh: true);

                          LoaderController.instance.isProcessing.value = true;

                          // LoaderController.instance.changeProcessingVal(true);

                          Provider.of<CartProvider>(context, listen: false)
                              .removeFromCart(
                                  context, widget.item['item_id'].toString(),
                                  isUpdateLoader: true);
                        },
                        icon: Icon(
                          Icons.delete,
                          size: 20,
                        ),
                        color: Colors.grey.shade300,
                        // iconSize: 20,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 5, top: 10),
                          child: Icon(
                            Icons.circle,
                            color: widget.item['qty'] < 1
                                ? Colors.red
                                : Colors.green,
                            size: 10,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 23.5, top: 10),
                          child: Text(
                            widget.item['qty'] < 1
                                ? 'OUT OF STOCK'
                                : 'IN STOCK',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 7, fontFamily: 'Arial, Regular'),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            if (shouldShowDropdown)
              Obx(() => Visibility(
                  visible: showChildren.value,
                  child: Column(
                    children: List.generate(
                        widget.item['children']?.length ?? 0,
                        (index) => Container(
                              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      SizedBox(
                                        width: 90,
                                        child: Text(
                                          widget.item['children'][index]
                                                  ['face_sub_area']
                                              .toString()
                                              .toUpperCase(),
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Arial, Regular',
                                            color: SplashScreenPageColors
                                                .backgroundColor,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      if(widget.item['children'][index]['shade_color'] != null && (widget.item['children'][index]['shade_color']?.toString().trim() ?? '').isNotEmpty)
                                      Container(
                                        height: 15,
                                        width: 15,
                                        decoration: BoxDecoration(
                                            color: Color(int.parse(
                                                    widget.item['children']
                                                            [index]
                                                            ['shade_color']
                                                        .toString()
                                                        .substring(1, 7),
                                                    radix: 16) +
                                                0xFF000000)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                          child: Column(
                                        children: [
                                          Text(
                                            widget.item['children'][index]
                                                ['name'],
                                            style: TextStyle(
                                              fontSize: 12.5,
                                              fontFamily: 'Arial, Regular',
                                              color: SplashScreenPageColors
                                                  .backgroundColor,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              "QTY: " +
                                                  (widget.item['children']
                                                                  [index]
                                                              ['product_qty'] ??
                                                          0)
                                                      .toString(),
                                              style: TextStyle(
                                                fontSize: 13,
                                                fontFamily: 'Arial, Regular',
                                                color: SplashScreenPageColors
                                                    .backgroundColor,
                                              ),
                                            ),
                                          ),
                                        ],
                                      )),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Divider(
                                    height: 0,
                                    color: AppColors.secondaryColor,
                                  ),
                                ],
                              ),
                            )),
                  ))),

            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Divider(
                height: 4,
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // Row(
                  //   children: [
                  //     InkWell(
                  //       onTap: () async {
                  //         if(widget.item['qty'] == 1){
                  //           Get.showSnackbar(GetSnackBar(
                  //             message: 'You have reached the min limit of 1',
                  //             duration: Duration(seconds: 2),
                  //           ));
                  //         } else {
                  //
                  //           var newVal = widget.item['qty'] - 1;
                  //           await changeQuantity(context, newVal, widget.item);
                  //
                  //         }
                  //
                  //       },
                  //       child: Container(
                  //         padding: EdgeInsets.all(5),
                  //         decoration: BoxDecoration(
                  //           shape: BoxShape.circle,
                  //           color: Colors.black12
                  //         ),
                  //         child: Icon(
                  //           Icons.remove,
                  //           color: Colors.black,
                  //           size: 14,
                  //         ),
                  //       ),
                  //     ),
                  //
                  //
                  //     Text(
                  //       "  " + widget.item['qty'].toString() + "  ",
                  //       style: TextStyle(
                  //         fontSize: 12,
                  //         fontFamily: 'Arial, Regular',
                  //         color: SplashScreenPageColors.backgroundColor,
                  //       ),
                  //     ),
                  //
                  //
                  //     InkWell(
                  //       onTap: () async {
                  //         if(widget.item['qty'] == 5){
                  //           Get.showSnackbar(GetSnackBar(
                  //             message: 'You have reached the max limit of 5',
                  //             duration: Duration(seconds: 2),
                  //           ));
                  //         } else {
                  //
                  //           // Provider.of<CartProvider>(context, listen: false).changeProcessingVal(true, isRefresh: true);
                  //
                  //
                  //           var newVal = widget.item['qty'] + 1;
                  //                 await changeQuantity(context, newVal, widget.item);
                  //
                  //
                  //         }
                  //
                  //       },
                  //       child: Container(
                  //         padding: EdgeInsets.all(5),
                  //         decoration: BoxDecoration(
                  //             shape: BoxShape.circle,
                  //             color: Colors.black12
                  //         ),
                  //         child: Icon(
                  //           Icons.add,
                  //           color: Colors.black,
                  //           size: 14,
                  //         ),
                  //       ),
                  //     ),
                  //
                  //
                  //
                  //
                  //   ],
                  // ),

                  //
                  // if (widget.item['children'].isNotEmpty) Text('',
                  //         style: TextStyle(
                  //           fontSize: 12,
                  //           fontFamily: 'Arial, Regular',
                  //           color: SplashScreenPageColors.backgroundColor,
                  //         ),
                  // ) else
                  Container(
                    width: 40,
                    height: 40,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(12)),
                    child: DropdownButton(
                      menuMaxHeight: MediaQuery.of(context).size.height * 0.4,
                      elevation: 0,
                      value: widget.item['qty'],
                      icon: Icon(Icons.expand_more_sharp),
                      iconSize: 30,
                      isExpanded: true,
                      items: Provider.of<CartProvider>(context)
                          .listItem
                          .map((valueItem) {
                        return DropdownMenuItem(
                            value: valueItem, child: Text('$valueItem'));
                      }).toList(),
                      onChanged: (newValue) async {
                        // valueChoose = newValue as int;
                        // setState(() {});
                        await changeQuantity(
                            context, newValue as int, widget.item,
                            isItLooks: widget.item['children'].isNotEmpty
                                ? true
                                : false);
                      },
                    ),
                  ),
                  Text(
                    '${(widget.item['price'] as num).toDouble().toString().toProperCurrency()}',
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 11,
                      fontFamily: 'Arial, Regular',
                      color: SplashScreenPageColors.backgroundColor,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> changeQuantity(BuildContext c, int current, Map item,
      {bool isItLooks = false}) async {
    var cartProvider = Provider.of<CartProvider>(c, listen: false);
    // cartProvider.changeProcessingVal(true);
    // LoaderController.instance.changeProcessingVal(true);

    LoaderController.instance.isProcessing.value = true;

    cPrint(item);
    int type = item['product_type'] == 'configurable' ? 1 : 0;
    try {
      await cartProvider.removeFromCart(c, '${item['item_id']}',
          refresh: false);
      isItLooks
          ? await cartProvider.addToCart(
              Get.context!, item['sku'],
              // [],
              item['product_option'] == null
                  ? []
                  : item['product_option']['extension_attributes'] == null
                      ? []
                      : item['product_option']['extension_attributes']
                          ['custom_options'],
              1,
              item["name"],
              refresh: false,
              quantity: current,
            )
          : await cartProvider.addToCart(
              Get.context!,
              item['sku'] != null
                  ? item['sku'].toString().contains("-")
                      ? item['sku'].toString().split("-").first
                      : item['sku']
                  : "",
              item['product_option'] == null
                  ? []
                  : item['product_option']['extension_attributes'] == null
                      ? []
                      : item['product_option']['extension_attributes'][
                          '${type == 1 ? 'custom_options' : 'configurable_item_options'}'],
              type,
              "",
              refresh: false,
              quantity: current,
            );

      log("===== at here =======");

      cartProvider.fetchCartDetails();
    } catch (e) {
      // LoaderController.instance.changeProcessingVal(false);
      LoaderController.instance.isProcessing.value = false;
      // cartProvider.changeProcessingVal(false);
      log("==== getting this error :: ${e.toString()} ====");
      cPrint(e.toString());
    }
  }
}
