import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/provider/cart_provider.dart';
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../../provider/account_provider.dart';

class CartPriceDistribution extends StatefulWidget {
  CartPriceDistribution({Key? key}) : super(key: key);

  @override
  _CartPriceDistributionState createState() => _CartPriceDistributionState();
}

class _CartPriceDistributionState extends State<CartPriceDistribution> {

  List<Map<String, dynamic>>? charges =  Provider.of<CartProvider>(Get.context!, listen: false).chargesList;

  @override
  void initState() {
    // todo: implement initState
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      Provider.of<CartProvider>(context, listen: false).fetchTiersList(
          Provider.of<AccountProvider>(context , listen: false).customerId);
      Provider.of<CartProvider>(context, listen: false).calculateCartPrice();
     await Provider.of<CartProvider>(context, listen: false)
          .fetchVipCoins(Provider.of<AccountProvider>(
          context, listen: false)
          .customerId, '2 . initstate of cart price distribution file');

    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    /// For Test Comments : Might be have to revert this change. So make it commented. - Working in Process
    // double minusAmount = (double.parse(Provider.of<AccountProvider>(context, listen: false).freeShippingAmount) -
    //     double.parse(Provider.of<CartProvider>(context).chargesList[0]['amount'].toString()));
    // Provider.of<CartProvider>(context).fetchTiersList(
    //     Provider.of<AccountProvider>(context).customerId);
    //
    // Provider.of<CartProvider>(context)
    //       .fetchVipCoins(Provider.of<AccountProvider>(
    //               context)
    //           .customerId, '2 . future builder of cart price distribution');
    // return FutureBuilder(
    //   future: Provider.of<CartProvider>(context).calculateCartPrice(),
    //   builder: (BuildContext _, snapshote) {
    //     List<Map<String, dynamic>> charges =
    //         Provider.of<CartProvider>(context).chargesList;
        return Container(
          child: Column(
            children: [
              Center(
                child: Center(
                  child: Container(
                    color: Color(0xFFBBF8D1),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Checkout now and earn ',
                            style: TextStyle(
                              fontSize: 12,
                              fontFamily: 'Arial, Regular',
                              color: Color(0xFF389D5A),
                            ),
                          ),
                          SizedBox(
                            width: 10.0,
                            child: Image.asset(
                              "assets/images/coin.png",
                            ),
                          ),

                        Consumer<CartProvider>(
                           builder: (context, provider, child) {
                        return Text(
                          provider.userReward
                              .toString() +
                              ' VIP Points for this order',
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Arial, Regular',
                            color: Color(0xFF389D5A),
                          ),
                        );
                       },
                       ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Center(
                child: Container(
                  color: Color(0xFFBBF8D1),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, right: 8.0, top: 5.0, bottom: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        !Provider.of<AccountProvider>(context, listen: false)
                                .isLoggedIn
                            ? Expanded(
                                flex: 1,
                                child: Center(
                                  child: Text(
                                    'Applies only to registered customers, may vary when logged in. ',
                                    softWrap: true,
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontFamily: 'Arial, Regular',
                                      color: Color(0xFF389D5A),
                                    ),
                                  ),
                                ),
                              )
                            : Container()
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'TOTAL',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'Arial, Bold',
                        fontWeight: FontWeight.bold,
                        color: SplashScreenPageColors.backgroundColor,
                      ),
                    ),
                    Text(
                      '${(charges![0]['amount'] as num).toDouble().toString().toProperCurrency()}',
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Arial, Regular',
                        color: SplashScreenPageColors.backgroundColor,
                      ),
                    )
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Total saving using VIP points',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12,
                        fontFamily: 'Arial, Bold',
                        color: SplashScreenPageColors.backgroundColor,
                      ),
                    ),
                    FutureBuilder(
                        future: Provider.of<CartProvider>(context)
                            .fetchTiersList(Provider.of<AccountProvider>(
                                    context,
                                    listen: false)
                                .customerId),
                        builder: (context, snapshot) {
                          return Text(
                            '${getTotalSaving(charges, context).toStringAsFixed(2).toProperCurrency()}',
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontSize: 12,
                              fontFamily: 'Arial, Bold',
                              color: SplashScreenPageColors.backgroundColor,
                            ),
                          );
                        }),
                  ],
                ),
              ),
            ],
          ),
        );
    //   },
    // );
  }

  int getPoints(charges, reward) {
    if (Provider.of<AccountProvider>(context, listen: false).isLoggedIn) {
      return reward;
    } else {
      double finalValue = (reward * (charges[3]['amount'] as num).toDouble());
      return finalValue.floor();
    }
  }

  double getTotalSaving(charges, context) {
    double finalValue = (0.1 * (charges[3]['amount'] as num).toDouble());
    return finalValue;
  }
}