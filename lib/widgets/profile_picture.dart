import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/provider/account_provider.dart';

class ProfilePicture extends StatelessWidget {
  final bool showDummyImage;

  const ProfilePicture({Key? key, this.showDummyImage = false})
      : super(key: key);

  // Future<File> getProfilePicture() async {
  //   var dir = await getExternalStorageDirectory();
  //   File file = File(join(dir!.path, 'front_facing.jpg'));
  //   return file;
  // }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final accountProvider = Provider.of<AccountProvider>(context, listen: true);
    if (!accountProvider.isLoggedIn) {
      return ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(size.height * 0.1)),
        child: showDummyImage
            ? Container(
                height: size.height * 0.08,
                width: size.height * 0.08,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                          'assets/images/mysofiqe.png',
                        ),
                        fit: BoxFit.cover)),
              )
            : Container(
                color: Colors.white,
                height: size.height * 0.08,
                width: size.height * 0.08,
                child: Center(
                  child: Text(
                    textAlign: TextAlign.center,
                    'Not logged in',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: Colors.black,
                          fontSize: 10,
                        ),
                  ),
                ),
              ),
      );
    }

    return accountProvider.loadingPicture.value
        ? ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(size.height * 0.1)),
            child: Container(
                color: Colors.white,
                height: size.height * 0.08,
                width: size.height * 0.08,
                child: Center(
                    child: SpinKitFadingCircle(
                  size: 40,
                  color: Colors.black,
                ))),
          )
        : accountProvider.userProfileImageUrl.value == ''
            ? ClipRRect(
                borderRadius:
                    BorderRadius.all(Radius.circular(size.height * 0.1)),
                child: Container(
                    color: Colors.white,
                    height: size.height * 0.08,
                    width: size.height * 0.08,
                    child: Center(
                        child: Text(
                      'No Image',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: Colors.black,
                            fontSize: 11,
                          ),
                    ))),
              )
            : ClipRRect(
                borderRadius:
                    BorderRadius.all(Radius.circular(size.height * 0.1)),
                child: Container(
                  color: Colors.white,
                  height: size.height * 0.08,
                  width: size.height * 0.08,
                  child: CachedNetworkImage(
                    imageUrl: '${accountProvider.userProfileImageUrl.value}',
                    fit: BoxFit.cover,
                    progressIndicatorBuilder:
                        (context, url, downloadProgress) => Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: CircularProgressIndicator(
                        value: downloadProgress.progress,
                        color: Colors.black,
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                  // child: Image.network('${snapshot.data}'),
                ),
              );
  }
}
