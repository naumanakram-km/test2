import 'package:flutter/material.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:get/instance_manager.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/controller/currencyController.dart';
import 'package:sofiqe/model/product_model.dart';
import 'package:sofiqe/model/searchAlternativecolorModel.dart';
import 'package:sofiqe/provider/cart_provider.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/widgets/png_icon.dart';
import 'package:sofiqe/widgets/product_image.dart';

import '../provider/try_it_on_provider.dart';
import '../screens/product_detail_1_screen.dart';

class TryOnProduct extends StatelessWidget {
  final Product? product;
  final bool? isDetail;
  final dynamic selectShadeOption;
  final List<dynamic>? values;
  TryOnProduct(
      {Key? key,
      this.product,
      this.isDetail,
      this.selectShadeOption,
      this.values})
      : super(key: key);
  final TryItOnProvider tiop = Get.find<TryItOnProvider>();
  @override
  Widget build(BuildContext context) {
    if (product!.sku!.isEmpty) {
      return Container();
    }
    debugPrint('==== rewards point for thi is ${product!.values}');
    print('==== rewards point for thi iseee ${product!.values}');

    return GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext c) {
                return ProductDetail1Screen(sku: product!.sku!);
              },
            ),
          );
        },
        child: Container(
          height: 97,
          decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border(
              bottom: BorderSide(color: Colors.grey[300] as Color),
            ),
          ),
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(width: 15),
              ItemImage(
                image: product!.image,
              ),
              const SizedBox(width: 15),
              Expanded(
                child: ItemInformation(
                  areaName: product!.faceSubAreaName,
                  productName: product!.name.toString(),
                  // productName: tiop.directProduct.value.toString(),
                  price: product!.price!,
                  reward: product!.rewardsPoint,
                ),
              ),
              const SizedBox(width: 15),
              ColorSelector(
                color: isDetail == true
                    ? selectShadeOption["extension_attributes"]["value_label"]
                        .toString()
                    : product!.color,
                values: isDetail == true ? values : product!.values,
              ),
              const SizedBox(width: 15),
              AddToBagButton(
                product: product!,
              ),
              const SizedBox(width: 15),
            ],
          ),
        ));
  }
}

class ItemImage extends StatelessWidget {
  final String image;

  ItemImage({
    Key? key,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      height: size.height * 0.08,
      width: size.height * 0.08,
      child: ProductImage(
        imageShortPath: image,
      ),
    );
  }
}

class ItemInformation extends StatelessWidget {
  final String areaName;
  final String productName;
  final String reward;
  final num price;

  ItemInformation(
      {Key? key,
      required this.areaName,
      required this.productName,
      required this.reward,
      required this.price})
      : super(key: key);
  final CurrencyController currencycntrl = Get.find();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      height: size.height * 0.12,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${areaName.toUpperCase()}',
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: Theme.of(context).textTheme.headline2!.copyWith(
                  color: Colors.black,
                  fontSize: 8,
                  letterSpacing: 0,
                ),
          ),
          SizedBox(
            width: size.width * 0.4,
            child: Text(
              '$productName',
              softWrap: true,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: Colors.black,
                    fontSize: 10.5,
                    letterSpacing: 0,
                  ),
            ),
          ),
          Text(
              currencycntrl.defaultCurrency.toString() +
                  ' ' +
                  price.toStringAsFixed(2),
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: Colors.black,
                    fontSize: size.height * 0.014,
                    letterSpacing: 0,
                  )),
          Row(
            children: [
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: 'Earn ' + reward,
                        style: TextStyle(color: Colors.green, fontSize: 11)),
                    WidgetSpan(
                      alignment: PlaceholderAlignment.middle,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 2.0),
                        child: PngIcon(
                          height: 12,
                          width: 12,
                          image: 'assets/images/goldencoin.png',
                        ),
                      ),
                    ),
                    TextSpan(
                        text: 'VIP points',
                        style: TextStyle(color: Colors.green, fontSize: 11)),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class ColorSelector extends StatefulWidget {
  final String color;
  final List<dynamic>? values;
  ColorSelector({
    Key? key,
    required this.color,
    required this.values,
  }) : super(key: key);

  @override
  _ColorSelectorState createState() => _ColorSelectorState();
}

class _ColorSelectorState extends State<ColorSelector> {
  Color? selectedColor; // Initial selected color

  void _showColorMenu(BuildContext context) {
    final RenderBox button = context.findRenderObject() as RenderBox;
    final Offset offset = button.localToGlobal(Offset.zero);
    final List<Map<String, dynamic>> colors =
        (widget.values as List).cast<Map<String, dynamic>>();

    final screenHeight = MediaQuery.of(context).size.height;
    final menuHeight = screenHeight * 0.5; // 50% of the screen height

    showMenu(
      context: context,
      constraints: const BoxConstraints.expand(width: 100, height: 500),
      position: RelativeRect.fromLTRB(
        offset.dx,
        offset.dy - menuHeight,
        offset.dx + button.size.width,
        offset.dy,
      ),
      items: colors.map((color) {
        Color colorValue = Color(int.parse(
                color['extension_attributes']['value_label'].substring(1),
                radix: 16) +
            0xFF000000);
        return PopupMenuItem<Color>(
          value: colorValue,
          padding: EdgeInsets.only(left: 10, bottom: 10),
          child: Container(
            width: 80,
            height: 60,
            color: colorValue,
          ),
        );
      }).toList(),
    ).then((selectedColor) {
      if (selectedColor != null) {
        setState(() {
          // Update the selected color
          this.selectedColor = selectedColor;
        });
        // Handle the selected color
        print("Selected Color: $selectedColor");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _showColorMenu(context);
      },
      child: Container(
        height: 60,
        width: 80,
        decoration: BoxDecoration(
          color:
              selectedColor ?? widget.color.toColor(), // Use selectedColor here
          border: Border.all(
            color: Color(0XFF707070),
          ),
        ),
      ),
    );
  }
}

class AddToBagButton extends StatelessWidget {
  final Product product;

  AddToBagButton({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: size.height * 0.08,
      child: GestureDetector(
        onTap: () async {
          cPrint("object;;;;;;;;;;;;;;;;;;;;;;;;");
          cPrint(product.typeid);
          // Commented for testing purpose to fast the add to cart job
          // var res = await sfAPIGetProductDetailsFromSKU(sku: product.sku!);
          // Map<String, dynamic> responseBody = json.decode(res.body);
          // cPrint("?????????????????????????????" + responseBody['type_id']);
          // String type = responseBody['type_id'];
          String type = product.typeid.toString();
          if (type == "configurable") {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext c) {
                  return ProductDetail1Screen(sku: product.sku!);
                },
              ),
            );
          } else {
            CartProvider cartP =
                Provider.of<CartProvider>(context, listen: false);
            cPrint("CartProvider  -->> SSs ${cartP.cartToken}");
            context.loaderOverlay.show();
            await cartP.addHomeProductsToCart(context, product);
            cPrint("Name  -->> EEE ${product.image}");
            context.loaderOverlay.hide();
            Get.showSnackbar(
              GetSnackBar(
                message: "Product has been successfully added to cart",
                duration: Duration(seconds: 2),
                isDismissible: true,
              ),
            );
            // Commented for Testing Purpose
            // ScaffoldMessenger.of(context).showSnackBar(
            //   SnackBar(
            //     padding: EdgeInsets.all(0),
            //     backgroundColor: Colors.black,
            //     duration: Duration(seconds: 1),
            //     content: Container(
            //       child: CustomSnackBar(
            //         sku: product.sku!,
            //         image: product.image.replaceAll(RegExp(
            //             APIEndPoints.baseUri1 +'/media/catalog/product'), ''),
            //         name: product.name!,
            //       ),
            //     ),
            //   ),
            // );
            // if(product.options != null && product.options!.isNotEmpty){
            //   Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //       builder: (BuildContext c) {
            //         return ProductDetail1Screen(sku: product.sku!);
            //       },
            //     ),
            //   );
            // }else{
            //   CartProvider cartP =
            //   Provider.of<CartProvider>(context, listen: false);

            //   await cartP.addHomeProductsToCart(context, Product(
            //         id: product.id,
            //         name: product.name,
            //         sku: product.sku,
            //         price: product.price!,
            //         image: product.image,
            //         description: product.description,
            //         faceSubArea: product.faceSubArea,
            //         avgRating: product.avgRating));
            //   ScaffoldMessenger.of(context).showSnackBar(
            //     SnackBar(
            //       padding: EdgeInsets.all(0),
            //       backgroundColor: Colors.transparent,
            //       duration: Duration(seconds: 1),
            //       content: Container(
            //         child: CustomSnackBar(
            //           sku: product.sku!,
            //           image: product.image,
            //           name: product.name!,
            //         ),
            //       ),
            //     ),
            //   );
            // }
          }
        },
        child: Container(
          height: AppBar().preferredSize.height * 0.7,
          width: AppBar().preferredSize.height * 0.7,
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.all(
                Radius.circular(AppBar().preferredSize.height * 0.7)),
          ),
          child: PngIcon(
            height: AppBar().preferredSize.height * 0.3,
            width: AppBar().preferredSize.height * 0.3,
            image: 'assets/icons/add_to_cart_white.png',
          ),
        ),
      ),
    );
  }
}
