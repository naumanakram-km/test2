import 'package:sofiqe/generated/json/base/json_convert_content.dart';
import 'package:sofiqe/model/update_profile_response_entity.dart';

UpdateProfileResponseEntity $UpdateProfileResponseEntityFromJson(
    Map<String, dynamic> json) {
  final UpdateProfileResponseEntity updateProfileResponseEntity = UpdateProfileResponseEntity();
  final String? message = jsonConvert.convert<String>(json['message']);
  if (message != null) {
    updateProfileResponseEntity.message = message;
  }
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    updateProfileResponseEntity.id = id;
  }
  final int? groupId = jsonConvert.convert<int>(json['group_id']);
  if (groupId != null) {
    updateProfileResponseEntity.groupId = groupId;
  }
  final String? defaultBilling = jsonConvert.convert<String>(
      json['default_billing']);
  if (defaultBilling != null) {
    updateProfileResponseEntity.defaultBilling = defaultBilling;
  }
  final String? defaultShipping = jsonConvert.convert<String>(
      json['default_shipping']);
  if (defaultShipping != null) {
    updateProfileResponseEntity.defaultShipping = defaultShipping;
  }
  final String? createdAt = jsonConvert.convert<String>(json['created_at']);
  if (createdAt != null) {
    updateProfileResponseEntity.createdAt = createdAt;
  }
  final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
  if (updatedAt != null) {
    updateProfileResponseEntity.updatedAt = updatedAt;
  }
  final String? createdIn = jsonConvert.convert<String>(json['created_in']);
  if (createdIn != null) {
    updateProfileResponseEntity.createdIn = createdIn;
  }
  final String? dob = jsonConvert.convert<String>(json['dob']);
  if (dob != null) {
    updateProfileResponseEntity.dob = dob;
  }
  final String? email = jsonConvert.convert<String>(json['email']);
  if (email != null) {
    updateProfileResponseEntity.email = email;
  }
  final String? firstname = jsonConvert.convert<String>(json['firstname']);
  if (firstname != null) {
    updateProfileResponseEntity.firstname = firstname;
  }
  final String? lastname = jsonConvert.convert<String>(json['lastname']);
  if (lastname != null) {
    updateProfileResponseEntity.lastname = lastname;
  }
  final String? middlename = jsonConvert.convert<String>(json['middlename']);
  if (middlename != null) {
    updateProfileResponseEntity.middlename = middlename;
  }
  final int? gender = jsonConvert.convert<int>(json['gender']);
  if (gender != null) {
    updateProfileResponseEntity.gender = gender;
  }
  final int? storeId = jsonConvert.convert<int>(json['store_id']);
  if (storeId != null) {
    updateProfileResponseEntity.storeId = storeId;
  }
  final int? websiteId = jsonConvert.convert<int>(json['website_id']);
  if (websiteId != null) {
    updateProfileResponseEntity.websiteId = websiteId;
  }
  final List<
      UpdateProfileResponseAddresses>? addresses = (json['addresses'] as List<
      dynamic>?)?.map(
          (e) =>
      jsonConvert.convert<UpdateProfileResponseAddresses>(
          e) as UpdateProfileResponseAddresses).toList();
  if (addresses != null) {
    updateProfileResponseEntity.addresses = addresses;
  }
  final int? disableAutoGroupChange = jsonConvert.convert<int>(
      json['disable_auto_group_change']);
  if (disableAutoGroupChange != null) {
    updateProfileResponseEntity.disableAutoGroupChange = disableAutoGroupChange;
  }
  final UpdateProfileResponseExtensionAttributes? extensionAttributes = jsonConvert
      .convert<UpdateProfileResponseExtensionAttributes>(
      json['extension_attributes']);
  if (extensionAttributes != null) {
    updateProfileResponseEntity.extensionAttributes = extensionAttributes;
  }
  final List<
      UpdateProfileResponseCustomAttributes>? customAttributes = (json['custom_attributes'] as List<
      dynamic>?)?.map(
          (e) =>
      jsonConvert.convert<UpdateProfileResponseCustomAttributes>(
          e) as UpdateProfileResponseCustomAttributes).toList();
  if (customAttributes != null) {
    updateProfileResponseEntity.customAttributes = customAttributes;
  }
  return updateProfileResponseEntity;
}

Map<String, dynamic> $UpdateProfileResponseEntityToJson(
    UpdateProfileResponseEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['message'] = entity.message;
  data['id'] = entity.id;
  data['group_id'] = entity.groupId;
  data['default_billing'] = entity.defaultBilling;
  data['default_shipping'] = entity.defaultShipping;
  data['created_at'] = entity.createdAt;
  data['updated_at'] = entity.updatedAt;
  data['created_in'] = entity.createdIn;
  data['dob'] = entity.dob;
  data['email'] = entity.email;
  data['firstname'] = entity.firstname;
  data['lastname'] = entity.lastname;
  data['middlename'] = entity.middlename;
  data['gender'] = entity.gender;
  data['store_id'] = entity.storeId;
  data['website_id'] = entity.websiteId;
  data['addresses'] = entity.addresses?.map((v) => v.toJson()).toList();
  data['disable_auto_group_change'] = entity.disableAutoGroupChange;
  data['extension_attributes'] = entity.extensionAttributes?.toJson();
  data['custom_attributes'] =
      entity.customAttributes?.map((v) => v.toJson()).toList();
  return data;
}

extension UpdateProfileResponseEntityExtension on UpdateProfileResponseEntity {
  UpdateProfileResponseEntity copyWith({
    String? message,
    int? id,
    int? groupId,
    String? defaultBilling,
    String? defaultShipping,
    String? createdAt,
    String? updatedAt,
    String? createdIn,
    String? dob,
    String? email,
    String? firstname,
    String? lastname,
    String? middlename,
    int? gender,
    int? storeId,
    int? websiteId,
    List<UpdateProfileResponseAddresses>? addresses,
    int? disableAutoGroupChange,
    UpdateProfileResponseExtensionAttributes? extensionAttributes,
    List<UpdateProfileResponseCustomAttributes>? customAttributes,
  }) {
    return UpdateProfileResponseEntity()
      ..message = message ?? this.message
      ..id = id ?? this.id
      ..groupId = groupId ?? this.groupId
      ..defaultBilling = defaultBilling ?? this.defaultBilling
      ..defaultShipping = defaultShipping ?? this.defaultShipping
      ..createdAt = createdAt ?? this.createdAt
      ..updatedAt = updatedAt ?? this.updatedAt
      ..createdIn = createdIn ?? this.createdIn
      ..dob = dob ?? this.dob
      ..email = email ?? this.email
      ..firstname = firstname ?? this.firstname
      ..lastname = lastname ?? this.lastname
      ..middlename = middlename ?? this.middlename
      ..gender = gender ?? this.gender
      ..storeId = storeId ?? this.storeId
      ..websiteId = websiteId ?? this.websiteId
      ..addresses = addresses ?? this.addresses
      ..disableAutoGroupChange = disableAutoGroupChange ??
          this.disableAutoGroupChange
      ..extensionAttributes = extensionAttributes ?? this.extensionAttributes
      ..customAttributes = customAttributes ?? this.customAttributes;
  }
}

UpdateProfileResponseAddresses $UpdateProfileResponseAddressesFromJson(
    Map<String, dynamic> json) {
  final UpdateProfileResponseAddresses updateProfileResponseAddresses = UpdateProfileResponseAddresses();
  final int? id = jsonConvert.convert<int>(json['id']);
  if (id != null) {
    updateProfileResponseAddresses.id = id;
  }
  final int? customerId = jsonConvert.convert<int>(json['customer_id']);
  if (customerId != null) {
    updateProfileResponseAddresses.customerId = customerId;
  }
  final UpdateProfileResponseAddressesRegion? region = jsonConvert.convert<
      UpdateProfileResponseAddressesRegion>(json['region']);
  if (region != null) {
    updateProfileResponseAddresses.region = region;
  }
  final int? regionId = jsonConvert.convert<int>(json['region_id']);
  if (regionId != null) {
    updateProfileResponseAddresses.regionId = regionId;
  }
  final String? countryId = jsonConvert.convert<String>(json['country_id']);
  if (countryId != null) {
    updateProfileResponseAddresses.countryId = countryId;
  }
  final List<String>? street = (json['street'] as List<dynamic>?)?.map(
          (e) => jsonConvert.convert<String>(e) as String).toList();
  if (street != null) {
    updateProfileResponseAddresses.street = street;
  }
  final String? telephone = jsonConvert.convert<String>(json['telephone']);
  if (telephone != null) {
    updateProfileResponseAddresses.telephone = telephone;
  }
  final String? postcode = jsonConvert.convert<String>(json['postcode']);
  if (postcode != null) {
    updateProfileResponseAddresses.postcode = postcode;
  }
  final String? city = jsonConvert.convert<String>(json['city']);
  if (city != null) {
    updateProfileResponseAddresses.city = city;
  }
  final String? firstname = jsonConvert.convert<String>(json['firstname']);
  if (firstname != null) {
    updateProfileResponseAddresses.firstname = firstname;
  }
  final String? lastname = jsonConvert.convert<String>(json['lastname']);
  if (lastname != null) {
    updateProfileResponseAddresses.lastname = lastname;
  }
  final bool? defaultShipping = jsonConvert.convert<bool>(
      json['default_shipping']);
  if (defaultShipping != null) {
    updateProfileResponseAddresses.defaultShipping = defaultShipping;
  }
  final bool? defaultBilling = jsonConvert.convert<bool>(
      json['default_billing']);
  if (defaultBilling != null) {
    updateProfileResponseAddresses.defaultBilling = defaultBilling;
  }
  return updateProfileResponseAddresses;
}

Map<String, dynamic> $UpdateProfileResponseAddressesToJson(
    UpdateProfileResponseAddresses entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['id'] = entity.id;
  data['customer_id'] = entity.customerId;
  data['region'] = entity.region?.toJson();
  data['region_id'] = entity.regionId;
  data['country_id'] = entity.countryId;
  data['street'] = entity.street;
  data['telephone'] = entity.telephone;
  data['postcode'] = entity.postcode;
  data['city'] = entity.city;
  data['firstname'] = entity.firstname;
  data['lastname'] = entity.lastname;
  data['default_shipping'] = entity.defaultShipping;
  data['default_billing'] = entity.defaultBilling;
  return data;
}

extension UpdateProfileResponseAddressesExtension on UpdateProfileResponseAddresses {
  UpdateProfileResponseAddresses copyWith({
    int? id,
    int? customerId,
    UpdateProfileResponseAddressesRegion? region,
    int? regionId,
    String? countryId,
    List<String>? street,
    String? telephone,
    String? postcode,
    String? city,
    String? firstname,
    String? lastname,
    bool? defaultShipping,
    bool? defaultBilling,
  }) {
    return UpdateProfileResponseAddresses()
      ..id = id ?? this.id
      ..customerId = customerId ?? this.customerId
      ..region = region ?? this.region
      ..regionId = regionId ?? this.regionId
      ..countryId = countryId ?? this.countryId
      ..street = street ?? this.street
      ..telephone = telephone ?? this.telephone
      ..postcode = postcode ?? this.postcode
      ..city = city ?? this.city
      ..firstname = firstname ?? this.firstname
      ..lastname = lastname ?? this.lastname
      ..defaultShipping = defaultShipping ?? this.defaultShipping
      ..defaultBilling = defaultBilling ?? this.defaultBilling;
  }
}

UpdateProfileResponseAddressesRegion $UpdateProfileResponseAddressesRegionFromJson(
    Map<String, dynamic> json) {
  final UpdateProfileResponseAddressesRegion updateProfileResponseAddressesRegion = UpdateProfileResponseAddressesRegion();
  final String? regionCode = jsonConvert.convert<String>(json['region_code']);
  if (regionCode != null) {
    updateProfileResponseAddressesRegion.regionCode = regionCode;
  }
  final String? region = jsonConvert.convert<String>(json['region']);
  if (region != null) {
    updateProfileResponseAddressesRegion.region = region;
  }
  final int? regionId = jsonConvert.convert<int>(json['region_id']);
  if (regionId != null) {
    updateProfileResponseAddressesRegion.regionId = regionId;
  }
  return updateProfileResponseAddressesRegion;
}

Map<String, dynamic> $UpdateProfileResponseAddressesRegionToJson(
    UpdateProfileResponseAddressesRegion entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['region_code'] = entity.regionCode;
  data['region'] = entity.region;
  data['region_id'] = entity.regionId;
  return data;
}

extension UpdateProfileResponseAddressesRegionExtension on UpdateProfileResponseAddressesRegion {
  UpdateProfileResponseAddressesRegion copyWith({
    String? regionCode,
    String? region,
    int? regionId,
  }) {
    return UpdateProfileResponseAddressesRegion()
      ..regionCode = regionCode ?? this.regionCode
      ..region = region ?? this.region
      ..regionId = regionId ?? this.regionId;
  }
}

UpdateProfileResponseExtensionAttributes $UpdateProfileResponseExtensionAttributesFromJson(
    Map<String, dynamic> json) {
  final UpdateProfileResponseExtensionAttributes updateProfileResponseExtensionAttributes = UpdateProfileResponseExtensionAttributes();
  final bool? isSubscribed = jsonConvert.convert<bool>(json['is_subscribed']);
  if (isSubscribed != null) {
    updateProfileResponseExtensionAttributes.isSubscribed = isSubscribed;
  }
  return updateProfileResponseExtensionAttributes;
}

Map<String, dynamic> $UpdateProfileResponseExtensionAttributesToJson(
    UpdateProfileResponseExtensionAttributes entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['is_subscribed'] = entity.isSubscribed;
  return data;
}

extension UpdateProfileResponseExtensionAttributesExtension on UpdateProfileResponseExtensionAttributes {
  UpdateProfileResponseExtensionAttributes copyWith({
    bool? isSubscribed,
  }) {
    return UpdateProfileResponseExtensionAttributes()
      ..isSubscribed = isSubscribed ?? this.isSubscribed;
  }
}

UpdateProfileResponseCustomAttributes $UpdateProfileResponseCustomAttributesFromJson(
    Map<String, dynamic> json) {
  final UpdateProfileResponseCustomAttributes updateProfileResponseCustomAttributes = UpdateProfileResponseCustomAttributes();
  final String? attributeCode = jsonConvert.convert<String>(
      json['attribute_code']);
  if (attributeCode != null) {
    updateProfileResponseCustomAttributes.attributeCode = attributeCode;
  }
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    updateProfileResponseCustomAttributes.value = value;
  }
  return updateProfileResponseCustomAttributes;
}

Map<String, dynamic> $UpdateProfileResponseCustomAttributesToJson(
    UpdateProfileResponseCustomAttributes entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['attribute_code'] = entity.attributeCode;
  data['value'] = entity.value;
  return data;
}

extension UpdateProfileResponseCustomAttributesExtension on UpdateProfileResponseCustomAttributes {
  UpdateProfileResponseCustomAttributes copyWith({
    String? attributeCode,
    String? value,
  }) {
    return UpdateProfileResponseCustomAttributes()
      ..attributeCode = attributeCode ?? this.attributeCode
      ..value = value ?? this.value;
  }
}