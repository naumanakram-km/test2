/// section_id : ["1"]
/// section_name : "Section 1 - Home Page Banner"
/// banner_id : "4"
/// banner_name : "Christmas"
/// startdatetime : "2022-12-01"
/// enddatetime : "2023-04-01"
/// country : ["All"]
/// redirect_url : ""

class BannerModel {
  BannerModel({
      List<String>? sectionId, 
      String? sectionName, 
      String? bannerId, 
      String? bannerName, 
      String? bannerContent, 
      String? startdatetime, 
      String? enddatetime, 
      List<String>? country, 
      String? redirectUrl,}){
    _sectionId = sectionId;
    _sectionName = sectionName;
    _bannerId = bannerId;
    _bannerName = bannerName;
    _bannerContent = bannerContent;
    _startdatetime = startdatetime;
    _enddatetime = enddatetime;
    _country = country;
    _redirectUrl = redirectUrl;
}

  BannerModel.fromJson(dynamic json) {
    _sectionId = json['section_id'] != null ? json['section_id'].cast<String>() : [];
    _sectionName = json['section_name'];
    _bannerId = json['banner_id'];
    _bannerName = json['banner_name'];
    _bannerContent = json['banner_content'];
    _startdatetime = json['startdatetime'];
    _enddatetime = json['enddatetime'];
    _country = json['country'] != null ? json['country'].cast<String>() : [];
    _redirectUrl = json['redirect_url'];
  }
  List<String>? _sectionId;
  String? _sectionName;
  String? _bannerId;
  String? _bannerName;
  String? _bannerContent;
  String? _startdatetime;
  String? _enddatetime;
  List<String>? _country;
  String? _redirectUrl;
BannerModel copyWith({  List<String>? sectionId,
  String? sectionName,
  String? bannerId,
  String? bannerName,
  String? bannerContent,
  String? startdatetime,
  String? enddatetime,
  List<String>? country,
  String? redirectUrl,
}) => BannerModel(  sectionId: sectionId ?? _sectionId,
  sectionName: sectionName ?? _sectionName,
  bannerId: bannerId ?? _bannerId,
  bannerName: bannerName ?? _bannerName,
  bannerContent: bannerContent ?? _bannerContent,
  startdatetime: startdatetime ?? _startdatetime,
  enddatetime: enddatetime ?? _enddatetime,
  country: country ?? _country,
  redirectUrl: redirectUrl ?? _redirectUrl,
);
  List<String>? get sectionId => _sectionId;
  String? get sectionName => _sectionName;
  String? get bannerId => _bannerId;
  String? get bannerName => _bannerName;
  String? get bannerContent => _bannerContent;
  String? get startdatetime => _startdatetime;
  String? get enddatetime => _enddatetime;
  List<String>? get country => _country;
  String? get redirectUrl => _redirectUrl;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['section_id'] = _sectionId;
    map['section_name'] = _sectionName;
    map['banner_id'] = _bannerId;
    map['banner_name'] = _bannerName;
    map['banner_content'] = _bannerContent;
    map['startdatetime'] = _startdatetime;
    map['enddatetime'] = _enddatetime;
    map['country'] = _country;
    map['redirect_url'] = _redirectUrl;
    return map;
  }

}