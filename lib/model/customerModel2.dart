import 'AddressClass.dart';

class CustomerModel2 {
  CustomerModel2({
      num? id, 
      num? groupId, 
      String? defaultBilling, 
      String? defaultShipping, 
      String? email, 
      String? firstname, 
      String? lastname, 
      num? gender, 
      num? storeId, 
      num? websiteId, 
      List<Addresses1>? addresses,
      List<dynamic>? customAttributes,}){
    _id = id;
    _groupId = groupId;
    _defaultBilling = defaultBilling;
    _defaultShipping = defaultShipping;
    _email = email;
    _firstname = firstname;
    _lastname = lastname;
    _gender = gender;
    _storeId = storeId;
    _websiteId = websiteId;
    _addresses = addresses;
    _customAttributes = customAttributes;
}

  CustomerModel2.fromJson(dynamic json) {
    _id = json['id'];
    _groupId = json['group_id'];
    _defaultBilling = json['default_billing'];
    _defaultShipping = json['default_shipping'];
    _email = json['email'];
    _firstname = json['firstname'];
    _lastname = json['lastname'];
    _gender = json['gender'];
    _storeId = json['store_id'];
    _websiteId = json['website_id'];
    if (json['addresses'] != null) {
      _addresses = [];
      json['addresses'].forEach((v) {
        _addresses?.add(Addresses1.fromJson(v));
      });
    }
    if (json['custom_attributes'] != null) {
      _customAttributes = [];
      json['custom_attributes'].forEach((v) {
        _customAttributes?.add(json['custom_attributes'].fromJson(v));
      });
    }
  }
  num? _id;
  num? _groupId;
  String? _defaultBilling;
  String? _defaultShipping;
  String? _email;
  String? _firstname;
  String? _lastname;
  num? _gender;
  num? _storeId;
  num? _websiteId;
  List<Addresses1>? _addresses;
  List<dynamic>? _customAttributes;
CustomerModel2 copyWith({  num? id,
  num? groupId,
  String? defaultBilling,
  String? defaultShipping,
  String? email,
  String? firstname,
  String? lastname,
  num? gender,
  num? storeId,
  num? websiteId,
  List<Addresses1>? addresses,
  List<dynamic>? customAttributes,
}) => CustomerModel2(  id: id ?? _id,
  groupId: groupId ?? _groupId,
  defaultBilling: defaultBilling ?? _defaultBilling,
  defaultShipping: defaultShipping ?? _defaultShipping,
  email: email ?? _email,
  firstname: firstname ?? _firstname,
  lastname: lastname ?? _lastname,
  gender: gender ?? _gender,
  storeId: storeId ?? _storeId,
  websiteId: websiteId ?? _websiteId,
  addresses: addresses ?? _addresses,
  customAttributes: customAttributes ?? _customAttributes,
);
  num? get id => _id;
  num? get groupId => _groupId;
  String? get defaultBilling => _defaultBilling;
  String? get defaultShipping => _defaultShipping;
  String? get email => _email;
  String? get firstname => _firstname;
  String? get lastname => _lastname;
  num? get gender => _gender;
  num? get storeId => _storeId;
  num? get websiteId => _websiteId;
  List<Addresses1>? get addresses => _addresses;
  List<dynamic>? get customAttributes => _customAttributes;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['group_id'] = _groupId;
    map['default_billing'] = _defaultBilling;
    map['default_shipping'] = _defaultShipping;
    map['email'] = _email;
    map['firstname'] = _firstname;
    map['lastname'] = _lastname;
    map['gender'] = _gender;
    map['store_id'] = _storeId;
    map['website_id'] = _websiteId;
    if (_addresses != null) {
      map['addresses'] = _addresses?.map((v) => v.toJson()).toList();
    }
    if (_customAttributes != null) {
      map['custom_attributes'] = _customAttributes;
      // map['custom_attributes'] = _customAttributes?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class Region {
  Region({
      String? regionCode, 
      String? region, 
      num? regionId,}){
    _regionCode = regionCode;
    _region = region;
    _regionId = regionId;
}

  Region.fromJson(dynamic json) {
    _regionCode = json['region_code'];
    _region = json['region'];
    _regionId = json['region_id'];
  }
  String? _regionCode;
  String? _region;
  num? _regionId;
Region copyWith({  String? regionCode,
  String? region,
  num? regionId,
}) => Region(  regionCode: regionCode ?? _regionCode,
  region: region ?? _region,
  regionId: regionId ?? _regionId,
);
  String? get regionCode => _regionCode;
  String? get region => _region;
  num? get regionId => _regionId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['region_code'] = _regionCode;
    map['region'] = _region;
    map['region_id'] = _regionId;
    return map;
  }

}
