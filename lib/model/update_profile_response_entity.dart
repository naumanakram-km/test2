import 'dart:convert';

import 'package:sofiqe/generated/json/base/json_field.dart';
import 'package:sofiqe/generated/json/update_profile_response_entity.g.dart';

export 'package:sofiqe/generated/json/update_profile_response_entity.g.dart';

@JsonSerializable()
class UpdateProfileResponseEntity {
  String? message;
  int? id;
  @JSONField(name: "group_id")
  int? groupId;
  @JSONField(name: "default_billing")
  String? defaultBilling;
  @JSONField(name: "default_shipping")
  String? defaultShipping;
  @JSONField(name: "created_at")
  String? createdAt;
  @JSONField(name: "updated_at")
  String? updatedAt;
  @JSONField(name: "created_in")
  String? createdIn;
  String? dob;
  String? email;
  String? firstname;
  String? lastname;
  String? middlename;
  int? gender;
  @JSONField(name: "store_id")
  int? storeId;
  @JSONField(name: "website_id")
  int? websiteId;
  List<UpdateProfileResponseAddresses>? addresses;
  @JSONField(name: "disable_auto_group_change")
  int? disableAutoGroupChange;
  @JSONField(name: "extension_attributes")
  UpdateProfileResponseExtensionAttributes? extensionAttributes;
  @JSONField(name: "custom_attributes")
  List<UpdateProfileResponseCustomAttributes>? customAttributes;

  UpdateProfileResponseEntity();

  factory UpdateProfileResponseEntity.fromJson(Map<String, dynamic> json) =>
      $UpdateProfileResponseEntityFromJson(json);

  Map<String, dynamic> toJson() => $UpdateProfileResponseEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class UpdateProfileResponseAddresses {
  int? id;
  @JSONField(name: "customer_id")
  int? customerId;
  UpdateProfileResponseAddressesRegion? region;
  @JSONField(name: "region_id")
  int? regionId;
  @JSONField(name: "country_id")
  String? countryId;
  List<String>? street;
  String? telephone;
  String? postcode;
  String? city;
  String? firstname;
  String? lastname;
  @JSONField(name: "default_shipping")
  bool? defaultShipping;
  @JSONField(name: "default_billing")
  bool? defaultBilling;

  UpdateProfileResponseAddresses();

  factory UpdateProfileResponseAddresses.fromJson(Map<String, dynamic> json) =>
      $UpdateProfileResponseAddressesFromJson(json);

  Map<String, dynamic> toJson() => $UpdateProfileResponseAddressesToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class UpdateProfileResponseAddressesRegion {
  @JSONField(name: "region_code")
  String? regionCode;
  String? region;
  @JSONField(name: "region_id")
  int? regionId;

  UpdateProfileResponseAddressesRegion();

  factory UpdateProfileResponseAddressesRegion.fromJson(
          Map<String, dynamic> json) =>
      $UpdateProfileResponseAddressesRegionFromJson(json);

  Map<String, dynamic> toJson() =>
      $UpdateProfileResponseAddressesRegionToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class UpdateProfileResponseExtensionAttributes {
  @JSONField(name: "is_subscribed")
  bool? isSubscribed;

  UpdateProfileResponseExtensionAttributes();

  factory UpdateProfileResponseExtensionAttributes.fromJson(
          Map<String, dynamic> json) =>
      $UpdateProfileResponseExtensionAttributesFromJson(json);

  Map<String, dynamic> toJson() =>
      $UpdateProfileResponseExtensionAttributesToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class UpdateProfileResponseCustomAttributes {
  @JSONField(name: "attribute_code")
  String? attributeCode;
  String? value;

  UpdateProfileResponseCustomAttributes();

  factory UpdateProfileResponseCustomAttributes.fromJson(
          Map<String, dynamic> json) =>
      $UpdateProfileResponseCustomAttributesFromJson(json);

  Map<String, dynamic> toJson() =>
      $UpdateProfileResponseCustomAttributesToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
