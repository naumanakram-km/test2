import 'package:sofiqe/utils/states/function.dart';

class CentralColorLeftmost {
  String? faceArea;
  String? faceSubArea;
  List<CentralColours>? centralColours;
  String? subareaLeftmostCentralColour;
  List<FaceSubareaLeftmostListOfProducts>? faceSubareaLeftmostListOfProducts;

  CentralColorLeftmost(
      {this.faceArea,
      this.faceSubArea,
      this.centralColours,
      this.subareaLeftmostCentralColour,
      this.faceSubareaLeftmostListOfProducts});

  CentralColorLeftmost.fromJson(Map<String, dynamic> json) {
    List<FaceSubareaLeftmostListOfProducts> temp = [];
    List<FaceSubareaLeftmostListOfProducts> temp2 = [];
    if (json['Face_subarea_leftmost_list_of_products'].length > 0) {
      json['Face_subarea_leftmost_list_of_products'].forEach((key, value) {
        FaceSubareaLeftmostListOfProducts item =
            FaceSubareaLeftmostListOfProducts.fromJson(value, key, json['Face_sub_area']);
        temp.add(item);

        int checkIfAddedToList(dynamic dv) => temp2.indexWhere((element) => item.distanceVariance == dv);

        ///----- check if its already added then don't add it else add it
        if (checkIfAddedToList(item.distanceVariance) == -1) {
          temp2.add(item);
        } else {}
      });
    }
    for (int i = 0; i < temp.length; i++) {
      cPrint("T2L1==>" + temp[i].distanceVariance.toString());
    }
    for (int i = 0; i < temp2.length; i++) {
      cPrint("T2L1==>" + temp2[i].distanceVariance.toString());
    }
    temp.map((e) => cPrint("T2L==>" + e.distanceVariance.toString()));
    temp2.map((e) => cPrint("T2L==>" + e.distanceVariance.toString()));
    cPrint("T2L2==>" + temp.toString());
    cPrint("T2L==>" + temp2.length.toString());
    cPrint("T2L2==>" + temp.length.toString());
    temp.sort((a, b) => a.distanceVariance!.compareTo(b.distanceVariance!));

    faceArea = json['Face_area'];
    faceSubArea = json['Face_sub_area'];
    if (json['central_colours'].length > 0) {
      centralColours = <CentralColours>[];
      json['central_colours'].forEach((v) {
        centralColours!.add(CentralColours.fromJson(v));
      });
    }
    subareaLeftmostCentralColour = json['Subarea_leftmost_central_colour'];
    faceSubareaLeftmostListOfProducts = temp;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Face_area'] = faceArea;
    data['Face_sub_area'] = faceSubArea;
    if (centralColours != null) {
      data['central_colours'] = centralColours!.map((v) => v.toJson()).toList();
    }
    data['Subarea_leftmost_central_colour'] = subareaLeftmostCentralColour;
    if (faceSubareaLeftmostListOfProducts != null) {
      data['Face_subarea_leftmost_list_of_products'] = faceSubareaLeftmostListOfProducts == null
          ? null
          : List<dynamic>.from(faceSubareaLeftmostListOfProducts!.map((x) => x.toJson()));
    }
    return data;
  }
}

class CentralColours {
  String? colourAltName;
  String? colourAltHEX;

  CentralColours({this.colourAltName, this.colourAltHEX});

  CentralColours.fromJson(Map<String, dynamic> json) {
    colourAltName = json['ColourAltName'];
    colourAltHEX = json['ColourAltHEX'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ColourAltName'] = colourAltName;
    data['ColourAltHEX'] = colourAltHEX;
    return data;
  }
}

class FaceSubareaLeftmostListOfProducts {
  String? entityId;
  String? attributeSetId;
  String? typeId;
  String? sku;
  String? hasOptions;
  String? requiredOptions;
  String? createdAt;
  String? updatedAt;
  String? name;
  String? image;
  String? smallImage;
  String? thumbnail;
  String? optionsContainer;
  String? msrpDisplayActualPriceType;
  String? urlKey;
  String? urlPath;
  String? faceColor;
  String? shadeName;
  String? directions;
  String? brand;
  String? volume;
  String? status;
  String? visibility;
  String? taxClassId;
  String? size;
  String? storeYear;
  String? faceArea;
  String? faceSubArea;
  String? faceSubAreaName;
  String? drySkin;
  String? oilBased;
  String? sensitiveSkin;
  String? matte;
  String? radiant;
  String? shadeColor;
  String? veganCrueltyFree;
  String? tryOn;
  String? brandName;
  String? price;
  String? dealFromDate;
  String? dealToDate;
  String? description;
  String? shortDescription;
  String? ingredients;
  int? storeId;
  double? distanceVariance;
  String? key;
  List alternateColors = [];
  FaceSubareaLeftmostListOfProducts(
      {this.entityId,
      this.attributeSetId,
      this.typeId,
      this.sku,
      this.hasOptions,
      this.requiredOptions,
      this.createdAt,
      this.updatedAt,
      this.name,
      this.image,
      this.smallImage,
      this.thumbnail,
      this.optionsContainer,
      this.msrpDisplayActualPriceType,
      this.urlKey,
      this.urlPath,
      this.faceColor,
      this.shadeName,
      this.directions,
      this.brand,
      this.volume,
      this.status,
      this.visibility,
      this.taxClassId,
      this.size,
      this.storeYear,
      this.faceArea,
      this.faceSubArea,
      this.drySkin,
      this.oilBased,
      this.sensitiveSkin,
      this.matte,
      this.radiant,
      this.shadeColor,
      this.veganCrueltyFree,
      this.tryOn,
      this.brandName,
      this.price,
      this.dealFromDate,
      this.dealToDate,
      this.description,
      this.shortDescription,
      this.ingredients,
      this.storeId,
      this.key,
      this.distanceVariance,
      required this.alternateColors});

  FaceSubareaLeftmostListOfProducts.fromJson(Map<String, dynamic> json, String key, String faceSubarea) {
    key = key;
    faceSubAreaName = faceSubarea;
    entityId = json['entity_id'];
    attributeSetId = json['attribute_set_id'];
    typeId = json['type_id'];
    sku = json['sku'];
    hasOptions = json['has_options'];
    requiredOptions = json['required_options'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    name = json['name'];
    image = json['image'];
    smallImage = json['small_image'];
    thumbnail = json['thumbnail'];
    optionsContainer = json['options_container'];
    msrpDisplayActualPriceType = json['msrp_display_actual_price_type'];
    urlKey = json['url_key'];
    urlPath = json['url_path'];
    faceColor = json['face_color'];
    shadeName = json['shade_name'];
    directions = json['directions'];
    brand = json['brand'];
    volume = json['volume'];
    status = json['status'];
    visibility = json['visibility'];
    taxClassId = json['tax_class_id'];
    size = json['size'];
    storeYear = json['store_year'];
    faceArea = json['face_area'];
    faceSubArea = json['face_sub_area'];
    drySkin = json['dry_skin'];
    oilBased = json['oil_based'];
    sensitiveSkin = json['sensitive_skin'];
    matte = json['matte'];
    radiant = json['radiant'];
    shadeColor = json['shade_color'];
    veganCrueltyFree = json['vegan_cruelty_free'];
    tryOn = json['try_on'];
    brandName = json['brand_name'];
    price = json['price'];
    dealFromDate = json['deal_from_date'];
    dealToDate = json['deal_to_date'];
    description = json['description'];
    shortDescription = json['short_description'];
    ingredients = json['ingredients'];
    storeId = json['store_id'];
    distanceVariance = double.parse(json['extension_attributes']["distance_variance"].toString());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['entity_id'] = entityId;
    data['attribute_set_id'] = attributeSetId;
    data['type_id'] = typeId;
    data['sku'] = sku;
    data['has_options'] = hasOptions;
    data['required_options'] = requiredOptions;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['name'] = name;
    data['image'] = image;
    data['small_image'] = smallImage;
    data['thumbnail'] = thumbnail;
    data['options_container'] = optionsContainer;
    data['msrp_display_actual_price_type'] = msrpDisplayActualPriceType;
    data['url_key'] = urlKey;
    data['url_path'] = urlPath;
    data['face_color'] = faceColor;
    data['shade_name'] = shadeName;
    data['directions'] = directions;
    data['brand'] = brand;
    data['volume'] = volume;
    data['status'] = status;
    data['visibility'] = visibility;
    data['tax_class_id'] = taxClassId;
    data['size'] = size;
    data['store_year'] = storeYear;
    data['face_area'] = faceArea;
    data['face_sub_area'] = faceSubArea;
    data['dry_skin'] = drySkin;
    data['oil_based'] = oilBased;
    data['sensitive_skin'] = sensitiveSkin;
    data['matte'] = matte;
    data['radiant'] = radiant;
    data['shade_color'] = shadeColor;
    data['vegan_cruelty_free'] = veganCrueltyFree;
    data['try_on'] = tryOn;
    data['brand_name'] = brandName;
    data['price'] = price;
    data['deal_from_date'] = dealFromDate;
    data['deal_to_date'] = dealToDate;
    data['description'] = description;
    data['short_description'] = shortDescription;
    data['ingredients'] = ingredients;
    data['store_id'] = storeId;
    data['extension_attributes'] = {"distance_variance": distanceVariance};
    return data;
  }
}
