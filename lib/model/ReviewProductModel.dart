class ReviewProductModel {
  String? sku;
  String? name;
  String? imagePath;
  String? typeId;
  String? productUrl;
  String? rating;
  String? ratingCount;

  ReviewProductModel(
      {this.sku, this.name, this.imagePath, this.typeId, this.productUrl, this.rating, this.ratingCount});
}
