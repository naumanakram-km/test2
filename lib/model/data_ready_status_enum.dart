enum DataReadyStatus {
  // ignore: constant_identifier_names
  INACTIVE,
  FETCHING,
  COMPLETED,
  ERROR,
}
