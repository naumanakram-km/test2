import 'package:sofiqe/utils/states/function.dart';
import 'Media.dart';

class Product {
  late int? id;
  late String? name;
  late int? qty;
  late String? sku;
  late String? typeid;
  late dynamic price;
  late double? discountedPrice;
  late String? tryOn = "0";
  late String image;
  late String description;
  late int faceSubArea;
  late String faceSubAreaS = "";
  late String faceSubAreaName = 'sofiqe';
  late List? options;
  late List? values;
  late String color = '#ffffff';
  late String brand = '';
  late String avgRating = "0.0";
  late String rewardsPoint = "";
  late String reviewCount = "";
  late String productURL = "";
  late List<Media>? media = [];
  bool hasOption = false;
  late double vipRatio;

  bool allergyInducing = false;
  Product({
    this.faceSubAreaS = "",
    required this.id,
    required this.name,
    required this.sku,
    required this.price,
    required this.image,
    required this.description,
    required this.faceSubArea,
    required this.avgRating,
    this.typeid,
    this.reviewCount = '0',
    this.rewardsPoint = '0',
    this.color = '#ffffff',
    this.faceSubAreaName = '',
    this.options,
    this.values,
    this.productURL = '',
    this.discountedPrice,
    this.allergyInducing = false,
    this.hasOption = false,
    this.tryOn = "0",
  }) {
    getSubAreaName();
  }

  Product.fromDefaultMap(Map m) {
    String image = '';
    String description = '';
    int faceSubArea = -1;
    faceSubAreaName = '';
    discountedPrice = null;
    tryOn = "0";

    if (m.containsKey('custom_attributes')) {
      List customAttributes = m['custom_attributes'];
      for (final attribute in customAttributes) {
        if (attribute['attribute_code'] == 'short_description') {
          description = attribute['value'];
        } else if (attribute['attribute_code'] == 'face_sub_area') {
          try {
            faceSubArea = double.parse(attribute['value']).toInt();
          } catch (e) {
            cPrint("VALUE 22${attribute['value']}");
          }
        } else if (attribute['attribute_code'] == 'image') {
          image = attribute['value'];
        } else if (attribute['attribute_code'] == 'face_color') {
          color = attribute['value'];
        } else if (attribute['attribute_code'] == 'try_on') {
          tryOn = attribute['value'];
        }
      }
    }

    id = m['id'];
    sku = m['sku'];
    name = m['name'];
    typeid = m['type_id'];
    price = m['price'] != null ? m['price'].toDouble() : 0.0;
    options = m['options'];
    media = (m['media_gallery_entries'] as List?)
        ?.map((dynamic e) => Media.fromJson(e as Map<String, dynamic>))
        .toList();

    this.image = image;
    this.description = description;
    this.faceSubArea = faceSubArea;
    if (m['extension_attributes'] != null &&
        m['extension_attributes']['avgrating'] != null) {
      avgRating = m['extension_attributes']['avgrating'];
    }
    if (m['extension_attributes'] != null &&
        m['extension_attributes']['product_url'] != null) {
      productURL = m['extension_attributes']['product_url'];
    }
    if (m.containsKey('extension_attributes')) {
      cPrint('aaaaaaaaaaa');
      cPrint(m['extension_attributes']['reward_points']);
      if (m['extension_attributes'] != null &&
          m['extension_attributes']['reward_points'] != null) {
        rewardsPoint = m['extension_attributes']['reward_points'];
      }
    } else {
      cPrint('bbbbbbbbbbbbb');
      cPrint(m['reward_points']);
      rewardsPoint = m['reward_points'].toString();
    }
    if (m['extension_attributes'] != null &&
        m['extension_attributes']['review_count'] != null) {
      reviewCount = m['extension_attributes']['review_count'];
    }
    if (m['extension_attributes'] != null &&
        m['extension_attributes']?['configurable_product_options']
                ?.first?['values'] !=
            null) {
      values = m['extension_attributes']?['configurable_product_options']
          ?.first?['values'];
    } else {
      values = [];
    }
    getSubAreaName();
  }

  Product.fromMap(Map m) {
    // tryOn = "";
    int tempId = m.containsKey('id') ? m['id'] : int.parse(m['entity_id']);

    id = tempId;
    name = m['name'];
    price = m['price'].runtimeType == String
        ? double.parse(m['price'])
        : m['price'].toDouble();
    sku = m['sku'];
    typeid = m['type_id'];
    options = m['options'] ?? [];
    image = m['image'] ?? '';
    brand = m['brand'] ?? '';
    discountedPrice = m['discountedPrice'];
    media = (m['media_gallery_entries'] as List?)
        ?.map((dynamic e) => Media.fromJson(e as Map<String, dynamic>))
        .toList();
    if (m.containsKey('face_sub_area')) {
      if (m['face_sub_area'] is String) {
        faceSubAreaS = m['face_sub_area'];
      } else {
        faceSubArea =
            m.containsKey('face_sub_area') ? int.parse(m['face_sub_area']) : -1;
      }
    } else {
      faceSubArea = -1;
    }

    color =
        m.containsKey('face_color') ? m['face_color'] ?? '#ffffff' : '#ffffff';
    List customAttributes =
        m.containsKey('custom_attributes') ? m['custom_attributes'] : [];
    if (m['extension_attributes'] != null &&
        m['extension_attributes']['avgrating'] != null) {
      avgRating = m['extension_attributes']['avgrating'];
    }

    if (m.containsKey('extension_attributes')) {
      cPrint('aaaaaaaaaaa---------');
      cPrint(m['extension_attributes']['reward_points']);
      if (m['extension_attributes'] != null &&
          m['extension_attributes']['reward_points'] != null) {
        rewardsPoint = m['extension_attributes']['reward_points'].toString();
      }
    } else {
      cPrint('bbbbbbbbbbbbb');
      cPrint(m['reward_points']);
      rewardsPoint = double.parse((m['reward_points'] ?? "0").toString())
          .toStringAsFixed(0);
    }
    if (m['extension_attributes'] != null &&
        m['extension_attributes']['review_count'] != null) {
      reviewCount = m['extension_attributes']['review_count'].toString();
    }
    if (m['extension_attributes'] != null &&
        m['extension_attributes']?['configurable_product_options']
                ?.first?['values'] !=
            null) {
      values = m['extension_attributes']?['configurable_product_options']
          ?.first?['values'];
    } else {
      values = [];
    }
    for (final ca in customAttributes) {
      if (ca.containsKey('description')) {
        description = ca['description'];
      }

      if (ca.containsKey('face_sub_area')) {
        switch (ca['face_sub_area'].runtimeType) {
          case String:
            faceSubArea = int.parse(ca['face_sub_area']);
            break;
          case int:
            faceSubArea = ca['face_sub_area'];
            break;
          default:
            faceSubArea = -1;
            break;
        }
      }
    }
    getSubAreaName();
  }

  Product.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> extensionAttributes = json['extension_attributes'];
    cPrint(extensionAttributes.length);
    qty = extensionAttributes['stock_item']['qty'];
    price = double.parse((json["price"] ?? "0").toString());
  }

  getSubAreaName() {
    if (faceSubAreaName.isNotEmpty) {
      return;
    }
    // if (faceSubAreaMapping.containsKey(faceSubArea) && faceSubArea != -1) {
    //   faceSubAreaName = faceSubAreaMapping[faceSubArea] as String;
    // } else {
    //   faceSubAreaName = 'popular';
    // }
  }
}
