class ModelForColorOrder {
  String face_color;
  double distance_variance;
  String type_id;
  String sku;

  ModelForColorOrder(
      {required this.face_color,
      required this.distance_variance,
      required this.type_id,
      required this.sku,});
}
