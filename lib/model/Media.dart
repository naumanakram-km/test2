import 'dart:ffi';

class Media {
  final int? id;
  final String? mediaType;
  final dynamic label;
  final int? position;
  final bool? disabled;
  final List<dynamic>? types;
  final String? file;

  Media({
    this.id = -1,
    required this.mediaType,
    required this.label,
    required this.position,
    required this.disabled,
    required this.types,
    required this.file,
  });

  Media.fromJson(Map<String, dynamic> json)
      : id = json['id'] as int?,
        mediaType = json['media_type'] as String?,
        label = json['label'] as String?,
        file = json['file'] as String?,
        disabled = json['disabled'] as bool?,
        position = json['position'] as int?,
        types = json['types'] as List?;
}
