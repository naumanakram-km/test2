import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:sofiqe/network_service/network_service.dart';
import 'package:sofiqe/utils/states/function.dart';

// Utils
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/constants/api_tokens.dart';

import '../../provider/wishlist_provider.dart';

Future<Map> sfAPIGetCatalogUnfilteredItems(int page) async {
  try {
    // add additional parameter to api for show only visible products
    Uri url = Uri.parse(
        '${APIEndPoints.catalogUnfiltereditems}$page&searchCriteria[filterGroups][0][filters][0][field]=visibility&searchCriteria[filterGroups][0][filters][0][value]=4&searchCriteria[filterGroups][0][filters][0][conditionType]=eq');

    http.Response response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer jihvg7q1b4hjnf9xc0ly5jlvmti972p2',
      //'Bearer ${APITokens.bearerToken}',
    });
    if (response.statusCode != 200) {
      throw response.body;
    }
    Map resultMap = json.decode(response.body);
    return resultMap;
  } catch (err) {
    cPrint(err);
    throw 'Could not fetch item list';
  }
}

Future<Map> sfAPIGetCatalogGiftCardItems() async {
  cPrint('===== check 3 print ====');

  try {
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer jihvg7q1b4hjnf9xc0ly5jlvmti972p2',
    };

    var request = http.Request(
        'GET',
        Uri.parse(APIEndPoints.baseUri +
            '/products?searchCriteria[pageSize]=20&searchCriteria[currentPage]=0&searchCriteria[filterGroups][0][filters][0][field]=visibility&searchCriteria[filterGroups][0][filters][0][value]=4&searchCriteria[filterGroups][0][filters][0][conditionType]=eq&searchCriteria[filterGroups][1][filters][0][field]=name&searchCriteria[filterGroups][1][filters][0][value]=%Giftcard%&searchCriteria[filterGroups][1][filters][0][conditionType]=like'));
    request.body = '''''';
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      String responseString = await response.stream.bytesToString();
      cPrint("campaign products  -->> 1 ${json.decode(responseString)}");
      Map responseBody = json.decode(responseString);
      return responseBody;
    } else {
      cPrint(response.reasonPhrase);
      throw 'Could not fetch Gift card items or  products';
    }
  } catch (err) {
    cPrint(err);
    throw 'Could not fetch item list';
  }
}

Future<Map> sfAPIGetCatalogAllSaleItems() async {
  try {
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer jihvg7q1b4hjnf9xc0ly5jlvmti972p2',
    };

    var request = http.Request(
        'GET',
        Uri.parse(
            APIEndPoints.mainBaseUrl + '/rest//V1/custom/getsaleproducts'));

    // var request = http.Request('GET', Uri.parse(APIEndPoints.getAllSaleProductsAPI));
    request.body = '''''';
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      String responseString = await response.stream.bytesToString();
      cPrint("campaign products  -->> 3 ${json.decode(responseString)}");
      Map responseBody = json.decode(responseString);
      return responseBody;
    } else {
      cPrint(response.reasonPhrase);
      throw 'Could not fetch All Sale  products';
    }
  } catch (err) {
    cPrint(err);
    throw 'Could not fetch item list';
  }
}

Future<Map> sfAPIGetUnfilteredFaceAreaItems(int page, int faceArea) async {
  try {
    Uri url =
        Uri.parse('${APIEndPoints.unfilteredFaceAreaItems(page, faceArea)}');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer jihvg7q1b4hjnf9xc0ly5jlvmti972p2',
    };

    cPrint('${APIEndPoints.unfilteredFaceAreaItems(page, faceArea)}');

    http.Request request = http.Request('GET', url);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode != 200) {
      throw await response.stream.bytesToString();
    }

    Map responseBody = json.decode(await response.stream.bytesToString());

    return responseBody;
  } catch (err) {
    rethrow;
  }
}

Future<Map> sfAPIGetSkinToneItems(String faceArea, String eyeColor,
    String hairColor, String lipcolor, String cheekColor, String token) async {
  try {
    Uri url = Uri.parse('${APIEndPoints.searchedSKinToneItems}');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };

    cPrint('${APIEndPoints.searchedSKinToneItems}');

    http.Request request = http.Request('POST', url);
    request.body = json.encode(
      {
        'eye_color': '$eyeColor',
        'lip_color': '$lipcolor',
        'face_sub_area': '$faceArea',
        'skin_tone': '$cheekColor',
        'hair_color': '$hairColor',
      },
    );
    cPrint('heloooooooooooooooooooo');
    cPrint(eyeColor);
    cPrint(lipcolor);
    cPrint(faceArea);
    cPrint(cheekColor);
    cPrint(hairColor);
    cPrint(request.body);
    cPrint(token);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode != 200) {
      throw await response.stream.bytesToString();
    }

    Map responseBody = json.decode(await response.stream.bytesToString());
    cPrint(responseBody);
    return responseBody;
  } catch (err) {
    rethrow;
  }
}

Future<Map> sfAPIGetSkinToneProfileItems(
    String faceArea, bool isProfileSearch) async {
  try {
    var token = await APITokens.customerSavedToken;
    Uri url = Uri.parse('${APIEndPoints.searchedSKinToneItems}');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
      //'Bearer ${APITokens.bearerToken}',
    };

    cPrint('${APIEndPoints.searchedSKinToneItems}');
    cPrint('$token');

    http.Request request = http.Request('POST', url);
    request.body = json.encode(
      {'profile_search': '$isProfileSearch', 'face_sub_area': '$faceArea'},
    );

    cPrint(request.body);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode != 200) {
      throw await response.stream.bytesToString();
    }

    Map responseBody = json.decode(await response.stream.bytesToString());
    cPrint(responseBody);
    return responseBody;
  } catch (err) {
    rethrow;
  }
}

Future<Map> sfAPIFetchProductItems(int page, int faceSubArea) async {
  try {
    Uri url = Uri.parse('${APIEndPoints.productItems(page, faceSubArea)}');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer jihvg7q1b4hjnf9xc0ly5jlvmti972p2',
      //'Bearer ${APITokens.bearerToken}',
    };

    http.Request request = http.Request('GET', url);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode != 200) {
      throw await response.stream.bytesToString();
    }

    Map responseBody = json.decode(await response.stream.bytesToString());

    return responseBody;
  } catch (err) {
    rethrow;
  }
}

Future<List> sfAPIFetchBrandFilteredItems(
    int page, String brand, String faceArea) async {
  try {
    Uri url = Uri.parse('${APIEndPoints.brandFilteredItems}');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer jihvg7q1b4hjnf9xc0ly5jlvmti972p2',
      //'Bearer ${APITokens.bearerToken}',
    };

    cPrint('${APIEndPoints.brandFilteredItems}');

    http.Request request = http.Request('POST', url);
    request.body = json.encode(
      {
        'brand': '$brand',
        'face_sub_area': '$faceArea',
        'page': page,
      },
    );

    cPrint(request.body);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode != 200) {
      throw await response.stream.bytesToString();
    }

    List responseBody = json.decode(await response.stream.bytesToString());

    return responseBody;
  } catch (err) {
    rethrow;
  }
}

Future<List> sfAPIGetCatalogPopularItems(int page) async {
  try {
    Uri url = Uri.parse('${APIEndPoints.catalogPopularItems}');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${APITokens.bearerToken}',
    };
    http.Request request = http.Request('POST', url);
    request.body = json.encode(
      {
        "page": page,
      },
    );
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode != 200) {
      throw await response.stream.bytesToString();
    }
    List resultMap = json.decode(await response.stream.bytesToString());
    cPrint("===.................++++++");
    cPrint(resultMap);
    return resultMap;
  } catch (err) {
    cPrint(err);
    throw 'Could not fetch item list';
  }
}

Future<List> sfAPIGetCatalogBetweenPriceItems(
    int page, int minPrice, int maxPrice, String faceArea) async {
  try {
    Uri url = Uri.parse('${APIEndPoints.catalogBetweenPriceItems}');

    cPrint('${APIEndPoints.catalogBetweenPriceItems}');

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${APITokens.bearerToken}',
    };
    http.Request request = http.Request('POST', url);
    request.body = json.encode(
      {
        "min_price": minPrice,
        "max_price": maxPrice,
        "page": page,
        "face_area": faceArea,
      },
    );

    cPrint(request.body);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode != 200) {
      throw await response.stream.bytesToString();
    }
    List resultMap = json.decode(await response.stream.bytesToString());

    return resultMap;
  } catch (err) {
    cPrint(err);
    throw 'Request Error $err';
  }
}

Future<Map<String, dynamic>> sfAPIGetBestSellers() async {
  Uri url = Uri.parse('${APIEndPoints.begetBestSellersList}');
  cPrint("URL URL ${url}");
  http.Response response = await http.get(
    url,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${APITokens.bearerToken}',
    },
  );
  cPrint(response.body);
  if (response.statusCode == 200) {
    cPrint("Come nnjh");
    var data = json.decode(response.body);
    Map<String, dynamic> map = data[0];
    cPrint("Best  -->> first $map");

    return map;
  } else {
    cPrint(response.body);
    throw 'Could not fetch bestsellers';
  }
}

Future<void> sfAPIGetInvoice({required String OrderId}) async {
  Uri url = Uri.parse('${APIEndPoints.InVoices(OrderId: OrderId)}');
  Map<String, String> headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ${APITokens.bearerToken}',
  };
  http.Response response = await http.post(url, body: {}, headers: headers);

  cPrint(response);
  cPrint("get_deal:>>  ");

  if (response.statusCode == 200) {
    String responseString = await response.body;
    cPrint("get_deal  -->> 1 ${json.decode(responseString)}");
  } else {
    cPrint(response.reasonPhrase);
    cPrint("get_deal  -->> 2");
    throw 'Could not fetch deals';
  }
}

Future<List<dynamic>> sfAPIGetDealOfTheDay(double lat, double long) async {
  Uri url = Uri.parse('${APIEndPoints.getDealOfTheDay}');

  Map<String, String> headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ${APITokens.bearerToken}',
  };
  http.Request request = http.Request('POST', url);
  request.body = json.encode({"lat": "$lat", "long": "$long"});
  request.headers.addAll(headers);
  http.StreamedResponse response = await request.send();
  cPrint(response);
  cPrint("get_deal:>>  ");

  if (response.statusCode == 200) {
    String responseString = await response.stream.bytesToString();
    cPrint("get_deal  -->> 1 ${json.decode(responseString)}");

    List<dynamic> responseBody = json.decode(responseString);
    return responseBody;
  } else {
    cPrint(response.reasonPhrase);
    cPrint("get_deal  -->> 2");
    throw 'Could not fetch deals';
  }
}

Future<List<dynamic>> getProductsByCampaignIdAPI(String campginId) async {
  Map<String, String> headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ${APITokens.bearerToken}',
  };

  var request = http.Request(
      'POST', Uri.parse(APIEndPoints.baseUri + '/custom/gettodayscampaign'));
  request.body = json.encode({"campaign": campginId});
  request.headers.addAll(headers);

  http.StreamedResponse response = await request.send();

  if (response.statusCode == 200) {
    String responseString = await response.stream.bytesToString();
    cPrint("campaign products  -->> 2 ${json.decode(responseString)}");
    // Check if the responseString is a non-empty JSON array.
    if (json.decode(responseString).length > 0) {
      // Decode the JSON responseString into a Map.
      Map responseBody = json.decode(responseString);

      // Extract the 'items' key from the responseBody.
      var items = responseBody['items'];

      // Return the extracted 'items' list.
      return items;
    } else {
      // If the responseString is empty or not a valid JSON array,
      // return an empty list to indicate no items were found.
      return [];
    }
  } else {
    cPrint(response.reasonPhrase);
    throw 'Could not fetch campaign based products';
  }
}

Future<List<dynamic>> getVendorDealsById(dynamic sellerId) async {
  Uri url = Uri.parse('${APIEndPoints.getVendorDealsById}');

  Map<String, String> headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ${APITokens.bearerToken}',
  };
  http.Request request = http.Request('POST', url);
  request.body = json.encode({"seller_id": "$sellerId"});

  request.headers.addAll(headers);

  http.StreamedResponse response = await request.send();
  cPrint(response);
  cPrint("Vendor_deal:>>  ");

  if (response.statusCode == 200) {
    String responseString = await response.stream.bytesToString();
    cPrint("Vendor_deal  -->> 1 ${json.decode(responseString)}");
    List<dynamic> responseBody = json.decode(responseString);
    return responseBody;
  } else {
    cPrint(response.reasonPhrase);
    cPrint("Vendor_deal  -->> 2");
    throw 'Could not fetch deals';
  }
}

Future<Map> sfAPIGetSearchedItems(String query) async {
  try {
    Uri url = Uri.parse('${APIEndPoints.searchedItems(query)}');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${APITokens.bearerToken}',
    };

    http.Request request = http.Request('GET', url);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();
    if (response.statusCode != 200) {
      throw await response.stream.bytesToString();
    }
    Map responseBody = json.decode(await response.stream.bytesToString());
    return responseBody;
  } catch (err) {
    rethrow;
  }
}

Future<Map> sfAPIGetSearchedSkinTone(String query) async {
  try {
    Uri url = Uri.parse('${APIEndPoints.searchedItems(query)}');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${APITokens.bearerToken}',
    };

    http.Request request = http.Request('GET', url);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode != 200) {
      throw await response.stream.bytesToString();
    }
    Map responseBody = json.decode(await response.stream.bytesToString());
    return responseBody;
  } catch (err) {
    rethrow;
  }
}

Future<List> sfAPIFetchCentralColorProducts(
  String token,
  String color,
  String undertone,
  String faceSubArea,
  int colorDepth,
) async {
  try {
    Uri url = Uri.parse('${APIEndPoints.centralColorProducts}');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };

    http.Request request = http.Request('POST', url);
    request.body = json.encode(
      {
        'color': '$color',
        'face_sub_area': '$faceSubArea',
        'undertone': '$undertone',
        'color_depth': '$colorDepth',
      },
    );

    cPrint(request.body);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode != 200) {
      throw await response.stream.bytesToString();
    }

    List responseBody = json.decode(await response.stream.bytesToString());
    return responseBody;
  } catch (err) {
    rethrow;
  }
}

Future<List> sfAPIFetchCentralColorProductsForFoundation(
  String token,
  String color,
  String faceSubArea,
  // int customer_id,
) async {
  try {
    Uri url = Uri.parse('${APIEndPoints.alternateColorProducts}');
    // Uri url = Uri.parse('${APIEndPoints.centralColorProducts}');
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };
    cPrint(token);
    http.Request request = http.Request('POST', url);
    request.body = json.encode(
      {
        'color': '$color',
        'face_sub_area': '$faceSubArea',
        'color_depth': '5',
        //  "customerId": customer_id
      },
    );
    cPrint(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode != 200) {
      throw await response.stream.bytesToString();
    }

    List responseBody = json.decode(await response.stream.bytesToString());
    cPrint('.......................');
    cPrint(responseBody);
    return responseBody;
  } catch (err) {
    rethrow;
  }
}
/*
* this function will fetch products as per rates
*
* */

Future<List> fetchItemsByReview(int star) async {
  var result;
  String url = '${APIEndPoints.fetchRatedItems(star: star)}';

  cPrint(url);

  try {
    http.Response? response =
        await NetworkHandler.getMethodCall(url: url, headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${APITokens.bearerToken}',
    });
    cPrint("after api-----------;;;;;;;;;-  ${response!.statusCode}");

    if (response.statusCode != 200) {
      throw response.body;
    }
    result = json.decode(response.body);
    cPrint(result);
    return result;
  } catch (e) {
    rethrow;
  }
}
