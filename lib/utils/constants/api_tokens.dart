import 'package:sofiqe/utils/states/local_storage.dart';

import '../states/function.dart';

class APITokens {
  // ignore: todo
  //TODO Change TOKEN

  static String get bearerToken {
//For Dev
    // return 'eyJraWQiOiIxIiwiYWxnIjoiSFMyNTYifQ.eyJ1aWQiOjgwLCJ1dHlwaWQiOjIsImlhdCI6MTY4MjU4MDgyMywiZXhwIjoxNjgyNTg0NDIzfQ.ux96pScoxOIVB5oshITjLxMe87LAAtCSuhmi-ktOJRg';

//For live.Sofiqe
    return 'jihvg7q1b4hjnf9xc0ly5jlvmti972p2';
  }

  static String get bearerTokenOld {
    return 'n3z1i4phrahoflb64tb1ej0fpu62b5y3';
  }

  static String get adminBearerId {
    // return 'eyJraWQiOiIxIiwiYWxnIjoiSFMyNTYifQ.eyJ1aWQiOjgwLCJ1dHlwaWQiOjIsImlhdCI6MTY4MjQ5NDExOSwiZXhwIjoxNjgyNDk3NzE5fQ.qUij44VBRZcR4Ceuc35NehL35mso4pFJbJiPpIJeusI';
//For Dev
    // return 'eyJraWQiOiIxIiwiYWxnIjoiSFMyNTYifQ.eyJ1aWQiOjgwLCJ1dHlwaWQiOjIsImlhdCI6MTY4MjU4MDgyMywiZXhwIjoxNjgyNTg0NDIzfQ.ux96pScoxOIVB5oshITjLxMe87LAAtCSuhmi-ktOJRg';

//For live
    return 'jihvg7q1b4hjnf9xc0ly5jlvmti972p2';
  }

  static String get testBearerId {
    return 'tx3sfrbyk0qjwr40c3nb4qqbkhxlx17n';
  }

  ///
  /// Token from local storage
  ///
  static Future<String> get customerSavedToken async {
    Map userTokenMap = await sfQueryForSharedPrefData(
        fieldName: 'user-token', type: PreferencesDataType.STRING);
    String token = userTokenMap['user-token'] ?? "";
    // cPrint("USERTOKEN = " + token.toString());
    return token;
  }

  static Future<String> get cartToken async {
    Map userTokenMap = await sfQueryForSharedPrefData(
        fieldName: 'cart-token', type: PreferencesDataType.STRING);
    String token = userTokenMap['cart-token'];
    cPrint("CARTTOKEN = " + token.toString());
    return token;
  }
}
