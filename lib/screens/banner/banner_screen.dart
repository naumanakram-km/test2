import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/provider/banner_provider.dart';

import '../../controller/controllers.dart';
import '../../main.dart';
import '../../utils/constants/app_colors.dart';
import '../catalog_screen.dart';
import '../product_detail_1_screen.dart';

extension ColorExtension on String {
  toColor() {
    var hexString = this;
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}

class Banner1 extends StatefulWidget {
  const Banner1({Key? key}) : super(key: key);

  @override
  State<Banner1> createState() => _Banner1State();
}

class _Banner1State extends State<Banner1> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final bannerHeight = size.height *
        (Theme.of(context).platform == TargetPlatform.iOS ? 0.65 : 0.75);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Consumer<BannerProvider>(
        builder: (_, provider, __) {
          return Stack(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: bannerHeight,
                child: provider.isLoading
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : CarouselSlider(
                        //----- SLIDER Options & Configurations
                        options: CarouselOptions(
                          onPageChanged: (index, value) {
                            provider.updateCurrentIndex(index);
                          },
                          height: bannerHeight,
                          viewportFraction: 1,
                          enableInfiniteScroll: true,
                          reverse: false,
                          autoPlay: true,
                          autoPlayInterval: const Duration(seconds: 6),
                          autoPlayAnimationDuration:
                              const Duration(milliseconds: 1000),
                          autoPlayCurve: Curves.linear,
                          scrollDirection: Axis.horizontal,
                        ),
                        items: List.generate(
                            provider.homePageSection1Data.length, (index) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15)),
                            child: GestureDetector(
                              onTap: () {
                                gtm.push(
                                  'Home_Page_Press_Banner',
                                );

                                ///------- Logic for implementing SKU / CAMPAIGN redirection
                                if (provider.homePageSection1Data[index]
                                        ['sku_campaign']
                                    .toString()
                                    .contains('SKU=')) {
                                  if (provider.homePageSection1Data[index]
                                          ['sku_campaign']
                                      .toString()
                                      .contains(',')) {
                                    var originalCollectiveSKUs = provider
                                        .homePageSection1Data[index]
                                            ['sku_campaign']
                                        .toString()
                                        .split('SKU=');
                                    var extractedListOfSKUs =
                                        originalCollectiveSKUs[1].split(',');
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext c) {
                                          return CatalogScreen(
                                            isFromSale: true,
                                            listOfOnSaleSKUs:
                                                extractedListOfSKUs,
                                            comingFromMainIndex: navController
                                                .currentMainIndex.value,
                                          );
                                        },
                                      ),
                                    );
                                  } else {
                                    var a = provider.homePageSection1Data[index]
                                            ['sku_campaign']
                                        .toString()
                                        .split('SKU=');

                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext c) {
                                          return ProductDetail1Screen(
                                              // sku: "20220420002122",
                                              sku: a[1],
                                              isOnSale: true);
                                        },
                                      ),
                                    );
                                  }
                                } else {
                                  var a = provider.homePageSection1Data[index]
                                          ['sku_campaign']
                                      .toString()
                                      .split('CAMPAIGN=');

                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (BuildContext c) {
                                        return CatalogScreen(
                                          isFromSale: true,
                                          campaignId: a[1],
                                          comingFromMainIndex: navController
                                              .currentMainIndex.value,
                                        );
                                      },
                                    ),
                                  );
                                }
                              },
                              child: Stack(
                                fit: StackFit.expand,
                                alignment: Alignment.center,
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(15),
                                    child: CachedNetworkImage(
                                      imageUrl:
                                          provider.homePageSection1Data[index]
                                              ['mobile_banner'],
                                      placeholder: (context, url) =>
                                          const SpinKitThreeBounce(
                                        color: Color(0xffF2CA8A),
                                        size: 40.0,
                                      ),
                                      errorWidget: (context, url, error) =>
                                          const Icon(Icons.error),
                                      fit: BoxFit.cover,
                                    ),
                                  ),

                                  Positioned(
                                    bottom: bannerHeight * 0.065,
                                    child: Container(
                                      height: 50,
                                      width: 155,
                                      child: ElevatedButton(
                                        style: ButtonStyle(
                                          shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(100),
                                            ),
                                          ),
                                          backgroundColor:
                                              MaterialStateProperty.all(
                                                  AppColors.primaryColor),
                                        ),
                                        onPressed: () {},
                                        child: Text(
                                          'SHOW DETAILS',
                                          style: TextStyle(
                                              fontSize: 11,
                                              fontFamily: 'Arial, Regular',
                                              color: AppColors
                                                  .navigationBarSelectedColor),
                                        ),
                                      ),
                                    ),
                                  ),

                                  ///----- Banners API response "text1" will be shown here
                                  if (provider.homePageSection1Data[
                                          provider.currentIndex]
                                      .containsKey('text1'))
                                    Positioned(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            top: 45, left: 20, right: 20),
                                        child: Text(
                                          provider.homePageSection1Data[provider
                                                  .currentIndex]['text1']
                                              .toString(),
                                          textAlign: TextAlign.center,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline4!
                                              .copyWith(
                                                color:
                                                    "${provider.homePageSection1Data[provider.currentIndex]['text1_color']}"
                                                        .toColor(),
                                                fontSize: size.height * 0.035,
                                                shadows: [
                                                  Shadow(
                                                      color: Colors.black
                                                          .withOpacity(0.1),
                                                      offset:
                                                          const Offset(10, 20),
                                                      blurRadius: 10),
                                                ],
                                                fontWeight: FontWeight.w900,
                                                letterSpacing: 0.55,
                                              ),
                                        ),
                                      ),
                                    )
                                  else
                                    const SizedBox.shrink(),

                                  ///----- Banners API response "text2" will be shown here

                                  if (provider.homePageSection1Data[
                                          provider.currentIndex]
                                      .containsKey('text2'))
                                    Positioned(
                                      bottom: provider.homePageSection1Data[
                                                  provider.currentIndex]
                                              .containsKey('text3')
                                          ? 55
                                          : bannerHeight * 0.2,
                                      child: SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width -
                                                50,
                                        child: Text(
                                          provider.homePageSection1Data[provider
                                                  .currentIndex]['text2']
                                              .toString(),
                                          textAlign: TextAlign.center,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3!
                                              .copyWith(
                                                color:
                                                    "${provider.homePageSection1Data[provider.currentIndex]['text2_color']}"
                                                        .toColor(),
                                                fontSize: size.height * 0.02,
                                                shadows: [
                                                  Shadow(
                                                      color: Colors.black
                                                          .withOpacity(0.1),
                                                      offset:
                                                          const Offset(10, 20),
                                                      blurRadius: 10),
                                                ],
                                                fontWeight: FontWeight.w900,
                                                letterSpacing: 0.55,
                                              ),
                                          maxLines: 2,
                                        ),
                                      ),
                                    )
                                  else
                                    const SizedBox.shrink(),

                                  ///----- Banners API response "text3" will be shown here
                                  if (provider.homePageSection1Data[
                                          provider.currentIndex]
                                      .containsKey('text3'))
                                    Positioned(
                                      bottom: 20,
                                      child: Text(
                                        provider.homePageSection1Data[
                                                provider.currentIndex]['text3']
                                            .toString(),
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline4!
                                            .copyWith(
                                              color:
                                                  "${provider.homePageSection1Data[provider.currentIndex]['text3_color']}"
                                                      .toColor(),
                                              fontSize: size.height * 0.035,
                                              shadows: [
                                                Shadow(
                                                    color: Colors.black
                                                        .withOpacity(0.1),
                                                    offset:
                                                        const Offset(10, 20),
                                                    blurRadius: 10),
                                              ],
                                              fontWeight: FontWeight.w900,
                                              letterSpacing: 0.55,
                                            ),
                                      ),
                                    )
                                  else
                                    const SizedBox.shrink()
                                ],
                              ),
                            ),
                          );
                        }),
                      ),
              ),
            ],
          );
        },
      ),
    );
  }
}

/// Banner 2
class Banner2 extends StatefulWidget {
  const Banner2({Key? key}) : super(key: key);

  @override
  State<Banner2> createState() => _Banner2State();
}

class _Banner2State extends State<Banner2> {
  bool isLoading = true;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Consumer<BannerProvider>(
        builder: (_, provider, __) {
          return Stack(
            children: [
              Container(
                  decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 2,
                          offset: Offset(0, 1),
                        )
                      ]),
                  width: size.width,
                  child: Container()),
              if (isLoading)
                Center(
                  child: CircularProgressIndicator(),
                )
              else
                SizedBox(),
            ],
          );
        },
      ),
    );
  }
}

/// Banner 3
class Banner3 extends StatefulWidget {
  const Banner3({Key? key}) : super(key: key);

  @override
  State<Banner3> createState() => _Banner3State();
}

class _Banner3State extends State<Banner3> {
  bool isLoading = true;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Consumer<BannerProvider>(
        builder: (_, provider, __) {
          return Stack(
            children: [
              Container(
                  decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 2,
                          offset: Offset(0, 1),
                        )
                      ]),
                  width: size.width,
                  child: Container()),
              if (isLoading)
                Center(
                  child: CircularProgressIndicator(),
                )
              else
                SizedBox(),
            ],
          );
        },
      ),
    );
  }
}
