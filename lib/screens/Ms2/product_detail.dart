// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:sofiqe/controller/orderDetailController.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../../controller/shoppinglistHistory.dart';
import '../../model/shoppingHistoryModel.dart';
import '../my_sofiqe.dart';

class ProductDetail extends StatefulWidget {
  Map<String, dynamic> data;
  String orderId;
  List<Item> items;

  ProductDetail(
      {required this.data, required this.orderId, required this.items});

  @override
  State<ProductDetail> createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
//   @override
  ShoppingHistory shoppingHistory = Get.put(ShoppingHistory());
  ShoppingHistory sHistory = Get.put(ShoppingHistory());
  OrderDetailController controller = Get.put(OrderDetailController());
  final TryItOnProvider tiop = Get.find();

  @override
  void initState() {
    super.initState();
    controller.getOrderDetails(widget.orderId);
  }

//   @override
  @override
  Widget build(BuildContext context) {
    cPrint('orderId  ${widget.orderId}');
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(size.height * 0.08),
          child: AppBar(
              //toolbarHeight: size.height * 0.15,
              backgroundColor: Colors.black,
              elevation: 0.0,
              leading: InkWell(
                  onTap: () => Get.back(),
                  child: const Icon(
                    Icons.arrow_back,
                  )),
              centerTitle: true,
              title: Column(
                children: [
                  SizedBox(
                    height: size.height * 0.01,
                  ),
                  Text(
                    'sofiqe',
                    style: Theme.of(context).textTheme.headline1!.copyWith(
                        color: Colors.white,
                        fontSize: size.height * 0.035,
                        letterSpacing: 0.6),
                  ),
                  SizedBox(
                    height: size.height * 0.005,
                  ),
                  const Text(
                    'ORDER DETAILS',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        letterSpacing: 1,
                        fontWeight: FontWeight.w300),
                  ),
                  SizedBox(
                    height: size.height * 0.025,
                  ),
                ],
              )),
        ),
        body: GetBuilder<OrderDetailController>(builder: (logic) {
          return ModalProgressHUD(
            inAsyncCall: logic.isInProgress,
            progressIndicator: const SpinKitDoubleBounce(
              color: Colors.black,
              size: 50.0,
            ),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    child: GetBuilder<OrderDetailController>(builder: (contrl) {
                      controller = contrl;
                      String paymentMethod = '';
                      if (contrl.orderModel != null) {
                        if (contrl.orderModel!.data!.paymentMethod!.method ==
                            'mpstripe') {
                          paymentMethod = "Card Payment";
                        } else if (contrl
                                .orderModel!.data!.paymentMethod!.method ==
                            'paypal') {
                          paymentMethod = "Paypal";
                        } else if (contrl
                                .orderModel!.data!.paymentMethod!.method ==
                            'clearpay') {
                          paymentMethod = "Clearpay";
                        } else {
                          paymentMethod =
                              contrl.orderModel!.data!.paymentMethod!.method!;
                        }
                      }
                      return (contrl.isOrderLoading)
                          ? Container(
                              height: Get.height,
                              width: Get.width,
                              child: const Center(
                                  child: CircularProgressIndicator()),
                            )
                          : (contrl.orderModel == null)
                              ? Container(
                                  height: Get.height,
                                  width: Get.width,
                                  child: const Center(
                                      child: Text("No Data Found")),
                                )
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'PRODUCTS (${contrl.orderModel!.data!.items!.length})',
                                      style: const TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    ListView.builder(
                                        shrinkWrap: true,
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        itemCount: contrl
                                            .orderModel!.data!.items!.length,
                                        itemBuilder: (context, i) {
                                          return InkWell(
                                            child: Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 10),
                                              child: Row(
                                                children: [
                                                  Container(
                                                    width: Get.width * 0.25,
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              10.0),
                                                      child: Image.network(
                                                        APIEndPoints
                                                                .mediaBaseUrl +
                                                            "${contrl.orderModel!.data!.items![i].image}",
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: Container(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 10),
                                                      child: Container(
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Text(
                                                              "${contrl.orderModel!.data!.items![i].name}",
                                                              style: const TextStyle(
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                            ),
                                                            const SizedBox(
                                                              height: 5,
                                                            ),
                                                            Text(
                                                              'Quantity: ${double.parse(contrl.orderModel!.data!.items![i].orderQty!).toStringAsFixed(0)} Item',
                                                              style: const TextStyle(
                                                                  fontSize: 15,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                            const SizedBox(
                                                              height: 5,
                                                            ),
                                                            Text(
                                                              'SKU: ${contrl.orderModel!.data!.items![i].sku}',
                                                              style: const TextStyle(
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500),
                                                            ),
                                                            const SizedBox(
                                                              height: 10,
                                                            ),
                                                            // Create Check if size is null it will not show size
                                                            if (contrl
                                                                    .orderModel!
                                                                    .data!
                                                                    .items![i]
                                                                    .customAttributes!
                                                                    .volume ==
                                                                null)
                                                              Container()
                                                            else
                                                              Text(
                                                                  "SIZE: ${contrl.orderModel!.data!.items![i].customAttributes!.volume}",
                                                                  style: const TextStyle(
                                                                      fontSize:
                                                                          12,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .normal)),
                                                            const SizedBox(
                                                              height: 5,
                                                            ),
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Container(
                                                                      child: widget
                                                                              .items[
                                                                                  i]
                                                                              .productOptions!
                                                                              .isEmpty
                                                                          ? Container()
                                                                          : Text(
                                                                              "Shade: ",
                                                                              style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal)),
                                                                    ),
                                                                    Container(
                                                                      width:
                                                                          15.0,
                                                                      height:
                                                                          15.0,
                                                                      child: Container(
                                                                          decoration: BoxDecoration(
                                                                        color: widget.items[i].productOptions!.isEmpty
                                                                            ? Colors
                                                                                .white
                                                                            : HexColor(widget.items[i].productOptions?.firstWhere((element) => element.label == Label.SHADE_COLOR).value ??
                                                                                ""),
                                                                        shape: BoxShape
                                                                            .rectangle,
                                                                      )),
                                                                    )
                                                                  ],
                                                                ),
                                                                const SizedBox(
                                                                  width: 100,
                                                                ),
                                                                Text(
                                                                  '${(double.parse(contrl.orderModel!.data!.items![i].price!)).toString().toProperCurrency()}',
                                                                  style: const TextStyle(
                                                                      fontSize:
                                                                          10,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .normal),
                                                                ),
                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          );
                                        }),
                                    const Divider(),
                                    const SizedBox(height: 10),
                                    const Text(
                                      'DELIVERY',
                                      style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                        "${contrl.orderModel!.data!.shippingAddress!.street}\n${contrl.orderModel!.data!.shippingAddress!.city}",
                                        style: const TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.normal)),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        const Text("DELIVERY DATE",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.normal)),
                                        Text(
                                            DateFormat('E, dd MMM yyyy')
                                                .format(DateTime.parse(contrl
                                                    .orderModel!.data!.date
                                                    .toString()))
                                                .toString(),
                                            style: const TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.normal)),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        const Text("PAYMENT METHOD",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.normal)),
                                        Row(
                                          children: [
                                            Text(paymentMethod,
                                                style: const TextStyle(
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.normal)),
                                            Text(
                                                contrl
                                                            .orderModel!
                                                            .data!
                                                            .paymentMethod!
                                                            .ccLast4 ==
                                                        null
                                                    ? ''
                                                    : "${contrl.orderModel!.data!.paymentMethod!.ccLast4}",
                                                style: const TextStyle(
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.normal)),
                                          ],
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    const Center(
                                      child: Text(
                                        'STATUS',
                                        style: TextStyle(
                                          fontSize: 10,
                                          letterSpacing: 1.30,
                                          fontWeight: FontWeight.normal,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Center(
                                      child: Text(
                                        "${contrl.orderModel!.data!.status}"
                                            .toUpperCase(),
                                        style: const TextStyle(
                                            fontSize: 18,
                                            letterSpacing: 1.40,
                                            fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                );
                    }),
                  ),
                ),
                GetBuilder<OrderDetailController>(
                    builder: (orderDetailController) {
                  return Get.find<OrderDetailController>().orderModel == null
                      ? const SizedBox()
                      : Container(
                          margin: const EdgeInsets.only(bottom: 10, top: 10),
                          alignment: Alignment.center,
                          child: Center(
                            child: Container(
                              alignment: Alignment.center,
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(30.0)),
                                  ),
                                  foregroundColor: MaterialStateProperty.all(
                                    AppColors.navigationBarSelectedColor,
                                  ),
                                  backgroundColor: MaterialStateProperty.all(
                                      const Color(0xffF2CA8A)),
                                  overlayColor:
                                      MaterialStateProperty.resolveWith<Color?>(
                                    (Set<MaterialState> states) {
                                      if (states
                                          .contains(MaterialState.pressed))
                                        return tiop.ontapColor; //<-- SEE HERE
                                      return null; // Defer to the widget's default.
                                    },
                                  ),
                                ),
                                onPressed: () async {
                                  tiop.isChangeButtonColor.value = true;
                                  tiop.playSound();
                                  Future.delayed(
                                          const Duration(milliseconds: 10))
                                      .then((value) {
                                    tiop.isChangeButtonColor.value = false;
                                  });

                                  orderDetailController
                                      .orderAgainAndAddAllToCurrentCart(
                                          context);

                                  // await sHistory.orderAgain(context, controller
                                  //     .orderModel!.data!.orderId!);
                                },
                                child: const Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 20, horizontal: 60),
                                  child: Text(
                                    'BUY AGAIN',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                }),
              ],
            ),
          );
        }),
      ),
    );
  }
}
