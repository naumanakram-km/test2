import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../controller/looksController.dart';
import '../model/product_model.dart';
import '../provider/account_provider.dart';
import '../provider/cart_provider.dart';
import '../utils/api/product_details_api.dart';
import '../utils/constants/api_end_points.dart';
import '../widgets/png_icon.dart';
import 'package:http/http.dart' as http;

class EvaluateScreen extends StatefulWidget {
  final String? imagePath;
  final String? sku;
  final String? name;

  EvaluateScreen(this.imagePath, this.sku, this.name);

  @override
  _EvaluateScreenState createState() => _EvaluateScreenState();
}

class _EvaluateScreenState extends State<EvaluateScreen> {
  LooksController looksController = Get.find();

/* Code to Fetch Details of Product from SKU to get the Product Id */
  @override
  void initState() {
    super.initState();
    loadProductDetails();
  }

  Future<http.Response> getProductDetails() async {
    cPrint("Review Screen => SKU Value => " + widget.sku.toString());
    return sfAPIGetProductDetailsFromSKU(sku: '${widget.sku}');
  }

  loadProductDetails() async {
    http.Response response = await getProductDetails();

    Map<String, dynamic> responseBodytemp = json.decode(response.body);
    Product producttemp = Product.fromDefaultMap(responseBodytemp);
    setState(() {
      productId = producttemp.id ?? 0;
    });
  }

  /* Code to Fetch Details of Product from SKU to get the Product Id [Ended Here]*/
  final TryItOnProvider tiop = Get.find();

  int productId = 0;

  int generalRating = 0;
  int priceRating = 0;
  int qualityRating = 0;
  String details = '';
  String nickName = '';

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.height * 0.08),
        child: AppBar(
          // shadowColor: Colors.white,
          // elevation: 1,
          leading: IconButton(
            icon: Transform.rotate(
              angle: 3.14159,
              child: PngIcon(
                image: 'assets/icons/arrow-2-white.png',
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          backgroundColor: Colors.black,
          centerTitle: true,
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: size.height * 0.01,
              ),
              Text(
                'sofiqe',
                style: Theme.of(context)
                    .textTheme
                    .headline1!
                    .copyWith(color: Colors.white, fontSize: size.height * 0.035, letterSpacing: 0.6),
              ),
              SizedBox(
                height: size.height * 0.005,
              ),
              Text(
                "RATING",
                style: TextStyle(color: Colors.white, fontSize: 12, letterSpacing: 1, fontWeight: FontWeight.w300),
              ),
              SizedBox(
                height: size.height * 0.025,
              ),
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.all(15),
            // width: Get.width,
            // height: Get.height,
            color: Colors.black,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 40.0),
                  child: Center(
                    child: Image(
                        width: 150,
                        height: 150,
                        image: NetworkImage(widget.imagePath!.contains('http')
                            ? widget.imagePath!
                            : APIEndPoints.mediaBaseUrl + widget.imagePath!)),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Center(
                  child: Text(
                    widget.name!,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'GENERAL',
                      style: TextStyle(color: Colors.white, fontSize: 15),
                      textAlign: TextAlign.left,
                    )),
                Align(
                  alignment: Alignment.center,
                  child: RatingBar.builder(
                    unratedColor: Colors.white,
                    itemSize: 40,
                    initialRating: 0,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 10.0),
                    itemBuilder: (context, _) => Icon(
                      generalRating > _ ? Icons.star : Icons.star_border,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      setState(() {
                        generalRating = rating.toInt();
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'PRICE',
                      style: TextStyle(color: Colors.white, fontSize: 15),
                      textAlign: TextAlign.left,
                    )),
                Align(
                  alignment: Alignment.center,
                  child: RatingBar.builder(
                    unratedColor: Colors.white,
                    itemSize: 40,
                    initialRating: 0,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 10.0),
                    itemBuilder: (context, _) => Icon(
                      priceRating > _ ? Icons.star : Icons.star_border,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      setState(() {});
                      priceRating = rating.toInt();
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'QUALITY',
                      style: TextStyle(color: Colors.white, fontSize: 15),
                      textAlign: TextAlign.left,
                    )),
                Align(
                  alignment: Alignment.center,
                  child: RatingBar.builder(
                    unratedColor: Colors.white,
                    itemSize: 40,
                    initialRating: 0,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 10.0),
                    itemBuilder: (context, _) => Icon(
                      qualityRating > _ ? Icons.star : Icons.star_border,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      setState(() {
                        qualityRating = rating.toInt();
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "COMMENT",
                    style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.w500),
                    textAlign: TextAlign.left,
                  ),
                ),
                TextField(
                  onChanged: (value) {
                    setState(() {
                      details = value.toString();
                    });
                  },
                  controller: null,
                  maxLines: 3,
                  textInputAction: TextInputAction.done,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(20.0),
                    fillColor: Colors.white,
                    filled: true,
                    border: OutlineInputBorder(),
                    labelStyle: TextStyle(color: Colors.pinkAccent, fontSize: 20, fontWeight: FontWeight.w500),
                    hintText: 'Enter your comment',
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Your Name".toUpperCase(),
                    style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.w500),
                    textAlign: TextAlign.left,
                  ),
                ),
                TextField(
                  onChanged: (value) {
                    setState(() {
                      nickName = value.toString();
                    });
                  },
                  controller: null,
                  maxLines: 1,
                  textInputAction: TextInputAction.done,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(20.0),
                    fillColor: Colors.white,
                    filled: true,
                    border: OutlineInputBorder(),
                    labelStyle: TextStyle(color: Colors.pinkAccent, fontSize: 20, fontWeight: FontWeight.w500),
                    hintText: 'Enter your name',
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Container(
                      width: double.infinity,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                          ),
                          foregroundColor: MaterialStateProperty.all(
                            AppColors.navigationBarSelectedColor,
                          ),
                          backgroundColor: MaterialStateProperty.all(
                              // AppColors.buttonBackgroundShopping,
                              Colors.white),
                          overlayColor: MaterialStateProperty.resolveWith<Color?>(
                            (Set<MaterialState> states) {
                              if (states.contains(MaterialState.pressed)) return tiop.ontapColor; //<-- SEE HERE
                              return null; // Defer to the widget's default.
                            },
                          ),
                        ),
                        onPressed: () async {
                          bool isLogin = Provider.of<CartProvider>(context, listen: false).isLoggedIn;
                          tiop.isChangeButtonColor.value = true;
                          tiop.playSound();
                          Future.delayed(Duration(milliseconds: 10)).then((value) async {
                            tiop.isChangeButtonColor.value = false;
                            if (details.isNotEmpty && nickName.isNotEmpty) {
                              int id = isLogin ? Provider.of<AccountProvider>(context, listen: false).customerId : 0;

                              try {
                                context.loaderOverlay.show();

                                int res = await looksController.createReview(details, nickName, widget.sku!,
                                    generalRating, priceRating, qualityRating, productId, isLogin, id);
                                context.loaderOverlay.hide();

                                if (res == 200) {
                                  Navigator.pop(context);
                                }
                              } on Exception catch (e) {
                                context.loaderOverlay.hide();

                                Get.showSnackbar(
                                  GetSnackBar(
                                    message: e.toString(),
                                    duration: Duration(seconds: 2),
                                    isDismissible: true,
                                  ),
                                );
                              }
                            } else {
                              Get.showSnackbar(
                                GetSnackBar(
                                  message: 'Please enter required details.',
                                  duration: Duration(seconds: 2),
                                  isDismissible: true,
                                ),
                              );
                            }
                          });
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 18, horizontal: 0),
                          child: Text(
                            'Share Review',
                            style: TextStyle(
                                color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500, letterSpacing: 1),
                          ),
                        ),
                      ),
                    )

                    // CapsuleButton(
                    //   backgroundColor: Colors.white,
                    //   borderColor: Color(0xFF393636),
                    //   onPress: () async {
                    //     if (details.isNotEmpty && nickName.isNotEmpty) {
                    //       looksController.createReview(
                    //           details,
                    //           nickName,
                    //           widget.sku!,
                    //           generalRating,
                    //           priceRating,
                    //           qualityRating,
                    //           productId);
                    //       Navigator.pop(context);
                    //     } else {
                    //       Get.showSnackbar(
                    //         GetSnackBar(
                    //           message: 'Please enter required details.',
                    //           duration: Duration(seconds: 2),
                    //           isDismissible: true,
                    //         ),
                    //       );
                    //     }
                    //   },
                    //   child: Text(
                    //     'Share Review',
                    //     style: TextStyle(
                    //         color: Colors.black,
                    //         fontSize: 16,
                    //         fontWeight: FontWeight.w500,
                    //         letterSpacing: 1),
                    //   ),
                    // ),
                    ),
              ],
            )),
      ),
    );
  }
}
