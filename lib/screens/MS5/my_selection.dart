// ignore_for_file: must_be_immutable, unused_element
// Commented for testing purpose. will be removed after QA tested
// import 'package:cached_network_image/cached_network_image.dart';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/controller/selectedProductController.dart';
import 'package:sofiqe/model/selectedProductModel.dart';
import 'package:sofiqe/provider/catalog_provider.dart' as cat;
import 'package:sofiqe/provider/page_provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/screens/MS5/product_item_card.dart';
import 'package:sofiqe/screens/shopping_bag_screen.dart';
import 'package:sofiqe/widgets/catalog/checkout/widget_loader_with_text.dart';
import 'package:sofiqe/widgets/png_icon.dart';
import 'package:sofiqe/widgets/product_detail/custom_notification_for_add_all.dart';
import 'package:sofiqe/widgets/round_button.dart';
import 'package:sofiqe/widgets/translucent_background.dart';
import 'package:sofiqe/utils/states/function.dart';

// import '../../model/product_model.dart';
import '../../provider/cart_provider.dart';
import '../../utils/constants/app_colors.dart';

class MySelectionMS5 extends StatelessWidget {
  MySelectionMS5({Key? key}) : super(key: key);

  final cat.CatalogProvider catp = Get.find();
  final TryItOnProvider tmo = Get.find();

  final SelectedProductController selectedProductController = Get.find();

  @override
  Widget build(BuildContext context) {
    tmo.getuserfacecolor(isfromRecomendation: true);
    selectedProductController.getSelectedProduct();
    Size size = MediaQuery.of(context).size;
    double padding = size.height * 0.04;
    return Scaffold(
      body: SizedBox(
        height: size.height,
        // height: size.height,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            _TopBanner(
                count: selectedProductController.isSelectedProductLoading
                    ? ""
                    : selectedProductController.selectedProduct!.items!.length.toString()),
            Obx(
              () {
                cat.FaceArea fa = catp.faceArea.value;
                return _MainFilter(faceArea: fa);
              },
            ),
            _SubFilter(selectedProductController: selectedProductController),
            GetBuilder<SelectedProductController>(builder: (contrl) {
              return (contrl.isSelectedProductLoading && tmo.centralLeftmostloading.isTrue)
                  ? Expanded(
                      child: Center(
                          child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SpinKitDoubleBounce(color: AppColors.primaryColor, size: 50.0),
                          Text(
                            "Please wait, we are collecting your selections",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.black, fontSize: 12, letterSpacing: 1, fontWeight: FontWeight.w400),
                          ),
                        ],
                      )),
                    )
                  : contrl.selectedProduct == null
                      ? Expanded(
                          child: Center(
                              child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [
                              Text(
                                "Customer has no selected products",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 12, letterSpacing: 1, fontWeight: FontWeight.w400),
                              ),
                            ],
                          )),
                        )
                      : Obx(() {
                          List<Items> temp = [];
                          cat.FaceArea face = catp.faceArea.value;
                          String area = "All";
// cPrint(widget.tmo.centralcolorleftmostselected[0]);
                          if (face == cat.FaceArea.ALL) {
                            area = "All";
                          } else if (face == cat.FaceArea.CHEEKS) {
                            area = "Cheeks";
                          } else if (face == cat.FaceArea.EYES) {
                            area = "Eyes";
                          } else if (face == cat.FaceArea.LIPS) {
                            area = "Lips";
                          }

                          List<Items> temp3 = [];
                          List<Items> temp1 = [];
                          List<Items> temp2 = [];
                          List<String> subeye = [];
                          List<String> subcheeks = [];
                          List<String> sublips = [];
                          tmo.centralcolorleftmostselected.forEach((element) {
                            for (int i = 0; i < contrl.selectedProduct!.items!.length; i++) {
                              Items? ele = contrl.selectedProduct!.items![i];
                              if (element.sku == ele.product!.sku) {
                                if (element.faceArea == "Eyes") {
                                  int index = subeye.indexOf(ele.faceSubArea.toString());
                                  if (index == -1) {
                                    temp1.add(ele);
                                    subeye.add(ele.faceSubArea.toString());
                                    break;
                                  }
                                } else if (element.faceArea == "Lips") {
                                  int index = sublips.indexOf(ele.faceSubArea.toString());
                                  if (index == -1) {
                                    temp2.add(ele);
                                    sublips.add(ele.faceSubArea.toString());
                                    break;
                                  }
                                } else if (element.faceArea == "Cheeks") {
                                  int index = subcheeks.indexOf(ele.faceSubArea.toString());
                                  if (index == -1) {
                                    temp3.add(ele);
                                    subcheeks.add(ele.faceSubArea.toString());
                                    break;
                                  }
                                }
                              }
                            }
                          });

                          if (area == "Eyes") {
                            temp = temp1;
                          } else if (area == "Lips") {
                            temp = temp2;
                          } else if (area == "Cheeks") {
                            temp = temp3;
                          } else {
                            temp = [...temp1, ...temp2, ...temp3];
                          }
                          contrl.tryMySelectionList.value = [...temp1, ...temp2, ...temp3];

                          return Expanded(
                              child: Container(
                            child: (temp.length < 1)
                                ? Center(
                                    child: Text("No Data Found"),
                                  )
                                : GridView.builder(
                                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      crossAxisSpacing: padding / 2,
                                      mainAxisSpacing: padding / 2,
                                      childAspectRatio: ((size.width - padding) / 2) / (size.height * 0.4),
                                    ),
                                    padding: EdgeInsets.all(padding / 2),
                                    itemCount: temp.length,
                                    itemBuilder: (ctx, i) {
                                      return SelectedProductItemCard(
                                        product: temp[i].product!,
                                        shadecolor: temp[i].shadeColour!,
                                      );
                                    },
                                  ),
                          ));
                        });
            }),
          ],
        ),
      ),
    );
  }
}

class _TopBanner extends StatelessWidget {
  final String count;

  _TopBanner({required this.count});

  final PageProvider pp = Get.find();
  final cat.CatalogProvider catp = Get.find();
  final SelectedProductController selectedProductController = Get.find();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var cartItems =
        Provider.of<CartProvider>(context).cart != null ? Provider.of<CartProvider>(context).cart!.length : 0;
    var cartTotalQty = Provider.of<CartProvider>(context).getTotalQty();

    return Stack(
      children: [
        Container(
          height: size.height * 0.15,
          width: size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage('assets/images/mySelection.png'),
            ),
          ),
        ),
        SizedBox(height: size.height * 0.15, width: size.width, child: TranslucentBackground(opacity: 0.2)),
        SizedBox(
          height: size.height * 0.15,
          width: size.width,
          child: Row(
            children: [
              GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  Get.back();
                },
                child: Container(
                  width: size.width * 0.18,
                  alignment: Alignment.center,
                  child: SvgPicture.asset('assets/svg/arrow.svg'),
                ),
              ),
            ],
          ),
        ),
        Container(
          width: size.width,
          height: size.height * 0.15,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Sofiqe',
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.white,
                      fontSize: size.height * 0.018,
                      fontWeight: FontWeight.bold,
                    ),
              ),
              SizedBox(height: size.height * 0.01),
              Text(
                'My Selection',
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.white,
                      fontSize: size.height * 0.018,
                    ),
              ),
              SizedBox(height: size.height * 0.01),
              if (selectedProductController.selectedProduct == null)
                Text(
                  '',
                  style: Theme.of(context).textTheme.headline2!.copyWith(
                        color: Colors.white,
                        fontSize: size.height * 0.015,
                      ),
                )
              else
                Obx(
                  () => Text(
                    (TryItOnProvider.instance.centralcolorleftmostselected.length > 0
                            ? TryItOnProvider.instance.centralcolorleftmostselected.length.toString()
                            : "") +
                        ' Products',
                    style: Theme.of(context).textTheme.headline2!.copyWith(
                          color: Colors.white,
                          fontSize: size.height * 0.015,
                        ),
                  ),
                ),
            ],
          ),
        ),
        Container(
          width: size.width,
          height: size.height * 0.15,
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.014),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              RoundButton(
                backgroundColor: Colors.black,
                size: size.height * 0.05,
                onPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext c) {
                        return ShoppingBagScreen();
                      },
                    ),
                  );
                },
                child: Stack(
                  alignment: Alignment.topRight,
                  children: [
                    PngIcon(image: 'assets/icons/add_to_cart_white.png'),
                    if (cartItems == 0)
                      SizedBox()
                    else
                      Container(
                          decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.red),
                          padding: EdgeInsets.all(5),
                          child: Text(cartTotalQty.toString()))
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

class SearchBar extends StatefulWidget {
  SearchBar({Key? key}) : super(key: key);

  @override
  State<SearchBar> createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  final cat.CatalogProvider catp = Get.find();
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    controller.addListener(storeInput);
  }

  void storeInput() {
    catp.inputText.value = controller.value.text;
  }

  @override
  Widget build(BuildContext context) {
    controller.addListener(() {});
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: size.width * 0.012, vertical: size.height * 0.008),
      alignment: Alignment.center,
      width: size.width * 0.6,
      height: size.height * 0.05,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(size.height * 0.05),
        ),
      ),
      child: Row(
        children: [
          Container(
            alignment: Alignment.center,
            width: size.width * 0.5,
            child: CupertinoTextField(
              padding: EdgeInsets.symmetric(horizontal: 4),
              controller: controller,
              autofocus: true,
              decoration: BoxDecoration(
                  // border: InputBorder.none,
                  ),
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: Colors.black,
                    fontSize: size.height * 0.0225,
                    decoration: TextDecoration.none,
                  ),
            ),
          ),
          GestureDetector(
            onTap: () {
              catp.search();
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: size.width * 0.01),
              child: SvgPicture.asset(
                'assets/svg/search.svg',
                color: Colors.black,
                height: size.height * 0.018,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _MainFilter extends StatelessWidget {
  final cat.FaceArea faceArea;

  _MainFilter({
    Key? key,
    required this.faceArea,
  }) : super(key: key);

  final cat.CatalogProvider catp = Get.find();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.055,
      width: size.width,
      decoration: BoxDecoration(
          color: Colors.black,
          border: Border(
            bottom: BorderSide(
              color: Color(0xFF645F5F),
              width: 0.5,
            ),
          )),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GestureDetector(
            onTap: () {
              catp.setFaceArea(cat.FaceArea.ALL);
            },
            child: Text(
              'ALL',
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: faceArea == cat.FaceArea.ALL ? Color(0xFFF2CA8A) : Colors.white,
                    fontSize: size.height * 0.018,
                  ),
            ),
          ),
          GestureDetector(
            onTap: () {
              catp.setFaceArea(cat.FaceArea.EYES);
            },
            child: Text(
              'EYES',
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: faceArea == cat.FaceArea.EYES ? Color(0xFFF2CA8A) : Colors.white,
                    fontSize: size.height * 0.018,
                  ),
            ),
          ),
          GestureDetector(
            onTap: () {
              catp.setFaceArea(cat.FaceArea.LIPS);
            },
            child: Text(
              'LIPS',
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: faceArea == cat.FaceArea.LIPS ? Color(0xFFF2CA8A) : Colors.white,
                    fontSize: size.height * 0.018,
                  ),
            ),
          ),
          GestureDetector(
            onTap: () {
              catp.setFaceArea(cat.FaceArea.CHEEKS);
            },
            child: Text(
              'CHEEKS',
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: faceArea == cat.FaceArea.CHEEKS ? Color(0xFFF2CA8A) : Colors.white,
                    fontSize: size.height * 0.018,
                  ),
            ),
          ),
        ],
      ),
    );
  }
}

class _SubFilter extends StatelessWidget {
  SelectedProductController selectedProductController;

  _SubFilter({required this.selectedProductController});

  final TryItOnProvider tmo = Get.find();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.075,
      width: size.width,
      decoration: BoxDecoration(
        color: Colors.black,
      ),
      child: GestureDetector(
        onTap: () {
          tmo.playSound();
          tmo.showLoaderDialog(context, loaderColor: Colors.black);
          AddToCardMySelection(context);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/Path_6.png',
              color: Colors.white,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              "ADD ALL TO BAG",
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
      ),
    );
  }

  AddToCardMySelection(BuildContext context) async {
    int length = 0;
    List errorProducts = [];
    // selectedcontroller
    //     .selectedProduct!.items?.forEach((element) async{
    // selectedcontroller.tryMySelectionList.forEach((element) async {
    cPrint("Humayunnnnn");
    cPrint(selectedProductController.tryMySelectionList.length);

    ///------ NEW Logic Here
    // String list of All SKUs
    String allSKUs = '';
    for (int i = 0; i < selectedProductController.tryMySelectionList.length; i++) {
      dynamic element = selectedProductController.tryMySelectionList[i];
      allSKUs += element.product!.sku;
      if (i < selectedProductController.tryMySelectionList.length - 1) {
        allSKUs += ',';
      }
    }

    Future.delayed(Duration(seconds: 2), () async {
      log('==== list of get SKUs is $allSKUs ====');
      CartProvider cartP = Provider.of<CartProvider>(context, listen: false);

      try {
        await cartP.addToCartForListOfSKUs(context: context, listOfSKUs: allSKUs, refresh: true);
        Navigator.pop(context);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          padding: EdgeInsets.all(0),
          backgroundColor: Colors.black,
          duration: Duration(seconds: 2),
          content: CustomSnackBarForAddAll(
            message: 'Added all products to cart',
          ),
        ));
      } catch (e) {
        Navigator.pop(context);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          padding: EdgeInsets.all(0),
          backgroundColor: Colors.black,
          duration: Duration(seconds: 2),
          content: CustomSnackBarForAddAll(
            message: 'Error in adding products',
          ),
        ));
      }
    });

    ///------ OLD Logic Here
    // for (int i = 0;
    //     i < selectedProductController.tryMySelectionList.length;
    //     i++) {
    //   cPrint("ENTER 11 $length");
    //   dynamic element = selectedProductController.tryMySelectionList[i];
    //   CartProvider cartP = Provider.of<CartProvider>(context, listen: false);
    //   cPrint("ENTER 33 $length");
    //   cPrint("CartProvider  -->> SSs ${cartP.cartToken}");
    //   try {
    //     context.loaderOverlay.show(
    //         widget: LoaderWithText(
    //       msg: "Adding " +
    //           element.product!.name +
    //           "\n\n" +
    //           (selectedProductController.tryMySelectionList.length - length)
    //               .toString() +
    //           " Remaining",
    //     ));
    //     await cartP.addToCartForAddAll(context, element.product!.sku, [], 0, "",
    //         refresh: true);
    //     length++;
    //   } catch (e) {
    //     errorProducts.add(element.product!.name);
    //   }
    //
    //   cPrint(
    //       "ENTER 22 $length  total ${selectedProductController.selectedProduct!.items!.length}");
    // }
    // if (length == selectedProductController.tryMySelectionList.length) {
    //   context.loaderOverlay.hide();
    //   // product snack bar
    //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //     padding: EdgeInsets.all(0),
    //     backgroundColor: Colors.black,
    //     duration: Duration(seconds: 2),
    //     content: CustomSnackBarForAddAll(
    //       message: 'Added all products $length',
    //     ),
    //   ));
    // } else {
    //   context.loaderOverlay.hide();
    //
    //   String productfailed = "\nFailed Products";
    //   for (int i = 0; i < errorProducts.length; i++) {
    //     productfailed += ("\n" + errorProducts[i]);
    //   }
    //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //     padding: EdgeInsets.all(0),
    //     backgroundColor: Colors.black,
    //     duration: Duration(seconds: 10),
    //     content: Container(
    //       child: CustomSnackBarForAddAll(
    //         message:
    //             "$length Products Added and ${selectedProductController.tryMySelectionList.length - length} Failed to Added due to some error." +
    //                 productfailed,
    //       ),
    //     ),
    //   ));
    // }
  }

// Commented For testing purpose. Will be removed after QA approval

}

class _SubFilterButton extends StatelessWidget {
  final String filterName;
  final String svgPath;
  final Color color;
  final bool premium;
  final void Function() onTap;

  const _SubFilterButton({
    Key? key,
    required this.filterName,
    required this.svgPath,
    required this.color,
    required this.onTap,
    this.premium = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: onTap,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: size.height * 0.01),
            SvgPicture.asset(
              '$svgPath',
              color: color,
              width: size.height * 0.02,
              height: size.height * 0.02,
            ),
            SizedBox(height: size.height * 0.005),
            Text(
              '$filterName',
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: color,
                    fontSize: size.height * 0.015,
                  ),
            ),
            Text(
              '${premium ? 'PREMIUM' : ''}',
              style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: Color(0xFFF2CA8A),
                    fontSize: size.height * 0.01,
                    letterSpacing: 0,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
