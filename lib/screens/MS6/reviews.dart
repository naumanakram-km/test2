//Code corrected by Ashwani on 14-04-2022, reviews were not properly synced

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:sofiqe/controller/reviewController.dart';
import 'package:sofiqe/provider/account_provider.dart';
import 'package:sofiqe/provider/page_provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/provider/wishlist_provider.dart';
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/widgets/cart/empty_bag.dart';

import '../../controller/fabController.dart';
import '../../controller/looksController.dart';
import '../../controller/controllers.dart';
import '../../model/reviewModel.dart';
import '../../provider/cart_provider.dart';
import '../../utils/constants/route_names.dart';
import '../../widgets/capsule_button.dart';
import '../../widgets/makeover/make_over_login_custom_widget.dart';
import '../MS8/looks_package_details.dart';
import '../Ms3/looks_screen.dart';
import '../my_sofiqe.dart';
import '../product_detail_1_screen.dart';

class ReviewsMS6 extends StatefulWidget {
  const ReviewsMS6({Key? key}) : super(key: key);
  @override
  _ReviewsMS6State createState() => _ReviewsMS6State();
}

class _ReviewsMS6State extends State<ReviewsMS6> {
  PageProvider pp = Get.find();
  int selectedReview = 0;

  changeReview(int i) {
    setState(() {
      selectedReview = i;
    });
  }

  // ReviewController reviewController = Get.put(ReviewController());
  ReviewController reviewController = Get.find();
  final WishListProvider wp = Get.find();
  bool ratingSorting = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    final TryItOnProvider tiop = Get.find();

    var cartItems = Provider.of<CartProvider>(context).cart!.length;
    var cartTotalQty = Provider.of<CartProvider>(context).getTotalQty();
    return Obx(() {
      return (navController.currentMainIndex.value == 0 ||
                  navController.currentMainIndex.value == 1 ||
                  navController.currentMainIndex.value == 2) &&
              navController.innerPageIndex.value == 0
          ? SafeArea(
              child: Scaffold(
                body: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: Get.height * 0.15,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                'assets/images/myReviews.png',
                              ),
                              fit: BoxFit.cover)),
                      child: Stack(
                        alignment: Alignment.center,
                        //  mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              IconButton(
                                  onPressed: () {
                                    Get.back();
                                  },
                                  icon: Icon(
                                    Icons.arrow_back,
                                    color: Colors.white,
                                  )),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 15),
                                child: Text(
                                  "sofiqe",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline1!
                                      .copyWith(
                                        color: Colors.white,
                                        fontSize:
                                            AppBar().preferredSize.height * 0.5,
                                      ),
                                ),
                              ),
                              Spacer(),
                              GestureDetector(
                                onTap: () {
                                  tiop.playSound();
                                  Navigator.pushNamed(
                                      context, RouteNames.cartScreen);
                                },
                                child: Stack(
                                  alignment: Alignment.topRight,
                                  children: [
                                    Container(
                                      width: Get.width * 0.10,
                                      child: CircleAvatar(
                                        radius: 28,
                                        backgroundColor: Colors.white,
                                        child: Center(
                                          child: Image.asset(
                                            "assets/images/Path_6.png",
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                    ),
                                    cartItems == 0
                                        ? SizedBox()
                                        : Container(
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.white),
                                            padding: EdgeInsets.all(5),
                                            child: Text(
                                              cartTotalQty.toString(),
                                              style:
                                                  TextStyle(color: Colors.red),
                                            ))
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: Get.width * 0.03,
                              ),
                            ],
                          ),
                          // Align(
                          //   alignment: Alignment.bottomRight,
                          //   child:
                          // )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          InkWell(
                            onTap: () {
                              changeReview(0);
                            },
                            child: IntrinsicWidth(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "SOFIQE REVIEWS",
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  SizedBox(
                                    height: 3,
                                  ),
                                  selectedReview == 0
                                      ? Container(
                                          //  width: 85.w,
                                          height: 2,
                                          color: Color(0xffF2CA8A),
                                        )
                                      : Container(),
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              changeReview(1);
                            },
                            child: IntrinsicWidth(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "MY REVIEWS",
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 3,
                                  ),
                                  selectedReview == 1
                                      ? Container(
                                          // width: 65.w,
                                          height: 2,
                                          color: Color(0xffF2CA8A),
                                        )
                                      : Container(),
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              changeReview(2);
                            },
                            child: IntrinsicWidth(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "WISHLIST",
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  SizedBox(
                                    height: 3,
                                  ),
                                  selectedReview == 2
                                      ? Container(
                                          // width: 50.w,
                                          height: 2,
                                          color: Color(0xffF2CA8A),
                                        )
                                      : Container(),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        child: selectedReview == 2
                            ? GetBuilder<ReviewController>(builder: (wshCntrl) {
                                return !Provider.of<AccountProvider>(context,
                                            listen: false)
                                        .isLoggedIn
                                    ? CustomEmptyBagPage(
                                        title: Provider.of<AccountProvider>(
                                                    context,
                                                    listen: false)
                                                .isLoggedIn
                                            ? "Oops we are missing your wish list"
                                            : 'Oops we cannot find your wish list',
                                        // title: 'Oops You have no reviews yet,\nwant to go to shopping? ',
                                        ontap: () async {
                                          //      profileController.screen.value = 0;
                                          tiop.playSound();
                                          Get.back();
                                          pp.goToPage(Pages.SHOP);
                                        },
                                        emptyBagButtonText: 'GO SHOPPING',
                                      )
                                    : wshCntrl.isWishListLoading
                                        ? Container(
                                            height: Get.height * 0.5,
                                            width: Get.width,
                                            alignment: Alignment.center,
                                            child: SpinKitDoubleBounce(
                                              color: Color(0xffF2CA8A),
                                              size: 50.0,
                                            ),
                                          )
                                        : (wshCntrl.wishlistModel!.result!
                                                    .length >
                                                0)
                                            ? Column(
                                                children: [
                                                  Container(
                                                      width: double.infinity,
                                                      height: Get.width * 0.12,
                                                      color:
                                                          Colors.grey.shade400,
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              vertical: 5),
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 15),
                                                            child: Text(
                                                              "Sort: ",
                                                              style: TextStyle(
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: 20,
                                                          ),
                                                          InkWell(
                                                            onTap: () {
                                                              // wshCntrl.wishlistModel!.result![i].product!.name

                                                              setState(() {
                                                                ratingSorting =
                                                                    !ratingSorting;
                                                                if (ratingSorting) {
                                                                  wshCntrl
                                                                      .wishlistModel!
                                                                      .result!
                                                                      .sort((a,
                                                                          b) {
                                                                    return a
                                                                        .product!
                                                                        .name
                                                                        .toString()
                                                                        .toLowerCase()
                                                                        .compareTo(b
                                                                            .product!
                                                                            .name
                                                                            .toString()
                                                                            .toLowerCase());
                                                                  });
                                                                } else {
                                                                  wshCntrl
                                                                      .wishlistModel!
                                                                      .result!
                                                                      .sort((a,
                                                                          b) {
                                                                    return b
                                                                        .product!
                                                                        .name
                                                                        .toString()
                                                                        .toLowerCase()
                                                                        .compareTo(a
                                                                            .product!
                                                                            .name
                                                                            .toString()
                                                                            .toLowerCase());
                                                                  });
                                                                }
                                                              });
                                                            },
                                                            child: Container(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      vertical:
                                                                          5,
                                                                      horizontal:
                                                                          10),
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              20),
                                                                  color: Color
                                                                      .fromARGB(
                                                                          255,
                                                                          184,
                                                                          36,
                                                                          54)),
                                                              child: Row(
                                                                children: [
                                                                  Text(
                                                                    "Product Name",
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            11,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w400,
                                                                        color: Colors
                                                                            .white),
                                                                  ),
                                                                  SizedBox(
                                                                    width: 5,
                                                                  ),
                                                                  Icon(
                                                                    Icons
                                                                        .arrow_upward,
                                                                    color: Colors
                                                                        .white,
                                                                    size: 15,
                                                                  ),
                                                                  Icon(
                                                                    Icons
                                                                        .arrow_downward,
                                                                    color: Colors
                                                                        .white,
                                                                    size: 15,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),

                                                          ///---- commented :: will be removed after QA
                                                          // SizedBox(
                                                          //   width: 20,
                                                          // ),
                                                          // Container(
                                                          //   padding:
                                                          //       EdgeInsets.symmetric(
                                                          //           vertical: 5,
                                                          //           horizontal: 7),
                                                          //   decoration: BoxDecoration(
                                                          //       borderRadius:
                                                          //           BorderRadius
                                                          //               .circular(20),
                                                          //       color: Color.fromARGB(
                                                          //           255,
                                                          //           184,
                                                          //           36,
                                                          //           54)),
                                                          //   child: Row(
                                                          //     children: [
                                                          //       Text(
                                                          //         "Rating",
                                                          //         style: TextStyle(
                                                          //             fontSize: 15,
                                                          //             fontWeight:
                                                          //                 FontWeight
                                                          //                     .w400,
                                                          //             color: Colors
                                                          //                 .white),
                                                          //       ),
                                                          //       SizedBox(
                                                          //         width: 5,
                                                          //       ),
                                                          //       Icon(
                                                          //         Icons.arrow_upward,
                                                          //         color: Colors.white,
                                                          //         size: 15,
                                                          //       ),
                                                          //       Icon(
                                                          //         Icons
                                                          //             .arrow_downward,
                                                          //         color: Colors.white,
                                                          //         size: 15,
                                                          //       ),
                                                          //     ],
                                                          //   ),
                                                          // )
                                                        ],
                                                      )

                                                      // Commented for testing purpose, will be removed after QA approves
                                                      // Row(
                                                      //   children: [
                                                      //     Expanded(
                                                      //       child: Center(
                                                      //         child: Text(
                                                      //           'YOU ARE SAVVY! ${wshCntrl.wishlistModel!.result!.length} DIFFERENT MAKEUPS',
                                                      //           style: TextStyle(
                                                      //               fontSize: 11,
                                                      //               fontWeight:
                                                      //                   FontWeight
                                                      //                       .bold),
                                                      //         ),
                                                      //       ),
                                                      //     ),
                                                      //     Padding(
                                                      //       padding: EdgeInsets.only(
                                                      //           right: 20),
                                                      //       child: GestureDetector(
                                                      //         child: Icon(Icons.share,
                                                      //             color:
                                                      //                 Colors.blueGrey),
                                                      //         onTap: () {
                                                      //           tiop.playSound();

                                                      //           if (wshCntrl
                                                      //               .wishlistModel!
                                                      //               .result!
                                                      //               .isNotEmpty) {
                                                      //             showShareDialog(
                                                      //                 context);
                                                      //           } else {
                                                      //             Get.showSnackbar(
                                                      //               GetSnackBar(
                                                      //                 message:
                                                      //                     'No wishlist to share.',
                                                      //                 duration:
                                                      //                     Duration(
                                                      //                         seconds:
                                                      //                             2),
                                                      //                 isDismissible:
                                                      //                     true,
                                                      //               ),
                                                      //             );
                                                      //           }
                                                      //         },
                                                      //       ),
                                                      //     ),
                                                      //   ],
                                                      // ),
                                                      ),
                                                  Expanded(
                                                      child: ListView.builder(
                                                    itemCount: wshCntrl
                                                        .wishlistModel!
                                                        .result!
                                                        .length,
                                                    shrinkWrap: true,
                                                    physics:
                                                        AlwaysScrollableScrollPhysics(),
                                                    itemBuilder: (ctx, i) {
                                                      try {
                                                        wshCntrl.wishlistModel!
                                                            .result![i].review!
                                                            .forEach(
                                                                (element) {});
                                                      } catch (e) {}
                                                      return Container(
                                                        color: Colors.white,
                                                        child: Column(
                                                          children: [
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .symmetric(
                                                                      horizontal:
                                                                          10),
                                                              height: 88,
                                                              width: double
                                                                  .infinity,
                                                              child: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceAround,
                                                                children: [
                                                                  Expanded(
                                                                    child:
                                                                        Container(
                                                                      // width: Get.width,
                                                                      child:
                                                                          Column(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.spaceEvenly,
                                                                        children: [
                                                                          Text(
                                                                            "${wshCntrl.wishlistModel!.result![i].product!.name}",
                                                                            maxLines:
                                                                                4,
                                                                            overflow:
                                                                                TextOverflow.ellipsis,
                                                                            style: TextStyle(
                                                                                color: Colors.black,
                                                                                fontSize: 12,
                                                                                fontWeight: FontWeight.w500),
                                                                          ),
                                                                          // Row(
                                                                          //   children: [
                                                                          //     Icon(
                                                                          //       Icons
                                                                          //           .star,
                                                                          //       color: Color(
                                                                          //           0xffF2CA8A),
                                                                          //     ),
                                                                          //     SizedBox(
                                                                          //       width:
                                                                          //           5,
                                                                          //     ),
                                                                          //     Text(rate
                                                                          //         .toString())
                                                                          //   ],
                                                                          // )
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  GestureDetector(
                                                                    behavior:
                                                                        HitTestBehavior
                                                                            .translucent,
                                                                    onTap:
                                                                        () async {
                                                                      cPrint(
                                                                          "USER MISSING??");
                                                                      try {
                                                                        if (Provider.of<AccountProvider>(context,
                                                                                listen: false)
                                                                            .isLoggedIn) {
                                                                          if (await wp.removeItemToWishList(wshCntrl
                                                                              .wishlistModel!
                                                                              .result![i]
                                                                              .product!
                                                                              .sku!)) {
                                                                            wshCntrl.wishlistModel!.result!.removeWhere((element) =>
                                                                                element.wishlistItemId ==
                                                                                wshCntrl.wishlistModel!.result![i].wishlistItemId!);
                                                                            await reviewController.getWishListData();
                                                                          } else {
                                                                            cPrint("UPDATE FAILED");
                                                                          }
                                                                        } else {
                                                                          cPrint(
                                                                              "USER MISSING");
                                                                        }
                                                                      } on Exception catch (e) {
                                                                        cPrint(e
                                                                            .toString());
                                                                      }
                                                                    },
                                                                    child: Container(
                                                                        margin: EdgeInsets.symmetric(horizontal: 20),
                                                                        width: Get.width * 0.020,
                                                                        // color: Colors.orange,
                                                                        child: Icon(
                                                                          Icons
                                                                              .favorite,
                                                                          color:
                                                                              Colors.red,
                                                                        )),
                                                                  ),
                                                                  GestureDetector(
                                                                    child: Icon(
                                                                        Icons
                                                                            .share,
                                                                        color: Colors
                                                                            .grey),
                                                                    onTap: () {
                                                                      tiop.playSound();

                                                                      Share.share(
                                                                          APIEndPoints.mainBaseUrl +
                                                                              wshCntrl.wishlistModel!.result![i].product!.requestPath
                                                                                  .toString(),
                                                                          subject: wshCntrl
                                                                              .wishlistModel!
                                                                              .result![i]
                                                                              .product!
                                                                              .name);
                                                                    },
                                                                  ),
                                                                  GestureDetector(
                                                                    onTap: () {
                                                                      // print(APIEndPoints
                                                                      //         .mediaBaseUrl +
                                                                      //     '${wshCntrl.wishlistModel!.result![i].product!.image}');

                                                                      if (wshCntrl
                                                                          .wishlistModel!
                                                                          .result![
                                                                              i]
                                                                          .product!
                                                                          .image!
                                                                          .contains(
                                                                              'looks')) {
                                                                        List
                                                                            temp =
                                                                            [];
                                                                        Get.find<LooksController>()
                                                                            .lookModel!
                                                                            .items!
                                                                            .forEach((element) {
                                                                          temp.add(
                                                                              element.name);
                                                                        });
                                                                        Get.to(() =>
                                                                            LookPackageMS8(
                                                                              item: Get.find<LooksController>().lookModel!.items![Get.find<LooksController>().lookModel!.items!.indexWhere((f) => f.name.toString() == wshCntrl.wishlistModel!.result![i].product!.name)],
                                                                              looklist: temp,
                                                                              id: Get.find<LooksController>().lookModel!.items!.indexWhere((f) => f.name.toString() == wshCntrl.wishlistModel!.result![i].product!.name),
                                                                            ));
                                                                      } else {
                                                                        Navigator
                                                                            .push(
                                                                          context,
                                                                          MaterialPageRoute(
                                                                            builder:
                                                                                (BuildContext c) {
                                                                              return ProductDetail1Screen(sku: wshCntrl.wishlistModel!.result![i].product!.sku!);
                                                                            },
                                                                          ),
                                                                        );
                                                                      }
                                                                    },
                                                                    child: Container(
                                                                        width: Get.width * 0.18,
                                                                        margin: EdgeInsets.symmetric(horizontal: 10),
                                                                        // color: Colors.green,
                                                                        child: Image.network(
                                                                          // 'assets/images/product.png',
                                                                          // APIEndPoints.mediaBaseUrl +
                                                                          "${wshCntrl.wishlistModel!.result![i].product!.image}",
                                                                          width:
                                                                              72,
                                                                          height:
                                                                              72,
                                                                        )),
                                                                  ),
                                                                  GestureDetector(
                                                                    onTap:
                                                                        () async {
                                                                      tiop.playSound();
                                                                      Navigator
                                                                          .push(
                                                                        context,
                                                                        MaterialPageRoute(
                                                                          builder:
                                                                              (BuildContext c) {
                                                                            return ProductDetail1Screen(sku: wshCntrl.wishlistModel!.result![i].product!.sku!);
                                                                          },
                                                                        ),
                                                                      );
                                                                    },
                                                                    child:
                                                                        Container(
                                                                      margin: EdgeInsets.only(
                                                                          left:
                                                                              10),
                                                                      width: Get
                                                                              .width *
                                                                          0.12,
                                                                      child:
                                                                          CircleAvatar(
                                                                        radius:
                                                                            28,
                                                                        backgroundColor:
                                                                            Colors.black,
                                                                        child:
                                                                            Center(
                                                                          child:
                                                                              Image.asset(
                                                                            "assets/images/Path_6.png",
                                                                            color:
                                                                                Colors.white,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                            ),
                                                            Divider(),
                                                          ],
                                                        ),
                                                      );
                                                    },
                                                  )),
                                                ],
                                              )
                                            : CustomEmptyBagPage(
                                                title:
                                                    'Oops we cannot find what your wish list is',
                                                ontap: () async {
                                                  tiop.playSound();
                                                  Get.back();
                                                  pp.goToPage(Pages.SHOP);
                                                },
                                                emptyBagButtonText:
                                                    'Go SHOPPING',
                                              );
                              })
                            : (selectedReview == 1)
                                ? GetBuilder<ReviewController>(
                                    builder: (revContrl) {
                                    return !Provider.of<AccountProvider>(
                                                context,
                                                listen: false)
                                            .isLoggedIn
                                        ? CustomEmptyBagPage(
                                            title: Provider.of<AccountProvider>(
                                                        context,
                                                        listen: false)
                                                    .isLoggedIn
                                                ? "Oops we are missing your reviews"
                                                : 'Oops we cannot find your reviews',
                                            // title: 'Oops You have no reviews yet,\nwant to go to shopping? ',
                                            ontap: () async {
                                              //      profileController.screen.value = 0;
                                              tiop.playSound();
                                              Get.back();
                                              pp.goToPage(Pages.SHOP);
                                            },
                                            emptyBagButtonText: 'GO SHOPPING',
                                          )
                                        : Container(
                                            alignment: Alignment.center,
                                            child: (reviewController
                                                    .isReviewLoading)
                                                ? Container(
                                                    height: Get.height * 0.5,
                                                    width: Get.width,
                                                    alignment: Alignment.center,
                                                    child: SpinKitDoubleBounce(
                                                      color: Color(0xffF2CA8A),
                                                      size: 50.0,
                                                    ),
                                                  )
                                                : ((reviewController.myReviewModel!
                                                                    .items ??
                                                                [])
                                                            .length >
                                                        0)
                                                    ? Column(
                                                        children: [
                                                          Container(
                                                              width: double
                                                                  .infinity,
                                                              height:
                                                                  Get.width *
                                                                      0.12,
                                                              color: Colors.grey
                                                                  .shade400,
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      vertical:
                                                                          5),
                                                              child: Row(
                                                                children: [
                                                                  Container(
                                                                    padding: EdgeInsets
                                                                        .only(
                                                                            left:
                                                                                15),
                                                                    child: Text(
                                                                      "Sort: ",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              14,
                                                                          fontWeight: FontWeight
                                                                              .w600,
                                                                          color:
                                                                              Colors.white),
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                    width: 20,
                                                                  ),
                                                                  InkWell(
                                                                    onTap: () {
                                                                      setState(
                                                                          () {
                                                                        ratingSorting =
                                                                            !ratingSorting;
                                                                        if (ratingSorting) {
                                                                          reviewController
                                                                              .reviewName
                                                                              .sort((a, b) {
                                                                            return a.name.toString().toLowerCase().compareTo(b.name.toString().toLowerCase());
                                                                          });
                                                                        } else {
                                                                          reviewController
                                                                              .reviewName
                                                                              .sort((a, b) {
                                                                            return b.name.toString().toLowerCase().compareTo(a.name.toString().toLowerCase());
                                                                          });
                                                                        }
                                                                      });
                                                                    },
                                                                    child:
                                                                        Container(
                                                                      padding: EdgeInsets.symmetric(
                                                                          vertical:
                                                                              5,
                                                                          horizontal:
                                                                              10),
                                                                      decoration: BoxDecoration(
                                                                          borderRadius: BorderRadius.circular(
                                                                              20),
                                                                          color: Color.fromARGB(
                                                                              255,
                                                                              184,
                                                                              36,
                                                                              54)),
                                                                      child:
                                                                          Row(
                                                                        children: const [
                                                                          Text(
                                                                            "Product Name",
                                                                            style: TextStyle(
                                                                                fontSize: 11,
                                                                                fontWeight: FontWeight.w400,
                                                                                color: Colors.white),
                                                                          ),
                                                                          SizedBox(
                                                                            width:
                                                                                5,
                                                                          ),
                                                                          Icon(
                                                                            Icons.arrow_upward,
                                                                            color:
                                                                                Colors.white,
                                                                            size:
                                                                                15,
                                                                          ),
                                                                          Icon(
                                                                            Icons.arrow_downward,
                                                                            color:
                                                                                Colors.white,
                                                                            size:
                                                                                15,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                    width: 20,
                                                                  ),
                                                                  InkWell(
                                                                    onTap: () {
                                                                      ///---- sort the products with rating ascendiing and descending
                                                                      setState(
                                                                          () {
                                                                        ratingSorting =
                                                                            !ratingSorting;
                                                                        if (ratingSorting) {
                                                                          reviewController
                                                                              .reviewName
                                                                              .sort((a, b) {
                                                                            return a.rating!.toString().toLowerCase().compareTo(b.rating!.toString().toLowerCase());
                                                                          });
                                                                        } else {
                                                                          reviewController
                                                                              .reviewName
                                                                              .sort((a, b) {
                                                                            return b.rating!.toString().toLowerCase().compareTo(a.rating!.toString().toLowerCase());
                                                                          });
                                                                        }
                                                                      });
                                                                    },
                                                                    child:
                                                                        Container(
                                                                      padding: EdgeInsets.symmetric(
                                                                          vertical:
                                                                              5,
                                                                          horizontal:
                                                                              10),
                                                                      decoration: BoxDecoration(
                                                                          borderRadius: BorderRadius.circular(
                                                                              20),
                                                                          color: Color.fromARGB(
                                                                              255,
                                                                              184,
                                                                              36,
                                                                              54)),
                                                                      child:
                                                                          Row(
                                                                        children: const [
                                                                          Text(
                                                                            "Rating",
                                                                            style: TextStyle(
                                                                                fontSize: 11,
                                                                                fontWeight: FontWeight.w400,
                                                                                color: Colors.white),
                                                                          ),
                                                                          SizedBox(
                                                                            width:
                                                                                5,
                                                                          ),
                                                                          Icon(
                                                                            Icons.arrow_upward,
                                                                            color:
                                                                                Colors.white,
                                                                            size:
                                                                                15,
                                                                          ),
                                                                          Icon(
                                                                            Icons.arrow_downward,
                                                                            color:
                                                                                Colors.white,
                                                                            size:
                                                                                15,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  )
                                                                ],
                                                              )),
                                                          Expanded(
                                                              child: ListView
                                                                  .builder(
                                                                      itemCount: reviewController
                                                                          .myReviewModel!
                                                                          .items!
                                                                          .length,
                                                                      shrinkWrap:
                                                                          true,
                                                                      physics:
                                                                          AlwaysScrollableScrollPhysics(),
                                                                      itemBuilder:
                                                                          (ctx,
                                                                              index1) {
                                                                        Item itemReview = reviewController
                                                                            .myReviewModel!
                                                                            .items![index1];
                                                                        return Container(
                                                                          margin:
                                                                              EdgeInsets.only(left: 20),
                                                                          child:
                                                                              Column(
                                                                            children: [
                                                                              SizedBox(
                                                                                height: 88,
                                                                                width: double.infinity,
                                                                                child: Row(
                                                                                  // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                                                  children: [
                                                                                    SizedBox(
                                                                                      width: Get.width * 0.4,
                                                                                      child: Column(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                                                        children: [
                                                                                          Align(
                                                                                              alignment: Alignment.centerLeft,
                                                                                              child: Text(
                                                                                                itemReview.product_name ?? "",
                                                                                                maxLines: 4,
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                style: TextStyle(color: Colors.black, fontSize: 12, fontWeight: FontWeight.w500),
                                                                                              )),
                                                                                          Row(
                                                                                            children: [
                                                                                              Icon(
                                                                                                Icons.star,
                                                                                                color: Color(0xffF2CA8A),
                                                                                              ),
                                                                                              SizedBox(
                                                                                                width: 5,
                                                                                              ),
                                                                                              Text(double.parse((itemReview.ratingavg ?? 0).toString()).toStringAsFixed(1)),
                                                                                              SizedBox(
                                                                                                width: 5,
                                                                                              ),
                                                                                              Text(
                                                                                                "(${(itemReview.ratings ?? []).length.toString()})",
                                                                                                style: TextStyle(fontSize: 12),
                                                                                              )
                                                                                            ],
                                                                                          )
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                    GestureDetector(
                                                                                      child: Icon(Icons.share, color: Colors.grey),
                                                                                      onTap: () {
                                                                                        tiop.playSound();

                                                                                        Share.share(itemReview.productUrl ?? "", subject: itemReview.product_name ?? "");
                                                                                      },
                                                                                    ),
                                                                                    GestureDetector(
                                                                                        onTap: () {
                                                                                          if (itemReview.image!.contains('looks')) {
                                                                                            List temp = [];
                                                                                            Get.find<LooksController>().lookModel!.items!.forEach((element) {
                                                                                              temp.add(element.name);
                                                                                            });
                                                                                            cPrint("ITEM IS == ${Get.find<LooksController>().lookModel!.items![Get.find<LooksController>().lookModel!.items!.indexWhere((f) => f.name.toString() == itemReview.product_name)].name}");
                                                                                            Get.to(() => LookPackageMS8(
                                                                                                  item: Get.find<LooksController>().lookModel!.items![Get.find<LooksController>().lookModel!.items!.indexWhere((f) => f.name.toString() == itemReview.product_name)],
                                                                                                  looklist: temp,
                                                                                                  id: Get.find<LooksController>().lookModel!.items!.indexWhere((f) => f.name.toString() == itemReview.product_name),
                                                                                                ));
                                                                                          } else {
                                                                                            Navigator.push(
                                                                                              context,
                                                                                              MaterialPageRoute(
                                                                                                builder: (BuildContext c) {
                                                                                                  return ProductDetail1Screen(
                                                                                                    sku: itemReview.sku!.toString(),
                                                                                                  );
                                                                                                },
                                                                                              ),
                                                                                            );
                                                                                          }
                                                                                        },
                                                                                        child: SizedBox(
                                                                                            width: Get.width * 0.25,
                                                                                            child: (itemReview.image ?? "") != ''
                                                                                                ? Image.network(
                                                                                                    // 'assets/images/product.png',
                                                                                                    // APIEndPoints.mediaBaseUrl +
                                                                                                    "${(itemReview.image ?? "")}",
                                                                                                    width: 72,
                                                                                                    height: 72,
                                                                                                  )
                                                                                                : Image.network(
                                                                                                    APIEndPoints.mediaBaseUrl + "null",
                                                                                                    width: 72,
                                                                                                    height: 72,
                                                                                                  ))),
                                                                                    GestureDetector(
                                                                                      onTap: () async {
                                                                                        tiop.playSound();
                                                                                        Navigator.push(
                                                                                          context,
                                                                                          MaterialPageRoute(
                                                                                            builder: (BuildContext c) {
                                                                                              return ProductDetail1Screen(sku: itemReview.sku!);
                                                                                            },
                                                                                          ),
                                                                                        );
                                                                                      },
                                                                                      child: Container(
                                                                                        width: Get.width * 0.2,
                                                                                        //  color: Colors.red,
                                                                                        child: CircleAvatar(
                                                                                          radius: 30,
                                                                                          backgroundColor: Colors.black,
                                                                                          child: Center(
                                                                                            child: Image.asset(
                                                                                              "assets/images/Path_6.png",
                                                                                              color: Colors.white,
                                                                                            ),
                                                                                          ),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Divider(),
                                                                            ],
                                                                          ),
                                                                        );
                                                                      })),
                                                        ],
                                                      )
                                                    : CustomEmptyBagPage(
                                                        title:
                                                            "Oops we are missing your reviews",

                                                        // title: 'Oops You have no reviews yet,\nwant to go to shopping? ',
                                                        ontap: () async {
                                                          //      profileController.screen.value = 0;
                                                          tiop.playSound();
                                                          Get.back();
                                                          pp.goToPage(
                                                              Pages.SHOP);
                                                        },
                                                        emptyBagButtonText:
                                                            'GO SHOPPING',
                                                      ));
                                  })
                                : GetBuilder<ReviewController>(
                                    builder: (revContrl) {
                                    return Container(
                                        alignment: Alignment.center,
                                        child: (reviewController
                                                .isGlobaleReviews)
                                            ? Container(
                                                height: Get.height * 0.5,
                                                width: Get.width,
                                                alignment: Alignment.center,
                                                child: SpinKitDoubleBounce(
                                                  color: Color(0xffF2CA8A),
                                                  size: 50.0,
                                                ),
                                              )
                                            : (reviewController.reviewModel!
                                                        .items!.length >
                                                    0)
                                                ? Column(
                                                    children: [
                                                      Container(
                                                          width:
                                                              double.infinity,
                                                          height:
                                                              Get.width * 0.12,
                                                          color: Colors
                                                              .grey.shade400,
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  vertical: 5),
                                                          child: Row(
                                                            children: [
                                                              Container(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            15),
                                                                child: Text(
                                                                  "Sort: ",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          14,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600,
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 20,
                                                              ),
                                                              InkWell(
                                                                onTap: () {
                                                                  ///---- sort the products with rating ascendiing and descending
                                                                  setState(() {
                                                                    ratingSorting =
                                                                        !ratingSorting;
                                                                    if (ratingSorting) {
                                                                      reviewController
                                                                          .reviewGlobaleName
                                                                          .sort((a,
                                                                              b) {
                                                                        return a
                                                                            .name
                                                                            .toString()
                                                                            .toLowerCase()
                                                                            .compareTo(b.name.toString().toLowerCase());
                                                                      });
                                                                    } else {
                                                                      reviewController
                                                                          .reviewGlobaleName
                                                                          .sort((a,
                                                                              b) {
                                                                        return b
                                                                            .name
                                                                            .toString()
                                                                            .toLowerCase()
                                                                            .compareTo(a.name.toString().toLowerCase());
                                                                      });
                                                                    }
                                                                  });
                                                                },
                                                                child:
                                                                    Container(
                                                                  padding: EdgeInsets
                                                                      .symmetric(
                                                                          vertical:
                                                                              5,
                                                                          horizontal:
                                                                              10),
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              20),
                                                                      color: Color.fromARGB(
                                                                          255,
                                                                          184,
                                                                          36,
                                                                          54)),
                                                                  child: Row(
                                                                    children: [
                                                                      Text(
                                                                        "Product Name",
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                11,
                                                                            fontWeight:
                                                                                FontWeight.w400,
                                                                            color: Colors.white),
                                                                      ),
                                                                      SizedBox(
                                                                        width:
                                                                            5,
                                                                      ),
                                                                      Icon(
                                                                        Icons
                                                                            .arrow_upward,
                                                                        color: Colors
                                                                            .white,
                                                                        size:
                                                                            15,
                                                                      ),
                                                                      Icon(
                                                                        Icons
                                                                            .arrow_downward,
                                                                        color: Colors
                                                                            .white,
                                                                        size:
                                                                            15,
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 20,
                                                              ),
                                                              InkWell(
                                                                  onTap: () {
                                                                    ///---- sort the products with rating ascendiing and descending
                                                                    setState(
                                                                        () {
                                                                      ratingSorting =
                                                                          !ratingSorting;
                                                                      if (ratingSorting) {
                                                                        reviewController
                                                                            .reviewGlobaleName
                                                                            .sort((a,
                                                                                b) {
                                                                          return a
                                                                              .rating!
                                                                              .toString()
                                                                              .toLowerCase()
                                                                              .compareTo(b.rating!.toString().toLowerCase());
                                                                        });
                                                                      } else {
                                                                        reviewController
                                                                            .reviewGlobaleName
                                                                            .sort((a,
                                                                                b) {
                                                                          return b
                                                                              .rating!
                                                                              .toString()
                                                                              .toLowerCase()
                                                                              .compareTo(a.rating!.toString().toLowerCase());
                                                                        });
                                                                      }
                                                                    });
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    padding: EdgeInsets.symmetric(
                                                                        vertical:
                                                                            5,
                                                                        horizontal:
                                                                            10),
                                                                    decoration: BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                20),
                                                                        color: Color.fromARGB(
                                                                            255,
                                                                            184,
                                                                            36,
                                                                            54)),
                                                                    child: Row(
                                                                      children: const [
                                                                        Text(
                                                                          "Rating",
                                                                          style: TextStyle(
                                                                              fontSize: 11,
                                                                              fontWeight: FontWeight.w400,
                                                                              color: Colors.white),
                                                                        ),
                                                                        SizedBox(
                                                                          width:
                                                                              5,
                                                                        ),
                                                                        Icon(
                                                                          Icons
                                                                              .arrow_upward,
                                                                          color:
                                                                              Colors.white,
                                                                          size:
                                                                              15,
                                                                        ),
                                                                        Icon(
                                                                          Icons
                                                                              .arrow_downward,
                                                                          color:
                                                                              Colors.white,
                                                                          size:
                                                                              15,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ))
                                                            ],
                                                          )),
                                                      // Commented for testing purpose, will be removed after QA approves
                                                      // Container(
                                                      //   width: double.infinity,
                                                      //   height: Get.width * 0.10,
                                                      //   color: Colors.grey,
                                                      //   child: Center(
                                                      //     child: Text(
                                                      //       'YOU ARE SAVVY! ${reviewController.reviewModel!.items!.length} DIFFERENT MAKEUPS',
                                                      //       style: TextStyle(
                                                      //           fontSize: 11,
                                                      //           fontWeight:
                                                      //               FontWeight
                                                      //                   .bold),
                                                      //     ),
                                                      //   ),
                                                      // ),
                                                      Expanded(
                                                          child:
                                                              ListView.builder(
                                                                  itemCount: reviewController
                                                                      .reviewModel!
                                                                      .items!
                                                                      .length,
                                                                  shrinkWrap:
                                                                      true,
                                                                  physics:
                                                                      AlwaysScrollableScrollPhysics(),
                                                                  itemBuilder:
                                                                      (ctx,
                                                                          index2) {
                                                                    return Container(
                                                                      margin: EdgeInsets.only(
                                                                          left:
                                                                              20),
                                                                      child:
                                                                          Column(
                                                                        children: [
                                                                          SizedBox(
                                                                            height:
                                                                                88,
                                                                            width:
                                                                                double.infinity,
                                                                            child:
                                                                                Row(
                                                                              // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                                              children: [
                                                                                SizedBox(
                                                                                  width: Get.width * 0.4,
                                                                                  child: Column(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                                    children: [
                                                                                      Align(
                                                                                          alignment: Alignment.centerLeft,
                                                                                          child: Text(
                                                                                            reviewController.reviewGlobaleName.isNotEmpty && reviewController.reviewGlobaleName.length > index2 ? "${reviewController.reviewGlobaleName[index2].name}" : "",
                                                                                            maxLines: 4,
                                                                                            overflow: TextOverflow.ellipsis,
                                                                                            // "${reviewController.reviewModel!.items![index2].}",
                                                                                            style: TextStyle(color: Colors.black, fontSize: 12, fontWeight: FontWeight.w500),
                                                                                          )),
                                                                                      Row(
                                                                                        children: [
                                                                                          Icon(
                                                                                            Icons.star,
                                                                                            color: Color(0xffF2CA8A),
                                                                                          ),
                                                                                          SizedBox(
                                                                                            width: 5,
                                                                                          ),
                                                                                          Text(double.parse((reviewController.reviewGlobaleName[index2].rating ?? 0).toString()).toStringAsFixed(1)),
                                                                                          SizedBox(
                                                                                            width: 5,
                                                                                          ),
                                                                                          Text(
                                                                                            "(${reviewController.reviewGlobaleName[index2].ratingCount!.toString()})",
                                                                                            style: TextStyle(fontSize: 12),
                                                                                          )
                                                                                        ],
                                                                                      )
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                GestureDetector(
                                                                                  child: Icon(Icons.share, color: Colors.grey),
                                                                                  onTap: () {
                                                                                    tiop.playSound();

                                                                                    Share.share(reviewController.reviewGlobaleName[index2].productUrl!, subject: reviewController.reviewGlobaleName[index2].name.toString());
                                                                                  },
                                                                                ),
                                                                                GestureDetector(
                                                                                  onTap: () {
                                                                                    if (reviewController.reviewGlobaleName[index2].imagePath!.contains('looks')) {
                                                                                      List temp = [];
                                                                                      Get.find<LooksController>().lookModel!.items!.forEach((element) {
                                                                                        temp.add(element.name);
                                                                                      });
                                                                                      cPrint("ITEM IS == ${Get.find<LooksController>().lookModel!.items![Get.find<LooksController>().lookModel!.items!.indexWhere((f) => f.name.toString() == reviewController.reviewGlobaleName[index2].name)].name}");
                                                                                      Get.to(() => LookPackageMS8(
                                                                                            item: Get.find<LooksController>().lookModel!.items![Get.find<LooksController>().lookModel!.items!.indexWhere((f) => f.name.toString() == reviewController.reviewGlobaleName[index2].name)],
                                                                                            looklist: temp,
                                                                                            id: Get.find<LooksController>().lookModel!.items!.indexWhere((f) => f.name.toString() == reviewController.reviewGlobaleName[index2].name),
                                                                                          ));
                                                                                    } else {
                                                                                      Navigator.push(
                                                                                        context,
                                                                                        MaterialPageRoute(
                                                                                          builder: (BuildContext c) {
                                                                                            // return LookPackageMS8();

                                                                                            return ProductDetail1Screen(sku: reviewController.reviewGlobaleName[index2].sku!.toString());
                                                                                          },
                                                                                        ),
                                                                                      );
                                                                                    }
                                                                                  },
                                                                                  // riken Test
                                                                                  child: SizedBox(
                                                                                      width: Get.width * 0.25,
                                                                                      child: reviewController.reviewGlobaleName[index2].imagePath != ''
                                                                                          ? Image.network(
                                                                                              // 'assets/images/product.png',
                                                                                              // APIEndPoints.mediaBaseUrl + "${reviewController.reviewGlobaleName[index2].imagePath}",
                                                                                              "${reviewController.reviewGlobaleName[index2].imagePath}",
                                                                                              width: 72,
                                                                                              height: 72,
                                                                                            )
                                                                                          : Image.network(
                                                                                              APIEndPoints.mediaBaseUrl + "null",
                                                                                              width: 72,
                                                                                              height: 72,
                                                                                            )),
                                                                                  // Test End
                                                                                ),
                                                                                GestureDetector(
                                                                                  onTap: () async {
                                                                                    tiop.playSound();
                                                                                    Navigator.push(context, MaterialPageRoute(
                                                                                      builder: (BuildContext c) {
                                                                                        return ProductDetail1Screen(sku: reviewController.reviewGlobaleName[index2].sku!);
                                                                                        // return ProductDetail1Screen(sku: reviewController.reviewModel!.items![index2].sku!);
                                                                                      },
                                                                                    ));
                                                                                  },
                                                                                  child: Container(
                                                                                    width: Get.width * 0.2,
                                                                                    //  color: Colors.red,
                                                                                    child: CircleAvatar(
                                                                                      radius: 30,
                                                                                      backgroundColor: Colors.black,
                                                                                      child: Center(
                                                                                        child: Image.asset(
                                                                                          "assets/images/Path_6.png",
                                                                                          color: Colors.white,
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                )
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          Divider(),
                                                                        ],
                                                                      ),
                                                                    );
                                                                  })),
                                                    ],
                                                  )
                                                : CustomEmptyBagPage(
                                                    title:
                                                        'Oops You have no reviews yet,\nwant to go to shopping? ',
                                                    ontap: () async {
                                                      //      profileController.screen.value = 0;
                                                      tiop.playSound();
                                                      Get.back();
                                                      pp.goToPage(Pages.SHOP);
                                                    },
                                                    emptyBagButtonText:
                                                        'GO SHOPPING',
                                                  ) /*Container(
                                          child: Center(
                                            child: Text(
                                                'Oops! INo Reviews Yet '),
                                          ),
                                        )*/

                                        //  CustomEmptyBagPage(
                                        //     title:
                                        //         'Hey! You have no reviews yet',
                                        //     ontap: () async {
                                        //       await Navigator.push(
                                        //         context,
                                        //         MaterialPageRoute(
                                        //           builder:
                                        //               (BuildContext _) {
                                        //             return MakeOverLoginPrompt();
                                        //           },
                                        //         ),
                                        //       );
                                        //     },
                                        //     emptyBagButtonText:
                                        //         'JOIN SOFIQE FOR FREE',

                                        //   )

                                        );
                                  })),
                  ],
                ),
              ),
            )
          : navController.innerPageIndex.value == 1
              ? LogInRegisterPromptPage(
                  flowFromMs: false,
                  makeOverMessage: profileController.makeOverMessage.value,
                  onBack: () {
                    FABController.to.closeOpenedMenu();
                    navController.innerPageIndex.value = 0;
                  },
                )
              : DoMakeOverAnalysisPromptPage(
                  onBack: () {
                    FABController.to.closeOpenedMenu();
                    navController.innerPageIndex.value = 0;
                  },
                  description:
                      'For this section you have to complete your Makeover first',
                  title: 'We are sofiqe',
                  flowFromMs: false,
                );
    });
  }

  Future showShareDialog(BuildContext context) {
    String message = '';
    String mails = '';
    // Size size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: HexColor("#EB7AC1"),
            contentPadding: EdgeInsets.zero,
            insetPadding: EdgeInsets.all(10),
            content: Stack(
              children: <Widget>[
                Form(
                  child: Container(
                    height: 400,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          color: HexColor("#EB7AC1"),
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Padding(
                                  padding: EdgeInsets.only(left: 8.0),
                                  child: Icon(
                                    Icons.close,
                                    size: 30,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Center(
                                    child: Text(
                                  'SHARE WISHLIST',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1),
                                )),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, top: 10),
                          child: Column(
                            children: [
                              TextField(
                                onChanged: (value) {
                                  setState(() {
                                    message = value.toString();
                                  });
                                },
                                controller: null,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(20.0),
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: OutlineInputBorder(),
                                  labelStyle: TextStyle(
                                    color: Colors.black,
                                  ),
                                  labelText:
                                      'Please write a message to your friend',
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextField(
                                onChanged: (value) {
                                  setState(() {
                                    mails = value.toString();
                                  });
                                },
                                controller: null,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(20.0),
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: OutlineInputBorder(),
                                  labelStyle: TextStyle(
                                      color: Colors.black,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500),
                                  labelText:
                                      'Add email, if more than one, please comma separate them',
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: CapsuleButton(
                            backgroundColor: Colors.white,
                            borderColor: Color(0xFF393636),
                            onPress: () async {
                              if (mails.isNotEmpty) {
                                await reviewController.shareWishlist(
                                    mails, message);
                                Navigator.pop(context);
                              } else {
                                Get.showSnackbar(
                                  GetSnackBar(
                                    message: 'Please enter required details.',
                                    duration: Duration(seconds: 2),
                                    isDismissible: true,
                                  ),
                                );
                              }
                            },
                            child: Text(
                              'Share Wishlist',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
