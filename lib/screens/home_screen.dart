import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/controller/msProfileController.dart';
import 'package:sofiqe/provider/catalog_provider.dart';
import 'package:sofiqe/provider/page_provider.dart';
import 'package:sofiqe/provider/total_make_over_provider.dart';
import 'package:sofiqe/utils/constants/route_names.dart';
import 'package:sofiqe/widgets/home/home_page.dart';
// Custom packages
import 'package:sofiqe/widgets/png_icon.dart';
import '../controller/controllers.dart';
import '../controller/fabController.dart';
import '../provider/cart_provider.dart';
import '../provider/try_it_on_provider.dart';
import '../widgets/makeover/make_over_login_custom_widget.dart';
import 'package:sofiqe/utils/states/function.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final PageProvider pp = Get.find();

  final CatalogProvider catp = Get.find();

  final MsProfileController profileController = Get.put(MsProfileController());

  final TotalMakeOverProvider tm = Get.put(TotalMakeOverProvider());
  final TryItOnProvider TIOP = Get.find();

  @override
  void initState() {
    super.initState();
    navController.currentMainIndex.value = 0;
    navController.innerPageIndex.value = 0;
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      if (message.notification != null) {
        cPrint(message.notification!.title);
        cPrint(message.notification!.body);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    FABController.to.closeOpenedMenu();
    profileController.getUserQuestionsInformations();
    var cartItems = Provider.of<CartProvider>(context).getCartLength();
    var cartTotalQty = Provider.of<CartProvider>(context).getTotalQty();
    return Obx(() {
      return navController.currentMainIndex.value == 0 &&
              navController.innerPageIndex.value == 0
          ? Scaffold(
              appBar: AppBar(
                leading: Container(
                  child: Center(
                    child: GestureDetector(
                      onTap: () {
                        FABController.to.closeOpenedMenu();
                        pp.goToPage(Pages.TRYITON);
                        catp.unhideSeachBar();
                        // cPrint(Provider.of<AccountProvider>(context, listen: false).userToken);
                      },
                      child: Container(
                        height: AppBar().preferredSize.height * 0.7,
                        width: AppBar().preferredSize.height * 0.7,
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.all(Radius.circular(
                              AppBar().preferredSize.height * 0.7)),
                          border: Border.all(color: Colors.white12, width: 1),
                        ),
                        child: PngIcon(
                          image: 'assets/icons/search_white.png',
                        ),
                      ),
                    ),
                  ),
                ),
                centerTitle: true,
                backgroundColor: Colors.black,
                title: Column(
                  children: [
                    SizedBox(
                      height: size.height * 0.02,
                    ),
                    Text(
                      'sofiqe',
                      style: Theme.of(context).textTheme.headline1!.copyWith(
                          color: Colors.white,
                          fontSize: size.height * 0.035,
                          letterSpacing: 0.6),
                    ),
                    SizedBox(
                      height: size.height * 0.03,
                    ),
                  ],
                ),
                actions: [
                  SizedBox(
                    height: AppBar().preferredSize.height,
                    width: AppBar().preferredSize.height * 1,
                    child: Center(
                      child: GestureDetector(
                        onTap: () {
                          FABController.to.closeOpenedMenu();
                          TIOP.isChangeButtonColor.value = true;
                          TIOP.playSound();

                          Future.delayed(Duration(milliseconds: 10))
                              .then((value) {
                            TIOP.isChangeButtonColor.value = false;
                            Navigator.pushNamed(context, RouteNames.cartScreen);
                          });
                        },
                        child: Obx(
                          () => Container(
                            height: AppBar().preferredSize.height * 0.7,
                            width: AppBar().preferredSize.height * 0.7,
                            decoration: BoxDecoration(
                              color: TIOP.isChangeButtonColor.isTrue
                                  ? TIOP.ontapColor
                                  : Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(
                                  AppBar().preferredSize.height * 0.7)),
                            ),
                            child: Stack(
                              alignment: Alignment.topRight,
                              children: [
                                PngIcon(
                                  image: 'assets/images/Path_6.png',
                                ),
                                if (cartItems == 0)
                                  SizedBox()
                                else
                                  Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.red),
                                      padding: EdgeInsets.all(5),
                                      child: Text(cartTotalQty.toString()))
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              body: HomePage(),
            )
          : navController.innerPageIndex.value == 1
              ? LogInRegisterPromptPage(
                  flowFromMs: false,
                  makeOverMessage: profileController.makeOverMessage.value,
                  onBack: () {
                    FABController.to.closeOpenedMenu();
                    navController.innerPageIndex.value = 0;
                  },
                )
              : DoMakeOverAnalysisPromptPage(
                  onBack: () {
                    FABController.to.closeOpenedMenu();
                    navController.innerPageIndex.value = 0;
                  },
                  description:
                      'For this section you have to complete your Makeover first',
                  title: 'We are sofiqe',
                  flowFromMs: false,
                );
    });
  }
}
