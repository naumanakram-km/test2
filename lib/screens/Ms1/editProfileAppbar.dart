import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/controller/controllers.dart';
import 'package:sofiqe/provider/account_provider.dart';
import 'package:sofiqe/widgets/profile_picture.dart';

class EditProfileAppbar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    AccountProvider ap = Provider.of<AccountProvider>(context);
    String? name = "Guest";
    if (ap.isLoggedIn && ap.user != null) {
      name = '${profileController.firstNameController.text} ${profileController.lastNameController.text}';
    }
    return Container(
      height: size.height * 0.11,
      color: Colors.black,
      child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 20,
              bottom: 20,
              left: 10,
              // right: 2
            ),
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                 Get.back();
                },
                splashColor: Colors.transparent,
                borderRadius: BorderRadius.circular(100),
                child: Container(
                  // color: Colors.yellow,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100)
                  ),
                  padding: EdgeInsets.only(
                    left: 10,
                    right: 10,
                  ),
                  child: SvgPicture.asset(
                    'assets/svg/arrow.svg',
                    width: 12,
                    height: 12,
                  ),
                ),
              ),
            ),
          ),

          ///------- Profile Image Viewer & Picker Function
          GestureDetector(
            onTap: () async {
              ap.getGalleryImage(context);
            },
            child: Stack(
              alignment: Alignment.bottomRight,
              children: <Widget>[
                ProfilePicture(),
                Container(
                  height: 24,
                  width: 24,
                  child: Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        // color: Colors.white
                        color: Color(0xFFAFA0A0)
                    ),
                    child: Center(
                      child: SvgPicture.asset(
                        'assets/images/add_a_photo_outlined.svg',
                        width: 15,
                        height: 15,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),


          SizedBox(width: size.width*0.09),
          Container(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 6),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  'sofiqe',
                  style: Theme.of(context).textTheme.headline1!.copyWith(
                    color: Colors.white,
                    fontSize: 23,
                  ),
                ),
                Text(
                  '$name',
                  style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: Colors.white,
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'Premium Member',
                  style: Theme.of(context).textTheme.headline2!.copyWith(
                    color: Color(0xFFAFA0A0),
                    fontSize: 10,
                  ),
                ),
              ],
            ),
          ),
         
        ],
      ),
    );
  }
}