import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/controller/controllers.dart';
import 'package:sofiqe/controller/currencyController.dart';
import 'package:sofiqe/controller/msProfileController.dart';
import 'package:sofiqe/model/currency.dart';
import 'package:sofiqe/provider/account_provider.dart';
import 'package:sofiqe/provider/page_provider.dart';
import 'package:sofiqe/provider/try_it_on_provider.dart';
import 'package:sofiqe/screens/Ms1/privacy_policy.dart';
import 'package:sofiqe/screens/Ms2/my_shopping_history.dart';
import 'package:sofiqe/screens/signup_screen.dart';
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/constants/app_colors.dart';
import 'package:sofiqe/utils/states/local_storage.dart';
import 'package:sofiqe/widgets/my_sofiqe/profile_information.dart';
import '../../controller/fabController.dart';
import '../../controller/natural_me_controller.dart';
import '../../main.dart';
import '../../provider/catalog_provider.dart';
import '../../utils/states/user_account_data.dart';
import '../../widgets/makeover/make_over_login_custom_widget.dart';
import '../catalog_screen.dart';
import '../login_screen.dart';
import '../my_sofiqe.dart';
import '../product_detail_1_screen.dart';
import '../try_it_on_screen.dart';
import 'package:sofiqe/utils/states/function.dart';

class Ms1Profile extends StatefulWidget {
  const Ms1Profile({Key? key}) : super(key: key);

  @override
  _Ms1ProfileState createState() => _Ms1ProfileState();
}

var itemsCurrency = [
  "GBP",
  "CAD",
  "DKK",
  "EUR",
  "ISK",
  "NOK",
  "SEK",
  "CHF",
  "USD"
];

class _Ms1ProfileState extends State<Ms1Profile>
    with SingleTickerProviderStateMixin {
  final TryItOnProvider tiop = Get.find();
  final CatalogProvider cata = Get.find();
  NaturalMeController naturalMeController = Get.put(NaturalMeController());
  late TabController _controller;
  List<RecentScan> recenScan = [
    RecentScan(
        image: 'assets/images/dior1.png',
        title: 'Rouge Dior Ultra Care - Batom',
        type: 'DIOR'),
    RecentScan(
        image: 'assets/images/dior2.png',
        title: 'Dior Addict Lacquer Plump',
        type: 'DIOR'),
    RecentScan(
        image: 'assets/images/dior3.png',
        title: 'Dior Addict Lips Stellar Shine 3 g',
        type: 'DIOR'),
    RecentScan(
        image: 'assets/images/dior1.png',
        title: 'Rouge Dior Ultra Care - Batom',
        type: 'DIOR'),
  ];

  // bool notification = false;
  bool sound = false;
  dynamic selectedCurrencyVal;

  @override
  void initState() {
    super.initState();
    AccountProvider ap = Provider.of<AccountProvider>(context, listen: false);
    ap.getProfilePictureFromServer();
    profileController.screen.value = 0;
    profileController.getRecenItems();
    profileController.getUserProfile();
    naturalMeController.getNaturalMe();
    _controller = new TabController(length: 2, vsync: this);
    setDefaultValue();
    profileController.getNotificationSetting();
  }

  setDefaultValue() {
    setState(() {
      selectedCurrencyVal = CurrencyController.to.currency;
    });
  }

  @override
  Widget build(BuildContext context) {
    FABController.to.closeOpenedMenu();
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        FABController.to.closeOpenedMenu();
      },
      child: Obx(() {
        if (profileController.screen.value == 1) {
          debugPrint(
              '==== returning value is ${profileController.makeOverMessage.value} ====');
          return LogInRegisterPromptPage(
            makeOverMessage: profileController.makeOverMessage.value,
          );
        } else if (profileController.screen.value == 2) {
          return DoMakeOverAnalysisPromptPage(
            description:
                'For this section you have to complete your Makeover first',
            title: 'We are sofiqe',
          );
        } else if (profileController.screen.value == 4) {
          return MyShoppingHistory();
        } else {
          return SafeArea(
            child: Scaffold(
              backgroundColor: Colors.white,
              body: Column(
                children: [
                  ProfileInformation(),
                  if (profileController.screen.value == 3)
                    Expanded(child: MySofiqeMS4(naturalMeController))
                  else
                    Expanded(
                        child: SingleChildScrollView(
                      child: mySofiqe(context),
                    ))
                ],
              ),
            ),
          );
        }
      }),
    );
  }

  Timer scheduleTimeout([int milliseconds = 10000]) =>
      Timer(Duration(milliseconds: milliseconds), handleTimeout);

  Container mySofiqe(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    AccountProvider account =
        Provider.of<AccountProvider>(context, listen: true);
    return Container(
        child: Column(
      children: [
        SizedBox(
          height: 5,
        ),
        displayTile(
            leading: "lipstick.png",
            title: "My Shopping History",
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            handler: () async {
              gtm.push(
                'press_myshopping_history',
              );
              FABController.to.closeOpenedMenu();
              profileController.makeOverMessage.value = false;
              !Provider.of<AccountProvider>(context, listen: false).isLoggedIn
                  ? profileController.screen.value = 1
                  : profileController.screen.value = 4;
            }),
        Divider(),
        displayTile(
            leading: "lipstick.png",
            leadingSvg: "gift-card-icon.svg",
            title: "   Gift Cards",
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            handler: () async {
              gtm.push(
                'press_giftcards',
              );
              FABController.to.closeOpenedMenu();

              ///----- Navigate to C2  with Red SALE Banner
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext c) {
                    return CatalogScreen(
                      isFromSale: true,
                      isViewingGiftCards: true,
                      comingFromMainIndex: navController.currentMainIndex.value,
                    );
                  },
                ),
              );
            }),
        Divider(),
        displayTile(
            leading: "user.png",
            title: "Natural Me",
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            handler: () async {
              if (Provider.of<AccountProvider>(context, listen: false)
                  .isLoggedIn) {
                if (!makeOverProvider.tryitOn.value) {
                  showLoaderDialog(context);
                } else {
                  profileController.screen.value = 3;
                }
              } else {
                showLoaderDialog(context);
              }
            }),
        Divider(),
        SizedBox(
          height: 5,
        ),
        Container(
          alignment: Alignment.topLeft,
          child: TabBar(
            onTap: (value) {
              gtm.push(
                'press_recent_scans',
              );
            },
            indicatorPadding: EdgeInsets.zero,
            isScrollable: true,
            indicatorColor: Color(0xffF2CA8A),
            labelColor: Colors.black,
            labelStyle: TextStyle(fontSize: 12, color: Colors.black),
            controller: _controller,
            tabs: const [
              Tab(
                text: 'RECENT SCANS',
              ),
              Tab(
                text: 'RECENT COLOURS',
              ),
            ],
          ),
        ),
        Container(
          height: (profileController.recentItem.data == null ||
                  profileController.recentItem.data!.items!.length == 0)
              ? MediaQuery.of(context).size.height * 0.27
              : MediaQuery.of(context).size.height * 0.27,
          child: TabBarView(
            controller: _controller,
            children: <Widget>[
              GetBuilder<MsProfileController>(builder: (contrl) {
                return (contrl.isRecentLoading)
                    ? Container(
                        child: Center(
                        child: SpinKitDoubleBounce(
                          color: Color(0xffF2CA8A),
                          size: 50.0,
                        ),
                      ))
                    : (profileController.recentItem.data == null ||
                            contrl.recentItem.data!.items!.length == 0 ||
                            contrl.recentItem.data!.items!.first.id == '' ||
                            !Provider.of<AccountProvider>(context,
                                    listen: false)
                                .isLoggedIn)
                        ? Container(
                            alignment: Alignment.center,
                            child: Center(
                              child: Container(
                                alignment: Alignment.center,
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0)),
                                    ),
                                    foregroundColor: MaterialStateProperty.all(
                                      AppColors.navigationBarSelectedColor,
                                    ),
                                    backgroundColor: MaterialStateProperty.all(
                                        Color(0xffF2CA8A)),
                                    overlayColor: MaterialStateProperty
                                        .resolveWith<Color?>(
                                      (Set<MaterialState> states) {
                                        if (states
                                            .contains(MaterialState.pressed))
                                          return tiop.ontapColor; //<-- SEE HERE
                                        return null; // Defer to the widget's default.
                                      },
                                    ),
                                  ),
                                  onPressed: () {
                                    tiop.isChangeButtonColor.value = true;
                                    tiop.playSound();
                                    Future.delayed(Duration(milliseconds: 10))
                                        .then((value) {
                                      tiop.isChangeButtonColor.value = false;
                                      SystemChrome.setEnabledSystemUIMode(
                                          SystemUiMode.immersive);
                                      FABController.to.closeOpenedMenu();
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (builder) =>
                                                  TryItOnScreen()));
                                    });
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 20, horizontal: 30),
                                    child: Text(
                                      "Scan products or colours",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12,
                                          letterSpacing: 1,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Container(
                            height: MediaQuery.of(context).size.height * 0.23,
                            width: double.infinity,
                            // color: Colors.green,
                            child: Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Align(
                                      alignment: Alignment.center,
                                      child: Container(
                                        width: 20,
                                        //margin: EdgeInsets.only(top: 30),
                                        child: Icon(
                                          Icons.arrow_back_ios_new,
                                          size: 15,
                                        ),
                                      )),
                                  Expanded(
                                      child: ListView.builder(
                                    physics: BouncingScrollPhysics(),
                                    scrollDirection: Axis.horizontal,
                                    itemCount:
                                        contrl.recentItem.data!.items!.length,
                                    itemBuilder: (context, index) {
                                      return GestureDetector(
                                        onTap: () {
                                          FABController.to.closeOpenedMenu();
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (BuildContext c) {
                                                return ProductDetail1Screen(
                                                  sku: contrl.recentItem.data!
                                                      .items![index].sku!,
                                                );
                                              },
                                            ),
                                          );
                                          //Get.to(() => MyShoppingHistory());
                                        },
                                        child: Column(
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 10),
                                                width: 96,
                                                child: Container(
                                                  height: 96,
                                                  width: 96,
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color: Colors.black)),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10.0),
                                                    child: (contrl
                                                            .recentItem
                                                            .data!
                                                            .items![index]
                                                            .image
                                                            .toString()
                                                            .isNotEmpty)
                                                        ? Image.network(
                                                            APIEndPoints
                                                                    .mediaBaseUrl +
                                                                "${contrl.recentItem.data!.items![index].image}",
                                                            // recenScan[0].image!,
                                                            height: 52,
                                                            width: 33,
                                                            fit: BoxFit.contain,
                                                          )
                                                        : Image.network(
                                                            APIEndPoints
                                                                    .mediaBaseUrl +
                                                                "null",
                                                            width: 52,
                                                            height: 33,
                                                          ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Text(
                                              contrl.recentItem.data!
                                                  .items![index].brand!,
                                              style: TextStyle(
                                                  fontSize: 10,
                                                  color: Color(0xff938282)),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Container(
                                              width: 90,
                                              child: Text(
                                                contrl.recentItem.data!
                                                    .items![index].name!,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.black),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Container(
                                              child: RatingBar.builder(
                                                initialRating: 3,
                                                minRating: 1,
                                                direction: Axis.horizontal,
                                                allowHalfRating: true,
                                                itemCount: 5,
                                                itemSize: 12.0,
                                                itemPadding:
                                                    EdgeInsets.symmetric(
                                                        horizontal: 4.0),
                                                itemBuilder: (context, _) =>
                                                    Icon(
                                                  Icons.star,
                                                  color: Colors.amber,
                                                ),
                                                onRatingUpdate: (rating) {
                                                  cPrint(rating);
                                                },
                                              ),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Text(
                                              'REVIEW',
                                              style: TextStyle(fontSize: 10),
                                            )
                                          ],
                                        ),
                                      );
                                    },
                                  )),
                                  Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      width: 20,
                                      // margin: EdgeInsets.only(top: 30),
                                      // color:Colors.pink,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
              }),
              GetBuilder<MsProfileController>(builder: (contrl) {
                return (contrl.isRecentLoading)
                    ? Container(
                        child: Center(
                        child: SpinKitDoubleBounce(
                          color: Color(0xffF2CA8A),
                          size: 50.0,
                        ),
                      ))
                    : (profileController.recentItem.data == null ||
                            contrl.recentItem.data!.items!.length == 0 ||
                            !Provider.of<AccountProvider>(context,
                                    listen: false)
                                .isLoggedIn)
                        ? Container(
                            alignment: Alignment.center,
                            child: Center(
                              child: Container(
                                alignment: Alignment.center,
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0)),
                                    ),
                                    foregroundColor: MaterialStateProperty.all(
                                      AppColors.navigationBarSelectedColor,
                                    ),
                                    backgroundColor: MaterialStateProperty.all(
                                        Color(0xffF2CA8A)),
                                    overlayColor: MaterialStateProperty
                                        .resolveWith<Color?>(
                                      (Set<MaterialState> states) {
                                        if (states
                                            .contains(MaterialState.pressed))
                                          return tiop.ontapColor; //<-- SEE HERE
                                        return null; // Defer to the widget's default.
                                      },
                                    ),
                                  ),
                                  onPressed: () {
                                    tiop.isChangeButtonColor.value = true;
                                    tiop.playSound();
                                    Future.delayed(Duration(milliseconds: 10))
                                        .then((value) {
                                      tiop.isChangeButtonColor.value = false;
                                      SystemChrome.setEnabledSystemUIMode(
                                          SystemUiMode.immersive);
                                      FABController.to.closeOpenedMenu();
                                      Get.to(() => TryItOnScreen());
                                    });
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 20, horizontal: 30),
                                    child: Text(
                                      "Scan products or colours",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12,
                                          letterSpacing: 1,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Align(
                                      alignment: Alignment.center,
                                      child: Container(
                                        width: 20,
                                        //margin: EdgeInsets.only(top: 30),
                                        child: Icon(
                                          Icons.arrow_back_ios_new,
                                          size: 15,
                                        ),
                                      )),
                                  Expanded(
                                      child: ListView.builder(
                                    physics: BouncingScrollPhysics(),
                                    scrollDirection: Axis.horizontal,
                                    itemCount:
                                        contrl.recentItem.data!.items!.length,
                                    itemBuilder: (context, index) {
                                      return Align(
                                        alignment: Alignment.center,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 40),
                                          child: GestureDetector(
                                            onTap: () {},
                                            child: Container(
                                              margin: EdgeInsets.only(left: 10),
                                              width: 96,
                                              child: Container(
                                                width: 96,
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      height: 96,
                                                      width: 96,
                                                      decoration: BoxDecoration(
                                                          color: HexColor(contrl
                                                              .recentItem
                                                              .data!
                                                              .items![index]
                                                              .scanColour
                                                              .toString()),
                                                          border: Border.all(
                                                              color:
                                                                  Colors.black,
                                                              width: 1.5)),
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Center(
                                                      child: Text(
                                                        'Hex' +
                                                            contrl
                                                                .recentItem
                                                                .data!
                                                                .items![index]
                                                                .scanColour
                                                                .toString(),
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bodyText1!
                                                            .copyWith(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 12,
                                                            ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  )),
                                  Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      width: 20,
                                      // margin: EdgeInsets.only(top: 30),
                                      // color:Colors.pink,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
              }),
            ],
          ),
        ),
        Container(
          height: 187,
          width: Get.width,
          decoration: BoxDecoration(
              color: Color(0x4D000000),
              image: DecorationImage(
                  image: AssetImage(
                      "assets/images/my_sofiqe_upgrade_background.png"),
                  fit: BoxFit.cover)),
          child: account.isLoggedIn
              ? Center(
                  child: Text(
                    'YOU ARE SOFIQE',
                    style: Theme.of(context).textTheme.headline2!.copyWith(
                          color: Colors.white,
                          fontSize: size.height * 0.028,
                          letterSpacing: 2,
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "Unlock Unlimited Sofiqe",
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                    GestureDetector(
                      onTap: () async {
                        // Get.to(() => PremiumSubscriptionScreen());
                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext c) {
                              return SignupScreen();
                            },
                          ),
                        );
                      },
                      child: Container(
                        height: 50,
                        width: Get.width * 0.6,
                        decoration: BoxDecoration(
                            color: Color(0xffF2CA8A),
                            borderRadius: BorderRadius.circular(50)),
                        alignment: Alignment.center,
                        child: Text(
                          "Subscribe",
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    )
                  ],
                ),
        ),
        displayTile(
            leading: "",
            title: "Currency Change",
            trailing: Obx(
              () => DropdownButton(
                // Initial Value
                value: CurrencyController.to.defaultCurrencyCode.value,

                // Down Arrow Icon
                icon: const Icon(Icons.keyboard_arrow_down),

                // Array list of items
                items: (CurrencyController
                            .to.currencyModelNew.value.availableCurrencyCodes ??
                        [])
                    .map((String items) {
                  return DropdownMenuItem(
                    value: items,
                    child: Text(items),
                  );
                }).toList(),
                // After selecting the desired option,it will
                // change button value to selected value
                onChanged: (String? newValue) {
                  FABController.to.closeOpenedMenu();

                  double? newExchangeRate = 0.0;
                  List list = (CurrencyController
                          .to.currencyModelNew.value.exchangeRates ??
                      []);
                  for (int i = 0; i < list.length; i++) {
                    ExchangeRates ex = list[i];
                    if (ex.currencyTo == newValue) {
                      newExchangeRate = ex.rate;
                    }
                  }
                  CurrencyController.to.defaultCurrencyCode.value =
                      newValue ?? "USD";
                  CurrencyController.to.exchangeRateinDouble.value =
                      newExchangeRate ?? 0;
                  sfStoreInSharedPrefData(
                      fieldName: "currency-code",
                      value: newValue ?? "USD",
                      type: PreferencesDataType.STRING);
                  sfStoreInSharedPrefData(
                      fieldName: "exchange-rate",
                      value: newExchangeRate ?? 0,
                      type: PreferencesDataType.DOUBLE);
                  cPrint(newValue.toString() + "New Currency");
                  cPrint(sfQueryForSharedPrefData(
                      fieldName: "currency-code",
                      type: PreferencesDataType.STRING));
                  cPrint(CurrencyController.to.defaultCurrencyCode.value);
                  cPrint("New Exchange Rate");
                  cPrint(CurrencyController.to.exchangeRateinDouble.value);
                },
              ),
            ),
            handler: () {
              FABController.to.closeOpenedMenu();
            }),
        Divider(),
        displayTile(
            leading: "",
            title: "Notifications",
            trailing: Obx(() {
              return Switch(
                  value: profileController.notificationsOn.value,
                  activeColor: Colors.white,
                  activeTrackColor: Colors.green,
                  inactiveThumbColor: Colors.white,
                  inactiveTrackColor: Colors.red,
                  onChanged: (val) {
                    FABController.to.closeOpenedMenu();
                    profileController.updateNotifications(val);
                  });
            }),
            handler: () {
              FABController.to.closeOpenedMenu();
            }),
        Divider(),
        displayTile(
            leading: "",
            title: "Sound",
            trailing: Obx(() {
              return Switch(
                  value: tiop.isplaying.value,
                  activeColor: Colors.white,
                  activeTrackColor: Colors.green,
                  inactiveThumbColor: Colors.white,
                  inactiveTrackColor: Colors.red,
                  onChanged: (val) {
                    FABController.to.closeOpenedMenu();
                    sound = val;
                    tiop.isplaying.value = val;
                    cata.isplaying.value = val;
                  });
            }),
            handler: () {
              FABController.to.closeOpenedMenu();
            }),
        Divider(),
        displayTile(
            leading: "",
            title: "Privacy policy",
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            handler: () {
              FABController.to.closeOpenedMenu();

              Get.to(() => PrivacyPolicyScreen(
                    isTerm: false,
                    isReturnPolicy: false,
                  ));
            }),
        Divider(),
        displayTile(
            leading: "",
            title: "Terms and conditions",
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            handler: () {
              FABController.to.closeOpenedMenu();

              Get.to(() => PrivacyPolicyScreen(
                    isTerm: true,
                    isReturnPolicy: false,
                  ));
            }),
        Divider(),
        displayTile(
            leading: "",
            title: "Return Policy",
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 15,
            ),
            handler: () {
              FABController.to.closeOpenedMenu();

              Get.to(() => PrivacyPolicyScreen(
                    isTerm: true,
                    isReturnPolicy: true,
                  ));
            }),
        account.isLoggedIn
            ? Column(
                children: [
                  Divider(),
                  displayTile(
                      leading: "",
                      title: "Log Out",
                      trailing: Icon(
                        Icons.arrow_forward_ios,
                        size: 15,
                      ),
                      handler: () {
                        FABController.to.closeOpenedMenu();

                        Get.defaultDialog<bool>(
                            title: '',
                            titleStyle: const TextStyle(fontSize: 1),
                            radius: 10,
                            titlePadding: EdgeInsets.zero,
                            contentPadding: EdgeInsets.only(
                                top: 17, left: 16, right: 16, bottom: 5),
                            content: Column(
                              children: [
                                Text(
                                  'sofiqe',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline1!
                                      .copyWith(
                                          color: Colors.black,
                                          fontSize: size.height * 0.04,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 0.6),
                                ),
                                SizedBox(
                                  height: 14,
                                ),
                                Text(
                                  'Are You Sure?',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 13,
                                      letterSpacing: 1,
                                      fontWeight: FontWeight.w500),
                                ),
                                SizedBox(
                                  height: 7,
                                ),
                                Text(
                                  "Would you like to logout from the app?",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      letterSpacing: 1,
                                      fontWeight: FontWeight.w400),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  // mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Expanded(
                                      child: InkWell(
                                        onTap: () {
                                          Navigator.pop(context);
                                        },
                                        borderRadius: BorderRadius.circular(20),
                                        // splashColor: kCustomLightGreenColor.withOpacity(.3),
                                        child: Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          child: Center(
                                            child: Text(
                                              'NO',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                  letterSpacing: 1,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Expanded(
                                      child: InkWell(
                                        onTap: () async {
                                          await sfRemoveAddressInformation();
                                          cPrint("ORDER  == Step 5");

                                          await MsProfileController.instance
                                              .resetControllers();
                                          cPrint("ORDER  == Step 6");
                                          account.logout();
                                          Navigator.pop(context);
                                        },
                                        borderRadius: BorderRadius.circular(20),
                                        // splashColor: kCustomLightGreenColor.withOpacity(.3),
                                        child: Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          decoration: BoxDecoration(
                                            color: Colors.black,
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          child: Center(
                                            child: Text(
                                              'YES',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  letterSpacing: 1,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ));
                      }),
                ],
              )
            : Column(
                children: [
                  Divider(),
                  displayTile(
                      leading: "",
                      title: "Sign In",
                      trailing: Icon(
                        Icons.arrow_forward_ios,
                        size: 15,
                      ),
                      handler: () async {
                        FABController.to.closeOpenedMenu();

                        profileController.screen.value = 0;
                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext _) {
                              return LoginScreen();
                            },
                          ),
                        );
                      }),
                  Divider(),
                  displayTile(
                      leading: "",
                      title: "Sign Up",
                      trailing: Icon(
                        Icons.arrow_forward_ios,
                        size: 15,
                      ),
                      handler: () async {
                        profileController.screen.value = 0;
                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext _) {
                              return SignupScreen();
                            },
                          ),
                        );
                      }),
                ],
              ),
        Divider(),
        Container(
            padding: EdgeInsets.only(top: 20, bottom: 30, left: 55),
            child: Row(
              children: [
                Text(
                  "Version: ${account.appVersion ?? ''}",
                  textAlign: TextAlign.start,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: Colors.black,
                        fontSize: 13,
                      ),
                ),
              ],
            )),
      ],
    ));
  }

  void handleTimeout() {
    // callback function
    // Do some work.

    gtm.push(
      'press_naturalme',
    );
    Navigator.pop(dcontext!, true);
    // debugPrint('==== value try on is ${makeOverProvider.tryitOn.value} ======');
    FABController.to.closeOpenedMenu();

    if (!Provider.of<AccountProvider>(context, listen: false).isLoggedIn) {
      pp.goToPage(Pages.MAKEOVER);
    } else if (!makeOverProvider.tryitOn.value) {
      pp.goToPage(Pages.MAKEOVER);
    } else {
      profileController.screen.value = 3;
    }
  }

  BuildContext? dcontext;
  showLoaderDialog(BuildContext context) {
    scheduleTimeout(5 * 1000);
    final alert = AlertDialog(
      title: const Text('We need to know more'),
      content: const Text(
          'To have access to this area please do the makeover analysis first. Redirecting you...'),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        dcontext = context;
        return alert;
      },
    );
  }

  Widget displayTile(
      {String? leading,
      String? title,
      Widget? trailing,
      required Function handler,
      String? leadingSvg}) {
    cPrint("assets/images/$leading");
    return Padding(
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      child: ListTile(
        onTap: () => handler(),
        leading: (leading == null || leading.isEmpty)
            ? SizedBox()
            : leadingSvg == null
                ? Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                        // color: Colors.black
                        image: DecorationImage(
                            fit: BoxFit.contain,
                            image: AssetImage("assets/images/$leading"))),
                  )
                : Container(
                    height: 25,
                    width: 28,
                    child: SvgPicture.asset(
                      "assets/images/$leadingSvg",
                      color: Color(0xffF2CA8A),
                      fit: BoxFit.contain,
                    )),
        title: Transform.translate(
          offset: Offset(-25, 0),
          child: Text(
            '$title',
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: Colors.black,
                  fontSize: 13,
                ),
          ),
        ),
        trailing: trailing,
      ),
    );
  }
}

class RecentScan {
  String? image;
  String? title;
  String? type;

  RecentScan({this.image, this.title, this.type});
}
