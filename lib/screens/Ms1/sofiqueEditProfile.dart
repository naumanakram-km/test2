// ignore_for_file: deprecated_member_use, unused_local_variable

import 'dart:convert';

import 'package:country_code_picker/country_code.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:provider/provider.dart';
import 'package:sofiqe/controller/msProfileController.dart';
import 'package:sofiqe/provider/account_provider.dart';
import 'package:sofiqe/screens/Ms1/editProfileAppbar.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../../model/AddressClass.dart';
import '../../model/customerModel2.dart' as newModel;
import '../../utils/api/shipping_address_api.dart';
import '../../utils/api/user_account_api.dart';
import '../../utils/constants/api_tokens.dart';
import '../../utils/constants/route_names.dart';
import '../../utils/states/user_account_data.dart';
import '../../widgets/account/Models/country.dart';
import '../../widgets/account/country_dropdown.dart';
import '../../widgets/account/phone_number_field.dart';
import '../../widgets/account/region_dropdown.dart';
import '../../widgets/catalog/payment/delivery_details_page.dart';
import '../../widgets/field_loader.dart';

class SofiqueEditProfile extends StatefulWidget {
  const SofiqueEditProfile({Key? key}) : super(key: key);

  @override
  _SofiqueEditProfileState createState() => _SofiqueEditProfileState();
}

class _SofiqueEditProfileState extends State<SofiqueEditProfile> {
  MsProfileController _ = Get.find<MsProfileController>();

  final _formKey = GlobalKey<FormState>();
  bool isUpdate = true;
  String billingInformation = "on";
  @override
  initState() {
    super.initState();
    int id = Provider.of<AccountProvider>(context, listen: false).customerId;
    loadCountries();
    if (!_.isShiping.value) {
      billingInformation = "Off";
    } else {
      billingInformation = "On";
    }
    _.getUserProfile();
  }

  Country? selectedCountry;
  AvailableRegions? selectedRegion;
  Country? selectedCountryBilling;
  AvailableRegions? selectedRegionBilling;
  List<Country> listCountries = [];
  List<AvailableRegions> listAvailableRegions = [];
  List<AvailableRegions> listAvailableRegionsBilling = [];
  bool flagLoadingCountries = true;
  loadCountries() async {
    List<Country> listCountriestemp = [];

    http.Response res = await sfAPIFetchCountryDetails();
    if (res.statusCode == 200) {
      List data = jsonDecode(res.body);
      for (int i = 0; i < data.length; i++) {
        listCountriestemp.add(new Country.fromJson(data[i]));
      }
      Country? selectedCountryTemp;
      AvailableRegions? selectedRegionsTemp;
      List<AvailableRegions> listTemp = [];

      Country? selectedCountryTempbilling;
      AvailableRegions? selectedRegionsTempbilling;
      List<AvailableRegions> listTempbilling = [];
      if (_.billingCountryCodeController.text.isNotEmpty) {
        for (int i = 0; i < listCountriestemp.length; i++) {
          if (_.billingCountryCodeController.text == listCountriestemp[i].id) {
            selectedCountryTempbilling = listCountriestemp[i];
            listTempbilling = listCountriestemp[i].availableRegions ?? [];
          }
        }
        for (int i = 0; i < listTempbilling.length; i++) {
          if (_.billingIdodeController.text == listTempbilling[i].id) {
            selectedRegionsTempbilling = listTempbilling[i];
          }
        }
      }
      if (_.countryCodeController.text.isNotEmpty) {
        for (int i = 0; i < listCountriestemp.length; i++) {
          if (_.countryCodeController.text == listCountriestemp[i].id) {
            selectedCountryTemp = listCountriestemp[i];
            listTemp = listCountriestemp[i].availableRegions ?? [];
          }
        }
        for (int i = 0; i < listTemp.length; i++) {
          if (_.regionIdController.text == listTemp[i].id) {
            selectedRegionsTemp = listTemp[i];
          }
        }
      }

      cPrint("List of countries");
      cPrint(listCountriestemp);
      cPrint(selectedRegion);
      cPrint(_.regionIdController.text);
      cPrint(listTemp);

      setState(() {
        selectedCountry = selectedCountryTemp;
        selectedRegion = selectedRegionsTemp;
        selectedCountryBilling = selectedCountryTempbilling;
        selectedRegionBilling = selectedRegionsTempbilling;
        listAvailableRegions = listTemp;
        listAvailableRegionsBilling = listTempbilling;
        listCountries = listCountriestemp;
        flagLoadingCountries = false;
      });
    } else {
      setState(() {
        flagLoadingCountries = false;
      });
    }
  }

  FocusNode focusNodeFirstName = FocusNode();
  FocusNode focusNodeLastName = FocusNode();
  FocusNode focusNodeEmail = FocusNode();
  FocusNode focusNodePhone = FocusNode();
  FocusNode focusNodeStreet = FocusNode();
  FocusNode focusNodeCity = FocusNode();
  FocusNode focusNodeZipCode = FocusNode();
  FocusNode focusNodeCountry = FocusNode();

  bool isUpdating = false;
  bool isBillingRegion = false;
  bool isBillingCountry = false;
  @override
  Widget build(BuildContext context) {
    AccountProvider ap = Provider.of<AccountProvider>(context);
    return SafeArea(
      child: Scaffold(
        body: SizedBox(
          height: Get.height,
          width: Get.width,
          child: ModalProgressHUD(
            inAsyncCall: isUpdating,
            progressIndicator: SpinKitDoubleBounce(
              color: Colors.black,
              size: 50.0,
            ),
            child: Column(
              children: [
                EditProfileAppbar(),
                Expanded(
                  child: SingleChildScrollView(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'THANKS FOR BEING A SOFIQE',
                            style: TextStyle(fontSize: 20, color: Colors.black),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Here you can amend your profile etc.',
                            style: TextStyle(fontSize: 10, color: Colors.black),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          coverWidgetWithPadding(
                            child: Row(
                              children: const [
                                Text(
                                  'I’M A',
                                  style: TextStyle(
                                      fontSize: 9, color: Colors.black),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                          coverWidgetWithPadding(
                            child: SizedBox(
                              height: 85,
                              width: Get.width,
                              // color: Colors.yellow,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  displayGenderContainer(
                                      image: "woman.png", title: 'FEMALE'),
                                  displayGenderContainer(
                                      image: "male.png", title: 'MALE'),
                                  displayGenderContainer(
                                      image: "heart-3.png",
                                      title: 'GENDERLESS'),
                                  displayGenderContainer(
                                      image: "heart-2.png", title: 'LBGT'),
                                ],
                              ),
                            ),
                          ),
                          displayColorDivider(),
                          displayTextFieldContainer(
                              title: 'MY FIRST NAME IS',
                              controller: _.firstNameController,
                              focusNode: focusNodeFirstName,
                              onFieldSubmitted: (p0) {
                                focusNodeLastName.requestFocus();
                              }),
                          Container(
                            height: 5,
                            color: Color(0xffF4F2F0),
                          ),
                          displayTextFieldContainer(
                              title: 'MY LAST NAME IS',
                              controller: _.lastNameController,
                              focusNode: focusNodeLastName,
                              onFieldSubmitted: (p0) {
                                focusNodeEmail.requestFocus();
                              }),
                          Container(
                            height: 5,
                            color: Color(0xffF4F2F0),
                          ),
                          displayTextFieldContainer(
                              title: 'MY EMAIL IS',
                              controller: _.emailController,
                              hint: "Email",
                              focusNode: focusNodeEmail,
                              onFieldSubmitted: (p0) {
                                focusNodePhone.requestFocus();
                              }),

                          coverWidgetWithPadding(
                              child: Text(
                            'SHIPPING ADDRESS',
                            style: TextStyle(
                                fontSize: 11, fontWeight: FontWeight.bold),
                          )),
                          // all individual field is copy shipping address to billing address in real time
                          displayTextFieldContainer(
                              onChange: (p0) {
                                _.billingStreetController.text =
                                    _.streetController.value.text;
                                setState(() {});
                              },
                              title: 'STREET',
                              controller: _.streetController,
                              hint: "",
                              focusNode: focusNodeStreet,
                              onFieldSubmitted: (p0) {
                                focusNodeCity.requestFocus();
                              }),
                          displayColorDivider(),
                          displayTextFieldContainer(
                              onChange: (p0) {
                                _.billingCityController.text =
                                    _.cityController.value.text;
                                setState(() {});
                              },
                              title: 'CITY',
                              controller: _.cityController,
                              hint: "",
                              focusNode: focusNodeCity,
                              onFieldSubmitted: (p0) {
                                focusNodeCountry.requestFocus();
                              }),
                          displayColorDivider(),
                          displayTextFieldPhoneContainer(
                              onChange: (p0) {
                                _.billingPhoneController.text =
                                    _.phoneController.value.text;
                                setState(() {});
                              },
                              title: 'PHONE',
                              controller: _.phoneController,
                              prefix: CountryCodeDropDown(
                                byDefaultSelection:
                                    _.phoneNumberCodeController.text.toString(),
                                callback: (CountryCode code) {
                                  _.phoneNumberCodeController.text =
                                      code.dialCode as String;
                                  _.billingPhoneNumberCodeController.text =
                                      _.phoneNumberCodeController.value.text;
                                  setState(() {});
                                  cPrint('picked country code is $code');
                                },
                                init: (CountryCode? code) {
                                  _.phoneNumberCodeController.text =
                                      code!.dialCode as String;
                                  cPrint('initial country code is $code');
                                },
                              ),
                              hint: '',
                              textInputType: TextInputType.phone),
                          displayColorDivider(),
                          displayTextFieldContainer(
                              onChange: (p0) {
                                _.billingPostZipController.text =
                                    _.postCodeController.value.text;
                              },
                              title: 'POST/ZIP CODE',
                              textInputType: TextInputType.text,
                              controller: _.postCodeController,
                              focusNode: focusNodeZipCode,
                              onFieldSubmitted: (p0) {
                                focusNodeLastName.unfocus();
                              }),
                          Container(
                            height: 5,
                            color: Color(0xffF4F2F0),
                          ),
                          if (flagLoadingCountries)
                            FieldLoader()
                          else
                            Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: 0, horizontal: 20),
                                child: CountryDropDown(listCountries,
                                    selectedItem: selectedCountry,
                                    onChanged: (country) {
                                  _.countryCodeController.text =
                                      country!.twoLetterAbbreviation ?? "";
                                  _.countryController.text =
                                      country.fullNameEnglish ?? "";
                                  _.regionCodeController.text = "";
                                  setState(() {
                                    selectedCountry = country;
                                    if (!isBillingRegion) {
                                      _.billingCountryCodeController.text =
                                          country.twoLetterAbbreviation ?? "";
                                      selectedCountryBilling = country;
                                      _.billingCountryController.text =
                                          country.fullNameEnglish ?? "";
                                      _.billingRegionCodeController.text = "";
                                      _.billingIdodeController.text = "";

                                      listAvailableRegionsBilling =
                                          country.availableRegions ?? [];
                                      selectedRegionBilling = null;
                                    }
                                    selectedRegion = null;
                                    listAvailableRegions =
                                        country.availableRegions ?? [];
                                    _.regionCodeController.text = "";
                                    _.regionIdController.text = "";
                                  });
                                })),
                          if (listAvailableRegions.isNotEmpty)
                            displayColorDivider(),
                          if (listAvailableRegions.isNotEmpty)
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 0, horizontal: 20),
                              child: AvailableRegionsDropDown(
                                listAvailableRegions,
                                selectedItem: selectedRegion,
                                onChanged: (ar) {
                                  selectedRegion = ar;
                                  _.regionCodeController.text = ar!.code ?? "";
                                  _.regionIdController.text = ar.id ?? "";
                                  if (!isBillingRegion || _.isShiping.value) {
                                    _.billingRegionCodeController.text =
                                        ar.code ?? "";
                                    _.billingIdodeController.text = ar.id ?? "";
                                    setState(() {
                                      selectedRegionBilling = ar;
                                    });
                                  }
                                },
                              ),
                            ),

                          displayColorDivider(),
                          displayColorDivider(),
                          coverWidgetWithPadding(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      height: 25,
                                      color: Color(0xffF4F2F0),
                                      alignment: Alignment.centerLeft,
                                      child: coverWidgetWithPadding(
                                          child: Text(
                                        'BILLING ADDRESS',
                                        style: TextStyle(
                                            fontSize: 11,
                                            fontWeight: FontWeight.bold),
                                      )),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Same as shipping address",
                                      style: TextStyle(
                                          fontSize: 11, color: Colors.black),
                                    ),
                                    Transform.scale(
                                      scale: 0.8,
                                      child: CupertinoSwitch(
                                          value: _.isShiping.value,
                                          activeColor: Colors.green,
                                          trackColor: Colors.red,
                                          thumbColor: Colors.white,
                                          onChanged: (val) {
                                            _.isShiping.value = val;
                                            setState(() {});
                                            // it's copy value shipping address to billing address when (same as shipping address button) is green
                                            if (!_.isShiping.value) {
                                              billingInformation = "Off";
                                            } else {
                                              _.billingNameController.text =
                                                  _.nameController.value.text;
                                              _.billingCountryController.text =
                                                  _.countryController.value
                                                      .text;
                                              _.billingCountryCodeController
                                                      .text =
                                                  _.countryCodeController.value
                                                      .text;
                                              _.billingStreetController.text =
                                                  _.streetController.value.text;
                                              _.billingPostZipController.text =
                                                  _.postCodeController.value
                                                      .text;
                                              _.billingCityController.text =
                                                  _.cityController.value.text;
                                              _.billingPhoneController.text =
                                                  _.phoneController.value.text;
                                              _.billingPhoneNumberCodeController
                                                      .text =
                                                  _.phoneNumberCodeController
                                                      .value.text;

                                              // selectedCountryBilling =
                                              //     selectedCountry;
                                              // selectedRegionBilling =
                                              //     selectedRegion;
                                              billingInformation = "On";
                                            }
                                          }),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          if (_.isShiping.value)
                            Container()
                          else
                            Column(
                              children: [
                                displayTextFieldContainer(
                                  title: 'STREET',
                                  controller: _.billingStreetController,
                                  hint: "",
                                ),
                                displayColorDivider(),
                                displayTextFieldContainer(
                                    title: 'CITY',
                                    controller: _.billingCityController,
                                    hint: ""),
                                displayColorDivider(),
                                displayTextFieldPhoneContainer(
                                    title: 'PHONE',
                                    controller: _.billingPhoneController,
                                    prefix: CountryCodeDropDown(
                                      byDefaultSelection: _
                                          .billingPhoneNumberCodeController.text
                                          .toString(),
                                      callback: (CountryCode code) {
                                        _.billingPhoneNumberCodeController
                                            .text = code.dialCode as String;
                                        cPrint('picked country code is $code');
                                      },
                                      init: (CountryCode? code) {
                                        _.billingPhoneNumberCodeController
                                            .text = code!.dialCode as String;
                                        cPrint('initial country code is $code');
                                      },
                                    ),
                                    hint: '',
                                    textInputType: TextInputType.phone),
                                displayColorDivider(),
                                displayTextFieldContainer(
                                    title: 'POST/ZIP CODE',
                                    textInputType: TextInputType.text,
                                    controller: _.billingPostZipController),
                                displayColorDivider(),
                                if (flagLoadingCountries)
                                  FieldLoader()
                                else
                                  Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 0, horizontal: 20),
                                      child: CountryDropDown(listCountries,
                                          selectedItem: selectedCountryBilling,
                                          onChanged: (country) {
                                        _.billingCountryCodeController.text =
                                            country!.twoLetterAbbreviation ??
                                                "";
                                        _.billingCountryController.text =
                                            country.fullNameEnglish ?? "";
                                        _.billingRegionCodeController.text = "";
                                        _.billingIdodeController.text = "";

                                        setState(() {
                                          isBillingCountry = true;
                                          selectedRegionBilling = null;
                                          listAvailableRegionsBilling =
                                              country.availableRegions ?? [];
                                        });
                                      })),
                                if (listAvailableRegionsBilling.isNotEmpty)
                                  displayColorDivider(),
                                if (listAvailableRegionsBilling.isNotEmpty)
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 0, horizontal: 20),
                                    child: AvailableRegionsDropDown(
                                      listAvailableRegionsBilling,
                                      selectedItem: selectedRegionBilling,
                                      onChanged: (ar) {
                                        setState(() {
                                          isBillingRegion = true;
                                        });

                                        _.billingRegionCodeController.text =
                                            ar!.code ?? "";
                                        _.billingIdodeController.text =
                                            ar.id ?? "";
                                      },
                                    ),
                                  ),
                                displayColorDivider(),
                                displayColorDivider(),
                              ],
                            ),

                          // displayColorDivider(),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: GestureDetector(
                    onTap: () async {
                      setState(() {});
                      final isValid = _formKey.currentState?.validate();
                      if (!checkEmpty()) {
                        cPrint("true");
                        //
                        if (!_.isShiping.value &&
                            _.billingStreetController.text.isEmpty) {
                          Get.showSnackbar(GetSnackBar(
                            message:
                                'Please add billing address as well or swtich the toggle',
                            duration: Duration(seconds: 2),
                          ));
                        } else {
                          setState(() {
                            isUpdating = true;
                          });
                          if (await _.updateUserProfile()) {
                            cPrint("Success Login");
                            ap.getUserDetails(
                                await APITokens.customerSavedToken);
                            _.getUserProfile();
                            setState(() {
                              isUpdating = false;
                            });
                          } else {
                            setState(() {
                              isUpdating = false;
                            });
                          }
                        }
                      } else {
                        Get.showSnackbar(GetSnackBar(
                          message: 'Please fill out all fields',
                          duration: Duration(seconds: 2),
                        ));
                      }
                    },
                    child: InkWell(
                      onTap: () async {
                        // if (listAvailableRegions.isNotEmpty)

                        if (!checkisEmpty()) {
                          if (_.firstNameController.value.text != "" &&
                              _.lastNameController.value.text != "" &&
                              _.emailController.value.text != "" &&
                              _.phoneController.value.text != "" &&
                              _.streetController.value.text != "" &&
                              _.cityController.value.text != "" &&
                              _.postCodeController.value.text != "" &&
                              _.countryController.value.text != "") {
                            if (listAvailableRegions.isNotEmpty) {
                              if (_.regionCodeController.value.text != "") {
                                saveData();
                              } else {
                                Get.showSnackbar(
                                  GetSnackBar(
                                    message: 'Please fill all requried fields',
                                    duration: Duration(seconds: 2),
                                    isDismissible: true,
                                  ),
                                );
                              }
                            } else {
                              saveData();
                            }
                          } else {
                            Get.showSnackbar(
                              GetSnackBar(
                                message: 'Please fill all requried fields',
                                duration: Duration(seconds: 2),
                                isDismissible: true,
                              ),
                            );
                          }
                        } else {
                          Get.showSnackbar(
                            GetSnackBar(
                              message: 'Please fill all requried fields',
                              duration: Duration(seconds: 2),
                              isDismissible: true,
                            ),
                          );
                        }
                      },
                      child: Container(
                        height: 45,
                        width: Get.width,
                        decoration: BoxDecoration(
                            color: Color(0xffF2CA8A),
                            borderRadius: BorderRadius.circular(50)),
                        alignment: Alignment.center,
                        child: Text(
                          "SAVE",
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  checkisEmpty() {
    cPrint('==== this check is empty method is called =======');

    if (_.countryController.value.text.isEmpty ||
        _.streetController.value.text.isEmpty ||
        _.nameController.value.text.isEmpty ||
        _.phoneController.value.text.isEmpty ||
        _.phoneNumberCodeController.value.text.isEmpty ||
        _.postCodeController.value.text.isEmpty ||
        _.cityController.value.text.isEmpty) {
      cPrint("Here 1");
      return true;
    } else {
      if (!_.isShiping.value) {
        if (_.billingNameController.value.text.isEmpty ||
            _.billingCountryController.value.text.isEmpty ||
            _.billingStreetController.value.text.isEmpty ||
            _.billingPostZipController.value.text.isEmpty ||
            _.billingPhoneController.value.text.isEmpty ||
            _.billingPhoneNumberCodeController.value.text.isEmpty ||
            _.billingCountryCodeController.value.text.isEmpty) {
          cPrint("Here 2");
          return true;
        } else {
          if (listAvailableRegionsBilling.isNotEmpty) {
            if (_.billingRegionCodeController.value.text.isEmpty) {
              cPrint("Here 3");
              return true;
            }
          }
          cPrint("Here 4");
          return false;
        }
      } else {
        if (listAvailableRegions.isNotEmpty) {
          if (_.regionCodeController.value.text.isEmpty) {
            cPrint("Here 5");
            return true;
          }
        }
        cPrint("Here 6");
        return false;
      }
    }
  }

  saveData() async {
    FocusManager.instance.primaryFocus?.unfocus();
    showLoaderDialog(context);
    Region r = Region(
        region: _.countryController.value.text,
        regionCode: '${_.regionCodeController.value.text}',
        regionId: int.parse(_.regionIdController.value.text.isEmpty
            ? "0"
            : _.regionIdController.value.text));
    Region r1 = Region(
        region: _.billingCountryController.value.text.trim().isEmpty
            ? _.countryController.value.text
            : _.billingCountryController.value.text,
        regionCode:
            '${_.billingCountryCodeController.value.text.trim().isEmpty ? _.regionCodeController.value.text : _.billingCountryCodeController.value.text}',
        regionId: int.parse(_.billingIdodeController.value.text.isEmpty
            ? "0"
            : _.billingIdodeController.value.text));
    Addresses1 shipping = Addresses1(
        city: _.cityController.text,
        countryId: _.countryCodeController.value.text,
        defaultBilling: true,
        defaultShipping: true,
        firstname: '${_.firstNameController.text}',
        lastname: _.lastNameController.text,
        postcode: _.postCodeController.text,
        region: r,
        street: ['${_.streetController.text}'],
        telephone: _.phoneController.text);

    Addresses1 biiling = Addresses1(
        city: _.billingCityController.text,
        countryId: _.countryCodeController.value.text,
        defaultBilling: true,
        defaultShipping: true,
        firstname: '${_.firstNameController.text}',
        lastname: '${_.lastNameController.text}',
        postcode: _.billingPostZipController.text.trim().isEmpty
            ? _.postCodeController.text
            : _.billingPostZipController.text,
        region: r1,
        street: [
          '${_.billingStreetController.text.trim().isEmpty ? _.streetController.text : _.billingStreetController.value.text}'
        ],
        telephone: _.billingPhoneController.text.trim().isEmpty
            ? _.phoneController.text
            : _.billingPhoneController.text);

    newModel.CustomerModel2 c = newModel.CustomerModel2(
        firstname: '${_.firstNameController.text}',
        lastname: '${_.lastNameController.text}',
        addresses: [shipping, biiling],
        email: "${_.emailController.value.text}",
        gender: _.selectedGender == "FEMALE"
            ? 1
            : _.selectedGender == "MALE"
                ? 0
                : _.selectedGender == "GENDERLESS"
                    ? 2
                    : _.selectedGender == "LBGT"
                        ? 3
                        : 0,
        customAttributes: [
          {
            "attribute_code": "country_code",
            "value": '${_.phoneNumberCodeController.text}'
          },
          {
            "attribute_code": "phone_number",
            "value": '${_.phoneController.text}'
          }
        ],
        websiteId: 1);

    cPrint(NewAddressClass(customer1: c).toJson());
    MsProfileController.to.profileName.value =
        '${_.firstNameController.text} ${_.lastNameController.text}';
    Provider.of<AccountProvider>(context, listen: false).user?.firstName =
        _.firstNameController.text;

    final response = await sfAPIUpdateUserDetails(
        await APITokens.customerSavedToken,
        NewAddressClass(customer1: c).toJson());
    if (response != null &&
        response.id != null &&
        response.email != null &&
        (response.message?.trim().isEmpty ?? true) == true) {
      int id = Provider.of<AccountProvider>(context, listen: false).customerId;

      await _.getUserProfile();

      Navigator.pop(context);
      setState(() {});

      Get.showSnackbar(
        GetSnackBar(
          message: 'Profile details has been updated successfully',
          duration: Duration(seconds: 2),
          isDismissible: true,
        ),
      );
    } else {
      final isEmailExistError = response?.message != null && response!.message!.contains('A customer with the same email address already exists in an associated website.');
      Navigator.pop(context);
      final size = MediaQuery.of(context).size;
      Get.defaultDialog<bool>(
          title: '',
          titleStyle: const TextStyle(fontSize: 1),
          radius: 10,
          barrierDismissible: false,
          titlePadding: EdgeInsets.zero,
          contentPadding:
              EdgeInsets.only(top: 17, left: 16, right: 16, bottom: 5),
          content: Column(
            children: [
              Text(
                'sofiqe',
                style: Theme.of(context).textTheme.headline1!.copyWith(
                    color: Colors.black,
                    fontSize: size.height * 0.04,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 0.6),
              ),
              SizedBox(
                height: 14,
              ),
              Text(
                'Failed to save',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    letterSpacing: 1,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 7,
              ),
              Text(
                isEmailExistError ? "This email already exists. Would you like to logout?"
                    : 'There was an error saving your information.',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 12,
                    letterSpacing: 1,
                    fontWeight: FontWeight.w400),
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                children: [
                  Spacer(flex: 1,),
                  Flexible(
                    flex: 2,
                    child: InkWell(
                      onTap: () async {
                        Navigator.pop(context);
                      },
                      borderRadius: BorderRadius.circular(20),
                      // splashColor: kCustomLightGreenColor.withOpacity(.3),
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Center(
                          child: Text(
                            isEmailExistError ? "No" : 'OK',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                                letterSpacing: 1,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Spacer(flex: 1,),
                  if(isEmailExistError)
                  Flexible(
                    flex: 2,
                    child: InkWell(
                      onTap: () async {
                        AccountProvider account =
                        Provider.of<AccountProvider>(context, listen: false);
                        await sfRemoveAddressInformation();
                        cPrint("ORDER  == Step 5");

                        await MsProfileController.instance
                            .resetControllers();
                        cPrint("ORDER  == Step 6");
                        account.logout();
                        Navigator.popUntil(context, (route) => route.settings.name == RouteNames.homeScreen);
                        },
                      borderRadius: BorderRadius.circular(20),
                      // splashColor: kCustomLightGreenColor.withOpacity(.3),
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Center(
                          child: Text(
                            'Yes',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                                letterSpacing: 1,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    ),
                  ),
                  if(isEmailExistError)
                  Spacer(flex: 1,),
                ],
              ),
            ],
          ));
    }
  }

  Widget coverWidgetWithPadding({Widget? child}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: child,
    );
  }

  Widget displayGenderContainer(
      {required String image, required String title}) {
    return InkWell(
      onTap: () {
        _.selectedGender = title;
        setState(() {});
      },
      child: Container(
        height: 65,
        width: 58,
        decoration: BoxDecoration(
          border: (title == _.selectedGender)
              ? Border(bottom: BorderSide(color: Color(0xffF2CA8A)))
              : null,
          color: (title == _.selectedGender) ? Color(0xffF4F2F0) : Colors.white,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              'assets/images/$image',
              height: 28,
              width: 15,
            ),
            Text(
              title,
              style: TextStyle(fontSize: 8, color: Colors.black),
            )
          ],
        ),
      ),
    );
  }

  bool checkEmpty() {
    if (_.firstNameController.value.text.isEmpty ||
        _.lastNameController.value.text.isEmpty ||
        _.emailController.value.text.isEmpty ||
        _.countryController.value.text.isEmpty ||
        _.streetController.value.text.isEmpty ||
        _.phoneController.value.text.isEmpty ||
        _.selectedGender == "") {
      return true;
    } else {
      return false;
    }
  }

  displayTextFieldContainer({
    String? title,
    TextEditingController? controller,
    Color? backgroundColor,
    String? prefix,
    TextInputType? textInputType,
    void Function(String)? onFieldSubmitted,
    FocusNode? focusNode,
    String? hint,
    void Function(String)? onChange,
  }) {
    return Container(
      height: 68,
      color: backgroundColor,
      padding: EdgeInsets.only(top: 5),
      child: coverWidgetWithPadding(
          child: Column(
        children: [
          Row(
            children: [
              Row(
                children: [
                  Text(
                    title!,
                    style: TextStyle(fontSize: 11, color: Colors.black),
                  ),
                  Text(
                    ' *',
                    style: TextStyle(fontSize: 11, color: Colors.red),
                  ),
                ],
              ),
            ],
          ),
          TextFormField(
            validator: (str) {
              if (str == '' || str == null) {
                isUpdate = false;
              }
              return null;
            },
            keyboardType: textInputType ?? TextInputType.text,
            controller: controller,
            onFieldSubmitted: onFieldSubmitted,
            focusNode: focusNode,
            decoration: InputDecoration(
                hintText: hint, prefixText: prefix, border: InputBorder.none),
            onChanged: onChange,
          ),
        ],
      )),
    );
  }

  displayTextFieldPhoneContainer(
      {String? title,
      TextEditingController? controller,
      Color? backgroundColor,
      Widget? prefix,
      TextInputType? textInputType,
      void Function(String)? onFieldSubmitted,
      void Function(String)? onChange,
      FocusNode? focusNode,
      String? hint}) {
    return Container(
      height: 68,
      color: backgroundColor,
      padding: EdgeInsets.only(top: 5),
      child: coverWidgetWithPadding(
          child: Column(
        children: [
          Row(
            children: [
              Row(
                children: [
                  Text(
                    title!,
                    style: TextStyle(fontSize: 11, color: Colors.black),
                  ),
                  Text(
                    ' *',
                    style: TextStyle(fontSize: 11, color: Colors.red),
                  ),
                ],
              ),
            ],
          ),
          TextFormField(
            onChanged: onChange,
            textAlignVertical: TextAlignVertical.center,
            validator: (str) {
              if (str == '' || str == null) {
                isUpdate = false;
              }
              return null;
            },
            focusNode: focusNode,
            onFieldSubmitted: onFieldSubmitted,
            keyboardType: textInputType ?? TextInputType.text,
            controller: controller,
            decoration: InputDecoration(
                hintText: hint, prefixIcon: prefix, border: InputBorder.none),
          ),
        ],
      )),
    );
  }

  displayColorDivider() {
    return Divider(
      color: Color(0xffF4F2F0),
      thickness: 5,
    );
  }
}
