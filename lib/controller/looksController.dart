// ignore_for_file: file_names

import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:sofiqe/controller/reviewController.dart';
import 'package:sofiqe/model/lookms3model.dart';
import 'package:sofiqe/network_service/network_service.dart';
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/constants/api_tokens.dart';
import 'package:sofiqe/utils/states/function.dart';

class LooksController extends GetxController {
  static LooksController get to => Get.find();

  LooksMs3Model? lookModel;
  bool isLookLoading = false;

  getLookList() async {
    isLookLoading = true;
    update();
    try {
      http.Response? response = await NetworkHandler.getMethodCall(
          url: APIEndPoints.baseDefaultUri + "/look/getCollection",
          headers: APIEndPoints.headers(await APITokens.customerSavedToken));
      cPrint("Looks Api Status Code :  ${response!.statusCode}");
      if (response.statusCode == 200) {
        var result = json.decode(response.body);
        lookModel = LooksMs3Model.fromJson(result[0]);
      } else {
        var result = json.decode(response.body);
        Get.snackbar('Error', '${result[0]["message"]}', isDismissible: true);
      }
      cPrint("Responce of API is ${response.body}");
    } catch (e) {
      lookModel = null;
    } finally {
      isLookLoading = false;
      update();
    }
  }

  Future<int> createReview(String detail, String name, String sku, int generalRating, int priceRating,
      int qualityRating, int productId, bool isLogin, int customerid) async {
    try {
      String body = "";
      if (isLogin) {
        body = jsonEncode({
          "review": {
            "title": "User Review",
            "detail": detail,
            "nickname": name,
            "customer_id": customerid,
            "ratings": [
              {"rating_name": "Rating", "value": generalRating},
              {"rating_name": "Quality", "value": qualityRating},
              {"rating_name": "Price", "value": priceRating}
            ],
            "review_entity": "product",
            "review_status": 2,
            "sku": sku,
            "entity_pk_value": productId
          }
        });
      } else {
        body = jsonEncode({
          "review": {
            "title": "User Review",
            "detail": detail,
            "nickname": name,
            "ratings": [
              {"rating_name": "Rating", "value": generalRating},
              {"rating_name": "Quality", "value": qualityRating},
              {"rating_name": "Price", "value": priceRating}
            ],
            "review_entity": "product",
            "review_status": 2,
            "sku": sku,
            "entity_pk_value": productId
          }
        });
      }

      cPrint("REview Body==> $body");
      Uri url = Uri.parse('${APIEndPoints.createReview}');
      cPrint('share wishlist ${url.toString()}');
      http.Response response = await http.post(url,
          headers: <String, String>{
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ${APITokens.bearerToken}',
          },
          body: body);
      cPrint(response.body);
      if (response.statusCode == 200) {
        Get.showSnackbar(
          GetSnackBar(
            message: 'Your review is added successfully and will display when it is approved by the admin',
            duration: Duration(seconds: 2),
            isDismissible: true,
          ),
        );
        ReviewController.to.getMyRiviewsData();
        return 200;
      } else {
        Get.showSnackbar(
          GetSnackBar(
            message: 'Failed to add Review, Please try again.',
            duration: Duration(seconds: 2),
            isDismissible: true,
          ),
        );
        return 500;
      }
    } catch (e) {
      rethrow;
      // cPrint('Error creating review: $e');
    }
  }
}
