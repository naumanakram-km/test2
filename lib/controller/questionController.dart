import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:sofiqe/model/make_over_question_model.dart';
import 'package:sofiqe/model/question_model.dart';
import 'package:sofiqe/provider/catalog_provider.dart';
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/constants/api_tokens.dart';
import 'package:sofiqe/utils/states/function.dart';

class QuestionsController extends GetxController {
  static QuestionsController get to => Get.find();

  static QuestionsController instance = Get.find();
  List<Result>? question;
  List<MakeOverQuestion> makeover = [];
  Result? skinToneQuestion;
  Result? eyeQuestion;
  Result? hairQuestion;

  ///
  /// Customize the request body
  /// [body] is the request body
  /// [url] is the url of the request
  /// [method] is the method of the request
  ///
  Future<List<MakeOverQuestion>> getAnaliticalQuestions() async {
    Uri url = Uri.parse('${APIEndPoints.questionnaireList}');
    http.Response response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${APITokens.adminBearerId}',
    });

    try {
      Iterable types = jsonDecode(response.body)['result'];
      cPrint("APIEndPoints.questionnaireList----" + APIEndPoints.questionnaireList);
      cPrint("APIEndPoints bodyy----" + APIEndPoints.questionnaireList);
      cPrint("APITokens.adminBearerId bodyy----" + APITokens.adminBearerId);
      question = types.map((e) => Result.fromJson(e)).toList();
      cPrint(question?.length);
      int count = 0;
      //var check = question![0].answers;
      for (final element in question!) {
        {
          if (element.answers != null) {
            makeover.add(MakeOverQuestion(
                choices: element.answers!,
                exclusive: '',
                index: count,
                // ignore: avoid_bool_literals_in_conditional_expressions
                multiSelect: ((element.answers!.length > 2 && element.questionId != 3) &&
                            (element.answers!.length < 2 && element.questionId != 10)) ||
                        (element.questionId == 2 || element.questionId == 4)
                    ? true
                    : false,
                id: element.questionId.toString(),
                question: element.questions!,
                answer: []));

            count++;
          }

          // Code for capturing the Question
          if (element.questions.toString().toLowerCase().contains("skin tone")) {
            skinToneQuestion = element;
            cPrint("Skin tone question encountered");
          }

          if (element.questions.toString().toLowerCase().contains("eyes do you have")) {
            eyeQuestion = element;
            cPrint("Skin tone question encountered");
          }

          if (element.questions.toString().toLowerCase().contains("colour to your hair")) {
            hairQuestion = element;
            cPrint("Skin tone question encountered");
          }
        }
      }
      return makeover;
    } catch (e) {
      return [];
    }
  }

  @override
  void onInit() async {
    await getAnaliticalQuestions();
    final CatalogProvider instance = Get.find();
    instance.setColors();
    super.onInit();
  }
}
