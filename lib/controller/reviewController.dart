import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:sofiqe/model/reviewModel.dart';
import 'package:sofiqe/model/wishListModel.dart';
import 'package:sofiqe/network_service/network_service.dart';
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/constants/api_tokens.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../model/ReviewProductModel.dart';
import '../model/my_review_by_sku.dart' as mrbs;

class ReviewController extends GetxController {
  static ReviewController get to => Get.find();

  void onStartController() async {
    getGloableReviews();
    print("CORC-->");

    String token = await APITokens.customerSavedToken;
    print("CORC-->" + token);
    if (token.isNotEmpty && token.toLowerCase() != "null") {
      getMyRiviewsData();
      getWishListData();
    }
  }

  ReviewModel? reviewModel;
  ReviewModel? myReviewModel = ReviewModel();
  mrbs.MyReviewSkuModel? myReviewBySkuModel = mrbs.MyReviewSkuModel();

  RxInt globleReviewCount = 0.obs;
  RxInt myReviewCount = 0.obs;
  RxInt wishList = 0.obs;

  bool isReviewLoading = false;
  bool isReviewBySkuLoading = false;
  bool isGlobaleReviews = false;
  String customerToken = '';
  List<ReviewProductModel> reviewName = <ReviewProductModel>[];
  List<ReviewProductModel> reviewGlobaleName = <ReviewProductModel>[];

  ///
  /// Customize the request body
  /// [body] is the request body
  /// [url] is the url of the request
  /// [method] is the method of the request
  /// [token] is the token of the request
  /// [isAuth] is the authentication of the request
  ///
  ///
  getGloableReviews() async {
    var result;
    try {
      isGlobaleReviews = true;
      update();
      http.Response? response = await NetworkHandler.getMethodCall(
          url: APIEndPoints.baseUri + "/reviews",
          headers: APIEndPoints.headers(APITokens.bearerToken));
      cPrint("after api  ${response!.statusCode}");
      cPrint("getGloableReviews  ${response.body}");
      if (response.statusCode == 200) {
        reviewModel?.items = [];
        var result = json.decode(response.body);
        reviewModel = ReviewModel.fromJson(result);
        globleReviewCount.value = reviewModel!.items!.length;
        cPrint("getGloableReviews  ${reviewModel!.items!.first.ratingavg}");
        for (int i = 0; i < reviewModel!.items!.length; i++) {
          reviewGlobaleName.add(ReviewProductModel(
              sku: reviewModel!.items![i].sku.toString(),
              name: reviewModel!.items![i].product_name.toString(),
              imagePath: reviewModel!.items![i].image.toString(),
              productUrl: reviewModel!.items![i].productUrl,
              rating: reviewModel!.items![i].ratingavg.toString(),
              ratingCount:
                  (reviewModel!.items![i].ratings ?? []).length.toString(),
              typeId: reviewModel!.items![i].typeId.toString()));
        }
        update();
        cPrint(globleReviewCount.value);
      } else {
        result = json.decode(response.body);
        Get.snackbar('Error', '${result["message"]}', isDismissible: true);
      }
      cPrint("Responce of API is ${response.body}");
      isGlobaleReviews = false;
    } catch (e) {
      Get.snackbar('Error', '${result["message"]}', isDismissible: true);
    } finally {
      isGlobaleReviews = false;
      update();
    }
  }

  productAddtoBag(Result result) async {
    update();
    var body = {
            'color': result.product?.shadeColor,
         'quantity': '1',
       'product_id': result.productId, 
      'total_price': result.product?.price
    };
    http.Response? response = await NetworkHandler.postMethodCall(
        body: body,
        url:
            APIEndPoints.myBaseUri + "/api/index.php/api/user/addToCartProduct",
        headers: APIEndPoints.headers(await APITokens.customerSavedToken));
    cPrint("After api  ${response!.statusCode}");
    if (response.statusCode == 200) {
      Get.snackbar('Successfully', 'product added in to Cart list',
          isDismissible: true);
    } else {
      Get.snackbar('Error', 'Error add to cart', isDismissible: true);
    }
  }

  getMyRiviewsData() async {
    var result;
    try {
      isReviewLoading = true;
      update();
      var token = await APITokens.customerSavedToken;
      cPrint(token);
      http.Response? response = await NetworkHandler.getMethodCall(
          url: APIEndPoints.baseDefaultUri + "/customer/reviews/",
          headers: APIEndPoints.headers(token));
      // cPrint("after api  ${response!.statusCode}");
      // cPrint("getMyRiviewsData  ${response.body}");
      if (response?.statusCode == 200) {
        myReviewModel?.items = [];
        result = json.decode(response!.body);
        myReviewModel = ReviewModel.fromJson(result);
        cPrint('myReviewModel  ---> ${myReviewModel!.items!.length}');
        cPrint('sku_id  ---> ${response.body}');
        myReviewCount.value = myReviewModel!.items!.length;
        cPrint("LENGTH LENGTH ${myReviewModel!.items!.length}");
        // for (int i = 0; i < myReviewModel!.items!.length; i++) {
        //   cPrint("LENGTH LENGTHa $i ${myReviewModel!.items![i].sku}");
        //   reviewName.add(ReviewProductModel(
        //       sku: myReviewModel!.items![i].sku.toString(),
        //       name: myReviewModel!.items![i].product_name.toString(),
        //       imagePath: myReviewModel!.items![i].image.toString(),
        //       productUrl: reviewModel!.items![i].productUrl,
        //       rating: reviewModel!.items![i].ratingavg.toString(),
        //       ratingCount: (reviewModel!.items![i].ratings ?? []).length.toString(),
        //       typeId: myReviewModel!.items![i].typeId.toString()));

        //   // // Edited on 14-04-2022 by Ashwani
        //   // await getMyRiviewsBySkuData(myReviewModel!.items![i].sku.toString()).then((value) {
        //   //   cPrint("CONTROLL ENTER $i  ${value?.name}");

        //   //   if (value != null) {

        //   //   } else {
        //   //     reviewName.add(ReviewProductModel(
        //   //         sku: myReviewModel!.items![i].sku.toString(),
        //   //         name: '',
        //   //         imagePath: '',
        //   //         typeId: '',
        //   //         productUrl: '',
        //   //         rating: '0'));
        //   //   }
        //   // });
        // }
        update();
      } else {
        result = json.decode(response!.body);
        Get.snackbar('Error', '${(result ?? {})["message"]}',
            isDismissible: true);
      }
      cPrint("Responce of API is  ${response.body}");
    } catch (e) {
      Get.snackbar('Error', '${(result ?? {})["message"]}',
          isDismissible: true);
    } finally {
      isReviewLoading = false;
      update();
    }
  }

  WishlistModel? wishlistModel;
  bool isWishListLoading = false;

  getWishListData() async {
    var result;
    try {
      isWishListLoading = true;
      update();
      http.Response? response = await NetworkHandler.getMethodCall(
          url: APIEndPoints.baseUri + "/wishlist/items",
          headers: APIEndPoints.headers(await APITokens.customerSavedToken));
      cPrint("after api  ${response!.statusCode}");
      cPrint("getWishListData  ${response.body}");
      if (response.statusCode == 200) {
        wishlistModel?.result = [];
        result = json.decode(response.body);
        wishlistModel = WishlistModel.fromJson(result);
        wishList.value = wishlistModel!.result!.length;
      } else {
        result = json.decode(response.body);
        Get.snackbar('Error', '${result["message"]}', isDismissible: true);
      }
      cPrint("Responce of API is getWishListData ${response.body}");
    } catch (e) {
      Get.snackbar('Error', '${(result ?? {})["message"]}',
          isDismissible: true);
    } finally {
      isWishListLoading = false;
      update();
    }
  }

  Future<mrbs.MyReviewSkuModel?> getMyRiviewsBySkuData(String sku) async {
    var result;
    try {
      isReviewBySkuLoading = true;
      //update();
      http.Response? response = await NetworkHandler.getMethodCall(
          url: APIEndPoints.baseUri + "/products/$sku",
          headers: APIEndPoints.headers(APITokens.adminBearerId));

      if (response!.statusCode == 200) {
        result = json.decode(response.body);

        myReviewBySkuModel = mrbs.MyReviewSkuModel.fromJson(result);

        return myReviewBySkuModel;
      } else {
        result = json.decode(response.body);
        //Get.snackbar('Error', '${result["message"]}', isDismissible: true);
        return null;
      }
    } catch (e) {
      //Get.snackbar('Error', '${result["message"]}', isDismissible: true);
      return null;
    } finally {
      isReviewBySkuLoading = false;
      //update();
    }
  }

  Future<void> shareWishlist(String email, String messages) async {
    try {
      // ShareWishListModel model = ShareWishListModel(emails: email, message: messages);
      var resBody = {};
      resBody["emails"] = email;
      resBody["message"] = messages;

      Uri url = Uri.parse('${APIEndPoints.shareWishList}');
      cPrint('share wishlist ${url.toString()}');
      http.Response response = await http.post(url,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ${await APITokens.customerSavedToken}',
          },
          // body: shareWishListModelToJson(model),
          body: jsonEncode(
              {'emails': email, 'message': messages}) //json.encode(resBody),
          );
      cPrint(response.body);
      if (response.statusCode == 200) {
        Get.showSnackbar(
          GetSnackBar(
            message: 'Your wishlist is shared successfully',
            duration: Duration(seconds: 2),
            isDismissible: true,
          ),
        );
      } else {
        Get.showSnackbar(
          GetSnackBar(
            message: 'Wishlist not shared',
            duration: Duration(seconds: 2),
            isDismissible: true,
          ),
        );
      }
    } catch (e) {
      cPrint('Error sharing wishlist: $e');
    }
  }

  addToCartConfigurableProducts() {}
}
