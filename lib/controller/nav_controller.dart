import 'package:get/get.dart';
import 'package:sofiqe/controller/fabController.dart';

// controller for hide navigation bar
class NavController extends GetxController {
  static NavController get to => Get.find();

  bool navbar = false;

  var currentMainIndex = 0.obs;
  var innerPageIndex = 0.obs;

  // set bool true and false
  setnavbar(bool val) {
    print("NBS==>" + val.toString());
    if (val) FABController.to.setfabvalue(true);
    this.navbar = val;
    update();
  }
}