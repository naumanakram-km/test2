import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:sofiqe/model/natural_me_ms4/natural_me_model.dart' as me;
import 'package:sofiqe/network_service/network_service.dart';
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/constants/api_tokens.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../model/UserDetailModel.dart';

class NaturalMeController extends GetxController {
  static NaturalMeController get to => Get.find();

  var naturalMe = [].obs;
  var isNaturalMeLoading = false.obs;

  Rx<me.NaturalMeModelNew> naturalMeModelNew = me.NaturalMeModelNew().obs;
  List<CustomAttribute>? customAttributes1;
  List<String> aller = [];

  Future<void> getNaturalMe({bool isRefresh = true}) async {
    String token = await APITokens.customerSavedToken;
    if (token.isNotEmpty && token.toLowerCase() != "null") {
      if (isRefresh) {
        isNaturalMeLoading(true);
      }

      ///
      /// await APITokens.customerSavedToken
      ///
      /// Customize the request body
      ///
      try {
        http.Response? response = await NetworkHandler.getMethodCall(
            url: APIEndPoints.myBaseUri + "/customers/me/",
            headers: APIEndPoints.headers(await APITokens.customerSavedToken));
        cPrint("after api  ${await APITokens.customerSavedToken}");
        cPrint("after api  ${response!.statusCode}");
        cPrint("after api body  ${response.body}");
        if (response.statusCode == 200) {
          var result = json.decode(response.body);
          me.NaturalMeModelNew model = me.NaturalMeModelNew.fromJson(result);
          naturalMeModelNew.value = model;
          //customAttributes1 = json.decode(naturalMeModelNew.value.customAttributes![1].value.toString());
          var dataSp = naturalMeModelNew.value.customAttributes![1].value!.split(',');
          dataSp.forEach((element) {
            if (element.toString() != '{}') {
              var a = element
                  .split(':')
                  .last
                  .toString()
                  .replaceAll('[', '')
                  .replaceAll(']', '')
                  .replaceAll('{', '')
                  .replaceAll('}', '')
                  .replaceAll(',', '');
              cPrint("AAAAA---->" + a.length.toString());
              if (a != " ") {
                aller.add(a.replaceAll('"', ''));
              }

              //  mapData=mapData+ element.split(':')[1];
              // mapData[element.split(':')[0]] = element.split(':')[1];
            }
            cPrint('element---->' + element.toString());
          });
          update();

          //   cPrint('naturalMeModelNew---->'+mapData.toString());

          // for (int i = 0; i < result.length; i++) {
          //   NaturalMeModel model = NaturalMeModel.fromJson(result[i]);
          //   naturalMe.add(model);
          // }
          isNaturalMeLoading(false);
          cPrint("_____________________________________");
          cPrint(result);
          cPrint("_____________________________________");
        } else {
          var result = json.decode(response.body);
          Get.snackbar('Error', '${result["message"]}', isDismissible: true);
          isNaturalMeLoading(false);
        }
        cPrint("Responce of API is--- ${response.body}");
      } catch (e) {
        isNaturalMeLoading(false);

        cPrint("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
        cPrint("Error in get natural me $e");
        cPrint("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
      } finally {
        isNaturalMeLoading(false);
      }
    }
  }

  Future<UserDetailModel?> getUserImage() async {
    UserDetailModel model = UserDetailModel();
    try {
      http.Response? response = await NetworkHandler.getMethodCall(
          url: APIEndPoints.myBaseUri + "/customers/me/photo",
          headers: APIEndPoints.headers(await APITokens.customerSavedToken));
      cPrint("after api  ${response!.statusCode}");
      if (response.statusCode == 200) {
        var result = json.decode(response.body);
        model = UserDetailModel.fromJson(result);
      } else {
        var result = json.decode(response.body);
        Get.snackbar('Error', '${result[0]["message"]}');
      }
      return model;
    } catch (e) {
      return null;
    }
  }

  @override
  void onInit() async {
    super.onInit();
    String customerSavedToken = await APITokens.customerSavedToken;
    if (customerSavedToken != null && customerSavedToken.isNotEmpty) {
        getNaturalMe();
    }
  }
}
