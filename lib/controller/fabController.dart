// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../widgets/scaffold/custom_fab.dart';

class FABController extends GetxController {
  static FABController get to => Get.find();
  RxBool showFab = true.obs;
  final GlobalKey<FabCircularMenuState> fabKey = GlobalKey();

  closeOpenedMenu() {
    fabKey.currentState != null ? fabKey.currentState?.close() : cPrint('');
  }

  setfabvalue(bool val) {
    showFab.value = val;
  }
}
