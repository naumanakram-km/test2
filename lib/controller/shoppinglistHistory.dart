import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:sofiqe/model/shoppingHistoryModel.dart';
import 'package:sofiqe/network_service/network_service.dart';
import 'package:sofiqe/provider/cart_provider.dart';
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/constants/api_tokens.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../utils/states/local_storage.dart';

class ShoppingHistory extends GetxController {
  static ShoppingHistory get to => Get.find();

  bool isShoppingListLoading = false;
  String isShoppingListError = "Forbidden";
  ShoppingHistoryModel? historyList;

  getShoppingHistory() async {
    isShoppingListLoading = true;
    update();
    var result;
    try {
      Map uidMap = await sfQueryForSharedPrefData(fieldName: 'uid', type: PreferencesDataType.INT);
      int uid = uidMap['uid'];
      cPrint('uuuuuu id  $uid');
      http.Response? response = await NetworkHandler.getMethodCall(
          url: APIEndPoints.baseUri + "/ms2/orders/customer/$uid",
          headers: APIEndPoints.headers(await APITokens.customerSavedToken));
      cPrint("after api  ${response!.statusCode}");
      if (response.statusCode == 200) {
        result = json.decode(response.body);
        cPrint("2222222222" + response.body);
        cPrint("333333333333");
        try {
          cPrint("historyList");
          (result[0]["result"] == "error")
              ? historyList = null
              : historyList = ShoppingHistoryModel.fromJson(result[0]);
        } catch (e) {
          historyList = null;
          Get.snackbar('Error', '${result[0]["message"]}', isDismissible: true);
        }
      } else if (response.statusCode == 401) {
        historyList = null;
        Get.snackbar('Error', 'User Session Token Expire', isDismissible: true);
      } else {
        historyList = null;
        var result = json.decode(response.body);
        Get.snackbar('Error', '${result[0]["message"]}', isDismissible: true);
      }
      cPrint("Responce of API is ${response.body}");
    } finally {}
    isShoppingListLoading = false;
    update();
  }

  orderAgain(BuildContext context, String orderId) async {
    try {
      http.Response? response = await NetworkHandler.postMethodCall(
          url: APIEndPoints.baseUri + "/customers/$orderId/buyagain",
          headers: APIEndPoints.headers(await APITokens.customerSavedToken));

      if (response!.statusCode == 200) {
        Get.showSnackbar(
          GetSnackBar(
            message: 'Please go to cart for checkout.',
            duration: Duration(seconds: 2),
            isDismissible: true,
          ),
        );
        Provider.of<CartProvider>(context, listen: false).fetchCartDetails();
      } else if (response.statusCode == 401) {
        Get.showSnackbar(
          GetSnackBar(
            message: 'Something went wrong.',
            duration: Duration(seconds: 2),
            isDismissible: true,
          ),
        );
      } else {
        Get.showSnackbar(
          GetSnackBar(
            message: 'Something went wrong.',
            duration: Duration(seconds: 2),
            isDismissible: true,
          ),
        );
      }
    } catch (e) {
      rethrow;
    }
  }







}