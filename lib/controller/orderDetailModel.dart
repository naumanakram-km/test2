class OrderDetailModel {
  String? result;
  Data? data;

  OrderDetailModel({this.result, this.data});

  OrderDetailModel.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['result'] = result;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  String? orderId;
  String? orderIncrementId;
  String? date;
  String? status;
  String? state;
  String? count;
  String? totals;
  String? totalQty;
  List<Items>? items;
  ShippingAddress? shippingAddress;
  BillingAddress? billingAddress;
  PaymentMethod? paymentMethod;

  Data(
      {this.orderId,
      this.orderIncrementId,
      this.date,
      this.status,
      this.state,
      this.count,
      this.totals,
      this.totalQty,
      this.items,
      this.shippingAddress,
      this.billingAddress,
      this.paymentMethod});

  Data.fromJson(Map<String, dynamic> json) {
    orderId = json['order_id'] ?? "";
    orderIncrementId =
        json['order_increment_id'] ?? "";
    date = json['date'] ?? "";
    status = json['status'] ?? "";
    state = json['state'] ?? "";
    count = json['count'] ?? "";
    totals = json['totals'] ?? "";
    totalQty = json['total_qty'] ?? "";
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(Items.fromJson(v));
      });
    }
    shippingAddress = json['shipping_address'] != null
        ? ShippingAddress.fromJson(json['shipping_address'])
        : null;
    billingAddress = json['billing_address'] != null
        ? BillingAddress.fromJson(json['billing_address'])
        : null;
    paymentMethod = json['payment_method'] != null
        ? PaymentMethod.fromJson(json['payment_method'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['order_id'] = orderId;
    data['order_increment_id'] = orderIncrementId;
    data['date'] = date;
    data['status'] = status;
    data['state'] = state;
    data['count'] = count;
    data['totals'] = totals;
    data['total_qty'] = totalQty;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    if (shippingAddress != null) {
      data['shipping_address'] = shippingAddress!.toJson();
    }
    if (billingAddress != null) {
      data['billing_address'] = billingAddress!.toJson();
    }
    if (paymentMethod != null) {
      data['payment_method'] = paymentMethod!.toJson();
    }
    return data;
  }
}

class Items {
  String? sku;
  String? name;
  String? image;
  String? orderQty;
  String? price;
  var productOptions;
  var additionalOptions;
  CustomAttributes? customAttributes;

  Items(
      {this.sku,
      this.name,
      this.image,
      this.orderQty,
      this.price,
      this.productOptions,
      this.additionalOptions,
      this.customAttributes});

  Items.fromJson(Map<String, dynamic> json) {
    sku = json['sku'];
    name = json['name'];
    image = json['image'];
    orderQty = json['order_qty'];
    price = json['price'];
    if (json['productOptions'] != null) {
      productOptions = [];
      productOptions = json['productOptions'];
    }
    if (json['additionalOptions'] != null) {
      additionalOptions = [];
      additionalOptions = json['additionalOptions'];
    }
    customAttributes = json['custom_attributes'] != null
        ? CustomAttributes.fromJson(json['custom_attributes'])
        : null;
    // extensionAttributes = json['extension_attributes'] != null ? new ExtensionAttributes.fromJson(json['extension_attributes']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['sku'] = sku;
    data['name'] = name;
    data['image'] = image;
    data['order_qty'] = orderQty;
    data['price'] = price;
    if (productOptions != null) {
      data['productOptions'] =
          productOptions.map((v) => v.toJson()).toList();
    }
    if (additionalOptions != null) {
      data['additionalOptions'] =
          additionalOptions.map((v) => v.toJson()).toList();
    }
    if (customAttributes != null) {
      data['custom_attributes'] = customAttributes!.toJson();
    }

    return data;
  }
}

class CustomAttributes {
  String? image;
  String? urlKey;
  String? shortDescription;
  String? smallImage;
  String? optionsContainer;
  String? description;
  String? brand;
  String? thumbnail;
  String? dealFromDate;
  String? taxClassId;
  String? directions;
  String? dealToDate;
  String? msrpDisplayActualPriceType;
  String? storeYear;
  String? drySkin;
  String? oilBased;
  List<String>? categoryIds;
  String? sensitiveSkin;
  String? urlPath;
  String? matte;
  String? radiant;
  String? faceArea;
  String? requiredOptions;
  String? hasOptions;
  String? faceSubArea;
  String? size;
  String? volume;

  CustomAttributes(
      {this.image,
      this.urlKey,
      this.shortDescription,
      this.smallImage,
      this.optionsContainer,
      this.description,
      this.brand,
      this.thumbnail,
      this.dealFromDate,
      this.taxClassId,
      this.directions,
      this.dealToDate,
      this.msrpDisplayActualPriceType,
      this.storeYear,
      this.drySkin,
      this.oilBased,
      this.categoryIds,
      this.sensitiveSkin,
      this.urlPath,
      this.matte,
      this.radiant,
      this.faceArea,
      this.requiredOptions,
      this.hasOptions,
      this.faceSubArea,
      this.size,
      this.volume});

  CustomAttributes.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    urlKey = json['url_key'];
    shortDescription = json['short_description'];
    smallImage = json['small_image'];
    optionsContainer = json['options_container'];
    description = json['description'];
    brand = json['brand'];
    thumbnail = json['thumbnail'];
    dealFromDate = json['deal_from_date'];
    taxClassId = json['tax_class_id'];
    directions = json['directions'];
    dealToDate = json['deal_to_date'];
    msrpDisplayActualPriceType = json['msrp_display_actual_price_type'];
    storeYear = json['store_year'];
    drySkin = json['dry_skin'];
    oilBased = json['oil_based'];
    categoryIds = json['category_ids'].cast<String>();
    sensitiveSkin = json['sensitive_skin'];
    urlPath = json['url_path'];
    matte = json['matte'];
    radiant = json['radiant'];
    faceArea = json['face_area'];
    requiredOptions = json['required_options'];
    hasOptions = json['has_options'];
    faceSubArea = json['face_sub_area'];
    size = json['size'];
    volume = json['volume'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['image'] = image;
    data['url_key'] = urlKey;
    data['short_description'] = shortDescription;
    data['small_image'] = smallImage;
    data['options_container'] = optionsContainer;
    data['description'] = description;
    data['brand'] = brand;
    data['thumbnail'] = thumbnail;
    data['deal_from_date'] = dealFromDate;
    data['tax_class_id'] = taxClassId;
    data['directions'] = directions;
    data['deal_to_date'] = dealToDate;
    data['msrp_display_actual_price_type'] = msrpDisplayActualPriceType;
    data['store_year'] = storeYear;
    data['dry_skin'] = drySkin;
    data['oil_based'] = oilBased;
    data['category_ids'] = categoryIds;
    data['sensitive_skin'] = sensitiveSkin;
    data['url_path'] = urlPath;
    data['matte'] = matte;
    data['radiant'] = radiant;
    data['face_area'] = faceArea;
    data['required_options'] = requiredOptions;
    data['has_options'] = hasOptions;
    data['face_sub_area'] = faceSubArea;
    data['size'] = size;
    data['volume'] = volume;
    return data;
  }
}

class ShippingAddress {
  String? entityId;
  String? parentId;
  String? customerAddressId;
  String? quoteAddressId;
  String? regionId;
  var customerId;
  var fax;
  String? region;
  String? postcode;
  String? lastname;
  String? street;
  String? city;
  String? email;
  var telephone;
  String? countryId;
  String? firstname;
  String? addressType;
  var prefix;
  var middlename;
  var suffix;
  var company;
  var vatId;
  var vatIsValid;
  var vatRequestId;
  var vatRequestDate;
  var vatRequestSuccess;
  var vertexVatCountryCode;

  ShippingAddress(
      {this.entityId,
      this.parentId,
      this.customerAddressId,
      this.quoteAddressId,
      this.regionId,
      this.customerId,
      this.fax,
      this.region,
      this.postcode,
      this.lastname,
      this.street,
      this.city,
      this.email,
      this.telephone,
      this.countryId,
      this.firstname,
      this.addressType,
      this.prefix,
      this.middlename,
      this.suffix,
      this.company,
      this.vatId,
      this.vatIsValid,
      this.vatRequestId,
      this.vatRequestDate,
      this.vatRequestSuccess,
      this.vertexVatCountryCode});

  ShippingAddress.fromJson(Map<String, dynamic> json) {
    entityId = json['entity_id'];
    parentId = json['parent_id'];
    customerAddressId = json['customer_address_id'];
    quoteAddressId = json['quote_address_id'];
    regionId = json['region_id'];
    customerId = json['customer_id'];
    fax = json['fax'];
    region = json['region'];
    postcode = json['postcode'];
    lastname = json['lastname'];
    street = json['street'];
    city = json['city'];
    email = json['email'];
    telephone = json['telephone'];
    countryId = json['country_id'];
    firstname = json['firstname'];
    addressType = json['address_type'];
    prefix = json['prefix'];
    middlename = json['middlename'];
    suffix = json['suffix'];
    company = json['company'];
    vatId = json['vat_id'];
    vatIsValid = json['vat_is_valid'];
    vatRequestId = json['vat_request_id'];
    vatRequestDate = json['vat_request_date'];
    vatRequestSuccess = json['vat_request_success'];
    vertexVatCountryCode = json['vertex_vat_country_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['entity_id'] = entityId;
    data['parent_id'] = parentId;
    data['customer_address_id'] = customerAddressId;
    data['quote_address_id'] = quoteAddressId;
    data['region_id'] = regionId;
    data['customer_id'] = customerId;
    data['fax'] = fax;
    data['region'] = region;
    data['postcode'] = postcode;
    data['lastname'] = lastname;
    data['street'] = street;
    data['city'] = city;
    data['email'] = email;
    data['telephone'] = telephone;
    data['country_id'] = countryId;
    data['firstname'] = firstname;
    data['address_type'] = addressType;
    data['prefix'] = prefix;
    data['middlename'] = middlename;
    data['suffix'] = suffix;
    data['company'] = company;
    data['vat_id'] = vatId;
    data['vat_is_valid'] = vatIsValid;
    data['vat_request_id'] = vatRequestId;
    data['vat_request_date'] = vatRequestDate;
    data['vat_request_success'] = vatRequestSuccess;
    data['vertex_vat_country_code'] = vertexVatCountryCode;
    return data;
  }
}

class BillingAddress {
  String? entityId;
  String? parentId;
  var customerAddressId;
  String? quoteAddressId;
  String? regionId;
  var customerId;
  var fax;
  String? region;
  String? postcode;
  String? lastname;
  String? street;
  String? city;
  String? email;
  var telephone;
  String? countryId;
  String? firstname;
  String? addressType;
  var prefix;
  var middlename;
  var suffix;
  var company;
  var vatId;
  var vatIsValid;
  var vatRequestId;
  var vatRequestDate;
  var vatRequestSuccess;
  var vertexVatCountryCode;

  BillingAddress(
      {this.entityId,
      this.parentId,
      this.customerAddressId,
      this.quoteAddressId,
      this.regionId,
      this.customerId,
      this.fax,
      this.region,
      this.postcode,
      this.lastname,
      this.street,
      this.city,
      this.email,
      this.telephone,
      this.countryId,
      this.firstname,
      this.addressType,
      this.prefix,
      this.middlename,
      this.suffix,
      this.company,
      this.vatId,
      this.vatIsValid,
      this.vatRequestId,
      this.vatRequestDate,
      this.vatRequestSuccess,
      this.vertexVatCountryCode});

  BillingAddress.fromJson(Map<String, dynamic> json) {
    entityId = json['entity_id'];
    parentId = json['parent_id'];
    customerAddressId = json['customer_address_id'];
    quoteAddressId = json['quote_address_id'];
    regionId = json['region_id'];
    customerId = json['customer_id'];
    fax = json['fax'];
    region = json['region'];
    postcode = json['postcode'];
    lastname = json['lastname'];
    street = json['street'];
    city = json['city'];
    email = json['email'];
    telephone = json['telephone'];
    countryId = json['country_id'];
    firstname = json['firstname'];
    addressType = json['address_type'];
    prefix = json['prefix'];
    middlename = json['middlename'];
    suffix = json['suffix'];
    company = json['company'];
    vatId = json['vat_id'];
    vatIsValid = json['vat_is_valid'];
    vatRequestId = json['vat_request_id'];
    vatRequestDate = json['vat_request_date'];
    vatRequestSuccess = json['vat_request_success'];
    vertexVatCountryCode = json['vertex_vat_country_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['entity_id'] = entityId;
    data['parent_id'] = parentId;
    data['customer_address_id'] = customerAddressId;
    data['quote_address_id'] = quoteAddressId;
    data['region_id'] = regionId;
    data['customer_id'] = customerId;
    data['fax'] = fax;
    data['region'] = region;
    data['postcode'] = postcode;
    data['lastname'] = lastname;
    data['street'] = street;
    data['city'] = city;
    data['email'] = email;
    data['telephone'] = telephone;
    data['country_id'] = countryId;
    data['firstname'] = firstname;
    data['address_type'] = addressType;
    data['prefix'] = prefix;
    data['middlename'] = middlename;
    data['suffix'] = suffix;
    data['company'] = company;
    data['vat_id'] = vatId;
    data['vat_is_valid'] = vatIsValid;
    data['vat_request_id'] = vatRequestId;
    data['vat_request_date'] = vatRequestDate;
    data['vat_request_success'] = vatRequestSuccess;
    data['vertex_vat_country_code'] = vertexVatCountryCode;
    return data;
  }
}

class PaymentMethod {
  String? entityId;
  String? parentId;
  var baseShippingCaptured;
  var shippingCaptured;
  var amountRefunded;
  var baseAmountPaid;
  var amountCanceled;
  var baseAmountAuthorized;
  var baseAmountPaidOnline;
  var baseAmountRefundedOnline;
  String? baseShippingAmount;
  String? shippingAmount;
  var amountPaid;
  var amountAuthorized;
  String? baseAmountOrdered;
  var baseShippingRefunded;
  var shippingRefunded;
  var baseAmountRefunded;
  String? amountOrdered;
  var baseAmountCanceled;
  var quotePaymentId;
  var additionalData;
  var ccExpMonth;
  String? ccSsStartYear;
  var echeckBankName;
  String? method;
  var ccDebugRequestBody;
  var ccSecureVerify;
  var protectionEligibility;
  var ccApproval;
  var ccLast4;
  var ccStatusDescription;
  var echeckType;
  var ccDebugResponseSerialized;
  String? ccSsStartMonth;
  var echeckAccountType;
  var lastTransId;
  var ccCidStatus;
  var ccOwner;
  var ccType;
  var poNumber;
  String? ccExpYear;
  var ccStatus;
  var echeckRoutingNumber;
  var accountStatus;
  var anetTransMethod;
  var ccDebugResponseBody;
  var ccSsIssue;
  var echeckAccountName;
  var ccAvsStatus;
  var ccNumberEnc;
  var ccTransId;
  var addressStatus;
  AdditionalInformation? additionalInformation;

  PaymentMethod(
      {this.entityId,
      this.parentId,
      this.baseShippingCaptured,
      this.shippingCaptured,
      this.amountRefunded,
      this.baseAmountPaid,
      this.amountCanceled,
      this.baseAmountAuthorized,
      this.baseAmountPaidOnline,
      this.baseAmountRefundedOnline,
      this.baseShippingAmount,
      this.shippingAmount,
      this.amountPaid,
      this.amountAuthorized,
      this.baseAmountOrdered,
      this.baseShippingRefunded,
      this.shippingRefunded,
      this.baseAmountRefunded,
      this.amountOrdered,
      this.baseAmountCanceled,
      this.quotePaymentId,
      this.additionalData,
      this.ccExpMonth,
      this.ccSsStartYear,
      this.echeckBankName,
      this.method,
      this.ccDebugRequestBody,
      this.ccSecureVerify,
      this.protectionEligibility,
      this.ccApproval,
      this.ccLast4,
      this.ccStatusDescription,
      this.echeckType,
      this.ccDebugResponseSerialized,
      this.ccSsStartMonth,
      this.echeckAccountType,
      this.lastTransId,
      this.ccCidStatus,
      this.ccOwner,
      this.ccType,
      this.poNumber,
      this.ccExpYear,
      this.ccStatus,
      this.echeckRoutingNumber,
      this.accountStatus,
      this.anetTransMethod,
      this.ccDebugResponseBody,
      this.ccSsIssue,
      this.echeckAccountName,
      this.ccAvsStatus,
      this.ccNumberEnc,
      this.ccTransId,
      this.addressStatus,
      this.additionalInformation});

  PaymentMethod.fromJson(Map<String, dynamic> json) {
    entityId = json['entity_id'];
    parentId = json['parent_id'];
    baseShippingCaptured = json['base_shipping_captured'];
    shippingCaptured = json['shipping_captured'];
    amountRefunded = json['amount_refunded'];
    baseAmountPaid = json['base_amount_paid'];
    amountCanceled = json['amount_canceled'];
    baseAmountAuthorized = json['base_amount_authorized'];
    baseAmountPaidOnline = json['base_amount_paid_online'];
    baseAmountRefundedOnline = json['base_amount_refunded_online'];
    baseShippingAmount = json['base_shipping_amount'];
    shippingAmount = json['shipping_amount'];
    amountPaid = json['amount_paid'];
    amountAuthorized = json['amount_authorized'];
    baseAmountOrdered = json['base_amount_ordered'];
    baseShippingRefunded = json['base_shipping_refunded'];
    shippingRefunded = json['shipping_refunded'];
    baseAmountRefunded = json['base_amount_refunded'];
    amountOrdered = json['amount_ordered'];
    baseAmountCanceled = json['base_amount_canceled'];
    quotePaymentId = json['quote_payment_id'];
    additionalData = json['additional_data'];
    ccExpMonth = json['cc_exp_month'];
    ccSsStartYear = json['cc_ss_start_year'];
    echeckBankName = json['echeck_bank_name'];
    method = json['method'];
    ccDebugRequestBody = json['cc_debug_request_body'];
    ccSecureVerify = json['cc_secure_verify'];
    protectionEligibility = json['protection_eligibility'];
    ccApproval = json['cc_approval'];
    ccLast4 = json['cc_last_4'];
    ccStatusDescription = json['cc_status_description'];
    echeckType = json['echeck_type'];
    ccDebugResponseSerialized = json['cc_debug_response_serialized'];
    ccSsStartMonth = json['cc_ss_start_month'];
    echeckAccountType = json['echeck_account_type'];
    lastTransId = json['last_trans_id'];
    ccCidStatus = json['cc_cid_status'];
    ccOwner = json['cc_owner'];
    ccType = json['cc_type'];
    poNumber = json['po_number'];
    ccExpYear = json['cc_exp_year'];
    ccStatus = json['cc_status'];
    echeckRoutingNumber = json['echeck_routing_number'];
    accountStatus = json['account_status'];
    anetTransMethod = json['anet_trans_method'];
    ccDebugResponseBody = json['cc_debug_response_body'];
    ccSsIssue = json['cc_ss_issue'];
    echeckAccountName = json['echeck_account_name'];
    ccAvsStatus = json['cc_avs_status'];
    ccNumberEnc = json['cc_number_enc'];
    ccTransId = json['cc_trans_id'];
    addressStatus = json['address_status'];
    additionalInformation = (json['additional_information'] != null
        ? AdditionalInformation.fromJson(json['additional_information'])
        : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['entity_id'] = entityId;
    data['parent_id'] = parentId;
    data['base_shipping_captured'] = baseShippingCaptured;
    data['shipping_captured'] = shippingCaptured;
    data['amount_refunded'] = amountRefunded;
    data['base_amount_paid'] = baseAmountPaid;
    data['amount_canceled'] = amountCanceled;
    data['base_amount_authorized'] = baseAmountAuthorized;
    data['base_amount_paid_online'] = baseAmountPaidOnline;
    data['base_amount_refunded_online'] = baseAmountRefundedOnline;
    data['base_shipping_amount'] = baseShippingAmount;
    data['shipping_amount'] = shippingAmount;
    data['amount_paid'] = amountPaid;
    data['amount_authorized'] = amountAuthorized;
    data['base_amount_ordered'] = baseAmountOrdered;
    data['base_shipping_refunded'] = baseShippingRefunded;
    data['shipping_refunded'] = shippingRefunded;
    data['base_amount_refunded'] = baseAmountRefunded;
    data['amount_ordered'] = amountOrdered;
    data['base_amount_canceled'] = baseAmountCanceled;
    data['quote_payment_id'] = quotePaymentId;
    data['additional_data'] = additionalData;
    data['cc_exp_month'] = ccExpMonth;
    data['cc_ss_start_year'] = ccSsStartYear;
    data['echeck_bank_name'] = echeckBankName;
    data['method'] = method;
    data['cc_debug_request_body'] = ccDebugRequestBody;
    data['cc_secure_verify'] = ccSecureVerify;
    data['protection_eligibility'] = protectionEligibility;
    data['cc_approval'] = ccApproval;
    data['cc_last_4'] = ccLast4;
    data['cc_status_description'] = ccStatusDescription;
    data['echeck_type'] = echeckType;
    data['cc_debug_response_serialized'] = ccDebugResponseSerialized;
    data['cc_ss_start_month'] = ccSsStartMonth;
    data['echeck_account_type'] = echeckAccountType;
    data['last_trans_id'] = lastTransId;
    data['cc_cid_status'] = ccCidStatus;
    data['cc_owner'] = ccOwner;
    data['cc_type'] = ccType;
    data['po_number'] = poNumber;
    data['cc_exp_year'] = ccExpYear;
    data['cc_status'] = ccStatus;
    data['echeck_routing_number'] = echeckRoutingNumber;
    data['account_status'] = accountStatus;
    data['anet_trans_method'] = anetTransMethod;
    data['cc_debug_response_body'] = ccDebugResponseBody;
    data['cc_ss_issue'] = ccSsIssue;
    data['echeck_account_name'] = echeckAccountName;
    data['cc_avs_status'] = ccAvsStatus;
    data['cc_number_enc'] = ccNumberEnc;
    data['cc_trans_id'] = ccTransId;
    data['address_status'] = addressStatus;
    if (additionalInformation != null) {
      data['additional_information'] = additionalInformation!.toJson();
    }
    return data;
  }
}

class AdditionalInformation {
  String? methodTitle;

  AdditionalInformation({this.methodTitle});

  AdditionalInformation.fromJson(Map<String, dynamic> json) {
    methodTitle = json['method_title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['method_title'] = methodTitle;
    return data;
  }
}
