import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:sofiqe/network_service/network_service.dart';
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../provider/cart_provider.dart';
import '../utils/constants/api_tokens.dart';
import '../widgets/product_detail/custom_notification_for_add_all.dart';
import 'orderDetailModel.dart';

class OrderDetailController extends GetxController {
  static OrderDetailController get to => Get.find();

  OrderDetailModel? orderModel;
  bool isOrderLoading = false;
  bool isInProgress = false;

  ///
  /// Customize the request body
  /// [body] is the request body
  /// [url] is the url of the request
  /// [method] is the method of the request
  getOrderDetails(String orderId) async {
    isOrderLoading = true;
    update();
    try {
      http.Response? response = await NetworkHandler.getMethodCall(
          url: APIEndPoints.baseUri + "/ms2/orders/order/$orderId",
          headers: APIEndPoints.headers(await APITokens.customerSavedToken));
      cPrint("== Order Id is :: $orderId ==");
      cPrint(
          "after order detail api response ::  ${response!.statusCode}  :: ${response.body} ");
      if (response.statusCode == 200) {
        var result = json.decode(response.body);
        orderModel = OrderDetailModel.fromJson(result[0]);
      } else {
        var result = json.decode(response.body);
        Get.snackbar('Error', '${result[0]["message"]}', isDismissible: true);
      }
      cPrint("Responce of API is ${response.body}");
    } catch (e) {
      cPrint("Catch : Responce of Order Detail API is Error $e");
    }
    isOrderLoading = false;
    update();
  }

  orderAgainAndAddAllToCurrentCart(BuildContext context) {
    isInProgress = true;
    update();

    String allSKUs = '';
    for (int i = 0; i < orderModel!.data!.items!.length; i++) {
      Items element = orderModel!.data!.items![i];

      allSKUs += element.sku.toString();
      if (i < orderModel!.data!.items!.length - 1) {
        allSKUs += ',';
      }
    }
    if (allSKUs.isEmpty) {
      isInProgress = false;
      Get.snackbar("Failed",
          "All products are excluded becuase the Volum/Size is null/0",
          margin: EdgeInsets.only(bottom: 20, left: 10, right: 10),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING,
          borderRadius: 8,
          backgroundColor: Colors.black,
          colorText: Colors.white,
          icon: Icon(
            Icons.check_circle,
            color: Colors.white,
          ));
    } else {
      print('==== list of get SKUs is $allSKUs ====');
      Future.delayed(Duration(milliseconds: 1500), () async {
        CartProvider cartP = Provider.of<CartProvider>(context, listen: false);

        try {
          await cartP.addToCartForListOfSKUs(
              context: Get.context!, listOfSKUs: allSKUs, refresh: true);
          isInProgress = false;
          update();
          Get.snackbar(
              'Successfully Added', 'All products successfully added to cart',
              margin: EdgeInsets.only(bottom: 20, left: 10, right: 10),
              snackPosition: SnackPosition.BOTTOM,
              snackStyle: SnackStyle.FLOATING,
              borderRadius: 8,
              backgroundColor: Colors.black,
              colorText: Colors.white,
              icon: Icon(
                Icons.check_circle,
                color: Colors.white,
              ));
        } catch (e) {
          isInProgress = false;

          Get.snackbar(
              'Error in Adding Products', 'Sorry, but something went wrong',
              margin: EdgeInsets.only(bottom: 20, left: 10, right: 10),
              snackPosition: SnackPosition.BOTTOM,
              snackStyle: SnackStyle.FLOATING,
              borderRadius: 8,
              backgroundColor: Colors.black,
              colorText: Colors.white,
              icon: Icon(
                Icons.error,
                color: Colors.white,
              ));
          update();
        }
      });
    }
  }
}
