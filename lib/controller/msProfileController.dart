// ignore_for_file: unused_catch_clause, avoid_bool_literals_in_conditional_expressions

import 'dart:convert';
import 'dart:developer';

import 'package:dart_ipify/dart_ipify.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:sofiqe/model/profile.dart';
import 'package:sofiqe/model/recentItemModel.dart';
import 'package:sofiqe/network_service/network_service.dart';
import 'package:sofiqe/utils/api/user_account_api.dart';
import 'package:sofiqe/utils/constants/api_end_points.dart';
import 'package:sofiqe/utils/constants/api_tokens.dart';
import 'package:sofiqe/utils/states/function.dart';

import '../services/firebase_notification.dart';
import '../services/notification_services.dart';
import '../utils/states/local_storage.dart';
import 'controllers.dart';

class MsProfileController extends GetxController {
  static MsProfileController get to => Get.find();

  static MsProfileController instance = Get.find();
  RecentItemModel recentItem = RecentItemModel();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  Profile? profileModle;
  var atmCards = <AtmCard>[];
  late var screen = 0.obs;
  bool isRecentLoading = false;
  bool isLoading = false;
  String selectedGender = "";
  var notificationsOn = true.obs;
  var makeOverMessage = false.obs;

  getNotificationSetting() async {
    // final status = await FirebaseMessaging.instance.requestPermission();
    // final bool isEnabled = status.authorizationStatus == AuthorizationStatus.authorized;
    // cPrint('Notifications enabled: $isEnabled');
    // notificationsOn.value = isEnabled;

    _firebaseMessaging
        .requestPermission()
        .then((NotificationSettings isEnabled) {
      notificationsOn.value = isEnabled as bool ? true : isEnabled as bool;
    });
  }

  updateNotifications(bool newVal) async {
    notificationsOn.value = newVal;
    if (newVal) {
      // _firebaseMessaging.requestPermission();
      await FirebaseNotification.init();
      await NotificationService.init();
    } else {
      _firebaseMessaging.deleteToken();
    }
  }

  @override
  void onInit() async {
    super.onInit();

    String customerSavedToken = await APITokens.customerSavedToken;
    if (customerSavedToken != null && customerSavedToken.isNotEmpty) {
      getUserProfile();
      getUserQuestionsInformations();
      getUserCardDetailes();
    }
  }

  checkisControllerhaveValues() {
    if ((firstNameController.text.isEmpty && lastNameController.text.isEmpty) ||
        emailController.text.isEmpty ||
        phoneController.text.isEmpty ||
        countryCodeController.text.isEmpty ||
        cityController.text.isEmpty ||
        streetController.text.isEmpty ||
        postCodeController.text.isEmpty) {
      return true;
    } else {
      return false;
    }
  }

  resetControllers() {
    billingNameController = TextEditingController();
    billingCountryController = TextEditingController();
    billingStreetController = TextEditingController();
    billingPostZipController = TextEditingController();
    billingCityController = TextEditingController();
    billingPhoneController = TextEditingController();
    billingPhoneNumberCodeController = TextEditingController(text: "+1");
    countryCodeController = TextEditingController();
    billingCountryCodeController = TextEditingController();
    regionCodeController = TextEditingController();
    billingRegionCodeController = TextEditingController();
    firstNameController = TextEditingController();
    lastNameController = TextEditingController();
    emailController = TextEditingController();
    countryController = TextEditingController();
    streetController = TextEditingController();
    nameController = TextEditingController();
    phoneController = TextEditingController();
    phoneNumberCodeController = TextEditingController(text: "+1");
    cardNumberController = TextEditingController();
    monthCardController = TextEditingController();
    cvcController = TextEditingController();
    postCodeController = TextEditingController();
    cityController = TextEditingController(text: "");
    cardNameController = TextEditingController();
    update();
  }

  RxString profileName = "".obs;

  ///
  /// TextFormFiels controllers that's will use to update profiles
  ///
  ///
  ///
  /// For Testing
  // TextEditingController firstNameController = TextEditingController(text: "Farooq");
  // TextEditingController lastNameController = TextEditingController(text: "Aziz");
  // TextEditingController emailController = TextEditingController(text: "farooqaziz20@gmail.com");
  // TextEditingController countryController = TextEditingController(text: "UK");
  // TextEditingController streetController = TextEditingController(text: "ABC1234");
  // TextEditingController nameController = TextEditingController(text: "Farooq ");
  // TextEditingController phoneController = TextEditingController(text: "076666767667");
  // TextEditingController phoneNumberCodeController = TextEditingController(text: "+44");

  // TextEditingController billingNameController = TextEditingController(text: "Farooq");
  // TextEditingController billingCountryController = TextEditingController(text: "UK");
  // TextEditingController billingStreetController = TextEditingController(text: "ABC1234");
  // TextEditingController billingPostZipController = TextEditingController(text: "123ABC");
  // TextEditingController billingCityController = TextEditingController(text: "ABC123");
  // TextEditingController billingPhoneController = TextEditingController(text: "076666767667");
  // TextEditingController billingPhoneNumberCodeController = TextEditingController(text: "+44");

  // TextEditingController countryCodeController = TextEditingController(text: "");
  // TextEditingController billingCountryCodeController = TextEditingController(text: "");
  // TextEditingController regionCodeController = TextEditingController(text: "ABC123");
  // TextEditingController billingRegionCodeController = TextEditingController(text: "");

  // TextEditingController cardNumberController = TextEditingController();
  // TextEditingController monthCardController = TextEditingController();
  // TextEditingController cvcController = TextEditingController();
  // TextEditingController postCodeController = TextEditingController();
  // TextEditingController cityController = TextEditingController(text: "ABC123");
  // TextEditingController cardNameController = TextEditingController();

  TextEditingController billingNameController = TextEditingController();
  TextEditingController billingCountryController = TextEditingController();
  TextEditingController billingStreetController = TextEditingController();
  TextEditingController billingPostZipController = TextEditingController();
  TextEditingController billingCityController = TextEditingController();
  TextEditingController billingPhoneController = TextEditingController();
  TextEditingController billingPhoneNumberCodeController =
      TextEditingController(text: "+1");

  TextEditingController countryCodeController = TextEditingController();
  TextEditingController billingCountryCodeController = TextEditingController();
  TextEditingController regionCodeController = TextEditingController();
  TextEditingController billingRegionCodeController = TextEditingController();

  TextEditingController regionIdController = TextEditingController();
  TextEditingController billingIdodeController = TextEditingController();

  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController phoneNumberCodeController =
      TextEditingController(text: "+1");

  TextEditingController cardNumberController = TextEditingController();
  TextEditingController monthCardController = TextEditingController();
  TextEditingController cvcController = TextEditingController();
  TextEditingController postCodeController = TextEditingController();
  TextEditingController cityController = TextEditingController(text: "");
  TextEditingController cardNameController = TextEditingController();

  String? phoneNo;

  RxBool isShiping = RxBool(true);
  RxBool isBilling = RxBool(false);
  RxString shippingAddress = "".obs;

  void setFirstNameLastName() {
    firstNameController.text = nameController.text.split(' ')[0];
    lastNameController.text = nameController.text.split(' ').length > 1
        ? nameController.text.split(' ')[1]
        : "";
  }

  //

  loadShippindAdressinProvider() async {
    dynamic shipping_address = await sfQueryForSharedPrefData(
        fieldName: 'shipping_address', type: PreferencesDataType.STRING);
    if (shipping_address["found"]) {
      dynamic shippind_data = jsonDecode(shipping_address["shipping_address"]);
      String address = ((shippind_data["street"] ?? [""])[0]) +
          ", " +
          (shippind_data["city"] ?? "") +
          ", " +
          (shippind_data["postcode"] ?? "") +
          ", " +
          (shippind_data["region"] ?? ""); // +
      // ", " +
      // (shippind_data["region_code"] ?? "");
      shippingAddress.value = address;
    } else {
      shippingAddress.value = "Please add your address";
    }
  }

  returnPhoneCode() {
    return profileModle!.getCountryCode();
  }

  returnPhoneNumber() {
    return profileModle!.getPhoneNumber();
  }

  String returnCityName() {
    return profileModle?.addresses?[0].city ?? "";
  }

  // loaddataToControllers() async {
  //   dynamic shipping_address =
  //       await sfQueryForSharedPrefData(fieldName: 'shipping_address', type: PreferencesDataType.STRING);
  //   dynamic billing_address =
  //       await sfQueryForSharedPrefData(fieldName: 'billing_address', type: PreferencesDataType.STRING);
  //   dynamic phoneNoCode = await sfQueryForSharedPrefData(fieldName: 'phone_no_code', type: PreferencesDataType.STRING);
  //   dynamic phoneNoCodeBilling =
  //       await sfQueryForSharedPrefData(fieldName: 'phone_no_code_billing', type: PreferencesDataType.STRING);
  //
  //   cPrint("Shippind Data");
  //   cPrint(shipping_address);
  //
  //   if (shipping_address["found"]) {
  //     dynamic shippind_data = jsonDecode(shipping_address["shipping_address"]);
  //     countryController.text = shippind_data["region"] ?? "";
  //     streetController.text = (shippind_data["street"] ?? [""])[0];
  //     nameController.text = (shippind_data["firstname"] ?? "") + " " + (shippind_data["lastname"] ?? "");
  //     phoneController.text = shippind_data["telephone"] ?? "";
  //     phoneNumberCodeController.text = phoneNoCode["found"] ? phoneNoCode["phone_no_code"] : "+1";
  //     postCodeController.text = shippind_data["postcode"] ?? "";
  //     cityController.text = shippind_data["city"] ?? "";
  //     countryCodeController.text = shippind_data["country_id"] ?? "";
  //     regionCodeController.text = shippind_data["region_code"] ?? "";
  //     regionIdController.text = (shippind_data["region_id"] ?? "").toString();
  //     emailController.text = shippind_data["email"] ?? "";
  //     firstNameController.text = (shippind_data["firstname"] ?? "");
  //     lastNameController.text = (shippind_data["lastname"] ?? "");
  //     isShiping.value = (shippind_data["sameAsBilling"] ?? "0").toString() == "1" ? true : false;
  //   }
  //   cPrint("Biliing Data");
  //   cPrint(billing_address);
  //
  //   if (billing_address["found"]) {
  //     dynamic billing_data = jsonDecode(billing_address["billing_address"]);
  //     cPrint((billing_data["firstname"] ?? "") + " " + (billing_data["lastname"] ?? ""));
  //     billingNameController.text = (billing_data["firstname"] ?? "") + " " + (billing_data["lastname"] ?? "");
  //     billingCountryController.text = billing_data["region"] ?? "";
  //     billingStreetController.text = (billing_data["street"] ?? [""])[0];
  //     billingPostZipController.text = billing_data["postcode"] ?? "";
  //     billingCityController.text = billing_data["city"] ?? "";
  //     billingPhoneController.text = billing_data["telephone"] ?? "";
  //     billingPhoneNumberCodeController.text =
  //         phoneNoCodeBilling["found"] ? phoneNoCodeBilling["phone_no_code_billing"] : "+1";
  //     billingCountryCodeController.text = billing_data["country_id"] ?? "";
  //     billingRegionCodeController.text = billing_data["region_code"] ?? "";
  //     billingIdodeController.text = (billing_data["region_id"] ?? "").toString();
  //   }
  // }

  Future<bool> updateUserProfile() async {
    isRecentLoading = true;
    var result;
    update();

    ///
    /// Customize the request body
    ///

    var body;

    ///-----new body with billing address
    if (isShiping.value) {
      ///---only both same
      body = {
        "customer": {
          "email": emailController.text,
          "firstname": firstNameController.text,
          "lastname": lastNameController.text,
          "gender": getGenderCount(),
          "website_id": profileModle?.websiteId,
          "custom_attributes": [
            {
              "attribute_code": "phone_number",
              "value": '${phoneController.text}'
            }
          ],
          "addresses": [
            {
              "region": {
                "region_code": "TX",
                "region": "Texas",
                "region_id": 57
              },
              "postcode": postCodeController.text,
              "city": cityController.text,
              "country_id": countryController.text.toUpperCase(),
              "firstname": firstNameController.text,
              "lastname": lastNameController.text,
              "street": ['${streetController.text}'],
              "telephone":
                  '${phoneNumberCodeController.text} ${phoneController.text}',
              "default_shipping": isShiping.value,
              "default_billing": isBilling.value,
            }
          ]
        }
      };
    } else {
      ///---only both are different
      cPrint('------ its updating with different addresses -------');
      body = {
        "customer": {
          "email": emailController.text,
          "firstname": firstNameController.text,
          "lastname": lastNameController.text,
          "gender": getGenderCount(),
          "website_id": profileModle?.websiteId,
          "custom_attributes": [
            {
              "attribute_code": "phone_number",
              "value": '${phoneController.text}'
            }
          ],
          "addresses": [
            {
              "region": {
                "region_code": "TX",
                "region": "Texas",
                "region_id": 57
              },
              "postcode": postCodeController.text,
              "city": cityController.text,
              "country_id": countryController.text.toUpperCase(),
              "firstname": firstNameController.text,
              "lastname": lastNameController.text,
              "street": ['${streetController.text}'],
              "telephone":
                  '${phoneNumberCodeController.text} ${phoneController.text}',
              "default_shipping": isShiping.value,
              "default_billing": isBilling.value,
            },
            {
              "region": {
                "region_code": "TX",
                "region": "Texas",
                "region_id": 57
              },
              "postcode": billingPostZipController.text,
              "city": billingCityController.text,
              "country_id": billingCountryController.text.toUpperCase(),
              "firstname": firstNameController.text,
              "lastname": lastNameController.text,
              "street": ['${billingStreetController.text}'],
              "telephone":
                  '${phoneNumberCodeController.text} ${phoneController.text}',
              "default_shipping": isShiping.value,
              "default_billing": isBilling.value,
            }
          ]
        }
      };
    }

    cPrint(body.toString());
    try {
      http.Response? response = await NetworkHandler.patchMethodWithBodyCall(
          body: jsonEncode(body),
          url: "${APIEndPoints.baseDefaultUri}/customers/me", // API 98, 103
          headers: APIEndPoints.headers(await APITokens.customerSavedToken));
      cPrint("Responce of API is ${response?.body}");

      if (response?.statusCode == 200) {
        Get.snackbar('Succesfully', 'User profile updated Succesfully',
            isDismissible: true,
            backgroundColor: Colors.white,
            snackPosition: SnackPosition.BOTTOM);
        return true;
      } else {
        result = json.decode(response!.body);
        Get.snackbar('Error', '${result[0]["message"]}',
            isDismissible: true,
            backgroundColor: Colors.white,
            snackPosition: SnackPosition.BOTTOM);
        return false;
      }
    } catch (e) {
      Get.snackbar('Error', 'Something went wrong',
          isDismissible: true,
          backgroundColor: Colors.white,
          snackPosition: SnackPosition.BOTTOM);
      update();
      return false;
    }
  }

  Future<bool> updateUserProfileAddress() async {
    isRecentLoading = true;
    var result;
    update();

    ///
    /// Customize the request body
    ///

    ///
    //todo: there is static region code
    //needs to update api as well as code

    var body = {
      "customer": {
        "email": profileModle?.email,
        "firstname": profileModle?.firstname,
        "lastname": profileModle?.lastname,
        "gender": profileModle?.gender,
        "website_id": profileModle?.websiteId,
        "custom_attributes": [
          {"attribute_code": "phone_number", "value": '${phoneController.text}'}
        ],
        "addresses": [
          {
            "region": {"region_code": "TX", "region": "Texas", "region_id": 57},
            "postcode": postCodeController.text,
            "city": cityController.text,
            "country_id": countryController.text.toUpperCase(),
            "firstname": firstNameController.text,
            "lastname": lastNameController.text,
            "street": ['${streetController.text}'],
            "telephone": '${phoneController.text}',
            // "telephone": '${phoneNumberCodeController.text} ${phoneController.text}',
            "default_shipping": isShiping.value,
            "default_billing": isBilling.value,
          }
        ]
      }
    };

    //#endregion
    cPrint(body.toString());
    cPrint("==========address========$body");
    try {
      http.Response? response = await NetworkHandler.patchMethodWithBodyCall(
          body: jsonEncode(body),
          url: "${APIEndPoints.baseDefaultUri}/customers/me", // API 98, 103
          headers: APIEndPoints.headers(await APITokens.customerSavedToken));
      cPrint("Responce of API is ${response?.body}");

      if (response?.statusCode == 200) {
        Get.snackbar('Successfully', 'Address has been updated.',
            backgroundColor: Colors.black,
            dismissDirection: DismissDirection.down,
            isDismissible: true);
        //  Get.snackbar('Error', 'Card Api is in under Development');
        updateCardDetailes();
        return true;
      } else {
        result = json.decode(response!.body);
        Get.snackbar('Error', '${result[0]["message"]}', isDismissible: true);
        return false;
      }
    } catch (e) {
      Get.snackbar('Error', '${result["message"]}', isDismissible: true);
      update();
      return false;
    }
  }

  getUserProfile() async {
    isRecentLoading = true;
    try {
      var result =
          await sfAPIGetUserDetails(await APITokens.customerSavedToken);

      if (result != null) {
        profileModle = Profile.fromJson(result);
        List<CustomAttribute> listCA = profileModle!.customAttributes ?? [];
        String phoneNo = "";
        String countryCode = "";
        for (final customAttribute in listCA) {
          if (customAttribute.attributeCode == "phone_number") {
            phoneNo = customAttribute.value;
          }
          if (customAttribute.attributeCode == "country_code") {
            countryCode = customAttribute.value.toString();
            countryCode.replaceAll("+", "");
          }
        }

        saveFCMToken(phoneNo, countryCode);

        if (profileModle?.gender == 1) {
          selectedGender = "FEMALE";
        } else if (profileModle?.gender == 0) {
          selectedGender = "MALE";
        } else if (profileModle?.gender == 2) {
          selectedGender = "GENDERLESS";
        } else if (profileModle?.gender == 3) {
          selectedGender = "LBGT";
        }

        firstNameController.text = profileModle?.firstname ?? "";

        lastNameController.text = profileModle?.lastname ?? "";
        nameController.text =
            '${profileModle?.firstname}${((profileModle?.firstname ?? "").trim().isEmpty || (profileModle?.lastname ?? "").trim().isEmpty) ? '' : ' '}${profileModle?.lastname}';
          phoneController.text = profileModle!.getPhoneNumber();
          phoneNumberCodeController.text =
              profileModle!.getCountryCode() ?? "+1";
        emailController.text = profileModle?.email ?? "";
        if (profileModle?.addresses != null &&
            profileModle?.addresses?.length != 0) {
          countryCodeController.text =
              profileModle?.addresses?[0].countryId ?? "";
          if (profileModle?.addresses?[0].region != null) {
            countryController.text =
                profileModle?.addresses?[0].region!.region ?? "";
            regionCodeController.text =
                profileModle?.addresses?[0].region!.regionCode ?? "";
          }

          regionIdController.text =
              (profileModle?.addresses?[0].regionId ?? "0").toString();
          isBilling.value = profileModle?.addresses?[0].defaultBilling ?? false;
          cityController.text = profileModle?.addresses?[0].city ?? "";
          postCodeController.text = profileModle?.addresses?[0].postcode ?? "";

          streetController.text = profileModle?.addresses?[0].street?[0] ?? "";

          if (profileModle!.addresses != null &&
              profileModle!.addresses!.length > 1) {
            billingStreetController.text =
                profileModle?.addresses?[1].street?[0] ?? "";
            billingCityController.text = profileModle?.addresses?[1].city ?? "";
            billingPostZipController.text =
                profileModle?.addresses?[1].postcode ?? "";
            billingCountryController.text =
                profileModle?.addresses?[1].countryId ?? "";
            billingNameController.text =
                (profileModle?.addresses?[1].firstname ?? "") +
                    ((profileModle?.addresses?[1].firstname ?? "").isEmpty
                        ? ""
                        : " ") +
                    (profileModle?.addresses?[1].lastname ?? "");

            billingPhoneController.text = profileModle!.getPhoneNumber();
            billingPhoneNumberCodeController.text =
                profileModle!.getCountryCode() ?? "+1";
            billingCountryCodeController.text =
                profileModle?.addresses?[1].countryId ?? "";

            if (profileModle?.addresses?[0].region != null) {
              billingCountryController.text =
                  profileModle?.addresses?[1].region!.region ?? "";
              billingRegionCodeController.text =
                  profileModle?.addresses?[1].region!.regionCode ?? "";
            }

            billingRegionCodeController.text =
                (profileModle?.addresses?[1].regionId ?? "0").toString();
          }
        }
        List listAddress = result["addresses"] ?? [];

        dynamic shippingaddress = {};
        dynamic billingaddress = {};

        for (int i = 0; i < listAddress.length; i++) {
          dynamic address = listAddress[i];
          if (address["default_shipping"] == true) {
            shippingaddress = address;
          }
          if (address["default_billing"] == true) {
            billingaddress = address;
          }
          if (address["default_shipping"] == true &&
              address["default_billing"] == true) {
            isShiping.value = true;
          }
        }
        String country = "";
        String regionCode = "";
        if (profileModle?.addresses?[0].region != null) {
          country = profileModle?.addresses?[0].region!.region ?? "";
          regionCode = profileModle?.addresses?[0].region!.regionCode ?? "";
        }

        Map<String, dynamic> addressInfo = {
          "addressInformation": {
            "shippingAddress": {
              "region": country,
              "region_id": shippingaddress["region_id"] ?? 0,
              "country_id": shippingaddress["country_id"] ?? "",
              "street": shippingaddress["street"] ?? [""],
              "company": "Revered-Tech",
              "telephone": shippingaddress["telephone"] ?? "",
              "postcode": shippingaddress["postcode"] ?? "",
              "city": shippingaddress["city"] ?? "",
              "firstname": shippingaddress["firstname"] ?? "",
              "lastname": shippingaddress["lastname"] ?? "",
              "email": result["email"] ?? "",
              "prefix": "",
              "region_code": regionCode,
              "sameAsBilling": isShiping.value ? 1 : 0
            },
            "billingAddress": {
              "region": (billingaddress["region"] ?? []).length > 0
                  ? (((billingaddress["region"] ?? [])[0]) ?? {})["region"] ??
                      ""
                  : "",
              "region_id": billingaddress["region_id"] ?? 0,
              "country_id": billingaddress["country_id"] ?? "",
              "street": billingaddress["street"] ?? [""],
              "company": "Revered-Tech",
              "telephone": billingaddress["telephone"] ?? "",
              "postcode": billingaddress["postcode"] ?? "",
              "city": billingaddress["city"] ?? "",
              "firstname": billingaddress["firstname"] ?? "",
              "lastname": billingaddress["lastname"] ?? "",
              "email": billingaddress["email"] ?? "",
              "prefix": "",
              "region_code": (billingaddress["region"] ?? []).length > 0
                  ? (((billingaddress["region"] ?? [])[0]) ??
                          {})["region_code"] ??
                      ""
                  : "",
            },
            "shipping_method_code": "flatrate",
            "shipping_carrier_code": "flatrate"
          }
        };
        sfStoreInSharedPrefData(
            fieldName: 'shipping_address',
            value: jsonEncode(
                addressInfo['addressInformation']['shippingAddress']),
            type: PreferencesDataType.STRING);
        sfStoreInSharedPrefData(
            fieldName: 'phone_no_code_billing',
            value: billingPhoneNumberCodeController.value.text,
            type: PreferencesDataType.STRING);
        sfStoreInSharedPrefData(
            fieldName: 'billing_address',
            value:
                jsonEncode(addressInfo['addressInformation']['billingAddress']),
            type: PreferencesDataType.STRING);

        update();
      } else {
        Get.snackbar('Error', '${result[0]["message"]}', isDismissible: true);
      }
      isRecentLoading = false;
      update();
    } catch (e) {
      cPrint(e.toString());
    } finally {
      isRecentLoading = false;
      update();
    }
  }

  getAddressToAssignTocart() {
    Map<String, dynamic> addressInfo = {
      "addressInformation": {
        "shippingAddress": {
          "region": '${countryController.value.text}',
          "region_id": int.parse(regionIdController.value.text.isEmpty
              ? "0"
              : regionIdController.value.text),
          "country_id": '${countryCodeController.value.text}',
          "street": ['${streetController.value.text}'],
          "company": "Revered-Tech",
          "telephone": phoneController.text,
          "postcode": '${postCodeController.value.text}',
          "city": '${cityController.text}',
          "firstname": '${firstNameController.text}',
          "lastname": '${lastNameController.text}',
          "email": "${emailController.value.text.trim()}",
          "prefix": "",
          "region_code": '${regionCodeController.value.text}',
          "sameAsBilling": (isShiping.value) ? 1 : 0
        },
        "billingAddress": {
          "region": '${billingCountryController.value.text}',
          "region_id": int.parse(billingIdodeController.value.text.isEmpty
              ? "0"
              : billingIdodeController.value.text),
          "country_id": '${billingCountryCodeController.value.text}',
          "street": ['${billingStreetController.value.text}'],
          "company": "Revered-Tech",
          "telephone": billingPhoneController.text,
          "postcode": "${billingPostZipController.value.text}",
          "city": billingCityController.text,
          "firstname": '${firstNameController.text}',
          "lastname": '${lastNameController.text}',
          "email": "${emailController.value.text.trim()}",
          "prefix": "",
          "region_code": '${billingRegionCodeController.value.text}',
        },
        "shipping_method_code": "flatrate",
        "shipping_carrier_code": "flatrate"
      }
    };
    return addressInfo;
  }

  getEstimateShipingAddressForCheckOut({int customerId = 0}) {
    var bodyForEstimateCost;

    cPrint("ESADRESS==> ${countryController.value.text}");
    cPrint("ESADRESS==> ${firstNameController.value.text}");
    cPrint("ESADRESS==> ${emailController.value.text}");

    bodyForEstimateCost = {
      "address": {
        "region": '${countryController.value.text}',
        "region_id": int.parse(regionIdController.value.text.isEmpty
            ? "0"
            : regionIdController.value.text),
        "region_code": '${regionCodeController.value.text}',
        "country_id": '${countryCodeController.value.text}',
        "street": ['${streetController.value.text}'],
        "postcode": "${postCodeController.value.text}",
        "city": cityController.text,
        "firstname": firstNameController.text,
        "lastname": lastNameController.text,
        "customer_id": customerId,
        "email": "${emailController.value.text.trim()}",
        "telephone": phoneController.text,
        "same_as_billing": (isShiping.value) ? 1 : 0
      }
    };

    return bodyForEstimateCost;
  }

  getEstimateShipingAddress(bool isLoggedIn, {int customerId = 0}) {
    var bodyForEstimateCost;

    if (isLoggedIn) {
      bodyForEstimateCost = {
        "address": {
          "region": '${billingCountryController.value.text}',
          "region_id": int.parse(billingIdodeController.value.text.isEmpty
              ? "0"
              : billingIdodeController.value.text),
          "region_code": '${billingRegionCodeController.value.text}',
          "country_id": '${billingCountryCodeController.value.text}',
          "street": ['${billingStreetController.value.text}'],
          "postcode": "${billingPostZipController.value.text}",
          "city": billingCityController.text,
          "firstname": billingNameController.text.split(' ')[0],
          "lastname": billingNameController.text.split(' ').length > 1
              ? billingNameController.text.split(' ')[1]
              : " ",
          "customer_id": customerId,
          "email": "${emailController.value.text.trim()}",
          "telephone": billingPhoneController.text,
          "same_as_billing": (isShiping.value) ? 1 : 0
        }
      };
    } else {
      bodyForEstimateCost = {
        "address": {
          "region": '${billingCountryController.value.text}',
          "region_id": int.parse(billingIdodeController.value.text.isEmpty
              ? "0"
              : billingIdodeController.value.text),
          "region_code": '${billingRegionCodeController.value.text}',
          "country_id": '${billingCountryCodeController.value.text}',
          "street": ['${billingStreetController.value.text}'],
          "postcode": "${billingPostZipController.value.text}",
          "city": billingCityController.text,
          "firstname": billingNameController.text.split(' ')[0],
          "lastname": billingNameController.text.split(' ').length > 1
              ? billingNameController.text.split(' ')[1]
              : " ",
          "email": "${emailController.value.text.trim()}",
          "telephone": billingPhoneController.text,
          "same_as_billing": (isShiping.value) ? 1 : 0
        }
      };
    }
    return bodyForEstimateCost;
  }

  saveFCMToken(String phoneNo, String code) async {
    cPrint("FCM == API call");
    try {
      String? fcmToken = await FirebaseMessaging.instance.getToken();
      String token = await APITokens.customerSavedToken;
      final ipv4 = await Ipify.ipv4();
      Uri url = Uri.parse('${APIEndPoints.setUserFCMToken}');
      int customerid = -1;
      Map<String, dynamic> customerIdMap = await sfQueryForSharedPrefData(
          fieldName: 'customer-id', type: PreferencesDataType.INT);

      if (customerIdMap['found']) {
        customerid = customerIdMap["customer-id"] ?? -1;
        cPrint("FCM == Test");
        cPrint(customerid);
        cPrint(fcmToken ?? "No Token");
      }
      cPrint("FCM == $token");
      String body = jsonEncode({
        "data":
            "{'fcmtoken':'$fcmToken','customer_id':'$customerid','subscriber_ip':'$ipv4','visitor_id':'','country_code':'$code','mobile_number':'$phoneNo'}"
      });
      cPrint("FCM == $body");
      http.Response response = await http.post(url,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer $token',
          },
          body: body);
      cPrint("FCM == Response Body");
      cPrint(response.body);
      cPrint(response.statusCode);
    } catch (e) {
      cPrint("FCM == ISSUE");
      cPrint(e);
    }
  }

  getUserQuestionsInformations() async {
    isRecentLoading = true;
    update();

    try {
      var result =
          await sfAPIGetUserDetails(await APITokens.customerSavedToken);

      if (result != null) {
        profileModle = Profile.fromJson(result);

        try {
          if (profileModle?.customAttributes!.length != 0) {
            makeOverProvider.tryitOn.value =
                (profileModle?.customAttributes ?? []).length > 4
                    ? true
                    : false;
            // makeOverProvider.tryitOn.value = profileModle?.customAttributes?[1].value != null ? true : false;
          }
        } on Exception catch (e) {
          makeOverProvider.tryitOn.value = false;
        }
      } else {
        Get.snackbar('Error', '${result[0]["message"]}', isDismissible: true);
      }
      isRecentLoading = false;
      debugPrint('==== No Error on Getting Questions ====');

      update();
    } catch (e) {
      debugPrint('==== Error on Getting Questions ====');
      cPrint(e.toString());
    } finally {
      isRecentLoading = false;
      update();
    }
  }

  getGenderCount() {
    if (selectedGender == "FEMALE") {
      return 0;
    } else if (selectedGender == "MALE") {
      return 1;
    } else if (selectedGender == "GENDERLESS") {
      return 2;
    } else if (selectedGender == "LBGT") {
      return 3;
    }
  }

//API 162. Store, Retrieve Stripe Customer Token For Card
  getCustomersavedcards() async {
    http.Response response = await http.post(
        Uri.parse(APIEndPoints.getCustomersavedCardsAPI),
        headers: APIEndPoints.headers(await APITokens.customerSavedToken));
    cPrint("SavedCards==> ${response.statusCode}");
    cPrint("SavedCards==> ${response.body}");
    cPrint("SavedCards==> ${await APITokens.customerSavedToken}");
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return [];
    }
  }

  //104
  getUserCardDetailes() async {
    try {
      http.Response? response = await NetworkHandler.getMethodCall(
          url:
              "${APIEndPoints.baseUri}/getcardinformation?number=1&name=1&expiry_date=1&type=1",
          headers: APIEndPoints.headers(await APITokens.customerSavedToken));
      cPrint("MM after api  ${response!.statusCode}");
      if (response.statusCode == 200) {
        Iterable types = json.decode(response.body);
        atmCards = types.map((e) => AtmCard.fromJson(e)).toList();
        cPrint("atm length ==  ${atmCards.length}");
        if (atmCards.length > 0) {
          cardNumberController.text = atmCards[0].number.toString();
          monthCardController.text = atmCards[0].expiryDate.toString();
          cvcController.text = atmCards[0].cvc.toString();
          cardNameController.text = atmCards[0].name.toString();
        } else {
          // Get.snackbar('Error', 'No Card Found');
        }
      } else {
        var result = json.decode(response.body);
        Get.snackbar('Error', '${result[0]["message"]}', isDismissible: true);
      }
      cPrint("Responce of API is ${response.body}");
    } finally {
      isRecentLoading = false;
      update();
    }
  }

  // 105
  Future<bool> updateCardDetailes() async {
    var body = {
      "card": {
        "number": cardNumberController.text.replaceAll(" ", ""),
        "expiry_date": monthCardController.text,
        "cvc": cvcController.text,
        "name": cardNameController.text,
      }
    };
    cPrint("MMcard=${body.toString()}");
    try {
      http.Response? response = await NetworkHandler.postMethodCall(
          body: body,
          url: "${APIEndPoints.baseUri}/getcardinformation",
          headers: APIEndPoints.headers(await APITokens.customerSavedToken));

      if (response?.statusCode == 200) {
        Get.showSnackbar(
          GetSnackBar(
            message: 'User profile updated Successfully',
            duration: Duration(seconds: 2),
            backgroundColor: Colors.white,
          ),
        );

        cPrint("LOGGG");
        return true;
      } else {
        cPrint("EROORRR");
        var result = json.decode(response!.body);
        Get.showSnackbar(
          GetSnackBar(
            message: '${result["message"]}',
            duration: Duration(seconds: 2),
          ),
        );
        return false;
      }
    } catch (e) {
      Get.showSnackbar(
        GetSnackBar(
          message: 'Card api not working \n ${e.toString()}',
          duration: Duration(seconds: 2),
        ),
      );
      isRecentLoading = false;
      update();
      return false;
    }
  }

  getRecenItems() async {
    String token = await APITokens.customerSavedToken;
    print("CORC-->" + token);
    if (token.isNotEmpty && token.toLowerCase() != "null") {
      isRecentLoading = true;
      update();
      cPrint("_____________________________________");
      cPrint("\t\tBeareer Token ${APITokens.bearerToken}\t\t");
      cPrint("_____________________________________");

      try {
        http.Response? response = await NetworkHandler.getMethodCall(
            url: "${APIEndPoints.baseDefaultUri}/customer/getscannedproduct/",
            headers: APIEndPoints.headers(await APITokens.customerSavedToken));
        cPrint("after history api  ${response!.statusCode}");
        if (response.statusCode == 200) {
          var result = json.decode(response.body);
          log('====== history api response is ${result[0]} ======');
          recentItem = RecentItemModel.fromJson(result[0]);
          update();
        } else {
          var result = json.decode(response.body);
          Get.snackbar('Error', '${result[0]["message"]}');
        }
        cPrint("Responce of API is ${response.body}");
      } catch (e) {
        cPrint('=======  Error in History API :: $e  ======');
      } finally {
        isRecentLoading = false;
        update();
      }
    }
  }
}
