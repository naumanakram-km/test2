// ignore_for_file: file_names

import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:sofiqe/model/currency.dart' as me;
import 'package:sofiqe/network_service/network_service.dart';
import 'package:sofiqe/utils/states/function.dart';
import 'package:sofiqe/utils/states/local_storage.dart';

import '../utils/constants/api_end_points.dart';

class CurrencyController extends GetxController {
  static CurrencyController get to => Get.find();
  @override
  void onInit() {
    super.onInit();
    getCurrency();
  }

  var currency = [].obs;
  var isCurrencyLoading = false.obs;
  String? defaultCurrency = r"$";
  RxString defaultCurrencySymbol = r"$".obs;

  RxString defaultCurrencyCode = "USD".obs;
  RxDouble exchangeRateinDouble = 1.25.obs;
  List exchangeRate = [].obs;

  Rx<me.Currency> currencyModelNew = me.Currency().obs;

  get availableCurrencyCodes => null;

  getCurrency() async {
    isCurrencyLoading(true);

    /// Customize the request body

    http.Response? response = await NetworkHandler.getMethodCall(
      url: APIEndPoints.mainBaseUrl + "/index.php/rest/V1/directory/currency",
      // headers: APIEndPoints.headers(await APITokens.customerSavedToken)
    );

    cPrint("after api  ${response!.statusCode}");
    cPrint("after api body  ${response.body}");
    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      me.Currency model = me.Currency.fromJson(result);
      currencyModelNew.value = model;

      Map userTokenMap = await sfQueryForSharedPrefData(fieldName: 'currency-code', type: PreferencesDataType.STRING);
      if (userTokenMap['found']) {
        defaultCurrency = model.defaultDisplayCurrencySymbol;
        defaultCurrencySymbol.value = model.defaultDisplayCurrencySymbol ?? r"$";
        defaultCurrencyCode.value = userTokenMap['currency-code'] ?? "USD";
      } else {
        defaultCurrency = model.defaultDisplayCurrencySymbol;
        defaultCurrencySymbol.value = model.defaultDisplayCurrencySymbol ?? r"$";
        defaultCurrencyCode.value = model.baseCurrencyCode ?? "USD";
      }
      Map userTokenMapexchangeRate =
          await sfQueryForSharedPrefData(fieldName: 'exchange-rate', type: PreferencesDataType.DOUBLE);
      if (userTokenMapexchangeRate['found']) {
        exchangeRateinDouble.value = userTokenMapexchangeRate["exchange-rate"] ?? 0.0;
      } else {
        exchangeRateinDouble.value = (model.baseCurrencyCode == "GBP") ? 1.0 : 1.25;
      }

      update();

      isCurrencyLoading(false);
      cPrint("_____________________________________");
      cPrint(result);
      cPrint("_____________________________________");
    } else {
      var result = json.decode(response.body);
      Get.snackbar('Error', '${result["message"]}', isDismissible: true);
      isCurrencyLoading(false);
    }
    cPrint("Responce of API is--- ${response.body}");
  }
}