# sofiqeflutter

flutter version 3.3.8
Dart version 2.18.4

# Web site
You can make simple checks on the website https://sofiqe.com
Create a new account for yourself so you do not use the same account as the other developers do. Then you never know what is in the cart.

# Development 
## When you start coding:
When you start coding you make a pull on develop branch. All development tasks are during the development process added to the development branch. Upon release the master will receive code from the develop branch and be updated. 

## How do you do your work #
1. Any new or changed code you add a comment to. Example: //bug #27/ Product shows loader all the time. resolved
2. Added functions you add text to it. Example: //Function: new functino to resolve brand name in product screen
3. In new modules that are installed you do all changes in external files and add it to to the add in, or you do proper commenting.

If the above is not done and I can see changes made the code will be refused.

## How to push code to git.
__Definitions:__<br>
>   *Type: Bug, Feature, Change Request(abreviatd with CR)  <br>
>   *Ticket number: you get ticket numbers and types in Redmine on https://r.conqorde.com <br> 

__How to do it:__ <br>
>1. git branch ```<type>/<ticket number>```, example: Bug/#27 MS1: Old button remove. 
>2. git add . 
>3. Git commit -m ```"<type>/<ticket number><ticket subject>"```, you can use same commit comment as branch name. Also, there is no limiation of what you write in the commit comment. You can store small progress to the git to be able to reverse later. Use git as a good tool to keep track and be able to roll back
>4. git push origin ```<type>/<ticket number>```
>5. Create a PR on git.conqorde.com with the specific bug and merge into develop branch. You create one branch and one Pull Request (PR) for every bug.
>6. Pull the development to get an updated version of the code so we do not deviate from the main branch. you do this since you must get the version number in the app from git. I check this every time so this is a sign of that you have done the steps here correctly.
>7. I review the Code
>8. I approve and it then merge to the branch develop
>9. Jenkins executes and test code and also compile it to an APK
>10. it is pushed to Gitea WIKI page in the repo. There you find logs and APK. If apk is zero the compile did not work

without correct formatting the code will not be approved when it is reviewed and compiled. Correct code nomenclature must be used since Jenkins is checking this automatically. The test level used is a very kind version. If the heavy one is used you will have massive number of issues.

BTW: there is no need to find the file for the checking. Jenkins and the tests are stored outside to prevent developers to change the tests. 

## Accessing APIs
Ask for the postman collection with environments
The API 66a and 66B give you access. 
these should be called ONE time when the app starts. The tokens have a lifetime of 365 days so you do not need to bombard the server at all.

Please also use sense when you call the create cart. The create cart API is giving you the cart number for the customer and it is the same all the time. Add it to a global variable.
Guest cart is giving you a new cart ID every time. Customer resend the one that is active. So you must store all properly to not have several carts.
Do this for both Guest cart ID and customer Cart ID. Then you ask for that variable instead of hammering the server and you save both code and time.

When the app loads it is calling all these variables. Stick to this and do not create a forest of API calls

# Miscellaneous
*.class
*.lock
*.log
*.pyc
*.swp
.DS_Store
.atom/
.buildlog/
.history
.svn/
# IntelliJ related
*.iml
*.ipr
*.iws
.idea/
# Visual Studio Code related
.vscode/
.vscode/*
# Flutter repo-specific
Version 3.4 in feature/correctcode

/bin/cache/
/bin/mingit/
/dev/benchmarks/mega_gallery/
/dev/bots/.recipe_deps
/dev/bots/android_tools/
/dev/docs/doc/
/dev/docs/lib/
/dev/docs/pubspec.yaml
/packages/flutter/coverage/
version
# Flutter/Dart/Pub related

**/doc/api/
.dart_tool/
.flutter-plugins
.packages
.pub-cache/
.pub/
build/
flutter_*.png
linked_*.ds
unlinked.ds
unlinked_spec.ds
# Android related
**/android/**/gradle-wrapper.jar
**/android/.gradle
**/android/captures/
**/android/gradlew
**/android/gradlew.bat
**/android/local.properties
**/android/**/GeneratedPluginRegistrant.java
# iOS/XCode related
**/ios/**/*.mode1v3
**/ios/**/*.mode2v3
**/ios/**/*.moved-aside
**/ios/**/*.pbxuser
**/ios/**/*.perspectivev3
**/ios/**/*sync/
**/ios/**/.sconsign.dblite
**/ios/**/.tags*
**/ios/**/.vagrant/
**/ios/**/DerivedData/
**/ios/**/Icon?
**/ios/**/Pods/
**/ios/**/.symlinks/
**/ios/**/profile
**/ios/**/xcuserdata
**/ios/.generated/
**/ios/Flutter/App.framework
**/ios/Flutter/Flutter.framework
**/ios/Flutter/Generated.xcconfig
**/ios/Flutter/app.flx
**/ios/Flutter/app.zip
**/ios/Flutter/flutter_assets/
**/ios/ServiceDefinitions.json
**/ios/Runner/GeneratedPluginRegistrant.*
# Exceptions to above rules.
!**/ios/**/default.mode1v3
!**/ios/**/default.mode2v3
!**/ios/**/default.pbxuser
!**/ios/**/default.perspectivev3
!/packages/flutter_tools/test/data/dart_dependencies_test/**/.packages# e_com_e_sof
## Dependencies<br/>

- camera: ^0.8.1+5
- cupertino_icons: ^1.0.2
- dio: ^4.0.0
- file_saver: ^0.0.10
- firebase_auth: ^3.0.1
- flutter_screenutil: ^5.1.1
- firebase_core: ^1.4.0
- devicelocale: ^0.5.0
- flutter_html: ^2.1.1
- geocoding: ^2.0.0
- get: ^4.3.8
- location: ^4.2.0
- intl: ^0.17.0
- email_validator: ^2.0.1
- image: ^3.0.2
- country_code_picker: ^2.0.2
- # The following adds the Cupertino Icons font to your application.
- # Use with the CupertinoIcons class for iOS style icons.
- http: ^0.13.3
- provider: ^5.0.0
- shared_preferences: ^2.0.6
- shimmer: ^2.0.0
- sqflite: ^2.0.0+3
- url_launcher: ^6.0.9
- dotted_border: ^2.0.0+1
- share_plus: ^2.1.4
- flutter_typeahead: ^3.2.0
- auto_size_text: ^3.0.0-nullsafety.0
- flutter_svg: ^0.22.0
- dotted_line: ^3.0.0
- flutter_isolate: ^2.0.0
- # carousel_slider: ^4.0.0
- # flutter_unity_widget: ^4.2.1
- flutter_rating_bar: ^4.0.0
- flutter_share: ^2.0.0
- flutter_math_fork: ^0.5.0

# Setup Redmine Git Hooks
We connect our tickets as well as commits to Redmine ticketing system. To achieve this goal we use so called GIT hooks.
GIT hooks are SHELL scripts which allow you to execute some actions during committing your code.
If you commit the code, then the commit message will appear in REdmine under particular ticket number.

## How to setup your repository GIT hooks
1. Generate Redmine API key
Please visit Remind My Account page: https://r.conqorde.com/my/account
![Redmine Api key](docs/git-hooks/redmine_api_key.png)
2. Grab the Redmine API key and update [env_vars](scripts/redmine-webhook/env_vars) line with your own key:
    ``` bash
    REDMINE_API_KEY='YOUR API KEY'
    ```
3. Execute [redmine_webhook.sh](redmine_webhook.sh) in your GIT BASH or any other BASH terminal:
    ``` bash
    ./redmine_webhook.sh
    ```
    Above will setup GIT with additional values.

## How to use Redmin GIT hooks
Each GIT commit should have following syntax:
refs #[REDMINE_TICKET_NUMBER]: [MEANINGFUL_MESSAGE]

So you need to run following GIT COMMIT command:
``` bash
git commit -m "refs #185: Add new feature"
```

Aboce will do following in exact order:
1. Check if you are assigned to the ticket. Only users assigned to particular ticket, in Redmine, can commit the code to that very ticket.
2. Check if you created proper GIT COMMIT message with ticket reference.

## How to use

To clone and run this application, you'll need [Git](https://git-scm.com/downloads) and [Flutter](https://flutter.dev/docs/get-started/install) installed on your computer. From your command line:

### Clone this repository

```
git clone https://github.com/MimirRnD/sofiqeflutter/invitations
```

### Go into the repository

```
cd sofiqe-flutter
```

### Install dependencies

```
flutter packages get
flutter pub get
```

### Run the app

```
flutter run
```

##