#!/bin/bash

regex="^(refs|fix)\s\#[0-9]+\:"
regex2="^(refs|fix)[[:space:]]\#[0-9]+\:"
var=`head -n 1 $1`
GITEA_COMMIT_URL='https://git.conqorde.com/sofiqe/sofiqeflutter/commit'

function info {
  echo >&2 $1
}

function debug {
  debug=false
  if $debug
  then
    echo >&2 $1
  fi
}

function get_json {
  python -c "import sys;import json;j=json.loads(sys.stdin.read());print(str(j${1}));"
}

function redmine_json {
  redmine_host=$(git config redmine.host)
  redmine_apikey=$(git config redmine.apikey)
  curl  --silent -k -H "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: $redmine_apikey" "$redmine_host/${1}"
}

function add_issues_notes() {
  redmine_host=$(git config redmine.host)
  redmine_apikey=$(git config redmine.apikey)

  local issue_id="${1}"
  local issue_subject="${2}"
  local notes_message="${3}"
  curl --silent -k -H "Accept: application/json" -H "Content-Type: application/json" -X PUT -H "X-Redmine-API-Key: $redmine_apikey" "$redmine_host/issues/${issue_id}.json" \
      -d "{\"issue\": {
                \"subject\": \"${issue_subject}\",
                \"notes\": \"${notes_message}\"
              }
          }"
}

if [[ "${var}" =~ ${regex} ]] || [[ "${var}" =~ ${regex2} ]]
then
  debug "Commit message: OK"
else
  # Define format  message forfirst line in commit message
  info "Commit need to be \"[refs|fix] #ID: message\""
  exit 1
fi

issue_id=`echo $var | sed 's/\(refs\|fix\)\ \#\([0-9]*\).*/\2/'`
issue=$(redmine_json "issues/${issue_id}.json")
issue_subject=$(echo $issue | get_json "['issue']['subject']")
commit_message=$(git log -n1 --pretty=%s | awk -F': ' '{print $2}')
commit_sha=$(git log -n1 --pretty='%H')
commit_url="${GITEA_COMMIT_URL}/${commit_sha}"

if [[ "$issue" != " " ]]
then
  debug "Issue exist:    OK"
else
  info "Issue $issue_id don't exist"
  exit 2;
fi

user=`redmine_json "users/current.json"`
user_id=`echo $user | get_json "['user']['id']"`
assigner_id=`echo $issue | get_json "['issue']['assigned_to']['id']"`

if [[ $user_id -eq $assigner_id ]]
then
  debug "Assigner:       OK"
else
  info "You are working as user $user_id but issue is not assigned to you"
  exit 3;
fi

add_issues_notes "${issue_id}" "${issue_subject}" "${commit_url} \n ${commit_message}"

redmine_host=$(git config redmine.host)

echo ""
echo "Issue ==========================="
echo "  hook:         commit-msg"
echo "  refs:         $redmine_host/issues/$issue_id"
echo "  number:       #"`echo $issue | get_json "['issue']['id']"`
echo "  priority:     "`echo $issue | get_json "['issue']['priority']['name']"`
echo "  create from:  "`echo $issue | get_json "['issue']['author']['name']"`
echo "  assigned to:  "`echo $issue | get_json "['issue']['assigned_to']['name']"`
echo "  updated with: ${commit_message}"
echo ""